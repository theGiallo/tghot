# tg hot
### hot reloading library

The library is in src/hot in the two files tghot.h and tghot.cpp.

As demo look into the files src/hot/somelibrary.cpp, src/hot/someotherlibrary.cpp, src/hot/tghot_test.cpp. They are executable and you can run them directly.

## Disclaimer: tested on linux with clang++ and g++, no Windows version yet

## What

From the main program and from libraries, **you can dynamically link libraries, load functions and get them reloaded**. You can even have cross-references. Yes, I know what you're thinking: "they are a code smell", but don't limit yourself to the classic library code subdivision. With some tooling you could separate a single library in many dls to compile and hotreload just a part of the library.

## API

In tghot.h you can find the public API of the library. If you find some discrepancies from this readme and that file, I'm sorry, let me now if you feel so.

Libraries are looked for in a folder whose path is provided via a define, at compile time: `TGHOT_DLIBS_DIR`. In this folder a txt file should contain the current version of the library, with no end of line, named `lib$LIBNAME.version.txt`, where `$LIBNAME` is the name of the library. The library file then will be constructed as `lib$LIBNAME.so.$LIBVERSION`, where `$LIBVERSION` is the content of the version txt.

### Main

From the main program you probably will want to load at least one library. You can do it via this function:

	bool
	request_dl( DL & dl, void ( * callback ) ( DL * dl, void * custom_data ), const char * registerer_name, void * custom_data = 0 );

You have to create a DL struct to describe the desired library.

You can easily create one with the Make static function.

	struct
	DL
	{
		const char * name;
		tg::Array<const char*> functions_names;
		tg::Array<void*      > functions;

		static
		inline
		DL
		Make( const char * name, std::initializer_list<const char *> functions_names );
	};

To manage the successful loading of the library and the requested symbols, you have to provide a callback, to which will be passed the same DL you passed to `request_dl`, so don't make it go out of scope. This callback will be called every time the library will be reloaded. The callback will also get the `void * custom_data` you provide. You can use this to implement "capture" of data. E.g. you can write a single method that loads a dl, that can load different implementations and store them in different places, memorizing in the `custom_data` such place.

When your callback gets called, look into `functions` to get your functions pointers.

You can make tghot reload the libraries calling this function:

	void
	reload_if_you_want();

Some uses of this are:
1. calling it in a loop, e.g. in a game loop
2. controlling the reload via hotkey
3. setup a watcher for the dls or for the sourcecode

### Dynamic Libraries

The libraries can setup the normal init function and fini function, but to interact with tghot other two functions are required.

	TG_DL_EXPORTED
	void
	hot_boot( tg::hot::Hot_Api * hot_api, tg::hot::Hot_Api_Lib_Data data );

	TG_DL_EXPORTED
	void
	hot_outdated();

The macro `TG_DL_EXPORTED` is to help you tell the compiler to export the function symbol in your dl.

`hot_boot` will be called when the library is loaded and `hot_outdated` will be called before a new version is reloaded, before the new `hot_boot` is called.
`hot_boot` receives a reference to the struct `Hot_Api`, that can be used to ask for other dls and to register it's data, and with the previously registered version of its data.


	struct
	Hot_Api_Lib_Data
	{
		void * data;
		s64    size;
	};

	struct
	Hot_Api
	{
		const char * lib_name;

		bool
		request_dl( DL & dl, void ( * callback ) ( DL * dl, void * custom_data ) )
		{
			return ::tg::hot::request_dl( dl, callback, lib_name, custom_data );
		}

		void
		set_lib_data( void * data, s64 size )
		{
			::tg::hot::set_lib_data( Hot_Api_Lib_Data{ data, size }, lib_name );
		}
	};


`Hot_Api` contains the library name, based on the filename, and two wrappers to the `request_dl` and `set_lib_data`, that will provide this name for you.
Look in the Main section above for request_dl documentation.
Each library can provide tghot it's data blob, to keep data through hotrelaods. You can call `set_lib_data` providing the blob pointer and the blob size.

### Some other stuff

You can query if a dl is loaded by calling

	bool
	dl_is_present( DL & dl );

You can get the file path of the txt containing the version of a library via

	bool
	build_lib_version_path( char * str, s32 buf_len, const char * dl_name );

and the versioned dl file path via

	bool
	build_lib_path_versioned( char * str, s32 buf_len, const char * dl_name, const char * lib_version );

