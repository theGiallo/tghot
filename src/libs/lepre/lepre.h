#ifndef LPR_HEADER
#define LPR_HEADER

#ifndef LPR_APP_NAME
#define LPR_APP_NAME "Lepre"
#endif

#if LPR_INTERNAL
	#define LPR_DEF static
#else
	#define LPR_DEF extern
#endif

#ifndef LPR_SHADERS_FOLDER_PATH
#define LPR_SHADERS_FOLDER_PATH "../../resources/shaders/"
#endif

#define LPR_STRINGIFY(x) #x
#define LPR_TOSTRING(x) LPR_STRINGIFY(x)
#define LPR_HERE() __FILE__ ":" LPR_TOSTRING(__LINE__)
#define LPR_HERE_COLOR() LPR_CNSL_PURPLE " @" LPR_CNSL_CYAN __FILE__ LPR_CNSL_GREEN ":" LPR_CNSL_WHITE LPR_TOSTRING(__LINE__) LPR_CNSL_RESET

#if __cplusplus
#define LPR_ZERO_STRUCT {}
#else
#define LPR_ZERO_STRUCT {0}
#endif

#ifdef LPR_NO_CONSOLE_COLORS
	#define LPR_CNSL_RESET
	#define LPR_CNSL_BLACK
	#define LPR_CNSL_RED
	#define LPR_CNSL_GREEN
	#define LPR_CNSL_YELLOW
	#define LPR_CNSL_BLUE
	#define LPR_CNSL_PURPLE
	#define LPR_CNSL_CYAN
	#define LPR_CNSL_WHITE
#else
	#define LPR_CNSL_RESET  "\033[00m"
	#define LPR_CNSL_BLACK  "\033[0;30m"
	#define LPR_CNSL_RED    "\033[0;31m"
	#define LPR_CNSL_GREEN  "\033[0;32m"
	#define LPR_CNSL_YELLOW "\033[0;33m"
	#define LPR_CNSL_BLUE   "\033[0;34m"
	#define LPR_CNSL_PURPLE "\033[0;35m"
	#define LPR_CNSL_CYAN   "\033[0;36m"
	#define LPR_CNSL_WHITE  "\033[0;37m"
#endif

#ifndef LPR_PERR
	#if defined(ANDROID) || defined(__ANDROID__)
		#define LPR_PERR(f, ...) __android_log_print( ANDROID_LOG_ERROR, LPR_APP_NAME,\
		                                              f, ##__VA_ARGS__ )
	#else
		#include <stdio.h>
		#define LPR_PERR(f, ...) fprintf( stderr, "[" LPR_APP_NAME "] " f "\n",\
		                                  ##__VA_ARGS__ )
	#endif
#endif
#ifndef LPR_PINFO
	#if defined(ANDROID) || defined(__ANDROID__)
		#define LPR_PINFO(f, ...) __android_log_print( ANDROID_LOG_DEBUG, LPR_APP_NAME,\
		                                               f, ##__VA_ARGS__ )
	#else
		#include <stdio.h>
		#define LPR_PINFO(f, ...) printf( "[" LPR_APP_NAME "] " f "\n", ##__VA_ARGS__ )
	#endif
#endif
#ifndef LPR_PDBG
	#if defined(ANDROID) || defined(__ANDROID__)
		#define LPR_PINFO(f, ...) __android_log_print( ANDROID_LOG_DEBUG, LPR_APP_NAME,\
		                                               f, ##__VA_ARGS__ )
	#else
		#include <stdio.h>
		#define LPR_PDBG(f, ...) printf( "[" LPR_APP_NAME "] " f "\n", ##__VA_ARGS__ )
	#endif
#endif

#if LPR_DEBUG
	#ifndef LPR_BREAK
		#if defined(__GNUC__) || defined(__GNUG__) || defined(__clang__)
			#if defined(__i386__) || defined(__x86_64__)
				// NOTE(theGiallo): the repeated INT instruction is a hack to make
				// gdb give the correct line # with gcc/g++ (clang works with only 1)
				#if defined(__clang__)
					#define LPR_BREAK() __asm__ volatile( "int $0x03" )
				#else
					#define LPR_BREAK() __asm__ volatile( "int $0x03;int $0x03" )
				#endif
			#elif defined(__thumb__)
				#define LPR_BREAK() __asm__ volatile( ".inst 0xde01" )
			#elif defined(__arm__) && !defined(__thumb__)
				#define LPR_BREAK() __asm__ volatile( ".inst 0xe7f001f0" )
			#elif defined(__aarch64__)
				#define LPR_BREAK() __asm__ volatile( ".inst 0xd4200000" )
			#elif defined(_WIN32)
				#define LPR_BREAK() __builtin_trap()
			#else
				#include <signal.h>
				#define LPR_BREAK() raise(SIGTRAP)
			#endif
		#elif defined(_MSC_VER)
			#include <intrin.h>
			#define LPR_BREAK() __debugbreak()
		#else
			#include <stdlib.h>
			#define LPR_BREAK() abort()
		#endif
	#endif

	#ifndef lpr_assert
	#define lpr_assert(e) \
	   if (e) (void) 0; else { \
	   lpr_log_err( "Failed assert: '" "%s" \
	   "'\n",  LPR_TOSTRING(e) ); \
	   LPR_BREAK(); }
	#endif
	#ifndef lpr_assert_m
	#define lpr_assert_m(e, fmt, ...) \
	   if (e) (void) 0; else { \
	   lpr_log_err( "Failed assert: '" "%s" \
	   "'\nMessage:\n" fmt, LPR_TOSTRING(e), ##__VA_ARGS__ ); \
	   LPR_BREAK(); }
	#endif

	#ifndef LPR_ILLEGAL_PATH
	#define LPR_ILLEGAL_PATH() \
	                   lpr_log_err( "Illegal path reached" ); LPR_BREAK()
	#endif
	#ifndef lpr_log_dbg
	#define lpr_log_dbg( f, ... ) LPR_PDBG( LPR_CNSL_YELLOW "[DEBUG] " f LPR_HERE_COLOR(), ##__VA_ARGS__ )
	#endif
#else
	#ifndef lpr_assert
	#define lpr_assert(e, ...) ((void) 0)
	#endif
	#ifndef lpr_assert_m
	#define lpr_assert_m(e, fmt, ...) ((void) 0)
	#endif
	#ifndef LPR_ILLEGAL_PATH
	#define LPR_ILLEGAL_PATH() ((void) 0)
	#endif
	#ifndef lpr_log_dbg
	#define lpr_log_dbg( f, ... ) ((void) 0)
	#endif
#endif
#ifndef lpr_log_err

#define lpr_log_err( f, ... ) LPR_PERR( LPR_CNSL_RED "[ERROR] " f\
                                          LPR_HERE_COLOR(),\
                                          ##__VA_ARGS__ )
#endif
#ifndef lpr_log_info
#define lpr_log_info( f, ... ) LPR_PINFO( "[INFO ] " f, ##__VA_ARGS__ )
#endif

#include <errno.h>
#define LPR_PERROR_CLEAN(...) do{perror(__VA_ARGS__); errno=0;}while(lpr_false)

#define LPR_COPY_STRUCT(dst_p, src_p) lpr_memcpy( dst_p, src_p, sizeof(*(src_p)) )

#ifdef _MSC_VER
	typedef unsigned char      lpr_u8;
	typedef unsigned short int lpr_u16;
	typedef unsigned int       lpr_u32;
	typedef unsigned long int  lpr_u64;
	typedef signed char        lpr_s8;
	typedef signed short int   lpr_s16;
	typedef signed int         lpr_s32;
	typedef signed long int    lpr_s64;
#else
	#include <stdint.h>
	typedef uint8_t  lpr_u8;
	typedef uint16_t lpr_u16;
	typedef uint32_t lpr_u32;
	typedef uint64_t lpr_u64;
	typedef int8_t   lpr_s8;
	typedef int16_t  lpr_s16;
	typedef int32_t  lpr_s32;
	typedef int64_t  lpr_s64;
#endif
typedef float lpr_f32;

#define LPR_MAX_U8  0xFF
#define LPR_MAX_U16 0xFFFF
#define LPR_MAX_U32 0xFFFFFFFF
#define LPR_MAX_U64 0xFFFFFFFFFFFFFFFF

#include <float.h>
#define LPR_FLT_MAX FLT_MAX

#if __cplusplus
typedef bool lpr_bool;
#define LPR_TRUE true
#define LPR_FALSE false
const lpr_bool lpr_true  = LPR_TRUE;
const lpr_bool lpr_false = LPR_FALSE;
#else
typedef lpr_u8 lpr_bool;
#define LPR_TRUE 1==1
#define LPR_FALSE 1!=1
const lpr_bool lpr_true  = LPR_TRUE;
const lpr_bool lpr_false = LPR_FALSE;
#endif

#if defined(lpr_calloc) || defined(lpr_free)
	#if !defined(lpr_calloc) || !defined(lpr_free)
		#error You have to define both lpr_calloc and lpr_free!
	#endif
#endif
#ifndef lpr_calloc
	#include <stdlib.h>
	#define lpr_calloc(size,count,c) calloc(size,count)
	#define lpr_free(ptr,c) free(ptr)
#endif
#ifndef lpr_memcpy
	#include <string.h>
	#define lpr_memcpy(dst,src,size) memcpy(dst,src,size)
#endif
#ifndef lpr_memset
	#include <string.h>
	#define lpr_memset(s,c,n) memset(s,c,n)
#endif
#ifndef lpr_putc
	#include <stdio.h>
	#define lpr_putc(c, s) putc(c, s)
#endif

#ifndef lpr_pow
	#if __cplusplus && !LPR_NO_CPP_MATH
		#include <cmath>
		#define lpr_pow std::pow
		#define lpr_sqrt std::sqrt
		#define lpr_acos std::acos
		#define lpr_asin std::asin
		#define lpr_cos std::cos
		#define lpr_sin std::sin
		#define lpr_atan std::atan
		#define lpr_atan2 std::atan2
		#define lpr_round std::round
		#define lpr_abs std::abs
		#include <algorithm>
		#define lpr_min std::min
		#define lpr_max std::max
		#define lpr_modf std::modf
	#else
		#include <math.h>
		#define lpr_pow powf
		#define lpr_sqrt sqrtf
		#define lpr_acos acosf
		#define lpr_asin asinf
		#define lpr_cos cosf
		#define lpr_sin sinf
		#define lpr_atan atanf
		#define lpr_atan2 atan2f
		#define lpr_round roundf
		#define lpr_abs(x) ((x)>=0?(x):-(x))
		#define lpr_min(v1, v2) ((v1)<(v2)?(v1):(v2))
		#define lpr_max(v1, v2) ((v1)>(v2)?(v1):(v2))
		#define lpr_modf modff
	#endif
#endif

#define lpr_sin_deg(a) lpr_sin( lpr_deg2rad(a) )
#define lpr_cos_deg(a) lpr_cos( lpr_deg2rad(a) )
#define lpr_clamp(v, min, max) lpr_min(lpr_max(v, min), max)
// NOTE(theGiallo): smoothstep as inline function

/* NOTE(theGiallo): example of usage:
	lpr_u8 moved[LPR_BITNSLOTS( 0xFFFF )] = LPR_ZERO_STRUCT;
	LPR_BITSET( moved, 10 )
	lpr_assert( LPR_BITTEST( moved, 10 ), "LPR_BITSET is not working!" );
	LPR_BITCLEAR( moved, 10 )
	lpr_assert( !LPR_BITTEST( moved, 10 ), "LPR_BITCLEAR is not working!" );
*/
#if LPR_NO_BITVECTOR
#define LPR_BITSET(a, b) (a)[b] = lpr_true
#define LPR_BITCLEAR(a, b) (a)[b] = lpr_false
#define LPR_BITTEST(a, b) (a)[b]
#define LPR_BITNSLOTS(nb) (nb)
#else
#ifndef LPR_CHAR_BIT
	#ifdef CHAR_BIT
		#define LPR_CHAR_BIT CHAR_BIT
	#else
		#if LPR_NO_LIMITS_H
			// NOTE(theGiallo): this is not safe, maybe is better to disable
			// bitvectors if you can't be sure.
			#define LPR_CHAR_BIT ( 8 * sizeof( lpr_u8 ) )
		#else
			#include <limits.h>
			#define LPR_CHAR_BIT CHAR_BIT
		#endif
	#endif
#endif

#define LPR_BITMASK(b) (1 << ((b) % LPR_CHAR_BIT))
#define LPR_BITSLOT(b) ((b) / LPR_CHAR_BIT)
#define LPR_BITSET(a, b) ((a)[LPR_BITSLOT(b)] |= LPR_BITMASK(b))
#define LPR_BITCLEAR(a, b) ((a)[LPR_BITSLOT(b)] &= ~LPR_BITMASK(b))
#define LPR_BITTEST(a, b) ((a)[LPR_BITSLOT(b)] & LPR_BITMASK(b))
#define LPR_BITNSLOTS(nb) ((nb + LPR_CHAR_BIT - 1) / LPR_CHAR_BIT)
#endif


#pragma pack(push, 1)
typedef
union
{
	struct
	{
		lpr_u8 r,g,b;
	};
	struct
	{
		lpr_u8 rgb[3];
	};
} __attribute__((packed)) Lpr_Rgb_u8;


typedef
union
{
	struct
	{
		lpr_u8 r,g,b,a;
	};
	struct
	{
		lpr_u8 rgb[3], _a;
	};
	struct
	{
		lpr_u8 rgba[4];
	};
	struct
	{
		lpr_u32 rgba_b;
	};
	struct
	{
		Lpr_Rgb_u8 lpr_rgb_u8;
		lpr_u8 __a;
	};
} __attribute__((packed)) Lpr_Rgba_u8;

#pragma pack(pop)

typedef
struct
{
	Lpr_Rgba_u8 red;
	Lpr_Rgba_u8 green;
	Lpr_Rgba_u8 blue;
	Lpr_Rgba_u8 cyan;
	Lpr_Rgba_u8 magenta;
	Lpr_Rgba_u8 yellow;
	Lpr_Rgba_u8 black;
	Lpr_Rgba_u8 white;
	Lpr_Rgba_u8 gray_50;
	Lpr_Rgba_u8 gray_25;
	Lpr_Rgba_u8 gray_75;
	Lpr_Rgba_u8 violet;
	Lpr_Rgba_u8 purple;
	Lpr_Rgba_u8 brown;
	Lpr_Rgba_u8 orange;
	Lpr_Rgba_u8 metallic_gold;
} Lpr_Rgba_Colors_u8;
const
Lpr_Rgba_Colors_u8
lpr_C_colors_u8 =
{
	{{0xff,0x00,0x00,0xff}}, // red
	{{0x00,0xff,0x00,0xff}}, // green
	{{0x00,0x00,0xff,0xff}}, // blue
	{{0x00,0xff,0xff,0xff}}, // cyan
	{{0xff,0x00,0xff,0xff}}, // magenta
	{{0xff,0xff,0x00,0xff}}, // yellow
	{{0x00,0x00,0x00,0xff}}, // black
	{{0xff,0xff,0xff,0xff}}, // white
	{{0x80,0x80,0x80,0xff}}, // gray_50
	{{0x40,0x40,0x40,0xff}}, // gray_25
	{{0xc0,0xc0,0xc0,0xff}}, // gray_75
	{{0x8a,0x2b,0xe2,0xff}}, // violet
	{{0x80,0x00,0x80,0xff}}, // purple
	{{0x80,0x40,0x00,0xff}}, // brown
	{{0xff,0x80,0x00,0xff}}, // orange
	{{0xd4,0xaf,0x37,0xff}}  // metallic_gold
};

#pragma pack(push, 1)
typedef
union
{
	struct
	{
		lpr_f32 r,g,b;
	};
	struct
	{
		lpr_f32 rgb[3];
	};
} Lpr_Rgb_f32;

typedef
union
{
	struct
	{
		lpr_f32 r,g,b,a;
	};
	struct
	{
		lpr_f32 rgb[3], _a;
	};
	struct
	{
		lpr_f32 rgba[4];
	};
	struct
	{
		Lpr_Rgb_f32 lpr_rgb_f32;
		lpr_f32 __a;
	};
} Lpr_Rgba_f32;
#pragma pack(pop)

#if __cplusplus
inline
Lpr_Rgb_u8
lpr_Rgb_u8_from_f32( Lpr_Rgb_f32 color )
{
	Lpr_Rgb_u8 ret;
	for ( int i = 0; i != 3; ++i )
	{
		ret.rgb[i] = (lpr_u8)lpr_round( color.rgb[i] * 255.0f );
	}
	return ret;
}
#else
extern
inline
Lpr_Rgba_u8
lpr_Rgb_u8_from_f32( Lpr_Rgba_f32 color );
#endif
#if __cplusplus
inline
Lpr_Rgba_u8
lpr_Rgba_u8_from_f32( Lpr_Rgba_f32 color )
{
	Lpr_Rgba_u8 ret;
	for ( int i = 0; i != 4; ++i )
	{
		ret.rgba[i] = (lpr_u8)lpr_round( color.rgba[i] * 255.0f );
	}
	return ret;
}
#else
extern
inline
Lpr_Rgba_u8
lpr_Rgba_u8_from_f32( Lpr_Rgba_f32 color );
#endif
#if __cplusplus
inline
Lpr_Rgb_f32
lpr_Rgb_f32_from_u8( Lpr_Rgb_u8 color )
{
	Lpr_Rgb_f32 ret;
	for ( int i = 0; i != 3; ++i )
	{
		ret.rgb[i] = color.rgb[i] / 255.0f;
	}
	return ret;
}
#else
extern
inline
Lpr_Rgb_f32
lpr_Rgb_f32_from_u8( Lpr_Rgb_u8 color );
#endif
#if __cplusplus
inline
Lpr_Rgba_f32
lpr_Rgba_f32_from_u8( Lpr_Rgba_u8 color )
{
	Lpr_Rgba_f32 ret;
	for ( int i = 0; i != 4; ++i )
	{
		ret.rgba[i] = color.rgba[i] / 255.0f;
	}
	return ret;
}
#else
extern
inline
Lpr_Rgba_f32
lpr_Rgba_f32_from_u8( Lpr_Rgba_u8 color );
#endif

typedef
struct
{
	Lpr_Rgba_f32 red;
	Lpr_Rgba_f32 green;
	Lpr_Rgba_f32 blue;
	Lpr_Rgba_f32 cyan;
	Lpr_Rgba_f32 magenta;
	Lpr_Rgba_f32 yellow;
	Lpr_Rgba_f32 black;
	Lpr_Rgba_f32 white;
	Lpr_Rgba_f32 gray_50;
	Lpr_Rgba_f32 gray_25;
	Lpr_Rgba_f32 gray_75;
	Lpr_Rgba_f32 violet;
	Lpr_Rgba_f32 purple;
	Lpr_Rgba_f32 brown;
	Lpr_Rgba_f32 orange;
	Lpr_Rgba_f32 metallic_gold;
} Lpr_Rgba_Colors_f32;
extern Lpr_Rgba_Colors_f32 lpr_C_colors_f32;

#if __cplusplus
inline
Lpr_Rgb_f32
lpr_Rgb_f32_sRGB_from_linear( Lpr_Rgb_f32 color )
{
	Lpr_Rgb_f32 ret;
	for ( int i = 0; i != 3; ++i )
	{
		ret.rgb[i] = lpr_pow( color.rgb[i], 1.0f / 2.2f );
	}
	return ret;
}
#else
extern
inline
Lpr_Rgb_f32
lpr_Rgb_f32_sRGB_from_linear( Lpr_Rgb_f32 color );
#endif

#if __cplusplus
inline
Lpr_Rgba_f32
lpr_Rgba_f32_sRGB_from_linear( Lpr_Rgba_f32 color )
{
	Lpr_Rgba_f32 ret;
	ret.lpr_rgb_f32 = lpr_Rgb_f32_sRGB_from_linear( color.lpr_rgb_f32 );
	ret.a = color.a;
	return ret;
}
#else
extern
inline
Lpr_Rgba_f32
lpr_Rgba_f32_sRGB_from_linear( Lpr_Rgba_f32 color )
#endif

#if __cplusplus
inline
Lpr_Rgb_f32
lpr_Rgb_f32_linear_from_sRGB( Lpr_Rgb_f32 color )
{
	Lpr_Rgb_f32 ret;
	for ( int i = 0; i != 3; ++i )
	{
		ret.rgb[i] = lpr_pow( color.rgb[i], 2.2f );
	}
	return ret;
}
#else
extern
inline
Lpr_Rgb_f32
lpr_Rgb_f32_linear_from_sRGB( Lpr_Rgb_f32 color );
#endif


#if __cplusplus
inline
Lpr_Rgba_f32
lpr_Rgba_f32_linear_from_sRGB( Lpr_Rgba_f32 color )
{
	Lpr_Rgba_f32 ret;
	ret.lpr_rgb_f32 = lpr_Rgb_f32_linear_from_sRGB( color.lpr_rgb_f32 );
	ret.a = color.a;
	return ret;
}
#else
extern
inline
Lpr_Rgba_f32
lpr_Rgba_f32_linear_from_sRGB( Lpr_Rgba_f32 color );
#endif
extern Lpr_Rgba_Colors_f32 lpr_C_colors_f32_linear;

typedef
struct
{
	GLfloat array[9];
} Lpr_Mx3;

typedef
union
{
	struct
	{
		float x,y;
	};
	struct
	{
		float w,h;
	};
	struct
	{
		float xy[2];
	};
	struct
	{
		float wh[2];
	};
} Lpr_V2;

typedef
union
{
	struct
	{
		lpr_u32 x,y;
	};
	struct
	{
		lpr_u32 w,h;
	};
	struct
	{
		lpr_u32 xy[2];
	};
	struct
	{
		lpr_u32 wh[2];
	};
} Lpr_V2u32;

typedef
struct
{
	Lpr_V2 pos;
	Lpr_V2 size;
} Lpr_AA_Rect;

typedef
struct
{
	Lpr_V2 pos;
	Lpr_V2 size;
	float rot_deg;
} Lpr_Rot_Rect;

typedef
struct
{
	lpr_u32 first;
	lpr_u32 count;
} Lpr_Sprites_Span;

// TODO(theGiallo, 2015/09/24): check for a max number textures that could be
// supported by driver, that makes sense. This could be dynamic at some point.
// So it would be necessary to insert a define into the shader and to split
// batches if too many textures.
#ifndef LPR_MAX_TEXTURES
#define LPR_MAX_TEXTURES 64
#endif
const int lpr_C_max_textures = LPR_MAX_TEXTURES;

#ifndef LPR_STENCILS_TEXTURES_COUNT
#define LPR_STENCILS_TEXTURES_COUNT 2
#endif
#define LPR_STENCIL_NONE 0xFF
const int lpr_C_stencils_textures_count = LPR_STENCILS_TEXTURES_COUNT;
const int lpr_C_max_stencils = LPR_STENCILS_TEXTURES_COUNT * 128 - 1;

#ifndef LPR_MAX_DEBUG_LINES
#define LPR_MAX_DEBUG_LINES 10000
#endif
const int lpr_C_max_lines = LPR_MAX_DEBUG_LINES;

#ifndef LPR_MAX_PERSISTENT_DEBUG_LINES
#define LPR_MAX_PERSISTENT_DEBUG_LINES 10000
#endif
const int lpr_C_max_persistent_lines = LPR_MAX_PERSISTENT_DEBUG_LINES;

#ifndef LPR_MAX_QUADS_PER_BATCH
#define LPR_MAX_QUADS_PER_BATCH LPR_MAX_U16
#endif
#if LPR_MAX_QUADS_PER_BATCH > LPR_MAX_U16
#error LPR_MAX_QUADS_PER_BATCH must be less than 65535 because we use 16bit indices
#endif
const int lpr_C_max_quads_per_batch = LPR_MAX_QUADS_PER_BATCH;

#pragma pack(push, 0)
typedef
struct
{
	GLfloat pos[2],
	        size[2],
	        rot;
	GLubyte color[4];

	// NOTE: IMPORTANT not the opengl one! the fragment shader one!
	// automatically set, do not set it manually!
	// NOTE(theGiallo): if it's 0xFF (=LPR_MAX_U8) no texture will be used,
	// only color
	GLubyte texture_id;
	// NOTE(theGiallo): id of the stencil bit. Stencils are 2 GL_RGBA32UI, so we
	// have 256 bits. Stencils are not the OpenGL ones, so we can have up to 256
	// stencils per draw-call with only 2 textures.
	GLubyte stencil_id;
	GLushort padding;

	// NOTE(theGiallo): these 2 are to set the quad UV coordinates from the [0,1]
	// NOTE(theGiallo): the resulting UV coordinates are in the limit space, not
	// in the entire texture space
	GLfloat uv_offset[2],
	        uv_scale[2],

	// NOTE(theGiallo): these 2 sets the rectangle in which to limit the sampling
	// and the overflow behavior. This defines the uv space: this rectangle is
	// considered [0,1]
	        uv_origin[2],
	        uv_limits[2];
	GLubyte texture_overflow_x,
	        texture_overflow_y,
	        texture_min_filter,
	        texture_mag_filter;
} Lpr_Quad_Data_T_Atlas;
#pragma pack(pop)

typedef
struct
{
	GLuint pos,
	       size,
	       rot, // NOTE(theGiallo): this is in deg but maybe it should be in radiants? TODO(theGiallo, 2015/12/11)
	       color,
	       texture_id,
	       stencil_id,
	       uv_offset,
	       uv_scale,
	       uv_origin,
	       uv_limits,
	       texture_overflow_x,
	       texture_overflow_y,
	       texture_min_filter,
	       texture_mag_filter;
} Lpr_Quad_Data_Loc;

// NOTE(theGiallo): IMPORTANT always initialize to {}
typedef
struct
{
	GLuint gl_texture;

	Lpr_V2u32 size;

	GLuint default_min_filter;
	GLuint default_mag_filter;
	Lpr_V2u32 default_overflow;
} Lpr_Texture;

typedef
struct
{
	Lpr_Texture texture;
	Lpr_V2u32 atlas_size_frames;
	Lpr_V2u32 frame_size_px;
	Lpr_V2    frame_size_perc;
} Lpr_Atlas;

LPR_DEF
void
lpr_make_Atlas( Lpr_Atlas * atlas, Lpr_Texture const * texture,
                Lpr_V2u32 frame_size_px );

#define LPR_EXPAND_AS_ENUM_IMG_FMT(a,b,c) a = b,
#define LPR_EXPAND_AS_SIZE_TABLE_IMG_FMT(a,b,c) c,
#define LPR_EXPAND_AS_STRING_TABLE_IMG_FMT(a,b,c) LPR_TOSTRING(a),
#define LPR_IMG_FMT_TABLE(ENTRY) \
	ENTRY(LPR_RGBA8,            0x0, 4) \
	ENTRY(LPR_RGB8,             0x1, 3) \
	ENTRY(LPR_GRAYSCALE8,       0x2, 1) \
	ENTRY(LPR_GRAYSCALE8_ALPHA, 0x3, 1) \

typedef
enum
{
	LPR_IMG_FMT_TABLE(LPR_EXPAND_AS_ENUM_IMG_FMT)
	// ---
	LPR_IMG_FMT_COUNT
} Lpr_Img_Fmt;
extern const lpr_u8 lpr_C_img_fmt_bytes_per_pixel[LPR_IMG_FMT_COUNT];
extern const char * lpr_C_img_fmt_strings[LPR_IMG_FMT_COUNT];


#define LPR_EXPAND_AS_ENUM_TEXTURE_FILTER(a,b,c) a = b,
#define LPR_EXPAND_AS_GLTEXPARAMETER_TABLE_TEXTURE_FILTER(a,b,c) c,
#define LPR_TEXTURE_FILTER_TABLE(ENTRY) \
	ENTRY(LPR_NEAREST,           0x0, GL_NEAREST               ) \
	ENTRY(LPR_BILINEAR,          0x1, GL_LINEAR                ) \
	ENTRY(LPR_MIPMAP_NEAREST,    0x2, GL_NEAREST_MIPMAP_NEAREST) \
	ENTRY(LPR_MIPMAP_BILINEAR,   0x3, GL_LINEAR_MIPMAP_NEAREST ) \
	ENTRY(LPR_MIPMAP_LINEAR,     0x4, GL_NEAREST_MIPMAP_LINEAR ) \
	ENTRY(LPR_MIPMAP_TRILINEAR,  0x5, GL_LINEAR_MIPMAP_LINEAR  ) \

typedef
enum
{
	LPR_TEXTURE_FILTER_TABLE(LPR_EXPAND_AS_ENUM_TEXTURE_FILTER)
	// ---
	LPR_TEXTURE_FILTER_COUNT
} Lpr_Texture_Filter;
extern const lpr_u32 lpr_C_texture_filter_gltexparameter[LPR_TEXTURE_FILTER_COUNT];


#define LPR_EXPAND_AS_ENUM_WRAP(a,b,c) a = b,
#define LPR_EXPAND_AS_GLTEXPARAMETER_TABLE_WRAP(a,b,c) c,
#define LPR_WRAP_TABLE(ENTRY) \
	ENTRY(LPR_CLAMP_TO_EDGE,   0x0, GL_CLAMP_TO_EDGE   ) \
	ENTRY(LPR_REPEAT,          0x1, GL_REPEAT          ) \
	ENTRY(LPR_MIRRORED_REPEAT, 0x2, GL_MIRRORED_REPEAT ) \

typedef
enum
{
	LPR_WRAP_TABLE(LPR_EXPAND_AS_ENUM_WRAP)
	// ---
	LPR_WRAP_COUNT
} Lpr_Wrap;
extern const lpr_u32 lpr_C_wrap_gltexparameter[LPR_WRAP_COUNT];

typedef
enum
{
	LPR_SCALE_LINEAR,
	// ---
	LPR_SCALE_COUNT
} Lpr_Scale_Filter;

#if 0
// TODO(theGiallo, 2015/12/06): check if this is needed
struct
Lpr_Texture_Atlas
{
	Lpr_Texture texture;
	Lpr_iV2 frame_size;
	Lpr_iV2 atlas_size_in_frames;
};
#endif

typedef
enum
{
	LPR_BLEND_BLEND        = 0x0,
	LPR_BLEND_MULTIPLY_1_2 = 0x1,
	LPR_BLEND_MULTIPLY_0_1 = 0x2,
	#if 0
	LPR_BLEND_ADD        = 0x,
	LPR_BLEND_SUBTRACT   = 0x,
	LPR_BLEND_MULTIPLY   = 0x,
	LPR_BLEND_SCREEN     = 0x,
	LPR_BLEND_SOFT_LIGHT = 0x,
	#endif
	// ---
	LPR_BLEND_MODE_COUNT
} Lpr_Blend_Mode;

#pragma pack(push, 0)
typedef
struct
{
	Lpr_V2 pos;
	Lpr_Rgba_u8 color;
}
Lpr_Colored_Vertex;
#pragma pack(pop)

typedef
struct
{
	GLuint lines_program;
	GLuint lines_vb;
	GLuint vertex_loc,
	       color_loc;
	GLuint VP_loc;

	Lpr_Colored_Vertex line_vertices[LPR_MAX_DEBUG_LINES*2];
	int lines_count;

	Lpr_Colored_Vertex persistent_line_vertices[LPR_MAX_PERSISTENT_DEBUG_LINES*2];
	int persistent_lines_count;
} Lpr_Debug_Data;

typedef
struct
{
	GLuint fbo,
	       fb_texture,
	       color_rt;
} Lpr_FBO_Texture_Color;

#define LPR_STENCIL_DRAW_MODE_TX_MASK 0x0FFFFF00u
#define LPR_STENCIL_DRAW_MODE_LOGIC_MASK 0x700000FFu
#define LPR_EXPAND_AS_ENUM_STENCIL_DRAW_MODE(a,b,c) a = b,
#define LPR_EXPAND_AS_STRING_TABLE_STENCIL_DRAW_MODE(a,b,c) c,
#define LPR_STENCIL_DRAW_MODES(ENTRY) \
	ENTRY(LPR_STENCIL_DRAW_MODE_TX_ALPHA_NOT_ZERO, 0x0F000000u, "texture alpha not zero" ) \
	ENTRY(LPR_STENCIL_DRAW_MODE_TX_ALPHA_ZERO,     0x0F000100u, "texture alpha zero"     ) \
	ENTRY(LPR_STENCIL_DRAW_MODE_TX_RGB_AVG_GT05,   0x0F000200u, "texture rgb avg > 0.5"  ) \
	ENTRY(LPR_STENCIL_DRAW_MODE_TX_R_GT05,         0x0F000200u, "texture r > 0.5"        ) \
	ENTRY(LPR_STENCIL_DRAW_MODE_TX_G_GT05,         0x0F000200u, "texture g > 0.5"        ) \
	ENTRY(LPR_STENCIL_DRAW_MODE_TX_B_GT05,         0x0F000200u, "texture b > 0.5"        ) \
	ENTRY(LPR_STENCIL_DRAW_MODE_TX_RGB_AVG_LT05,   0x0F000600u, "texture rgb avg < 0.5"  ) \
	ENTRY(LPR_STENCIL_DRAW_MODE_TX_R_LT05,         0x0F000200u, "texture r < 0.5"        ) \
	ENTRY(LPR_STENCIL_DRAW_MODE_TX_G_LT05,         0x0F000200u, "texture g < 0.5"        ) \
	ENTRY(LPR_STENCIL_DRAW_MODE_TX_B_LT05,         0x0F000200u, "texture b < 0.5"        ) \
	ENTRY(LPR_STENCIL_DRAW_MODE_SET_ZERO,  0x70000000u, "zero"          ) \
	ENTRY(LPR_STENCIL_DRAW_MODE_SET_ONE,   0x70000001u, "one"           ) \
	ENTRY(LPR_STENCIL_DRAW_MODE_XOR_ZERO,  0x70000002u, "xor with zero" ) \
	ENTRY(LPR_STENCIL_DRAW_MODE_XOR_ONE,   0x70000003u, "xor with one"  ) \
	ENTRY(LPR_STENCIL_DRAW_MODE_INVERT,    0x70000004u, "invert"        ) \
	ENTRY(LPR_STENCIL_DRAW_MODE_UNCHANGED, 0x7FFFFFFFu, "unchanged"     ) \

typedef
enum
{
	LPR_STENCIL_DRAW_MODES(LPR_EXPAND_AS_ENUM_STENCIL_DRAW_MODE)
	// ---
	LPR_STENCIL_DRAW_MODES_COUNT = 16 // NOTE(theGiallo): IMPORTANT manually adjust count!
} Lpr_Stencil_Draw_Mode;
extern const char * lpr_C_stencil_draw_modes_names[LPR_STENCIL_DRAW_MODES_COUNT];


typedef
struct
{
	GLuint textures[LPR_MAX_TEXTURES],
	       textures_count;
	// NOTE(theGiallo): it's double buffered to be able to stencil the stencil drawing
	Lpr_FBO_Texture_Color stencil_fbos[2][LPR_STENCILS_TEXTURES_COUNT];
	lpr_u32  stencils_count;
	lpr_bool stencils_enabled;
	lpr_bool manage_stencils_rt;
	GLsizei quads_count;
	Lpr_Quad_Data_T_Atlas instances_data[LPR_MAX_QUADS_PER_BATCH];
	lpr_u16               z             [LPR_MAX_QUADS_PER_BATCH];
	lpr_u8                blend_modes   [LPR_MAX_QUADS_PER_BATCH];
	// NOTE(theGiallo): ids is here to avoid allocations
	/* TODO(theGiallo, 2016/01/06): maybe there's a better solution, like using a
	user provided allocator, but that allocator should be very fast, and the
	user of Lepre should know that, and should have one or be able to implement
	one. This charging with this supposed knowledge seems bad to me.
	*/
	lpr_u16               ids           [LPR_MAX_QUADS_PER_BATCH];
	/* TODO(theGiallo, 2016/01/21): ids could stay here, but this seems bad.
	Having one per batch could be useful for concurrency, but we could make the
	user allocate it...
	*/
	lpr_u16               tmp           [LPR_MAX_QUADS_PER_BATCH];
	/* NOTE(theGiallo): this is for optimization. If you want to use the same
	blend mode for all the instances set has_many_blend_modes to lpr_false and set
	single_blend_mode to your blend mode
	*/
	lpr_bool has_many_blend_modes;
	lpr_u8 single_blend_mode;
	Lpr_Mx3 VP;
	Lpr_V2 camera_pos;
	float camera_angle;
	Lpr_V2 vanishing_point;
	Lpr_V2 camera_world_span;

	// NOTE(theGiallo): initialized once per run. Will contain the instances data.
	GLuint per_instance_vb;
} Lpr_Batch_Data;

typedef
struct
{
	// NOTE(theGiallo): initialized once per run
	GLuint vertex_array_ID;
	GLuint fullscreen_quad_vb;
	GLuint quad_vb;
	struct
	Instanced_Shader_Data
	{
		GLuint quad_program;
		Lpr_Quad_Data_Loc per_instance_data_loc;
		GLuint va_vertex_loc,
		       va_uv_loc;
		GLuint max_textures_count,
		       textures_locs[LPR_MAX_TEXTURES],
		       stencils_locs[LPR_STENCILS_TEXTURES_COUNT];
		GLuint VP_loc;
	} no_stencils, stencils;
	struct
	Stencil_Shader_Data
	{
		GLuint program;
		GLuint vertex_loc,
		       uv_loc;
		GLuint texture_loc,
		       target_stencil_loc,
		       stencil_loc;
		GLuint draw_mode_loc,
		       target_stencil_id_loc,
		       stencil_id_loc,
		       VP_loc;
	} stencil_shader_no_stencils, stencil_shader_stencils;
	GLuint stencil_draw_vb, stencil_draw_ib;
	GLubyte stencil_draw_index_buffer[6]; // NOTE(theGiallo): = { 0, 3, 1, 2, 1, 3 };
	// NOTE(theGiallo): framebuffer_is_sRGB indicates if the current hw
	// give support for sRGB framebuffers. If not a linear one will be used, and
	// sRGB conversion will be applyes through a shader pass.
	// IMPORTANT It has to be initialized by the user.
	lpr_bool framebuffer_is_sRGB;
	// NOTE(theGiallo): gamma correction data
	Lpr_FBO_Texture_Color linear_fbo;
	GLuint gamma_correction_program;
	GLuint gc_vertex_loc,
	       gc_uv_loc,
	       gc_linear_fb_texture_loc;

	// NOTE(theGiallo): cleaned and filled each frame, managed by the user
	Lpr_Batch_Data * batches;
	lpr_u32 batches_count;

	// NOTE(theGiallo): updated when necessary by the user
	Lpr_V2u32 window_size;
	Lpr_V2u32 viewport_origin;
	Lpr_V2u32 viewport_size;

	#if LPR_DEBUG_LINES
	Lpr_Debug_Data debug;
	#endif
} Lpr_Draw_Data;

// NOTE(theGiallo): math const
#define LPR_PI 3.141592653589793238462643383279f
const float lpr_c_pi = LPR_PI;

#if __cplusplus
inline
float
lpr_deg2rad( float angle )
{
	return angle * LPR_PI / 180.0f;
}
#else
extern
inline
float
lpr_deg2rad( float angle );
#endif

#if __cplusplus
inline
float
lpr_rad2deg( float angle )
{
	return angle * 180.0f / LPR_PI;
}
#else
extern
inline
float
lpr_rad2deg( float angle );
#endif

#if __cplusplus
inline
float
lpr_signf( float a )
{
	return ( ( a ) > 0 ) - ( ( a ) < 0 );
}
#else
extern
inline
float
lpr_signf( float a );
#endif

#if __cplusplus
inline
float
lpr_smoothstep( float v0, float v1, float a )
{
	a = lpr_clamp( (a - v0) / (v1 - v0), 0.0f, 1.0f );
	return a*a * ( 3.0f - 2.0f * a );
}
#else
extern
inline
float
lpr_smoothstep( float v0, float v1, float a );
#endif

#if __cplusplus
inline
float
lpr_smootherstep( float v0, float v1, float a )
{
	a = lpr_clamp( (a - v0) / (v1 - v0), 0.0f, 1.0f );
	return a*a*a*(a*(a*6.0f - 15.0f) + 10.0f);
}
#else
extern
inline
float
lpr_smootherstep( float v0, float v1, float a );
#endif

LPR_DEF
Lpr_V2
lpr_V2( float x, float y);

LPR_DEF
Lpr_V2u32
lpr_V2u32( lpr_u32 x, lpr_u32 y);

#if __cplusplus
inline
Lpr_V2
lpr_V2_from_V2u32( Lpr_V2u32 v )
{
	Lpr_V2 ret = { (float)v.x, (float)v.y };

	return ret;
}
#else
extern
inline
Lpr_V2
lpr_V2_from_V2u32( Lpr_V2u32 v );
#endif

LPR_DEF
Lpr_V2
lpr_V2_rotated_deg( Lpr_V2 v, float a );

#if __cplusplus
inline
float
lpr_V2_sqlength( Lpr_V2 v )
{
	float ret = v.x*v.x + v.y*v.y;
	return ret;
}
#else
extern
inline
float
lpr_V2_sqlength( Lpr_V2 v );
#endif


#if __cplusplus
inline
float
lpr_V2_length( Lpr_V2 v )
{
	float ret = lpr_sqrt( v.x*v.x + v.y*v.y );
	return ret;
}
#else
extern
inline
float
lpr_V2_length( Lpr_V2 v );
#endif

#if __cplusplus
inline
Lpr_V2
lpr_V2_abs( Lpr_V2 v0 )
{
	Lpr_V2 ret = { lpr_abs( v0.x ), lpr_abs( v0.y ) };
	return ret;
}
#else
extern
inline
Lpr_V2
lpr_V2_abs( Lpr_V2 v0 );
#endif

#if __cplusplus
inline
Lpr_V2
lpr_V2_add_V2( Lpr_V2 v0, Lpr_V2 v1 )
{
	Lpr_V2 ret = { v0.x + v1.x, v0.y + v1.y };
	return ret;
}
#else
extern
inline
Lpr_V2
lpr_V2_add_V2( Lpr_V2 v0, Lpr_V2 v1 );
#endif

#if __cplusplus
inline
Lpr_V2
lpr_V2_sub_V2( Lpr_V2 v0, Lpr_V2 v1 )
{
	Lpr_V2 ret = { v0.x - v1.x, v0.y - v1.y };
	return ret;
}
#else
extern
inline
Lpr_V2
lpr_V2_sub_V2( Lpr_V2 v0, Lpr_V2 v1 );
#endif

#if __cplusplus
inline
Lpr_V2
lpr_V2_div_f32( Lpr_V2 v0, float f )
{
	Lpr_V2 ret = { v0.x / f, v0.y / f };
	return ret;
}
#else
extern
inline
Lpr_V2
lpr_V2_div_f32( Lpr_V2 v0, float f );
#endif

#if __cplusplus
inline
Lpr_V2
lpr_V2_mult_f32( Lpr_V2 v0, float f )
{
	Lpr_V2 ret = { v0.x * f, v0.y * f };
	return ret;
}
#else
extern
inline
Lpr_V2
lpr_V2_mult_f32( Lpr_V2 v0, float f );
#endif

#if __cplusplus
inline
Lpr_V2u32
lpr_V2u32_add_V2u32( Lpr_V2u32 v0, Lpr_V2u32 v1 )
{
	Lpr_V2u32 ret = { v0.x + v1.x, v0.y + v1.y };
	return ret;
}
#else
extern
inline
Lpr_V2u32
lpr_V2u32_add_V2u32( Lpr_V2u32 v0, Lpr_V2u32 v1 );
#endif

#if __cplusplus
inline
Lpr_V2u32
lpr_V2u32_sub_V2u32( Lpr_V2u32 v0, Lpr_V2u32 v1 )
{
	Lpr_V2u32 ret = { v0.x - v1.x, v0.y - v1.y };
	return ret;
}
#else
extern
inline
Lpr_V2u32
lpr_V2u32_sub_V2u32( Lpr_V2u32 v0, Lpr_V2u32 v1 );
#endif

#if __cplusplus
inline
Lpr_Mx3
lpr_Mx3_make_identity()
{
	Lpr_Mx3 ret = LPR_ZERO_STRUCT;
	ret.array[0*3 + 0] = 1.0;
	ret.array[1*3 + 1] = 1.0;
	ret.array[2*3 + 2] = 1.0;

	return ret;
}
#else
extern
inline
Lpr_Mx3
lpr_Mx3_make_identity();
#endif

LPR_DEF
GLfloat
lpr_Mx3_get( Lpr_Mx3 const * mx3, int row, int col );

LPR_DEF
void
lpr_Mx3_set( Lpr_Mx3 * mx3, int row, int col, GLfloat value );

/*
	NOTE(theGiallo): creates into mx3 an orthographic projection matrix, that
	covers an area of given size, cented in the vanishing point, that is passed
	as a percentage value in respect to size.
 */
LPR_DEF
void
lpr_Mx3_orthographic( Lpr_Mx3 * mx3, Lpr_V2 vanishing_point, Lpr_V2 size );

LPR_DEF
void
lpr_Mx3_translation( Lpr_Mx3 * mx3, Lpr_V2 t );

// NOTE(theGiallo): rotation is CCW
LPR_DEF
void
lpr_Mx3_rotation_deg( Lpr_Mx3 * mx3, float angle );

LPR_DEF
void
lpr_Mx3_scale( Lpr_Mx3 * mx3, float scale );

LPR_DEF
void
lpr_Mx3_product( const Lpr_Mx3 * mx3_a, const Lpr_Mx3 * mx3_b,
                 Lpr_Mx3 * mx3_res );

#if __cplusplus
inline
float
lpr_clamp_angle_deg( float angle )
{
	int A = angle / 360 ;
	A *= 360;
	angle = angle - A;
	if ( angle < 0 ) angle += 360;

	return angle;
}
#else
extern
inline
float
lpr_clamp_angle_deg( float angle );
#endif

#if __cplusplus
inline
Lpr_AA_Rect
lpr_AA_Rect_from_minmax( Lpr_V2 min, Lpr_V2 max )
{
	Lpr_AA_Rect ret;
	ret.pos  = lpr_V2_mult_f32( lpr_V2_add_V2( min, max ), 0.5f );
	ret.size = lpr_V2_sub_V2( max, min );
	return ret;
}
#else
extern
inline
Lpr_AA_Rect
lpr_AA_Rect_from_minmax( Lpr_V2 min, Lpr_V2 max );
#endif



////////////////////////////////////////////////////////////////////////////////
// UTF-8

typedef
union
{
	lpr_u32 all;
	lpr_u8 subchars[4];
} Lpr_UTF8_Fullchar;

// UTF-8
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// misc

// NOTE(theGiallo): T table for Pearson hashing from RFC 3074.
static
const
lpr_u8
lpr_C_pearson_table[256] =
{
   251, 175, 119, 215, 81, 14, 79, 191, 103, 49, 181, 143, 186, 157,  0,
   232, 31, 32, 55, 60, 152, 58, 17, 237, 174, 70, 160, 144, 220, 90, 57,
   223, 59,  3, 18, 140, 111, 166, 203, 196, 134, 243, 124, 95, 222, 179,
   197, 65, 180, 48, 36, 15, 107, 46, 233, 130, 165, 30, 123, 161, 209, 23,
   97, 16, 40, 91, 219, 61, 100, 10, 210, 109, 250, 127, 22, 138, 29, 108,
   244, 67, 207,  9, 178, 204, 74, 98, 126, 249, 167, 116, 34, 77, 193,
   200, 121,  5, 20, 113, 71, 35, 128, 13, 182, 94, 25, 226, 227, 199, 75,
   27, 41, 245, 230, 224, 43, 225, 177, 26, 155, 150, 212, 142, 218, 115,
   241, 73, 88, 105, 39, 114, 62, 255, 192, 201, 145, 214, 168, 158, 221,
   148, 154, 122, 12, 84, 82, 163, 44, 139, 228, 236, 205, 242, 217, 11,
   187, 146, 159, 64, 86, 239, 195, 42, 106, 198, 118, 112, 184, 172, 87,
   2, 173, 117, 176, 229, 247, 253, 137, 185, 99, 164, 102, 147, 45, 66,
   231, 52, 141, 211, 194, 206, 246, 238, 56, 110, 78, 248, 63, 240, 189,
   93, 92, 51, 53, 183, 19, 171, 72, 50, 33, 104, 101, 69, 8, 252, 83, 120,
   76, 135, 85, 54, 202, 125, 188, 213, 96, 235, 136, 208, 162, 129, 190,
   132, 156, 38, 47, 1, 7, 254, 24, 4, 216, 131, 89, 21, 28, 133, 37, 153,
   149, 80, 170, 68, 6, 169, 234, 151
};

// misc
////////////////////////////////////////////////////////////////////////////////

// TODO(theGiallo, 2015/12/31): when shaders are finalized put them here
#define LPR_DEV_SHADERS_FROM_FILES 1
#define LPR_GAMMA_CORRECTION_VERTEX_SHADER_TEXT ""
#define LPR_GAMMA_CORRECTION_FRAGMENT_SHADER_TEXT ""
#define LPR_2D_DEBUG_SEGMENTS_VERTEX_SHADER_TEXT ""
#define LPR_2D_DEBUG_SEGMENTS_FRAGMENT_SHADER_TEXT ""
#define LPR_QUAD_ATLAS_VERTEX_SHADER_TEXT ""
#define LPR_QUAD_ATLAS_FRAGMENT_SHADER_TEXT ""
#define LPR_SHADER_DEFINE_STENCILS_ENABLED "#define STENCILS_ENABLED 1\n"

// NOTE(theGiallo): IMPORTANT this function has to be called before anything else!
LPR_DEF
void
lpr_init();

LPR_DEF
void
lpr_make_px_perfect( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                     Lpr_V2u32 px_size, Lpr_V2 world_pos,
                     lpr_bool viewport_pxs, Lpr_V2 * out_pos, Lpr_V2 * out_size );

////////////////////////////////////////////////////////////////////////////////
// debug lines

#if LPR_DEBUG_LINES
LPR_DEF
void
lpr_add_debug_line( Lpr_Draw_Data * draw_data,
                    Lpr_V2 start, Lpr_V2 end,
                    Lpr_Rgba_u8 s_color, Lpr_Rgba_u8 e_color );

LPR_DEF
void
lpr_add_debug_persistent_line( Lpr_Draw_Data * draw_data,
                               Lpr_V2 start, Lpr_V2 end,
                               Lpr_Rgba_u8 s_color, Lpr_Rgba_u8 e_color );

LPR_DEF
void
lpr_add_debug_persistent_lines( Lpr_Draw_Data * draw_data );

LPR_DEF
void
lpr_draw_debug_lines( Lpr_Draw_Data * draw_data, lpr_s32 batch_id );
#endif

// debug lines
////////////////////////////////////////////////////////////////////////////////

// NOTE(theGiallo): all these add* return the id of the newly inserted instance
// data. -1 on error.

LPR_DEF
lpr_s32
lpr_add_quad_to_batch( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                       Lpr_Quad_Data_T_Atlas * const quad_data,
                       Lpr_Texture const * texture );

#if __cplusplus
LPR_DEF
lpr_s32
lpr_add_sprite( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                Lpr_Texture const * texture, Lpr_V2 pos,
                float scale = 1.0f, float rot_deg = 0.0f,
                Lpr_Rgba_u8 color = lpr_C_colors_u8.white );
#define lpr_add_sprite_w_scale lpr_add_sprite
#else
LPR_DEF
lpr_s32
lpr_add_sprite_w_scale( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                        Lpr_Texture const * texture, Lpr_V2 pos,
                        float scale, float rot_deg,
                        Lpr_Rgba_u8 color );
#endif

#if __cplusplus
LPR_DEF
lpr_s32
lpr_add_sprite( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                Lpr_Texture const * texture, Lpr_V2 pos,
                Lpr_V2 size, float rot_deg = 0.0f,
                Lpr_Rgba_u8 color = lpr_C_colors_u8.white );
#define lpr_add_sprite_w_size lpr_add_sprite
#else
LPR_DEF
lpr_s32
lpr_add_sprite_w_size( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                       Lpr_Texture const * texture, Lpr_V2 pos,
                       Lpr_V2 size, float rot_deg, Lpr_Rgba_u8 color );
#endif

#if __cplusplus
LPR_DEF
lpr_s32
lpr_add_sprite_px_perfect( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                           Lpr_Texture const * texture, Lpr_V2 pos,
                           lpr_bool screen_aligned = lpr_true,
                           lpr_bool viewport_pxs = lpr_false,
                           Lpr_Rgba_u8 color = lpr_C_colors_u8.white );
#else
LPR_DEF
lpr_s32
lpr_add_sprite_px_perfect( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                           Lpr_Texture const * texture, Lpr_V2 pos,
                           lpr_bool screen_aligned, lpr_bool viewport_pxs,
                           Lpr_Rgba_u8 color );
#endif

#if __cplusplus
LPR_DEF
lpr_s32
lpr_add_atlas_frame( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                     Lpr_Atlas const * atlas,
                     Lpr_V2 pos, Lpr_V2 size,
                     lpr_u32 frame_id,
                     float rot_deg = 0.0f,
                     Lpr_Rgba_u8 color = lpr_C_colors_u8.white );
#define lpr_add_atlas_frame_w_size lpr_add_atlas_frame
#else
LPR_DEF
lpr_s32
lpr_add_atlas_frame_w_size( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                            Lpr_Atlas const * atlas,
                            Lpr_V2 pos, Lpr_V2 size,
                            lpr_u32 frame_id,
                            float rot_deg, Lpr_Rgba_u8 color );
#endif

#if __cplusplus
LPR_DEF
lpr_s32
lpr_add_atlas_frame( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                     Lpr_Atlas const * atlas, Lpr_V2 pos, float scale,
                     lpr_u32 frame_id, float rot_deg = 0.0f,
                     Lpr_Rgba_u8 color = lpr_C_colors_u8.white );
#define lpr_add_atlas_frame_w_scale lpr_add_atlas_frame
#else
LPR_DEF
lpr_s32
lpr_add_atlas_frame_w_scale( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                             Lpr_Atlas const * atlas,
                             Lpr_V2 pos, float scale,
                             lpr_u32 frame_id,
                             float rot_deg, Lpr_Rgba_u8 color );
#endif

#if __cplusplus
LPR_DEF
lpr_s32
lpr_add_atlas_frame_px_perfect( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                                Lpr_Atlas const * atlas, Lpr_V2 pos,
                                lpr_u32 frame_id,
                                lpr_bool screen_aligned = lpr_true,
                                lpr_bool viewport_pxs = lpr_false,
                                Lpr_Rgba_u8 color = lpr_C_colors_u8.white );
#else
LPR_DEF
lpr_s32
lpr_add_atlas_frame_px_perfect( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                                Lpr_Atlas const * atlas, Lpr_V2 pos,
                                lpr_u32 frame_id, lpr_bool screen_aligned,
                                lpr_bool viewport_pxs, Lpr_Rgba_u8 color );
#endif

LPR_DEF
lpr_s32
#if __cplusplus
lpr_add_atlas_frame_tiled_rectangle( Lpr_Draw_Data * draw_data,
                                     lpr_u32 batch_id,
                                     Lpr_Atlas const * atlas,
                                     Lpr_V2 pos,
                                     Lpr_V2 scale,
                                     Lpr_V2 offset_percentage,
                                     Lpr_V2u32 overflow_policies,
                                     lpr_u32 frame_id,
                                     float rot_deg = 0.0f,
                                     Lpr_Rgba_u8 color = lpr_C_colors_u8.white );
#define lpr_add_atlas_frame_tiled_rectangle_w_scale lpr_add_atlas_frame_tiled_rectangle
#else
lpr_add_atlas_frame_tiled_rectangle_w_scale( Lpr_Draw_Data * draw_data,
                                             lpr_u32 batch_id,
                                             Lpr_Atlas const * atlas,
                                             Lpr_V2 pos,
                                             Lpr_V2 scale,
                                             Lpr_V2 offset_percentage,
                                             Lpr_V2u32 overflow_policies,
                                             lpr_u32 frame_id,
                                             float rot_deg,
                                             Lpr_Rgba_u8 color );
#endif

LPR_DEF
lpr_s32
#if __cplusplus
lpr_add_atlas_frame_tiled_rectangle( Lpr_Draw_Data * draw_data,
                                     lpr_u32 batch_id,
                                     Lpr_Atlas const * atlas,
                                     Lpr_V2 pos,
                                     Lpr_V2 size,
                                     Lpr_V2 uv_size,
                                     Lpr_V2 offset_percentage,
                                     Lpr_V2u32 overflow_policies,
                                     lpr_u32 frame_id,
                                     float rot_deg = 0.0f,
                                     Lpr_Rgba_u8 color = lpr_C_colors_u8.white );
#define lpr_add_atlas_frame_tiled_rectangle_w_size lpr_add_atlas_frame_tiled_rectangle
#else
lpr_add_atlas_frame_tiled_rectangle_w_size( Lpr_Draw_Data * draw_data,
                                            lpr_u32 batch_id,
                                            Lpr_Atlas const * atlas,
                                            Lpr_V2 pos,
                                            Lpr_V2 size,
                                            Lpr_V2 uv_size,
                                            Lpr_V2 offset_percentage,
                                            Lpr_V2u32 overflow_policies,
                                            lpr_u32 frame_id,
                                            float rot_deg,
                                            Lpr_Rgba_u8 color );
#endif

#if __cplusplus
LPR_DEF
lpr_s32
lpr_add_colored_rect( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                      Lpr_V2 pos, Lpr_V2 size,
                      Lpr_Rgba_u8 color = lpr_C_colors_u8.white,
                      float rot_deg = 0.0f );
#else
LPR_DEF
lpr_s32
lpr_add_colored_rect( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                      Lpr_V2 pos, Lpr_V2 size, Lpr_Rgba_u8 color,
                      float rot_deg );
#endif

// NOTE(theGiallo): texture cannot be NULL
#if __cplusplus
LPR_DEF
lpr_s32
lpr_add_rect_full_window( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                          Lpr_Texture * texture,
                          Lpr_Rgba_u8 color = lpr_C_colors_u8.white,
                          float rot_deg = 0.0f,
                          Lpr_V2 displacement_percentage = {0.0f,0.0f} );
#else
LPR_DEF
lpr_s32
lpr_add_rect_full_window( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                          Lpr_Texture * texture,
                          Lpr_Rgba_u8 color, float rot_deg,
                          Lpr_V2 displacement_percentage );
#endif

#if __cplusplus
LPR_DEF
lpr_s32
lpr_add_colored_rect_full_window( Lpr_Draw_Data * draw_data,
                                  lpr_u32 batch_id,
                                  Lpr_Rgba_u8 color = lpr_C_colors_u8.white,
                                  float rot_deg = 0.0f,
                                  Lpr_V2 displacement_percentage = {0.0f,0.0f} );
#else
LPR_DEF
lpr_s32
lpr_add_colored_rect_full_window( Lpr_Draw_Data * draw_data,
                                  lpr_u32 batch_id,
                                  Lpr_Rgba_u8 color, float rot_deg,
                                  Lpr_V2 displacement_percentage );
#endif

////////////////////////////////////////////////////////////////////////////////
// font API

typedef
enum
{
	LPR_TXT_HALIGN_LEFT,
	LPR_TXT_HALIGN_CENTERED,
	LPR_TXT_HALIGN_RIGHT,
} Lpr_Txt_HAlign;

typedef
enum
{
	LPR_TXT_VALIGN_TOP,
	LPR_TXT_VALIGN_MIDDLE,
	LPR_TXT_VALIGN_BOTTOM,
} Lpr_Txt_VAlign;


typedef
enum
{
	LPR_TXT_WRAP_NONE,
	LPR_TXT_WRAP_CHAR,
	LPR_TXT_WRAP_WORD,
} Lpr_Txt_Wrap;

#define LPR_MAX_FONT_GLYPHS (1024+512+128) // 1664 -> 2,768,896
#define LPR_FONT_HM_SIZE (LPR_MAX_FONT_GLYPHS*2)

typedef
struct
{
	lpr_s32 ascent, descent, line_gap;
} Lpr_Font_Metrics;

typedef
struct
{
	lpr_s32 advance_width, left_side_bearing;
} Lpr_Glyph_Metrics;

typedef
struct
{
	// NOTE(theGiallo): relative to atlas
	Lpr_V2u32 px_size;
	// NOTE(theGiallo): relative to current point, in px
	Lpr_V2 pos_offset_px;
	Lpr_V2 bl_uv;
	Lpr_V2 uv_size;
	float x_advance_px; // NOTE(theGiallo): advance current point of this
} Lpr_Glyph_In_Atlas;

typedef
struct
{
	// NOTE(theGiallo): hashed utf8 fullchars point into this ht, that contains
	// for glyphs_in_atlas
	lpr_s32            glyphs_indexes_ht[LPR_FONT_HM_SIZE];
	Lpr_Glyph_In_Atlas glyphs_in_atlas[LPR_MAX_FONT_GLYPHS];
	// NOTE(theGiallo): ids of font atlases into a MultiFont
	lpr_s32            atlases_of_glyphs[LPR_MAX_FONT_GLYPHS];
	// NOTE(theGiallo): oversampling is to obtain better subpixel positioning.
	// Read https://github.com/nothings/stb/tree/master/tests/oversample for
	// more infos.
	// NOTE(theGiallo): oversampled fonts are saved in the atlas bigger than
	// normal. E.g. a glyph 15px x 21px oversampled of (2,1) will be occupy an
	// area of 30px x 21px into the atlas.
	// NOTE(theGiallo): oversampling of 1 is normal sampling
	// NOTE(theGiallo): oversampling_scale is 1/oversampling
	Lpr_V2 oversampling_scale;
} Lpr_Font_Of_Size;

typedef
struct
{
	Lpr_Font_Metrics metrics;
	Lpr_Glyph_Metrics glyphs_metrics[LPR_FONT_HM_SIZE];
	// NOTE(theGiallo): kerning table is one per font,
	// has to be multiplied by the scale
	lpr_s32 kerning_table[LPR_MAX_FONT_GLYPHS][LPR_MAX_FONT_GLYPHS];

	Lpr_Font_Of_Size * of_size;
	// NOTE(theGiallo): we linearly search into these arrays
	// NOTE(theGiallo): these have are parallel to 'of_size'
	float * scales;
	float * px_heights;
	lpr_s32 sizes_count;
} Lpr_Font;

typedef
struct
{
	/**
	 * NOTE(theGiallo):
	 * glyph of font f1 of size s1 is in atlas a1
	 * glyph of font f2 of size s2 is in atlas a2
	 * glyph of font f2 of size s3 is in atlas a1
	 *  size is font scale / pixel height
	 **/

	Lpr_Texture * atlases;
	lpr_u32 atlases_count;

	Lpr_Font * fonts;
	lpr_u32 fonts_count;
} Lpr_Multi_Font;


LPR_DEF
lpr_u32
lpr_hash_fullchar( Lpr_UTF8_Fullchar fullchar );

LPR_DEF
lpr_s32
lpr_glyph_id_from_fullchar( Lpr_Font_Of_Size * of_size,
                            Lpr_UTF8_Fullchar fullchar );

LPR_DEF
float
lpr_kerned_line_length_px( Lpr_Multi_Font * multi_font, lpr_s32 font_id,
                           lpr_s32 size_id,
                           const char * utf8_string, lpr_u32 str_bytes_size );

LPR_DEF
float
lpr_word_length_px( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                    Lpr_Multi_Font * multi_font, lpr_s32 font_id,
                    lpr_s32 size_id,
                    const char * utf8_string, lpr_u32 str_bytes_size,
                    float scale,
                    lpr_bool px_perfect, lpr_bool viewport_pxs );

LPR_DEF
float
lpr_line_length_px_wrap( Lpr_Multi_Font * multi_font, lpr_s32 font_id,
                         lpr_s32 size_id,
                         const char * utf8_string, lpr_u32 str_bytes_size,
                         const Lpr_Txt_Wrap wrap, float max_px_width,
                         lpr_bool kerned,
                         lpr_u32 * num_chars,
                         const char ** next_line_ptr );

LPR_DEF
lpr_u32
lpr_lines_count( const char * utf8_string );

LPR_DEF
lpr_u32
lpr_lines_count_wrap( Lpr_Multi_Font * multi_font, lpr_s32 font_id,
                      lpr_s32 size_id,
                      const char * utf8_string, lpr_u32 str_bytes_size,
                      const Lpr_Txt_Wrap wrap, float max_px_width,
                      lpr_bool kerned );


LPR_DEF
lpr_s32
lpr_add_glyph( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
               Lpr_Multi_Font * multi_font, lpr_s32 font_id, lpr_s32 size_id,
               lpr_s32 glyph_id,
               Lpr_V2 curr_pt, Lpr_Rgba_u8 color, float scale,
               lpr_bool px_perfect, lpr_bool viewport_pxs, lpr_u32 stencil_id );

LPR_DEF
lpr_s32
lpr_add_rotated_glyph( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                       Lpr_Multi_Font * multi_font,
                       lpr_s32 font_id, lpr_s32 size_id,
                       lpr_s32 glyph_id,
                       Lpr_V2 curr_pt, Lpr_V2 right_dir, float glyph_rot_deg,
                       Lpr_Rgba_u8 color, float scale,
                       lpr_bool px_perfect, lpr_bool viewport_pxs,
                       lpr_u32 stencil_id );

#define LPR_CLIPPED_OUT -2
LPR_DEF
lpr_s32
lpr_add_rotated_glyph_clipped( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                               Lpr_Multi_Font * multi_font,
                               lpr_s32 font_id, lpr_s32 size_id,
                               lpr_s32 glyph_id,
                               Lpr_V2 curr_pt,
                               Lpr_V2 right_dir,
                               float glyph_rot_deg,
                               Lpr_Rgba_u8 color,
                               // NOTE(theGiallo): LPR_ZERO means no clipping
                               Lpr_AA_Rect clipping_rect,
                               float scale,
                               lpr_bool px_perfect,
                               lpr_bool viewport_pxs,
                               lpr_u32 stencil_id );

LPR_DEF
lpr_s32
lpr_add_utf8_char( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                   Lpr_Multi_Font * multi_font, lpr_s32 font_id,
                   lpr_s32 size_id,
                   Lpr_UTF8_Fullchar fullchar,
                   Lpr_V2 curr_pt, Lpr_Rgba_u8 color,
                   float scale, lpr_bool px_perfect, lpr_bool viewport_pxs,
                   lpr_u32 stencil_id,
                   lpr_s32 * glyph_id );

LPR_DEF
lpr_s32
lpr_add_kerned_glyph( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                      Lpr_Multi_Font * multi_font, lpr_s32 font_id,
                      lpr_s32 size_id, lpr_s32 glyph_id,
                      Lpr_V2 curr_pt, lpr_s32 prev_glyph_id,
                      Lpr_Rgba_u8 color, float scale,
                      lpr_bool px_perfect, lpr_bool viewport_pxs,
                      lpr_u32 stencil_id,
                      float * kerning );

LPR_DEF
lpr_s32
lpr_add_kerned_utf8_char( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                          Lpr_Multi_Font * multi_font, lpr_s32 font_id,
                          lpr_s32 size_id, Lpr_UTF8_Fullchar fullchar,
                          Lpr_V2 curr_pt,
                          lpr_s32 prev_glyph_id, lpr_s32 * glyph_id,
                          Lpr_Rgba_u8 color, float scale,
                          lpr_bool px_perfect, lpr_bool viewport_pxs,
                          lpr_u32 stencil_id,
                          float * kerning );

LPR_DEF
lpr_bool
lpr_add_multiline_text( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                        Lpr_Multi_Font * multi_font, lpr_s32 font_id,
                        lpr_s32 size_id,
                        Lpr_V2 starting_pt,
                        const char * utf8_string, lpr_u32 str_bytes_size,
                        Lpr_Rgba_u8 color,
                        const lpr_bool set_z, const lpr_u16 z,
                        float scale,
                        lpr_bool px_perfect, lpr_bool viewport_pxs,
                        lpr_u32 stencil_id,
                        lpr_s32 * glyph_id,
                        Lpr_AA_Rect * bounding_box, Lpr_V2 * ending_pt,
                        Lpr_Sprites_Span * sprites_span );

LPR_DEF
lpr_bool
lpr_add_multiline_rotated_text( Lpr_Draw_Data * draw_data,
                                lpr_u32 batch_id,
                                Lpr_Multi_Font * multi_font, lpr_s32 font_id,
                                lpr_s32 size_id,
                                Lpr_V2 starting_pt,
                                const char * utf8_string,
                                lpr_u32 str_bytes_size,
                                Lpr_Rgba_u8 color,
                                const lpr_bool set_z, const lpr_u16 z,
                                float scale, float rot_deg,
                                lpr_bool px_perfect, lpr_bool viewport_pxs,
                                lpr_bool camera_aligned,
                                lpr_u32 stencil_id,
                                lpr_s32 * glyph_id,
                                Lpr_Rot_Rect * bounding_box,
                                Lpr_V2 * ending_pt,
                                Lpr_Sprites_Span * sprites_span );

LPR_DEF
lpr_bool
lpr_add_multiline_text_in_rect( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                                Lpr_Multi_Font * multi_font, lpr_s32 font_id,
                                lpr_s32 size_id,
                                Lpr_AA_Rect rect,
                                const char * utf8_string,
                                lpr_u32 str_bytes_size,
                                Lpr_Rgba_u8 color,
                                const lpr_bool set_z, const lpr_u16 z,
                                float scale,
                                lpr_bool px_perfect, lpr_bool viewport_pxs,
                                lpr_u32 stencil_id,
                                lpr_s32 * glyph_id,
                                Lpr_AA_Rect * aa_bb, Lpr_V2 * ending_pt,
                                Lpr_Sprites_Span * sprites_span );

LPR_DEF
lpr_bool
lpr_add_multiline_text_in_rot_rect( Lpr_Draw_Data * draw_data,
                                    lpr_u32 batch_id,
                                    Lpr_Multi_Font * multi_font,
                                    lpr_s32 font_id,
                                    lpr_s32 size_id,
                                    Lpr_Rot_Rect rect,
                                    const char * utf8_string,
                                    lpr_u32 str_bytes_size,
                                    Lpr_Rgba_u8 color,
                                    const lpr_bool set_z, const lpr_u16 z,
                                    float scale,
                                    lpr_bool px_perfect,
                                    lpr_bool viewport_pxs,
                                    lpr_bool camera_aligned,
                                    lpr_u32 stencil_id,
                                    lpr_s32 * glyph_id,
                                    Lpr_Rot_Rect * bounding_box,
                                    Lpr_V2 * ending_pt,
                                    Lpr_Sprites_Span * sprites_span );

LPR_DEF
lpr_bool
lpr_add_multiline_text_in_rect_aligned( Lpr_Draw_Data * draw_data,
                                        lpr_u32 batch_id,
                                        Lpr_Multi_Font * multi_font,
                                        lpr_s32 font_id,
                                        lpr_s32 size_id,
                                        Lpr_AA_Rect rect,
                                        const char * utf8_string,
                                        lpr_u32 str_bytes_size,
                                        const Lpr_Txt_HAlign h_alignment,
                                        const Lpr_Txt_VAlign v_alignment,
                                        Lpr_Rgba_u8 color,
                                        const lpr_bool set_z, const lpr_u16 z,
                                        float scale, lpr_bool px_perfect,
                                        lpr_bool viewport_pxs,
                                        lpr_u32 stencil_id,
                                        lpr_s32 * glyph_id,
                                        Lpr_AA_Rect * aa_bb,
                                        Lpr_V2 * ending_pt,
                                        Lpr_Sprites_Span * sprites_span );

LPR_DEF
lpr_bool
lpr_add_multiline_text_in_rot_rect_aligned( Lpr_Draw_Data * draw_data,
                                            lpr_u32 batch_id,
                                            Lpr_Multi_Font * multi_font,
                                            lpr_s32 font_id,
                                            lpr_s32 size_id,
                                            Lpr_Rot_Rect rect,
                                            const char * utf8_string,
                                            lpr_u32 str_bytes_size,
                                            const Lpr_Txt_HAlign h_alignment,
                                            const Lpr_Txt_VAlign v_alignment,
                                            Lpr_Rgba_u8 color,
                                            const lpr_bool set_z,
                                            const lpr_u16 z,
                                            float scale, lpr_bool px_perfect,
                                            lpr_bool viewport_pxs,
                                            lpr_bool camera_aligned,
                                            lpr_u32 stencil_id,
                                            lpr_s32 * glyph_id,
                                            Lpr_Rot_Rect * bounding_box,
                                            Lpr_V2 * ending_pt,
                                            Lpr_Sprites_Span * sprites_span );

LPR_DEF
lpr_bool
lpr_add_multiline_text_in_rot_rect_aligned_wrap(
   Lpr_Draw_Data * draw_data,
   lpr_u32 batch_id,
   Lpr_Multi_Font * multi_font,
   lpr_s32 font_id,
   lpr_s32 size_id,
   Lpr_Rot_Rect rect,
   const char * utf8_string,
   lpr_u32 str_bytes_size,
   const Lpr_Txt_HAlign h_alignment,
   const Lpr_Txt_VAlign v_alignment,
   const Lpr_Txt_Wrap wrap,
   Lpr_Rgba_u8 color,
   const lpr_bool set_z,
   const lpr_u16 z,
   float scale,
   lpr_u32 stencil_id,
   lpr_bool px_perfect,
   lpr_bool viewport_pxs,
   lpr_bool camera_aligned,
   lpr_s32 * glyph_id,
   Lpr_Rot_Rect * bounding_box,
   Lpr_V2 * ending_pt,
   Lpr_Sprites_Span * sprites_span );

LPR_DEF
lpr_bool
lpr_add_multiline_text_in_rot_rect_aligned_wrap_clipped(
   Lpr_Draw_Data * draw_data,
   lpr_u32 batch_id,
   Lpr_Multi_Font * multi_font,
   lpr_s32 font_id,
   lpr_s32 size_id,
   Lpr_Rot_Rect rect,
   const char * utf8_string,
   lpr_u32 str_bytes_size,
   const Lpr_Txt_HAlign h_alignment,
   const Lpr_Txt_VAlign v_alignment,
   const Lpr_Txt_Wrap wrap,
   Lpr_Rgba_u8 color,
   // NOTE(theGiallo): LPR_ZERO means no clipping
   Lpr_AA_Rect clipping_rect,
   const lpr_bool set_z,
   const lpr_u16 z,
   float scale,
   lpr_u32 stencil_id,
   lpr_bool px_perfect,
   lpr_bool viewport_pxs,
   lpr_bool camera_aligned,
   lpr_s32 * glyph_id,
   Lpr_Rot_Rect * bounding_box,
   Lpr_V2 * ending_pt,
   Lpr_Sprites_Span * sprites_span );

LPR_DEF
lpr_bool
lpr_move_sprites( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                  Lpr_Sprites_Span sprites_span, Lpr_V2 displacement );

LPR_DEF
lpr_bool
lpr_change_color_of_sprites( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                             Lpr_Sprites_Span sprites_span, Lpr_Rgba_u8 color );

LPR_DEF
lpr_bool
lpr_change_color_and_move_sprites( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                                   Lpr_Sprites_Span sprites_span,
                                   Lpr_Rgba_u8 color, Lpr_V2 displacement );

// font API
///////////////////////////////////////////////////////////////////////////////

LPR_DEF
u8
lpr_new_stencil( Lpr_Draw_Data * draw_data, lpr_u32 batch_id );

LPR_DEF
void
lpr_stencil_clear_all( const Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                       bool value );

LPR_DEF
void
lpr_stencil_clear( const Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                   u8 stencil_id, bool value );

LPR_DEF
void
lpr_stencil_draw_aa_rect(
   const Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
   u8 target_stencil, Lpr_AA_Rect aa_rect, Lpr_Stencil_Draw_Mode draw_mode,
   u8 applied_stencil
#if __cplusplus
   = LPR_STENCIL_NONE
#endif
   );

LPR_DEF
void
lpr_stencil_draw_rot_rect(
   const Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
   u8 target_stencil, Lpr_Rot_Rect rot_rect,
   Lpr_Stencil_Draw_Mode draw_mode
#if __cplusplus
   = LPR_STENCIL_DRAW_MODE_SET_ONE
#endif
   , u8 applied_stencil
#if __cplusplus
   = LPR_STENCIL_NONE
#endif
   , bool camera_aligned
#if __cplusplus
   = false
#endif
   );

#if __cplusplus
LPR_DEF
void
lpr_stencil_draw_sprite(
   const Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
   u8 target_stencil,
   Lpr_Texture const * texture, Lpr_Rot_Rect rot_rect,
   Lpr_Stencil_Draw_Mode draw_mode_logic = LPR_STENCIL_DRAW_MODE_SET_ONE,
   Lpr_Stencil_Draw_Mode draw_mode_tex   = LPR_STENCIL_DRAW_MODE_TX_ALPHA_NOT_ZERO,
   u8 applied_stencil = LPR_STENCIL_NONE, bool camera_aligned = false
   );
#define lpr_stencil_draw_sprite_w_size lpr_stencil_draw_sprite
#else
LPR_DEF
void
lpr_stencil_draw_sprite_w_size(
   const Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
   u8 target_stencil,
   Lpr_Texture const * texture, Lpr_Rot_Rect rot_rect,
   Lpr_Stencil_Draw_Mode draw_mode_logic,
   Lpr_Stencil_Draw_Mode draw_mode_tex,
   u8 applied_stencil, bool camera_aligned
   );
#endif

#if __cplusplus
LPR_DEF
void
lpr_stencil_draw_sprite(
   const Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
   u8 target_stencil,
   Lpr_Texture const * texture, Lpr_V2 pos,
   float scale = 1.0f, float rot_deg = 0.0f,
   Lpr_Stencil_Draw_Mode draw_mode = LPR_STENCIL_DRAW_MODE_SET_ONE,
   Lpr_Stencil_Draw_Mode draw_mode_logic = LPR_STENCIL_DRAW_MODE_SET_ONE,
   Lpr_Stencil_Draw_Mode draw_mode_tex   = LPR_STENCIL_DRAW_MODE_TX_ALPHA_NOT_ZERO,
   u8 applied_stencil = LPR_STENCIL_NONE, bool camera_aligned = false
   );
#define lpr_stencil_draw_sprite_w_scale lpr_stencil_draw_sprite
#else
LPR_DEF
void
lpr_stencil_draw_sprite_w_scale(
   const Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
   u8 target_stencil,
   Lpr_Texture const * texture, Lpr_V2 pos,
   float scale, float rot_deg,
   Lpr_Stencil_Draw_Mode draw_mode_logic,
   Lpr_Stencil_Draw_Mode draw_mode_tex,
   u8 applied_stencil, bool camera_aligned
   );
#endif

// NOTE(theGiallo): instance_id is the value returned by a add* function
LPR_DEF
void
lpr_set_instance_stencil_id( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                             lpr_u16 instance_id, lpr_u8 stencil_id );

// NOTE(theGiallo): instance_id is the value returned by a add* function
LPR_DEF
void
lpr_set_instance_z( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                    lpr_u16 instance_id, lpr_u16 z );

// NOTE(theGiallo): instance_id is the value returned by a add* function
LPR_DEF
void
lpr_set_instance_blend_mode( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                             lpr_u16 instance_id, Lpr_Blend_Mode blend_mode );

#if __cplusplus
inline
void
lpr_reset_batch( Lpr_Batch_Data * batch )
{
	batch->quads_count    = 0;
	batch->textures_count = 0;
	batch->stencils_count = 0;
	batch->has_many_blend_modes = lpr_false;
}
#else
extern
inline
void
lpr_reset_batch( Lpr_Batch_Data * batch );
#endif

LPR_DEF
void
lpr_reset_all_batches( Lpr_Draw_Data * draw_data );

LPR_DEF
void
lpr_set_batch_camera( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                      Lpr_V2 camera_pos, float camera_angle,
                      Lpr_V2 vanishing_point, Lpr_V2 camera_world_span );

LPR_DEF
void
lpr_clear_framebuffer_zero( const Lpr_Draw_Data * draw_data,
                            const Lpr_Rgba_f32 * clear_color );

// NOTE(theGiallo): binds the linear framebuffer or the default one, and sets
// the viewport to the whole framebuffer
LPR_DEF
void
lpr_set_draw_target_default_full( Lpr_Draw_Data * draw_data );

// NOTE(theGiallo): binds the linear framebuffer or the default one, and sets
// the viewport as given
LPR_DEF
void
lpr_set_draw_target_default( Lpr_Draw_Data * draw_data,
                             Lpr_V2u32 bl, Lpr_V2u32 size );

// NOTE(theGiallo): to be called before all the lpr_draw_batch
LPR_DEF
void
lpr_prep_batch_drawing( const Lpr_Draw_Data * draw_data );

LPR_DEF
void
lpr_draw_batch( const Lpr_Draw_Data * draw_data, lpr_u32 batch_id );

// NOTE(theGiallo): calls lpr_draw_batch for all the batches in draw_data
LPR_DEF
void
lpr_draw_all_batches( const Lpr_Draw_Data * draw_data );

// NOTE(theGiallo): calls lpr_draw_batch for all the batches in draw_data
// after having sorted them
LPR_DEF
void
lpr_draw_all_batches_sorted( Lpr_Draw_Data * draw_data );

// NOTE(theGiallo): if !framebuffer_is_sRGB it performs sRGB conversion
LPR_DEF
void
lpr_finalize_draw( const Lpr_Draw_Data * draw_data );

LPR_DEF
lpr_bool
lpr_resize( Lpr_Draw_Data * draw_data, Lpr_V2u32 size );

LPR_DEF
lpr_bool
lpr_create_stencils_fbs( Lpr_Draw_Data * draw_data, lpr_u32 batch_id );

LPR_DEF
lpr_bool
lpr_create_linear_fb(  Lpr_Draw_Data * draw_data );

LPR_DEF
GLuint
lpr_create_quad_vb();

// NOTE(theGiallo): total_size is in Bytes, data is the buffer that will contain
// the per instance data
GLuint
_lpr_create_per_instance_vb( GLsizei total_size, void * data );
#define lpr_create_per_instance_vb( data ) _lpr_create_per_instance_vb( sizeof( data ), ( data ) )

#if __cplusplus
LPR_DEF
lpr_bool
lpr_create_program( GLuint * gl_program,
                    const char * vertex_shader_file_path,
                    const char * fragment_shader_file_path,
                    void * alloc_context = NULL );
#define lpr_create_program_from_files lpr_create_program
#else
LPR_DEF
lpr_bool
lpr_create_program_from_files( GLuint * gl_program,
                               const char * vertex_shader_file_path,
                               const char * fragment_shader_file_path,
                               void * alloc_context );
#endif

#if __cplusplus
LPR_DEF
lpr_bool
lpr_create_program( GLuint *     gl_program,
                    const char * vertex_shader_text,
                    GLint        vertex_shader_text_length,
                    const char * fragment_shader_text,
                    GLint        fragment_shader_text_length );
#define lpr_create_program_from_text lpr_create_program
#else
LPR_DEF
lpr_bool
lpr_create_program_from_text( GLuint *     gl_program,
                              const char * vertex_shader_text,
                              GLint        vertex_shader_text_length,
                              const char * fragment_shader_text,
                              GLint        fragment_shader_text_length );
#endif


/*
	NOTE(theGiallo):
	No stride is supported. Size of raw pointed memory must be
	lpr_C_img_fmt_bytes_per_pixel[format] * size.w * size.h
	raw_is_sRGB determines the OpenGL encoding.
	If !carthesian (origin left-top) the image will be flipped up-down, with
	origin left-bottom. IMPORTANT the original one it will be flipped!
	Size will be saved into texture.width and texture.height.
	A OpenGL texture will be created and texture.gl_texture will hold its handle.
 */
LPR_DEF
lpr_bool
lpr_create_texture( void * raw, Lpr_V2u32 size, Lpr_Img_Fmt format,
                    lpr_bool raw_is_sRGB, lpr_bool carthesian,
                    lpr_bool has_premultiplied_alpha,
                    Lpr_Texture * texture,
                    Lpr_Texture_Filter min_filter,
                    Lpr_Texture_Filter mag_filter,
                    Lpr_Wrap wrap_x,
                    Lpr_Wrap wrap_y,
                    Lpr_Scale_Filter mipmap_filter,
                    Lpr_V2 atlas_size_in_frames,
                    void * alloc_context
                     );

LPR_DEF
void
lpr_flip_RGBA8_image_vertically( void * raw, Lpr_V2u32 size,
                                 lpr_bool premultiply_alpha,
                                 lpr_bool is_sRGB );

LPR_DEF
void
lpr_flip_RGB8_image_vertically( void * raw, Lpr_V2u32 size );

LPR_DEF
void
lpr_flip_GRAYSCALE8_image_vertically( void * raw, Lpr_V2u32 size );

LPR_DEF
void
lpr_premultiply_alpha_of_RGBA8_image( void * raw, Lpr_V2u32 size,
                                      lpr_bool is_sRGB );

// NOTE(theGiallo): texture locs must be large enough to contain LPR_MAX_TEXTURES
LPR_DEF
lpr_bool
lpr_get_uniform_texture_array_location( GLuint gl_program,
                                        const char * sampler_name,
                                        lpr_u32 count,
                                        GLuint * textures_locs,
                                        GLuint * textures_count );

#if __cplusplus
LPR_DEF
lpr_bool
lpr_init_draw_data( Lpr_Draw_Data * draw_data, int batches_count = 0,
                    void * alloc_context = NULL );
#else
LPR_DEF
lpr_bool
lpr_init_draw_data( Lpr_Draw_Data * draw_data, int batches_count,
                    void * alloc_context );
#endif

#if __cplusplus
LPR_DEF
lpr_bool
lpr_create_batches( Lpr_Draw_Data * draw_data, int batches_count,
                    void * alloc_context = NULL );
#else
LPR_DEF
lpr_bool
lpr_create_batches( Lpr_Draw_Data * draw_data, int batches_count,
                    void * alloc_context );
#endif

LPR_DEF
void
lpr_create_stencils_fbs_for_batches( Lpr_Draw_Data * draw_data );

////////////////////////////////////////////////////////////////////////////////
// misc

// NOTE(theGiallo): reorders the data array as data[i] = data[ids[i]] i=0..count-1
LPR_DEF
void
lpr_reorder_16_s( lpr_u16 * ids, void * data, lpr_u32 count,
                  lpr_u32 elem_size );
LPR_DEF
void
lpr_reorder_16_8( lpr_u16 * ids, lpr_u8 * data, lpr_u32 count );
LPR_DEF
void
lpr_reorder_16( lpr_u16 * ids, lpr_u16 * data, lpr_u32 count );
LPR_DEF
void
lpr_reorder_32( lpr_u32 * ids, lpr_u32 * data, lpr_u32 count );

/* NOTE(theGiallo): sorts data according to keys with ascending order
   tmp has to be at least of count size. It will be written many times.
   At the end it will contain no useful data. It's allocation is on the user
   because it's performance critical.
   IMPORTANT keys are not sorted! You can sort then with lpr_reorder_*.
   'mc' stands for many-count. This is because the counting of the radices is
   done in only one pass, so many are counted at once.

   This is the fastest sorting algorithm lepre has!
*/
LPR_DEF
void
lpr_radix_sort_mc_16( lpr_u16 * keys, lpr_u16 * data, lpr_u16 * tmp,
                      lpr_u32 count );
LPR_DEF
void
lpr_radix_sort_mc_32( lpr_u32 * keys, lpr_u32 * data, lpr_u32 * tmp,
                      lpr_u32 count );

LPR_DEF
void
lpr_radix_sort_mc_32_16( lpr_u32 * keys, lpr_u16 * ids, lpr_u16 * tmp,
                         lpr_u32 count );

LPR_DEF
void
lpr_radix_sort_mc_32_keys_only( lpr_u32 * keys, lpr_u32 * tmp, lpr_u32 count );

#ifndef LPR_INSERTION_SORT_16_IS_BETTER_MAX_COUNT
	#define LPR_INSERTION_SORT_16_IS_BETTER_MAX_COUNT 84
#endif
#ifndef LPR_INSERTION_SORT_32_IS_BETTER_MAX_COUNT
	#define LPR_INSERTION_SORT_32_IS_BETTER_MAX_COUNT 84
#endif

// NOTE(theGiallo): insertion sort is the fastest only with very small count.
// On my machine it's ~80. Implace radix sort is already optimized to use it with.
// This is regulated by LPR_INSERTION_SORT_*_IS_BETTER_MAX_COUNT
LPR_DEF
void
lpr_insertion_sort_16( lpr_u16 * keys, lpr_u16 * data, lpr_u32 count );
LPR_DEF
void
lpr_insertion_sort_32( lpr_u32 * keys, lpr_u32 * data, lpr_u32 count );

LPR_DEF
void
lpr_radix_sort_bucket_32( lpr_u32 * keys, lpr_u32 * data, lpr_u32 count,
                          lpr_u8 shift );

LPR_DEF
void
lpr_radix_sort_32( lpr_u32 * keys, lpr_u32 * data, lpr_u32 count );

LPR_DEF
void
lpr_radix_sort_bucket_16( lpr_u16 * keys, lpr_u16 * data, lpr_u32 count,
                          lpr_u8 shift );

LPR_DEF
void
lpr_radix_sort_16( lpr_u16 * keys, lpr_u16 * data, lpr_u32 count );

#ifndef LPR_MAX_STACK_QSIT
#define LPR_MAX_STACK_QSIT 64
#endif

LPR_DEF
void
lpr_quicksort_iterative_32( lpr_u32 * keys, lpr_u32 * data, lpr_u32 count );

LPR_DEF
void
lpr_quicksort_iterative_16( lpr_u16 * keys, lpr_u16 * data, lpr_u32 count );

// NOTE(theGiallo): the state (seeds) must be seeded so that it is not
// everywhere zero
LPR_DEF
lpr_u64
lpr_xorshift128plus( lpr_u64 seeds[2] );

LPR_DEF
lpr_u8
lpr_u8_hash_32( lpr_s32 v );

LPR_DEF
lpr_u32
lpr_u32_hash_32( lpr_s32 v );

// misc
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// file I/O

#define LPR_EXPAND_AS_ENUM_3(a,b,c) a = b,
#define LPR_EXPAND_AS_STRING_TABLE_3(a,b,c) c,
#define LPR_IO_ERRORS_TABLE(ENTRY) \
	ENTRY(LPR_IO_ZERO_READ,         0x0, "No error. File empty or not existing") \
	ENTRY(LPR_IO_OUT_MEM_TOO_SHORT, 0x1, "Trying to read a file larger than the out memory.") \
	ENTRY(LPR_IO_CANT_OPEN_FILE,    0x2, "Cannot open the file.") \
	ENTRY(LPR_IO_CANT_MALLOC,       0x3, "A dynamic allocation failed.") \
	ENTRY(LPR_IO_READING_ERROR,     0x4, "En error occurred reading the file.") \

enum Lpr_File_Sys_Error
{
	LPR_IO_ERRORS_TABLE(LPR_EXPAND_AS_ENUM_3)
	//---
	lpr_File_Sys_Errors_count
};
// TODO(theGiallo, 2015/12/03): trying not to use global variables. Hmm...
extern const char * Lpr_File_Sys_Error_strings[lpr_File_Sys_Errors_count];

#if __cplusplus
inline
lpr_bool
lpr_is_IO_OK( lpr_s64 res )
{
	return res > 0;
}
#else
extern
inline
lpr_bool
lpr_is_IO_OK( lpr_s64 res );
#endif

/*
   NOTE:
     *out_mem can be NULL. If it is it will contain the result of a lpr_calloc.
     So it has to be freed with 'lpr_free(out_mem);'.
     If the file is too large for out_mem_size OUT_MEM_TOO_SHORT is returned.

     Returns #Bytes read or error. Errors are negative.
     You can use lpr_is_IO_OK( res ) to check if there was an error.
     To use the enum Lpr_File_Sys_Error you have to negate them or the error because
     those enums are positive.
     To get the error string Lpr_File_Sys_Error_strings[-read].
 */
#if __cplusplus
LPR_DEF
lpr_s64
lpr_read_binary_file( const char * file_path, void ** out_mem, int out_mem_size,
                      void * alloc_context = NULL );
#else
LPR_DEF
lpr_s64
lpr_read_binary_file( const char * file_path, void ** out_mem, int out_mem_size,
                      void * alloc_context );
#endif

// file I/O
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// UTF-8

// NOTE(theGiallo): a subchar is a lpr_u8

// NOTE(theGiallo):  IMPORTANT: n is repeated into the macro!
#define LPR_UTF8_COMPARE_LEFT_BYTES(a,b,n) ( (a) >> (n) == (b) >> (n) )

// NOTE(theGiallo): the mask determines the length of the full character
const lpr_u8 lpr_C_utf8_key_mask_1st_byte[4] = { (lpr_u8)0x0,
                                                 (lpr_u8)0xC0,
                                                 (lpr_u8)0xE0,
                                                 (lpr_u8)0xF0 };
const lpr_u8 lpr_C_utf8_key_mask_1st_byte_left_bits_count[4] = { 7, 5, 4, 3 };
const lpr_u8 lpr_C_utf8_bits_mask_1st_byte[4] = { (lpr_u8)0x7F,
                                                  (lpr_u8)0x1F,
                                                  (lpr_u8)0xF,
                                                  (lpr_u8)0x7 };
const lpr_u8 lpr_C_utf8_bits_mask_following_bytes = 0x3F;
// NOTE(theGiallo): bytes after the 1st have this mask on
const lpr_u8 lpr_C_utf8_key_mask_following_bytes = 0x80;
const lpr_u8 lpr_C_utf8_key_mask_following_left_bits_count = 6;

/*
   NOTE(theGiallo):
     returns the pointer into the given UTF-8 string to the character next to
     the given one.
     IMPORTANT: assumes the given one is a valid UTF-8 char position.
     IMPORTANT: does not check if inside_a_string is really inside.

     Returns NULL on not valid inside_a_string (not a valid 1st byte) or if
     inside_a_string character ends after buffer end.
     Returns base_string + bytes_size if inside_a_string character was the last
     one.
 */
LPR_DEF
lpr_u8 *
lpr_utf8_next_char_ptr( const lpr_u8 * const inside_a_string,
                        const lpr_u8 * const base_string, lpr_u64 bytes_size );

/*
   NOTE(theGiallo):
   returns the pointer into the given UTF-8 string to the character next to the
   given one.
   Do not assumes the given one is a valid UTF-8 char position.
   inside_a_string can be inside a full character.
   IMPORTANT: does not check if inside_a_string is really inside.
   IMPORTANT: does not check if the returned pointed subchar is a valid 1st.
 */
LPR_DEF
lpr_u8 *
lpr_utf8_first_next_char_ptr( const lpr_u8 * inside_a_string,
                              const lpr_u8 * base_string, lpr_u64 bytes_size );

/*
   NOTE(theGiallo):
   size is the byte size of the string buffer.
   Returns the number of full characters of the string.
 */
LPR_DEF
lpr_s32
#if __cplusplus
lpr_utf8_char_count( const lpr_u8 * base_string, lpr_u64 bytes_size, const lpr_bool null_terminated = lpr_false );
#else
lpr_utf8_char_count( const lpr_u8 * base_string, lpr_u64 bytes_size, const lpr_bool null_terminated );
#endif

LPR_DEF
Lpr_UTF8_Fullchar
lpr_utf8_full_char( const lpr_u8 * first );

LPR_DEF
void
#if __cplusplus
lpr_utf8_print_char( const Lpr_UTF8_Fullchar fullchar, FILE * out = stdout );
#else
lpr_utf8_print_char( const Lpr_UTF8_Fullchar fullchar, FILE * out );
#endif

LPR_DEF
lpr_bool
lpr_utf8_is_1st_subchar( lpr_u8 subchar );

LPR_DEF
lpr_bool
lpr_utf8_is_mb_subchar( lpr_u8 subchar );

LPR_DEF
lpr_u32
lpr_utf8_fullchar_to_ucs4( Lpr_UTF8_Fullchar fullchar );

// UTF-8
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// OpenGL debug
LPR_DEF
char *
lpr_convert_internal_format_to_string( GLenum format, char * buf, size_t buf_size );
LPR_DEF
char *
lpr_get_texture_parameters( GLuint id, char * buf, size_t buf_size );
LPR_DEF
char *
lpr_get_renderbuffer_parameters( GLuint id, char * buf, size_t buf_size );
LPR_DEF
void
lpr_log_framebuffer_info();

#if __cplusplus
inline
lpr_bool
lpr_check_and_log_framebuffer_status()
{
	// NOTE(theGiallo): check FBO status
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	switch(status)
	{
		case GL_FRAMEBUFFER_COMPLETE:
			lpr_log_info( "Framebuffer complete." );
			return lpr_true;

		case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
			lpr_log_err( "Framebuffer incomplete: Attachment is NOT complete." );
			return lpr_false;

		case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
			lpr_log_err( "Framebuffer incomplete: No image is attached to FBO." );
			return lpr_false;
		#if 0
		case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
			lpr_log_err( "Framebuffer incomplete: Attached images have different dimensions." );
			return lpr_false;

		case GL_FRAMEBUFFER_INCOMPLETE_FORMATS:
			lpr_log_err( "Framebuffer incomplete: Color attached images have different internal formats." );
			return lpr_false;
		#endif
		case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
			lpr_log_err( "Framebuffer incomplete: Draw buffer." );
			return lpr_false;

		case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
			lpr_log_err( "Framebuffer incomplete: Read buffer." );
			return lpr_false;

		case GL_FRAMEBUFFER_UNSUPPORTED:
			lpr_log_err( "Framebuffer incomplete: Unsupported by FBO implementation." );
			return lpr_false;

		default:
			lpr_log_err( "Framebuffer incomplete: Unknown error." );
			return lpr_false;
	}
}
#else
extern
inline
lpr_bool
lpr_check_and_log_framebuffer_status();
#endif

// OpenGL debug
////////////////////////////////////////////////////////////////////////////////

// NOTE(theGiallo): LPR_HEADER end
#endif
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
#if LPR_IMPLEMENTATION==1 && !defined(_LPR_IMPLEMENTATION)
#define _LPR_IMPLEMENTATION

// NOTE(theGiallo): static variables
Lpr_Rgba_Colors_f32 lpr_C_colors_f32;
Lpr_Rgba_Colors_f32 lpr_C_colors_f32_linear;

const lpr_u8 lpr_C_img_fmt_bytes_per_pixel[LPR_IMG_FMT_COUNT] = {
LPR_IMG_FMT_TABLE(LPR_EXPAND_AS_SIZE_TABLE_IMG_FMT) };

const char * lpr_C_img_fmt_strings[LPR_IMG_FMT_COUNT] = {
LPR_IMG_FMT_TABLE(LPR_EXPAND_AS_STRING_TABLE_IMG_FMT) };

const lpr_u32 lpr_C_texture_filter_gltexparameter[LPR_TEXTURE_FILTER_COUNT] = {
LPR_TEXTURE_FILTER_TABLE(LPR_EXPAND_AS_GLTEXPARAMETER_TABLE_TEXTURE_FILTER) };

const lpr_u32 lpr_C_wrap_gltexparameter[LPR_WRAP_COUNT] = {
LPR_WRAP_TABLE(LPR_EXPAND_AS_GLTEXPARAMETER_TABLE_WRAP) };

const char * lpr_C_stencil_draw_modes_names[LPR_STENCIL_DRAW_MODES_COUNT] = {
LPR_STENCIL_DRAW_MODES(LPR_EXPAND_AS_STRING_TABLE_STENCIL_DRAW_MODE) };

// NOTE(theGiallo): C99 inline functions
#if !__cplusplus
inline
Lpr_V2
lpr_V2_from_V2u32( Lpr_V2u32 v )
{
	Lpr_V2 ret = { (float)v.x, (float)v.y };

	return ret;
}

inline
float
lpr_V2_sqlength( Lpr_V2 v )
{
	float ret = v.x*v.x + v.y*v.y;
	return ret;
}

inline
float
lpr_V2_length( Lpr_V2 v )
{
	float ret = lpr_sqrt( v.x*v.x + v.y*v.y );
	return ret;
}

inline
Lpr_V2
lpr_V2_abs( Lpr_V2 v0 )
{
	Lpr_V2 ret = { lpr_abs( v0.x ), lpr_abs( v0.y ) };
	return ret;
}

inline
Lpr_V2
lpr_V2_add_V2( Lpr_V2 v0, Lpr_V2 v1 )
{
	Lpr_V2 ret = { v0.x + v1.x, v0.y + v1.y };
	return ret;
}

inline
Lpr_V2
lpr_V2_sub_V2( Lpr_V2 v0, Lpr_V2 v1 )
{
	Lpr_V2 ret = { v0.x - v1.x, v0.y - v1.y };
	return ret;
}

inline
Lpr_V2
lpr_V2_div_f32( Lpr_V2 v0, float f )
{
	Lpr_V2 ret = { v0.x / f, v0.y / f };
	return ret;
}

inline
Lpr_V2
lpr_V2_mult_f32( Lpr_V2 v0, float f )
{
	Lpr_V2 ret = { v0.x * f, v0.y * f };
	return ret;
}

inline
Lpr_V2u32
lpr_V2u32_add_V2u32( Lpr_V2u32 v0, Lpr_V2u32 v1 )
{
	Lpr_V2u32 ret = { v0.x + v1.x, v0.y + v1.y };
	return ret;
}

inline
Lpr_V2u32
lpr_V2u32_sub_V2u32( Lpr_V2u32 v0, Lpr_V2u32 v1 )
{
	Lpr_V2u32 ret = { v0.x - v1.x, v0.y - v1.y };
	return ret;
}

inline
Lpr_Mx3
lpr_Mx3_make_identity()
{
	Lpr_Mx3 ret = LPR_ZERO_STRUCT;
	ret.array[0*3 + 0] = 1.0;
	ret.array[1*3 + 1] = 1.0;
	ret.array[2*3 + 2] = 1.0;

	return ret;
}

inline
Lpr_Rgb_u8
lpr_Rgb_u8_from_f32( Lpr_Rgb_f32 color )
{
	Lpr_Rgb_u8 ret;
	for ( int i = 0; i != 3; ++i )
	{
		ret.rgb[i] = (lpr_u8)lpr_round( color.rgb[i] * 255.0f );
	}
	return ret;
}
inline
Lpr_Rgba_u8
lpr_Rgba_u8_from_f32( Lpr_Rgba_f32 color )
{
	Lpr_Rgba_u8 ret;
	for ( int i = 0; i != 4; ++i )
	{
		ret.rgba[i] = (lpr_u8)lpr_round( color.rgba[i] * 255.0f );
	}
	return ret;
}
inline
Lpr_Rgb_f32
lpr_Rgb_f32_from_u8( Lpr_Rgb_u8 color )
{
	Lpr_Rgb_f32 ret;
	for ( int i = 0; i != 3; ++i )
	{
		ret.rgb[i] = color.rgb[i] / 255.0f;
	}
	return ret;
}
inline
Lpr_Rgba_f32
lpr_Rgba_f32_from_u8( Lpr_Rgba_u8 color )
{
	Lpr_Rgba_f32 ret;
	for ( int i = 0; i != 4; ++i )
	{
		ret.rgba[i] = color.rgba[i] / 255.0f;
	}
	return ret;
}
inline
Lpr_Rgb_f32
lpr_Rgb_f32_sRGB_from_linear( Lpr_Rgb_f32 color )
{
	Lpr_Rgb_f32 ret;
	for ( int i = 0; i != 3; ++i )
	{
		ret.rgb[i] = lpr_pow( color.rgb[i], 1.0f / 2.2f );
	}
	return ret;
}
inline
Lpr_Rgba_f32
lpr_Rgba_f32_sRGB_from_linear( Lpr_Rgba_f32 color )
{
	Lpr_Rgba_f32 ret;
	ret.lpr_rgb_f32 = lpr_Rgb_f32_sRGB_from_linear( color.lpr_rgb_f32 );
	ret.a = color.a;
	return ret;
}
inline
Lpr_Rgb_f32
lpr_Rgb_f32_linear_from_sRGB( Lpr_Rgb_f32 color )
{
	Lpr_Rgb_f32 ret;
	for ( int i = 0; i != 3; ++i )
	{
		ret.rgb[i] = lpr_pow( color.rgb[i], 2.2f );
	}
	return ret;
}
inline
Lpr_Rgba_f32
lpr_Rgba_f32_linear_from_sRGB( Lpr_Rgba_f32 color )
{
	Lpr_Rgba_f32 ret;
	ret.lpr_rgb_f32 = lpr_Rgb_f32_linear_from_sRGB( color.lpr_rgb_f32 );
	ret.a = color.a;
	return ret;
}
inline
void
lpr_reset_batch( Lpr_Batch_Data * batch )
{
	batch->quads_count    = 0;
	batch->textures_count = 0;
	batch->stencils_count = 0;
	batch->has_many_blend_modes = lpr_false;
}
inline
lpr_bool
lpr_is_IO_OK( lpr_s64 res )
{
	return res > 0;
}
inline
lpr_bool
lpr_check_and_log_framebuffer_status()
{
	// NOTE(theGiallo): check FBO status
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	switch(status)
	{
		case GL_FRAMEBUFFER_COMPLETE:
			lpr_log_info( "Framebuffer complete." );
			return lpr_true;

		case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
			lpr_log_err( "Framebuffer incomplete: Attachment is NOT complete." );
			return lpr_false;

		case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
			lpr_log_err( "Framebuffer incomplete: No image is attached to FBO." );
			return lpr_false;
		#if 0
		case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
			lpr_log_err( "Framebuffer incomplete: Attached images have different dimensions." );
			return lpr_false;

		case GL_FRAMEBUFFER_INCOMPLETE_FORMATS:
			lpr_log_err( "Framebuffer incomplete: Color attached images have different internal formats." );
			return lpr_false;
		#endif
		case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
			lpr_log_err( "Framebuffer incomplete: Draw buffer." );
			return lpr_false;

		case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
			lpr_log_err( "Framebuffer incomplete: Read buffer." );
			return lpr_false;

		case GL_FRAMEBUFFER_UNSUPPORTED:
			lpr_log_err( "Framebuffer incomplete: Unsupported by FBO implementation." );
			return lpr_false;

		default:
			lpr_log_err( "Framebuffer incomplete: Unknown error." );
			return lpr_false;
	}
}
inline
float
lpr_deg2rad( float angle )
{
	return angle * LPR_PI / 180.0f;
}
inline
float
lpr_rad2deg( float angle )
{
	return angle * 180.0f / LPR_PI;
}
inline
float
lpr_clamp_angle_deg( float angle )
{
	int A = angle / 360 ;
	A *= 360;
	angle = angle - A;
	if ( angle < 0 ) angle += 360;

	return angle;
}
inline
Lpr_AA_Rect
lpr_AA_Rect_from_minmax( Lpr_V2 min, Lpr_V2 max )
{
	Lpr_AA_Rect ret;
	ret.pos  = lpr_V2_mult_f32( lpr_V2_add_V2( min, max ), 0.5f );
	ret.size = lpr_V2_sub_V2( max, min );
	return ret;
}
inline
float
lpr_signf( float a )
{
	return ( ( a ) > 0 ) - ( ( a ) < 0 );
}
inline
float
lpr_smoothstep( float v0, float v1, float a )
{
	a = lpr_clamp( (a - v0) / (v1 - v0), 0.0f, 1.0f );
	return a*a * ( 3.0f - 2.0f * a );
}
inline
float
lpr_smootherstep( float v0, float v1, float a )
{
	a = lpr_clamp( (a - v0) / (v1 - v0), 0.0f, 1.0f );
	return a*a*a*(a*(a*6.0f - 15.0f) + 10.0f);
}
#endif

LPR_DEF
Lpr_V2
lpr_V2( float x, float y)
{
	Lpr_V2 ret = {{x, y}};
	return ret;
}

LPR_DEF
Lpr_V2u32
lpr_V2u32( lpr_u32 x, lpr_u32 y)
{
	Lpr_V2u32 ret = {{x, y}};
	return ret;
}

LPR_DEF
Lpr_V2
lpr_V2_rotated_deg( Lpr_V2 v, float a )
{
	float r_a = lpr_deg2rad( a );
	float s = lpr_sin( r_a );
	float c = lpr_cos( r_a );
	float x = v.x*c - v.y*s;
	float y = v.x*s + v.y*c;
	return lpr_V2( x, y );
}

LPR_DEF
GLfloat
lpr_Mx3_get( Lpr_Mx3 const * mx3, int row, int col )
{
	GLfloat ret = mx3->array[row * 3 + col];
	return ret;
}

LPR_DEF
void
lpr_Mx3_set( Lpr_Mx3 * mx3, int row, int col, GLfloat value )
{
	mx3->array[row * 3 + col] = value;
}

LPR_DEF
void
lpr_Mx3_orthographic( Lpr_Mx3 * mx3, Lpr_V2 vanishing_point, Lpr_V2 size )
{
	float left   = -vanishing_point.x    * size.w;
	float right  = (1.0f-vanishing_point.x) * size.w;
	float bottom = -vanishing_point.y    * size.h;
	float top    = (1.0f-vanishing_point.y) * size.h;

	float l = left,
	      r = right,
	      t = top,
	      b = bottom;

	/**
	 * NOTE(theGiallo): http://www.songho.ca/opengl/gl_projectionmatrix.html
	 **/
	Lpr_Mx3
	res = {{ 2.0f/size.w, 0.0f,       -(r+l)/size.w,
	         0.0f,       2.0f/size.h, -(t+b)/size.h,
	         0.0f,       0.0f,       1.0f          }};
	*mx3 = res;
}

LPR_DEF
void
lpr_Mx3_translation( Lpr_Mx3 * mx3, Lpr_V2 t )
{
	Lpr_Mx3
	res = {{ 1.0f, 0.0f, t.x,
	         0.0f, 1.0f, t.y,
	         0.0f, 0.0f, 1.0f }};
	* mx3 = res;
}

LPR_DEF
void
lpr_Mx3_rotation_deg( Lpr_Mx3 * mx3, float angle )
{
	float rangle = lpr_deg2rad( angle ),
	      c = cos( rangle ),
	      s = sin( rangle );
	Lpr_Mx3
	res = {{ c,   -s,    0.0f,
	         s,    c,    0.0f,
	         0.0f, 0.0f, 1.0f }};
	* mx3 = res;
}

LPR_DEF
void
lpr_Mx3_scale( Lpr_Mx3 * mx3, float scale )
{
	Lpr_Mx3
	res = {{ scale, 0.0f,  0.0f,
	         0.0f,  scale, 0.0f,
	         0.0f,  0.0f,  1.0f }};
	* mx3 = res;
}

LPR_DEF
void
lpr_Mx3_product( const Lpr_Mx3 * mx3_a, const Lpr_Mx3 * mx3_b,
                   Lpr_Mx3 * mx3_res )
{
	for ( int i=0 ; i != 3 ; ++i )
	{
		for ( int j=0; j != 3 ; ++j )
		{
			lpr_Mx3_set(
			   mx3_res, i, j,
			   lpr_Mx3_get( mx3_a, i, 0 ) * lpr_Mx3_get( mx3_b, 0, j ) +
			   lpr_Mx3_get( mx3_a, i, 1 ) * lpr_Mx3_get( mx3_b, 1, j ) +
			   lpr_Mx3_get( mx3_a, i, 2 ) * lpr_Mx3_get( mx3_b, 2, j ) );
		}
	}
}

LPR_DEF
void
lpr_make_Atlas( Lpr_Atlas * atlas, Lpr_Texture const * texture,
                Lpr_V2u32 frame_size_px )
{
	atlas->texture = *texture;
	atlas->frame_size_px = frame_size_px;
	atlas->atlas_size_frames = lpr_V2u32( texture->size.w  / frame_size_px.x,
	                                        texture->size.h / frame_size_px.y );
	atlas->frame_size_perc = lpr_V2( frame_size_px.x / (float)texture->size.w,
	                                   frame_size_px.y / (float)texture->size.h );
}

// NOTE(theGiallo): IMPORTANT this function has to be called before anything else!
LPR_DEF
void
lpr_init()
{
	lpr_C_colors_f32 =
	(Lpr_Rgba_Colors_f32){
		lpr_Rgba_f32_from_u8( lpr_C_colors_u8.red           ), // red
		lpr_Rgba_f32_from_u8( lpr_C_colors_u8.green         ), // green
		lpr_Rgba_f32_from_u8( lpr_C_colors_u8.blue          ), // blue
		lpr_Rgba_f32_from_u8( lpr_C_colors_u8.cyan          ), // cyan
		lpr_Rgba_f32_from_u8( lpr_C_colors_u8.magenta       ), // magenta
		lpr_Rgba_f32_from_u8( lpr_C_colors_u8.yellow        ), // yellow
		lpr_Rgba_f32_from_u8( lpr_C_colors_u8.black         ), // black
		lpr_Rgba_f32_from_u8( lpr_C_colors_u8.white         ), // white
		lpr_Rgba_f32_from_u8( lpr_C_colors_u8.gray_50       ), // gray_50
		lpr_Rgba_f32_from_u8( lpr_C_colors_u8.gray_25       ), // gray_25
		lpr_Rgba_f32_from_u8( lpr_C_colors_u8.gray_75       ), // gray_75
		lpr_Rgba_f32_from_u8( lpr_C_colors_u8.violet        ), // violet
		lpr_Rgba_f32_from_u8( lpr_C_colors_u8.purple        ), // purple
		lpr_Rgba_f32_from_u8( lpr_C_colors_u8.brown         ), // brown
		lpr_Rgba_f32_from_u8( lpr_C_colors_u8.orange        ), // orange
		lpr_Rgba_f32_from_u8( lpr_C_colors_u8.metallic_gold )  // metallic_gold
	};
	lpr_C_colors_f32_linear =
	(Lpr_Rgba_Colors_f32){
		lpr_Rgba_f32_linear_from_sRGB( lpr_C_colors_f32.red           ), // red
		lpr_Rgba_f32_linear_from_sRGB( lpr_C_colors_f32.green         ), // green
		lpr_Rgba_f32_linear_from_sRGB( lpr_C_colors_f32.blue          ), // blue
		lpr_Rgba_f32_linear_from_sRGB( lpr_C_colors_f32.cyan          ), // cyan
		lpr_Rgba_f32_linear_from_sRGB( lpr_C_colors_f32.magenta       ), // magenta
		lpr_Rgba_f32_linear_from_sRGB( lpr_C_colors_f32.yellow        ), // yellow
		lpr_Rgba_f32_linear_from_sRGB( lpr_C_colors_f32.black         ), // black
		lpr_Rgba_f32_linear_from_sRGB( lpr_C_colors_f32.white         ), // white
		lpr_Rgba_f32_linear_from_sRGB( lpr_C_colors_f32.gray_50       ), // gray_50
		lpr_Rgba_f32_linear_from_sRGB( lpr_C_colors_f32.gray_25       ), // gray_25
		lpr_Rgba_f32_linear_from_sRGB( lpr_C_colors_f32.gray_75       ), // gray_75
		lpr_Rgba_f32_linear_from_sRGB( lpr_C_colors_f32.violet        ), // violet
		lpr_Rgba_f32_linear_from_sRGB( lpr_C_colors_f32.purple        ), // purple
		lpr_Rgba_f32_linear_from_sRGB( lpr_C_colors_f32.brown         ), // brown
		lpr_Rgba_f32_linear_from_sRGB( lpr_C_colors_f32.orange        ), // orange
		lpr_Rgba_f32_linear_from_sRGB( lpr_C_colors_f32.metallic_gold )  // metallic_gold
	};
}

LPR_DEF
lpr_s32
lpr_add_quad_to_batch( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                       Lpr_Quad_Data_T_Atlas * const quad_data,
                       Lpr_Texture const * texture )
{
	lpr_assert_m( batch_id < draw_data->batches_count,
	              "batch_id must be less than batches count!" );
	lpr_s32 ret = -1;
	Lpr_Batch_Data * const batch = draw_data->batches + batch_id;
	if ( batch->quads_count == LPR_MAX_QUADS_PER_BATCH - 1 )
	{
		return ret;
	}

	lpr_assert_m( (int)batch->textures_count <= lpr_C_max_textures,
	              "batch textures must be less than lpr_C_max_textures!" );
	if ( texture )
	{
		// NOTE(theGiallo): check if the texture has already been used
		const int textures_count = batch->textures_count;
		      int i;
		const GLuint gl_texture = texture->gl_texture;
		for ( i = 0; i != textures_count; ++i )
		{
			if ( batch->textures[i] != gl_texture )
			{
				continue;
			}

			// NOTE(theGiallo): texture found
			quad_data->texture_id = i;
			break;
		}
		if ( i == textures_count )
		{
			if ( textures_count == lpr_C_max_textures )
			{
				lpr_log_err( "trying to use too many textures in a single rendering! ( %d and max is %d)", textures_count, lpr_C_max_textures );
				return ret;
			}
			// NOTE(theGiallo): not found => add it
			quad_data->texture_id = textures_count;
			batch->textures[textures_count] = gl_texture;
			++batch->textures_count;
		}
	} else
	{
		quad_data->texture_id = LPR_MAX_U8;
	}

	// NOTE(theGiallo): buffer the quad data
	ret = batch->quads_count;
	batch->instances_data[batch->quads_count++] = *quad_data;
	return ret;
}

LPR_DEF
void
lpr_make_px_perfect( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                     Lpr_V2u32 px_size, Lpr_V2 world_pos,
                     lpr_bool viewport_pxs, Lpr_V2 * out_pos, Lpr_V2 * out_size )
{
	// NOTE(theGiallo): assuming scale is uniform
	lpr_u32 px_in_w = viewport_pxs ? draw_data->viewport_size.w :
	                                 draw_data->window_size.w;
	lpr_assert( px_in_w );

	float x_px_per_unit = px_in_w /
	                      draw_data->batches[batch_id].camera_world_span.w;
	float x_unit_per_px = draw_data->batches[batch_id].camera_world_span.w /
	                      (float)px_in_w;

	Lpr_V2 c_px  = {px_size.w * 0.5f, px_size.h * 0.5f},
	       bl_px = { world_pos.x * x_px_per_unit - c_px.x,
	                 world_pos.y * x_px_per_unit - c_px.y};

	float ip;
	Lpr_V2 dp;
	dp.x = lpr_abs( lpr_modf( bl_px.x, &ip ) );
	dp.y = lpr_abs( lpr_modf( bl_px.y, &ip ) );

	if ( dp.x >= 0.5f )
	{
		world_pos.x += ( 1.0f - dp.x ) * x_unit_per_px * lpr_signf( bl_px.x );
	} else
	{
		world_pos.x -= dp.x * x_unit_per_px * lpr_signf( bl_px.x );
	}
	if ( dp.y >= 0.5f )
	{
		world_pos.y += ( 1.0f - dp.y ) * x_unit_per_px * lpr_signf( bl_px.y );
	} else
	{
		world_pos.y -= dp.y * x_unit_per_px * lpr_signf( bl_px.y );
	}
	//world_pos.x += 0.5f * x_unit_per_px; // NOTE(theGiallo): why was this here?

#ifdef LPR_DEBUG
	float v = lpr_abs( lpr_modf( world_pos.x * x_px_per_unit - px_size.x * 0.5f, &ip ) );
	float epsylon = 0.0005f;
	lpr_assert( v <= epsylon || 1.0f - v <= epsylon );
	v = lpr_abs( lpr_modf( world_pos.y * x_px_per_unit - px_size.y * 0.5f, &ip ) );
	lpr_assert( v <= epsylon || 1.0f - v <= epsylon );

	lpr_u32 px_in_h = viewport_pxs ? draw_data->viewport_size.h :
	                                 draw_data->window_size.h;
	if ( px_in_w % 2 )
	{
#if 0
		lpr_log_dbg( "px_in_w = %u", px_in_w );
		lpr_log_dbg( "camera_world_span.w = %f", draw_data->batches[batch_id].camera_world_span.w );
		lpr_log_dbg(
		   "\nworld_pos.x = %f\nworld_pos.x * x_px_per_unit = %f\n"
		   "0.5f*x_unit_per_px = %f\npx_in_w = %d\npx_size.x = %u",
		   world_pos.x, world_pos.x * x_px_per_unit,
		   0.5f*x_unit_per_px, px_in_w, px_size.x );
		lpr_log_dbg(
		   "\nworld_pos.y = %f\nworld_pos.y * x_px_per_unit = %f\n"
		   "0.5f*x_unit_per_px = %f\npx_in_w = %d\npx_size.y = %u\n",
		   world_pos.y, world_pos.y * x_px_per_unit, 0.5f*x_unit_per_px,
		   px_in_w, px_size.y );
		lpr_log_dbg( "\nbl = %f, %f",
		   world_pos.x - px_size.x * 0.5f * x_unit_per_px,
		   world_pos.y - px_size.y * 0.5f * x_unit_per_px );
		lpr_log_dbg( "\nbl px = %f, %f",
		   world_pos.x * x_px_per_unit - px_size.x * 0.5f,
		   world_pos.y * x_px_per_unit - px_size.y * 0.5f );
#endif
		world_pos.x += 0.5f * x_unit_per_px;
		// NOTE(theGiallo): it seems this corrects unalignment with NEAREST
		// TODO(theGiallo, 2016-03-12): investigate this
	}
	if ( px_in_h % 2 )
	{
#if 0
		lpr_log_dbg( "px_in_h = %u", px_in_h );
		lpr_log_dbg( "camera_world_span.h = %f", draw_data->batches[batch_id].camera_world_span.h );
#endif
		world_pos.y += 0.5f * x_unit_per_px;
	}
#endif

	*out_pos = world_pos;
	*out_size = lpr_V2_mult_f32( lpr_V2_from_V2u32( px_size ), x_unit_per_px );
}

////////////////////////////////////////////////////////////////////////////////
// debug lines

#if LPR_DEBUG_LINES
LPR_DEF
void
lpr_add_debug_line( Lpr_Draw_Data * draw_data,
                    Lpr_V2 start, Lpr_V2 end,
                    Lpr_Rgba_u8 s_color, Lpr_Rgba_u8 e_color )
{
	lpr_assert( draw_data->debug.lines_count < LPR_MAX_DEBUG_LINES - 1 );

	Lpr_Debug_Data * debug = &draw_data->debug;
	debug->line_vertices[2 * debug->lines_count     ].pos   = start;
	debug->line_vertices[2 * debug->lines_count     ].color = s_color;
	debug->line_vertices[2 * debug->lines_count + 1 ].pos   = end;
	debug->line_vertices[2 * debug->lines_count + 1 ].color = e_color;

	++debug->lines_count;
}

LPR_DEF
void
lpr_add_debug_persistent_line( Lpr_Draw_Data * draw_data,
                               Lpr_V2 start, Lpr_V2 end,
                               Lpr_Rgba_u8 s_color, Lpr_Rgba_u8 e_color )
{
	if ( draw_data->debug.persistent_lines_count ==
	     LPR_MAX_PERSISTENT_DEBUG_LINES )
	{
		return;
	}

	Lpr_Debug_Data * debug = &draw_data->debug;
	debug->persistent_line_vertices[2 * debug->persistent_lines_count     ].pos   = start;
	debug->persistent_line_vertices[2 * debug->persistent_lines_count     ].color = s_color;
	debug->persistent_line_vertices[2 * debug->persistent_lines_count + 1 ].pos   = end;
	debug->persistent_line_vertices[2 * debug->persistent_lines_count + 1 ].color = e_color;

	++debug->persistent_lines_count;
}

LPR_DEF
void
lpr_add_debug_persistent_lines( Lpr_Draw_Data * draw_data )
{
	for ( int i = 0; i != draw_data->debug.persistent_lines_count; ++i )
	{
		lpr_add_debug_line( draw_data,
		   draw_data->debug.persistent_line_vertices[2 * i    ].pos,
		   draw_data->debug.persistent_line_vertices[2 * i + 1].pos,
		   draw_data->debug.persistent_line_vertices[2 * i    ].color,
		   draw_data->debug.persistent_line_vertices[2 * i + 1].color );
	}
}

LPR_DEF
void
lpr_draw_debug_lines( Lpr_Draw_Data * draw_data, lpr_s32 batch_id )
{
	if ( ! draw_data->debug.lines_count )
	{
		return;
	}
	glUseProgram( draw_data->debug.lines_program );

	// NOTE(theGiallo): uniform data

	glUniformMatrix3fv( draw_data->debug.VP_loc, 1, GL_FALSE,
	                    draw_data->batches[batch_id].VP.array );

	glEnableVertexAttribArray( draw_data->debug.vertex_loc );
	glBindBuffer( GL_ARRAY_BUFFER, draw_data->debug.lines_vb );
	glBufferData( GL_ARRAY_BUFFER, // target
	              sizeof( draw_data->debug.line_vertices ), // size
	              NULL, // data
	              GL_STREAM_DRAW ); // usage
	glBufferData( GL_ARRAY_BUFFER,  // target
	              sizeof( draw_data->debug.line_vertices ), // size
	              draw_data->debug.line_vertices, // data
	              GL_STREAM_DRAW ); // usage

	int stride = sizeof( draw_data->debug.line_vertices[0] );
	glVertexAttribPointer( draw_data->debug.vertex_loc, // location
	                       2,        // size
	                       GL_FLOAT, // type
	                       GL_FALSE, // normalized?
	                       stride,   // stride
	                       (void*)0  // array buffer offset
	                     );
	glEnableVertexAttribArray( draw_data->debug.color_loc );
	glVertexAttribPointer( draw_data->debug.color_loc,     // location
	                       4,                              // size
	                       GL_UNSIGNED_BYTE,               // type
	                       GL_TRUE,                        // normalized?
	                       stride,                         // stride
	                       (void*)(2 * sizeof( GLfloat ) ) // array buffer offset
	                     );

	// NOTE(theGiallo): from vertex 0; lines_count*2 vertices, lines_count lines
	glDrawArrays( GL_LINES, 0, draw_data->debug.lines_count * 2 );

	glDisableVertexAttribArray( draw_data->gc_vertex_loc );
	glDisableVertexAttribArray( draw_data->gc_uv_loc );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glUseProgram( 0 );

	// NOTE(theGiallo): resets lines buffer
	draw_data->debug.lines_count = 0;
}
#endif

// debug lines
////////////////////////////////////////////////////////////////////////////////


LPR_DEF
lpr_s32
lpr_add_sprite_w_scale( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                        Lpr_Texture const * texture, Lpr_V2 pos,
                        float scale, float rot_deg,
                        const Lpr_Rgba_u8 color )
{
	Lpr_Quad_Data_T_Atlas quad_data = LPR_ZERO_STRUCT;

	quad_data.pos[0] = pos.x;
	quad_data.pos[1] = pos.y;

	quad_data.size[0] = texture->size.w  * scale;
	quad_data.size[1] = texture->size.h * scale;

	quad_data.rot = rot_deg;

	*(lpr_u32*)quad_data.color = color.rgba_b;

	quad_data.uv_scale[0] =
	quad_data.uv_scale[1] = 1.0f;

	// NOTE(theGiallo): initialized to zero
	// quad_data.uv_offset[0] = 0.0f;
	// quad_data.uv_offset[1] = 0.0f;

	// quad_data.uv_origin[0] = quad_data.uv_offset[0];
	// quad_data.uv_origin[1] = quad_data.uv_offset[1];

	quad_data.uv_limits[0] = quad_data.uv_scale[0];
	quad_data.uv_limits[1] = quad_data.uv_scale[1];

	quad_data.texture_overflow_x = texture->default_overflow.x;
	quad_data.texture_overflow_y = texture->default_overflow.y;

	quad_data.texture_min_filter = texture->default_min_filter;
	quad_data.texture_mag_filter = texture->default_mag_filter;

	quad_data.stencil_id = LPR_STENCIL_NONE;

	lpr_s32 ret =
	lpr_add_quad_to_batch( draw_data, batch_id, &quad_data, texture );

	return ret;
}

LPR_DEF
lpr_s32
lpr_add_sprite_w_size( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                       Lpr_Texture const * texture, Lpr_V2 pos,
                       Lpr_V2 size, float rot_deg, Lpr_Rgba_u8 color )
{
	Lpr_Quad_Data_T_Atlas quad_data = LPR_ZERO_STRUCT;

	quad_data.pos[0] = pos.x;
	quad_data.pos[1] = pos.y;

	quad_data.size[0] = size.x;
	quad_data.size[1] = size.y;

	quad_data.rot = rot_deg;

	*(lpr_u32*)quad_data.color = color.rgba_b;

	quad_data.uv_scale[0] =
	quad_data.uv_scale[1] = 1.0f;

	// NOTE(theGiallo): initialized to zero
	// quad_data.uv_offset[0] = 0.0f;
	// quad_data.uv_offset[1] = 0.0f;

	// quad_data.uv_origin[0] = quad_data.uv_offset[0];
	// quad_data.uv_origin[1] = quad_data.uv_offset[1];

	quad_data.uv_limits[0] = quad_data.uv_scale[0];
	quad_data.uv_limits[1] = quad_data.uv_scale[1];

	quad_data.texture_overflow_x = texture->default_overflow.x;
	quad_data.texture_overflow_y = texture->default_overflow.y;

	quad_data.texture_min_filter = texture->default_min_filter;
	quad_data.texture_mag_filter = texture->default_mag_filter;

	quad_data.stencil_id = LPR_STENCIL_NONE;

	lpr_s32 ret =
	lpr_add_quad_to_batch( draw_data, batch_id, &quad_data, texture );

	return ret;
}

LPR_DEF
lpr_s32
lpr_add_sprite_px_perfect( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                           Lpr_Texture const * texture, Lpr_V2 pos,
                           lpr_bool screen_aligned, lpr_bool viewport_pxs,
                           Lpr_Rgba_u8 color )
{
	Lpr_Quad_Data_T_Atlas quad_data = LPR_ZERO_STRUCT;

	Lpr_V2 w_pos, w_size;
	lpr_make_px_perfect( draw_data, batch_id, texture->size, pos, viewport_pxs, &w_pos, &w_size );

	quad_data.pos[0] = w_pos.x;
	quad_data.pos[1] = w_pos.y;

	quad_data.size[0] = w_size.w;
	quad_data.size[1] = w_size.h;

	quad_data.rot = screen_aligned ? draw_data->batches[batch_id].camera_angle :
	                                 0.0f;

	*(lpr_u32*)quad_data.color = color.rgba_b;

	quad_data.uv_scale[0] =
	quad_data.uv_scale[1] = 1.0f;

	// NOTE(theGiallo): initialized to zero
	// quad_data.uv_offset[0] = 0.0f;
	// quad_data.uv_offset[1] = 0.0f;

	// quad_data.uv_origin[0] = quad_data.uv_offset[0];
	// quad_data.uv_origin[1] = quad_data.uv_offset[1];

	quad_data.uv_limits[0] = quad_data.uv_scale[0];
	quad_data.uv_limits[1] = quad_data.uv_scale[1];

	quad_data.texture_overflow_x = texture->default_overflow.x;
	quad_data.texture_overflow_y = texture->default_overflow.y;

	quad_data.texture_min_filter = texture->default_min_filter;
	quad_data.texture_mag_filter = texture->default_mag_filter;

	quad_data.stencil_id = LPR_STENCIL_NONE;

	lpr_s32 ret =
	lpr_add_quad_to_batch( draw_data, batch_id, &quad_data, texture );

	return ret;
}

LPR_DEF
lpr_s32
lpr_add_atlas_frame_w_size( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                            Lpr_Atlas const * atlas,
                            Lpr_V2 pos, Lpr_V2 size,
                            lpr_u32 frame_id,
                            float rot_deg, Lpr_Rgba_u8 color )
{
	Lpr_Quad_Data_T_Atlas quad_data = LPR_ZERO_STRUCT;

	quad_data.pos[0] = pos.x;
	quad_data.pos[1] = pos.y;

	quad_data.size[0] = size.x;
	quad_data.size[1] = size.y;

	quad_data.rot = rot_deg;

	*(lpr_u32*)quad_data.color = color.rgba_b;

	quad_data.uv_scale[0] = 1.0;
	quad_data.uv_scale[1] = 1.0;

	quad_data.uv_offset[0] = 0.0f;
	quad_data.uv_offset[1] = 0.0f;

	quad_data.uv_limits[0] = atlas->frame_size_perc.x;
	quad_data.uv_limits[1] = atlas->frame_size_perc.y;

	// NOTE(theGiallo): frame indexing has origin top left, image uv left bottom
	quad_data.uv_origin[0] = ( frame_id % atlas->atlas_size_frames.x ) *
	                         quad_data.uv_limits[0];
	quad_data.uv_origin[1] = 1.0f -
	                         ( 1.0f + frame_id / atlas->atlas_size_frames.y ) *
	                         quad_data.uv_limits[1];

	quad_data.texture_overflow_x = atlas->texture.default_overflow.x;
	quad_data.texture_overflow_y = atlas->texture.default_overflow.y;

	quad_data.texture_min_filter = atlas->texture.default_min_filter;
	quad_data.texture_mag_filter = atlas->texture.default_mag_filter;

	quad_data.stencil_id = LPR_STENCIL_NONE;

	lpr_s32 ret =
	lpr_add_quad_to_batch( draw_data, batch_id, &quad_data, &atlas->texture );

	return ret;
}

LPR_DEF
lpr_s32
lpr_add_atlas_frame_w_scale( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                             Lpr_Atlas const * atlas,
                             Lpr_V2 pos, float scale,
                             lpr_u32 frame_id,
                             float rot_deg, Lpr_Rgba_u8 color )
{
	lpr_s32 ret =
	lpr_add_atlas_frame_w_size( draw_data, batch_id, atlas, pos,
	                            lpr_V2( atlas->frame_size_px.x * scale,
	                                      atlas->frame_size_px.y * scale ),
	                            frame_id, rot_deg, color );

	return ret;
}

LPR_DEF
lpr_s32
lpr_add_atlas_frame_px_perfect( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                                Lpr_Atlas const * atlas, Lpr_V2 pos,
                                lpr_u32 frame_id, lpr_bool screen_aligned,
                                lpr_bool viewport_pxs, Lpr_Rgba_u8 color )
{
	Lpr_V2 w_pos, w_size;
	lpr_make_px_perfect( draw_data, batch_id, atlas->frame_size_px, pos, viewport_pxs, &w_pos, &w_size );

	float rot = screen_aligned ? draw_data->batches[batch_id].camera_angle :
	                             0.0f;

	lpr_s32 ret =
	lpr_add_atlas_frame_w_size( draw_data, batch_id, atlas,
	                            w_pos, w_size,
	                            frame_id, rot, color );

	return ret;
}

LPR_DEF
lpr_s32
lpr_add_atlas_frame_tiled_rectangle_w_scale( Lpr_Draw_Data * draw_data,
                                             lpr_u32 batch_id,
                                             Lpr_Atlas const * atlas,
                                             Lpr_V2 pos,
                                             Lpr_V2 scale,
                                             Lpr_V2 offset_percentage,
                                             Lpr_V2u32 overflow_policies,
                                             lpr_u32 frame_id,
                                             float rot_deg,
                                             Lpr_Rgba_u8 color )
{
	Lpr_Quad_Data_T_Atlas quad_data = LPR_ZERO_STRUCT;

	quad_data.pos[0] = pos.x;
	quad_data.pos[1] = pos.y;

	quad_data.size[0] = atlas->frame_size_px.x * scale.x;
	quad_data.size[1] = atlas->frame_size_px.y * scale.y;

	quad_data.rot = rot_deg;

	*(lpr_u32*)quad_data.color = color.rgba_b;

	quad_data.uv_scale[0] = scale.x;
	quad_data.uv_scale[1] = scale.y;

	quad_data.uv_offset[0] = -offset_percentage.x;
	quad_data.uv_offset[1] = -offset_percentage.y;

	quad_data.uv_limits[0] = atlas->frame_size_perc.x;
	quad_data.uv_limits[1] = atlas->frame_size_perc.y;

	// NOTE(theGiallo): frame indexing has origin top left, image uv left bottom
	quad_data.uv_origin[0] = ( frame_id % atlas->atlas_size_frames.x ) *
	                         quad_data.uv_limits[0];
	quad_data.uv_origin[1] = 1.0f -
	                         ( 1.0f + frame_id / atlas->atlas_size_frames.y ) *
	                         quad_data.uv_limits[1];

	quad_data.texture_overflow_x = overflow_policies.x;
	quad_data.texture_overflow_y = overflow_policies.y;

	quad_data.texture_min_filter = atlas->texture.default_min_filter;
	quad_data.texture_mag_filter = atlas->texture.default_mag_filter;

	quad_data.stencil_id = LPR_STENCIL_NONE;

	lpr_s32 ret =
	lpr_add_quad_to_batch( draw_data, batch_id, &quad_data, &atlas->texture );

	return ret;
}

LPR_DEF
lpr_s32
lpr_add_atlas_frame_tiled_rectangle_w_size( Lpr_Draw_Data * draw_data,
                                            lpr_u32 batch_id,
                                            Lpr_Atlas const * atlas,
                                            Lpr_V2 pos,
                                            Lpr_V2 size,
                                            Lpr_V2 uv_size,
                                            Lpr_V2 offset_percentage,
                                            Lpr_V2u32 overflow_policies,
                                            lpr_u32 frame_id,
                                            float rot_deg,
                                            Lpr_Rgba_u8 color )
{
	Lpr_Quad_Data_T_Atlas quad_data = LPR_ZERO_STRUCT;

	quad_data.pos[0] = pos.x;
	quad_data.pos[1] = pos.y;

	quad_data.size[0] = size.x;
	quad_data.size[1] = size.y;

	quad_data.rot = rot_deg;

	*(lpr_u32*)quad_data.color = color.rgba_b;

	quad_data.uv_scale[0] = uv_size.x;
	quad_data.uv_scale[1] = uv_size.y;

	quad_data.uv_offset[0] = -offset_percentage.x;
	quad_data.uv_offset[1] = -offset_percentage.y;

	quad_data.uv_limits[0] = atlas->frame_size_perc.x;
	quad_data.uv_limits[1] = atlas->frame_size_perc.y;

	// NOTE(theGiallo): frame indexing has origin top left, image uv left bottom
	quad_data.uv_origin[0] = ( frame_id % atlas->atlas_size_frames.x ) *
	                         quad_data.uv_limits[0];
	quad_data.uv_origin[1] = 1.0f -
	                         ( 1.0f + frame_id / atlas->atlas_size_frames.y ) *
	                         quad_data.uv_limits[1];

	quad_data.texture_overflow_x = overflow_policies.x;
	quad_data.texture_overflow_y = overflow_policies.y;

	quad_data.texture_min_filter = atlas->texture.default_min_filter;
	quad_data.texture_mag_filter = atlas->texture.default_mag_filter;

	quad_data.stencil_id = LPR_STENCIL_NONE;

	lpr_s32 ret =
	lpr_add_quad_to_batch( draw_data, batch_id, &quad_data, &atlas->texture );

	return ret;
}
LPR_DEF
lpr_s32
lpr_add_colored_rect( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                      Lpr_V2 pos, Lpr_V2 size, Lpr_Rgba_u8 color,
                      float rot_deg )
{
	Lpr_Quad_Data_T_Atlas quad_data = LPR_ZERO_STRUCT;

	quad_data.pos[0] = pos.x;
	quad_data.pos[1] = pos.y;

	quad_data.size[0] = size.x;
	quad_data.size[1] = size.y;

	quad_data.rot = rot_deg;

	*(lpr_u32*)quad_data.color = color.rgba_b;

	quad_data.stencil_id = LPR_STENCIL_NONE;

	// NOTE(theGiallo): NULL is for no texture
	lpr_s32 ret =
	lpr_add_quad_to_batch( draw_data, batch_id, &quad_data, NULL );

	return ret;
}

LPR_DEF
lpr_s32
lpr_add_rect_full_window( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                          Lpr_Texture * texture,
                          Lpr_Rgba_u8 color, float rot_deg,
                          Lpr_V2 displacement_percentage )
{
	lpr_assert_m( batch_id < draw_data->batches_count,
	              "batch_id must be less than batches count!" );
	lpr_assert_m( texture,
	              "texture cannot be NULL!" );

	Lpr_Batch_Data * batch = draw_data->batches + batch_id;
	Lpr_V2 tr, size;
	size = batch->camera_world_span;
	tr.x = ( 0.5f - batch->vanishing_point.x ) * size.x;
	tr.y = ( 0.5f - batch->vanishing_point.y ) * size.y;
	tr = lpr_V2_rotated_deg( tr, batch->camera_angle );
	Lpr_V2 displacement = {size.x * displacement_percentage.x,
	                       size.y * displacement_percentage.y};
	displacement = lpr_V2_rotated_deg( displacement , batch->camera_angle );
	tr.x += displacement.x;
	tr.y += displacement.y;

	Lpr_Quad_Data_T_Atlas quad_data = LPR_ZERO_STRUCT;

	quad_data.pos[0] = batch->camera_pos.x + tr.x;
	quad_data.pos[1] = batch->camera_pos.y + tr.y;

	quad_data.size[0] = size.x;
	quad_data.size[1] = size.y;

	quad_data.rot = batch->camera_angle - rot_deg;

	*(lpr_u32*)quad_data.color = color.rgba_b;

	quad_data.uv_scale[0] =
	quad_data.uv_scale[1] = 1.0f;

	// NOTE(theGiallo): initialized to zero
	// quad_data.uv_offset[0] = 0.0f;
	// quad_data.uv_offset[1] = 0.0f;

	// quad_data.uv_origin[0] = quad_data.uv_offset[0];
	// quad_data.uv_origin[1] = quad_data.uv_offset[1];

	quad_data.uv_limits[0] = quad_data.uv_scale[0];
	quad_data.uv_limits[1] = quad_data.uv_scale[1];

	quad_data.texture_overflow_x = texture->default_overflow.x;
	quad_data.texture_overflow_y = texture->default_overflow.y;

	quad_data.texture_min_filter = texture->default_min_filter;
	quad_data.texture_mag_filter = texture->default_mag_filter;

	quad_data.stencil_id = LPR_STENCIL_NONE;

	lpr_s32 ret =
	lpr_add_quad_to_batch( draw_data, batch_id, &quad_data, texture );

	return ret;
}

LPR_DEF
lpr_s32
lpr_add_colored_rect_full_window( Lpr_Draw_Data * draw_data,
                                  lpr_u32 batch_id,
                                  Lpr_Rgba_u8 color, float rot_deg,
                                  Lpr_V2 displacement_percentage )
{
	lpr_assert_m( batch_id < draw_data->batches_count,
	              "batch_id must be less than batches count!" );

	Lpr_Batch_Data * batch = draw_data->batches + batch_id;
	Lpr_V2 tr, size;
	size = batch->camera_world_span;
	tr.x = ( 0.5f - batch->vanishing_point.x ) * size.x;
	tr.y = ( 0.5f - batch->vanishing_point.y ) * size.y;
	tr = lpr_V2_rotated_deg( tr, batch->camera_angle );
	Lpr_V2 displacement = {size.x * displacement_percentage.x,
	                       size.y * displacement_percentage.y};
	displacement = lpr_V2_rotated_deg( displacement , batch->camera_angle );
	tr.x += displacement.x;
	tr.y += displacement.y;

	Lpr_Quad_Data_T_Atlas quad_data = LPR_ZERO_STRUCT;

	quad_data.pos[0] = batch->camera_pos.x + tr.x;
	quad_data.pos[1] = batch->camera_pos.y + tr.y;

	quad_data.size[0] = size.x;
	quad_data.size[1] = size.y;

	quad_data.rot = batch->camera_angle - rot_deg;

	*(lpr_u32*)quad_data.color = color.rgba_b;

	quad_data.stencil_id = LPR_STENCIL_NONE;

	// NOTE(theGiallo): NULL is for no texture
	lpr_s32 ret =
	lpr_add_quad_to_batch( draw_data, batch_id, &quad_data, NULL );

	return ret;
}

///////////////////////////////////////////////////////////////////////////////
// font API

LPR_DEF
lpr_u32
lpr_hash_fullchar( Lpr_UTF8_Fullchar fullchar )
{
	lpr_u32 h = lpr_u32_hash_32( fullchar.all ) % LPR_FONT_HM_SIZE;

	return h;
}

LPR_DEF
lpr_s32
lpr_glyph_id_from_fullchar( Lpr_Font_Of_Size * of_size,
                            Lpr_UTF8_Fullchar fullchar )
{
	lpr_u32 h = lpr_hash_fullchar( fullchar );
	lpr_s32 glyph_id = of_size->glyphs_indexes_ht[h];

	return glyph_id;
}

LPR_DEF
float
lpr_kerned_line_length_px( Lpr_Multi_Font * multi_font, lpr_s32 font_id,
                           lpr_s32 size_id,
                           const char * utf8_string, lpr_u32 str_bytes_size )
{
	Lpr_Font * font = multi_font->fonts + font_id;
	Lpr_Font_Of_Size * of_size = font->of_size + size_id;
	float font_scale = font->scales[size_id];
	// NOTE(theGiallo): we multiply at each step to reduce fp error

	float ret = 0;
	{
		Lpr_UTF8_Fullchar fullchar = lpr_utf8_full_char( (lpr_u8*)utf8_string );
		lpr_s32 glyph_id = lpr_glyph_id_from_fullchar( of_size, fullchar );
		ret -= font->glyphs_metrics[glyph_id].left_side_bearing * font_scale;
	}

	lpr_s32 curr_glyph_id,
	    prev_glyph_id = 0;
	lpr_u8 * curr_utf8_ptr = (lpr_u8*)utf8_string;
	lpr_u32 col = 0;

	for (;;)
	{
		if ( !curr_utf8_ptr )
		{
			break;
		}
		Lpr_UTF8_Fullchar fullchar = lpr_utf8_full_char( curr_utf8_ptr );
		if ( !fullchar.all )
		{
			break;
		}
		curr_utf8_ptr =
		lpr_utf8_next_char_ptr( curr_utf8_ptr, (lpr_u8*)utf8_string,
		                        str_bytes_size );
		if ( !curr_utf8_ptr )
		{
			// NOTE(theGiallo): this means fullchar is not valid or outside the
			// string end
			break;
		}
		if ( fullchar.subchars[0] == '\n' )
		{
			break;
		}
		if ( fullchar.subchars[0] == '\r' )
		{
			continue;
		}

		lpr_s32 count = 1;
		if ( fullchar.subchars[0] == '\t' )
		{
			count = 8 - ( col % 8 );
			fullchar.subchars[0] = ' ';
		}

		curr_glyph_id = lpr_glyph_id_from_fullchar( of_size, fullchar );
		for ( lpr_s32 i = 0; i != count; ++i )
		{
			float kerning =
			   font_scale *
			   (float)font->kerning_table[prev_glyph_id][curr_glyph_id];
			ret += kerning;

			ret +=
			   font_scale *
			   (float)font->glyphs_metrics[curr_glyph_id].advance_width;

			prev_glyph_id = curr_glyph_id;
			++col;
		}
	}

	// NOTE(theGiallo): this worsen the centering, so I commented it out
	// NOTE(theGiallo): that is caused by padding :[ We could store padding,
	// but this way seems not too bad
	//ret -= font->glyphs_metrics[curr_glyph_id].advance_width * font_scale;
	//ret += font->glyphs_metrics[curr_glyph_id].left_side_bearing * font_scale;
	//ret += of_size->glyphs_in_atlas[curr_glyph_id].pos_offset_px.x;
	//ret += of_size->glyphs_in_atlas[curr_glyph_id].px_size.w * 0.5f *
	//       of_size->oversampling_scale.x;

	return ret;
}

LPR_DEF
float
lpr_line_length_px_wrap( Lpr_Multi_Font * multi_font, lpr_s32 font_id,
                         lpr_s32 size_id,
                         const char * utf8_string, lpr_u32 str_bytes_size,
                         const Lpr_Txt_Wrap wrap, float max_px_width,
                         lpr_bool kerned,
                         lpr_u32 * num_chars,
                         const char ** next_line_ptr )
{
	Lpr_Font * font = multi_font->fonts + font_id;
	Lpr_Font_Of_Size * of_size = font->of_size + size_id;
	float font_scale = font->scales[size_id];

	float ret = 0;
	{
		Lpr_UTF8_Fullchar fullchar = lpr_utf8_full_char( (lpr_u8*)utf8_string );
		lpr_s32 glyph_id = lpr_glyph_id_from_fullchar( of_size, fullchar );
		ret -= font->glyphs_metrics[glyph_id].left_side_bearing * font_scale;
	}

	lpr_s32 curr_glyph_id,
	    prev_glyph_id = 0;
	lpr_u8 * curr_utf8_ptr = (lpr_u8*)utf8_string;
	lpr_u8 * next_utf8_ptr = NULL;
	lpr_u8 * int_next_line_ptr = curr_utf8_ptr;
	lpr_u8 * int_middle_word_next_line_ptr = curr_utf8_ptr;
	lpr_u32 col = 0;
	lpr_u32 int_num_chars = 0;
	lpr_u32 last_word_num_chars = 0;
	float word_length_px = 0;
	lpr_bool was_blank = lpr_false;

	for (;;)
	{
		if ( !curr_utf8_ptr )
		{
			break;
		}

		next_utf8_ptr =
		lpr_utf8_next_char_ptr( curr_utf8_ptr, (lpr_u8*)utf8_string,
		                        str_bytes_size );
		if ( !next_utf8_ptr )
		{
			// NOTE(theGiallo): this means fullchar is not valid or outside the
			// string end
			break;
		}
		Lpr_UTF8_Fullchar fullchar = lpr_utf8_full_char( curr_utf8_ptr );
		if ( !fullchar.all )
		{
			int_num_chars += last_word_num_chars;
			last_word_num_chars = 0;
			ret += word_length_px;
			word_length_px = 0;

			int_next_line_ptr = curr_utf8_ptr;
			break;
		}

		if ( wrap == LPR_TXT_WRAP_WORD &&
		     ( fullchar.subchars[0] == '\t' ||
		       fullchar.subchars[0] == '\n' ||
		       fullchar.subchars[0] == ' ' ) )
		{
			int_next_line_ptr = curr_utf8_ptr;
			int_num_chars += last_word_num_chars;
			last_word_num_chars = 0;
			ret += word_length_px;
			word_length_px = 0;
			if ( fullchar.subchars[0] == '\t' ||
			     fullchar.subchars[0] == ' ' )
			{
				was_blank = lpr_true;
			}
		}

		curr_utf8_ptr = next_utf8_ptr;

		if ( fullchar.subchars[0] == '\n' ||
		     fullchar.subchars[0] == '\r' )
		{
			int_next_line_ptr = curr_utf8_ptr;
			++int_num_chars;
			break;
		}

		lpr_s32 count = 1;
		if ( fullchar.subchars[0] == '\t' )
		{
			count = 8 - ( col % 8 );
			fullchar.subchars[0] = ' ';
		}

		float tot_adv = 0.0f;
		curr_glyph_id = lpr_glyph_id_from_fullchar( of_size, fullchar );
		for ( lpr_s32 i = 0; i != count; ++i )
		{
			if ( kerned )
			{
				float kerning =
				   font_scale *
				   (float)font->kerning_table[prev_glyph_id][curr_glyph_id];
				tot_adv += kerning;
			}

			tot_adv +=
			   font_scale *
			   (float)font->glyphs_metrics[curr_glyph_id].advance_width;

			prev_glyph_id = curr_glyph_id;
			++col;
		}

		if ( wrap != LPR_TXT_WRAP_NONE && ret + tot_adv > max_px_width )
		{
			break;
		}
		if ( wrap == LPR_TXT_WRAP_WORD &&
		     ret + word_length_px + tot_adv > max_px_width )
		{
			break;
		}
		if ( wrap != LPR_TXT_WRAP_WORD )
		{
			int_next_line_ptr = curr_utf8_ptr;
			ret += tot_adv;
			++int_num_chars;
		} else
		{
			int_middle_word_next_line_ptr = curr_utf8_ptr;
			word_length_px += tot_adv;
			++last_word_num_chars;
		}
		if ( wrap == LPR_TXT_WRAP_WORD &&
		     was_blank )
		{
			int_next_line_ptr = curr_utf8_ptr;
			int_num_chars += last_word_num_chars;
			last_word_num_chars = 0;
			ret += word_length_px;
			word_length_px = 0;
			was_blank = lpr_false;
		}
	}

	// NOTE(theGiallo): word break
	if ( wrap == LPR_TXT_WRAP_WORD && int_num_chars == 0 )
	{
		ret = word_length_px;
		int_num_chars = last_word_num_chars;
		int_next_line_ptr = int_middle_word_next_line_ptr;
	}

	*num_chars = int_num_chars;
	*next_line_ptr = (char*)int_next_line_ptr;

	// NOTE(theGiallo): this worsen the centering, so I commented it out
	// NOTE(theGiallo): that is caused by padding :[ We could store padding,
	// but this way seems not too bad
	//ret -= font->glyphs_metrics[curr_glyph_id].advance_width * tot_scale;
	//ret += font->glyphs_metrics[curr_glyph_id].left_side_bearing * tot_scale;
	//ret += of_size->glyphs_in_atlas[curr_glyph_id].pos_offset_px.x * scale;
	//ret += of_size->glyphs_in_atlas[curr_glyph_id].px_size.w * 0.5f *
	//       scale * of_size->oversampling_scale.x;

	if ( ret < 0 )
	{
		ret = 0;
	}

	return ret;
}

LPR_DEF
float
lpr_word_length_px( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                    Lpr_Multi_Font * multi_font, lpr_s32 font_id,
                    lpr_s32 size_id,
                    const char * utf8_string, lpr_u32 str_bytes_size,
                    float scale,
                    lpr_bool px_perfect, lpr_bool viewport_pxs )
{
	Lpr_Font * font = multi_font->fonts + font_id;
	Lpr_Font_Of_Size * of_size = font->of_size + size_id;
	float font_scale = font->scales[size_id];

	float tot_scale = 1.0f;
	if ( px_perfect )
	{
		lpr_u32 px_in_w = viewport_pxs ? draw_data->viewport_size.w :
		                                 draw_data->window_size.w;
		tot_scale = draw_data->batches[batch_id].camera_world_span.w /
		            (float)px_in_w;
	}
	tot_scale *= scale * font_scale;

	float ret = 0;
	{
		Lpr_UTF8_Fullchar fullchar = lpr_utf8_full_char( (lpr_u8*)utf8_string );
		lpr_s32 glyph_id = lpr_glyph_id_from_fullchar( of_size, fullchar );
		ret -= font->glyphs_metrics[glyph_id].left_side_bearing * tot_scale;
	}

	lpr_s32 curr_glyph_id,
	    prev_glyph_id = 0;
	lpr_u8 * curr_utf8_ptr = (lpr_u8*)utf8_string;

	for (;;)
	{
		if ( !curr_utf8_ptr )
		{
			break;
		}
		Lpr_UTF8_Fullchar fullchar = lpr_utf8_full_char( curr_utf8_ptr );
		if ( !fullchar.all )
		{
			break;
		}
		curr_utf8_ptr =
		lpr_utf8_next_char_ptr( curr_utf8_ptr, (lpr_u8*)utf8_string,
		                        str_bytes_size );
		if ( !curr_utf8_ptr )
		{
			// NOTE(theGiallo): this means fullchar is not valid or outside the
			// string end
			break;
		}
		if ( fullchar.subchars[0] == '\n' ||
		     fullchar.subchars[0] == '\t' ||
		     fullchar.subchars[0] == ' ' )
		{
			break;
		}
		if ( fullchar.subchars[0] == '\r' )
		{
			continue;
		}

		curr_glyph_id = lpr_glyph_id_from_fullchar( of_size, fullchar );
		float kerning =
		   tot_scale *
		   (float)font->kerning_table[prev_glyph_id][curr_glyph_id];
		ret += kerning;

		ret +=
		   tot_scale *
		   (float)font->glyphs_metrics[curr_glyph_id].advance_width;

		prev_glyph_id = curr_glyph_id;
	}

	// NOTE(theGiallo): this worsen the centering, so I commented it out
	// NOTE(theGiallo): that is caused by padding :[ We could store padding,
	// but this way seems not too bad
	//ret -= font->glyphs_metrics[curr_glyph_id].advance_width * tot_scale;
	//ret += font->glyphs_metrics[curr_glyph_id].left_side_bearing * tot_scale;
	//ret += of_size->glyphs_in_atlas[curr_glyph_id].pos_offset_px.x * scale;
	//ret += of_size->glyphs_in_atlas[curr_glyph_id].px_size.w * 0.5f *
	//       scale * of_size->oversampling_scale.x;

	return ret;
}

LPR_DEF
lpr_u32
lpr_lines_count( const char * utf8_string )
{
	lpr_u32 ret = 1;
	while ( *utf8_string++ )
	{
		if ( *utf8_string == '\n' )
		{
			++ret;
		}
	}
	return ret;
}

LPR_DEF
lpr_u32
lpr_lines_count_wrap( Lpr_Multi_Font * multi_font, lpr_s32 font_id,
                      lpr_s32 size_id,
                      const char * utf8_string, lpr_u32 str_bytes_size,
                      const Lpr_Txt_Wrap wrap, float max_px_width,
                      lpr_bool kerned )
{
	lpr_u32 num_chars;

	lpr_u32 ret = 0;
	while( lpr_line_length_px_wrap( multi_font, font_id, size_id, utf8_string,
	                                str_bytes_size, wrap, max_px_width,
	                                kerned, &num_chars, &utf8_string ) )
	{
		++ret;
	}
	return ret;
}


LPR_DEF
lpr_s32
lpr_add_glyph( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
               Lpr_Multi_Font * multi_font, lpr_s32 font_id, lpr_s32 size_id,
               lpr_s32 glyph_id,
               Lpr_V2 curr_pt, Lpr_Rgba_u8 color, float scale,
               lpr_bool px_perfect, lpr_bool viewport_pxs, lpr_u32 stencil_id )
{
	Lpr_Quad_Data_T_Atlas quad_data = LPR_ZERO_STRUCT;
	float pxpscale = 1.0f;
	if ( px_perfect )
	{
		lpr_u32 px_in_w = viewport_pxs ? draw_data->viewport_size.w :
		                                 draw_data->window_size.w;
		pxpscale = draw_data->batches[batch_id].camera_world_span.w /
		            (float)px_in_w;
	}

	Lpr_Font * font = multi_font->fonts + font_id;
	Lpr_Font_Of_Size * of_size = font->of_size + size_id;
	float font_scale = font->scales[size_id];
	Lpr_Glyph_In_Atlas * g = of_size->glyphs_in_atlas + glyph_id;
	Lpr_V2 pos = curr_pt;
	Lpr_V2 offset = g->pos_offset_px;
	offset = lpr_V2_mult_f32( offset, scale * pxpscale );
	offset.x += font->glyphs_metrics[glyph_id].left_side_bearing *
	            scale * pxpscale * font_scale;
	pos = lpr_V2_add_V2( pos, offset );

	Lpr_V2u32 px_size = g->px_size;
	px_size.x *= of_size->oversampling_scale.x;
	px_size.y *= of_size->oversampling_scale.y;

	Lpr_V2 size = lpr_V2_from_V2u32( px_size );

	if ( px_perfect )
	{
		lpr_make_px_perfect( draw_data, batch_id, px_size, pos,
		                     viewport_pxs, &pos, &size );

	// TODO(theGiallo, 2016-03-16): investigating this. Seems to
	// solve some little imperfections with LPR_NEAREST in some cases.
	// NOTE(theGiallo): moved this correction into lpr_make_px_perfect, but still unknown.
#if 0
		if ( draw_data->window_size.w % 2 )
		{
	#if 0
			lpr_log_dbg( "------------------------" );
			lpr_log_dbg( "pos = %f, %f", pos.x, pos.y );
			lpr_log_dbg( "pos / pxpscale = %f, %f", pos.x/pxpscale, pos.y/pxpscale );
			lpr_log_dbg( "px_size = %u, %u", g->px_size.x, g->px_size.y );
	#endif
			// TODO(theGiallo, 2016-03-13): this is here instead of into make_px_perfect, waiting for more investigations
			pos.x += 0.5f * pxpscale;
		}
		if ( draw_data->window_size.h % 2 )
		{
			pos.y += 0.5f * pxpscale;
		}
#endif
	}


	size = lpr_V2_mult_f32( size, scale );

	quad_data.pos[0] = pos.x;
	quad_data.pos[1] = pos.y;
	quad_data.size[0] = size.x;
	quad_data.size[1] = size.y;
	if ( !quad_data.size[0] || !quad_data.size[1] )
	{
		return -0xface;
	}
	quad_data.uv_origin[0] = g->bl_uv.x;
	quad_data.uv_origin[1] = g->bl_uv.y;
	quad_data.uv_limits[0] = g->uv_size.w;
	quad_data.uv_limits[1] = g->uv_size.h;
	quad_data.uv_scale[0] = 1;
	quad_data.uv_scale[1] = 1;

	lpr_s32 atlas_id = of_size->atlases_of_glyphs[glyph_id];
	Lpr_Texture * texture = &multi_font->atlases[atlas_id];

	quad_data.texture_overflow_x = texture->default_overflow.x;
	quad_data.texture_overflow_y = texture->default_overflow.y;

	quad_data.texture_min_filter = texture->default_min_filter;
	quad_data.texture_mag_filter = texture->default_mag_filter;
	*(lpr_u32*)quad_data.color = color.rgba_b;

	quad_data.stencil_id = stencil_id;

	lpr_s32 ret =
	lpr_add_quad_to_batch( draw_data, batch_id, &quad_data, texture );

	return ret;
}

LPR_DEF
lpr_s32
lpr_add_rotated_glyph( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                       Lpr_Multi_Font * multi_font,
                       lpr_s32 font_id, lpr_s32 size_id,
                       lpr_s32 glyph_id,
                       Lpr_V2 curr_pt, Lpr_V2 right_dir, float glyph_rot_deg,
                       Lpr_Rgba_u8 color, float scale,
                       lpr_bool px_perfect, lpr_bool viewport_pxs,
                       lpr_u32 stencil_id )
{
	Lpr_Quad_Data_T_Atlas quad_data = LPR_ZERO_STRUCT;

	Lpr_Font * font = multi_font->fonts + font_id;
	Lpr_Font_Of_Size * of_size = font->of_size + size_id;
	float font_scale = font->scales[size_id];
	Lpr_Glyph_In_Atlas * g = of_size->glyphs_in_atlas + glyph_id;

	Lpr_V2u32 px_size = g->px_size;
	px_size.x *= of_size->oversampling_scale.x;
	px_size.y *= of_size->oversampling_scale.y;
	Lpr_V2 size = lpr_V2_from_V2u32( px_size );
	float pxpscale = 1.0f;
	if ( px_perfect )
	{
		lpr_u32 px_in_w = viewport_pxs ? draw_data->viewport_size.w :
		                                 draw_data->window_size.w;
		pxpscale = draw_data->batches[batch_id].camera_world_span.w /
		            (float)px_in_w;
		size = lpr_V2_mult_f32( lpr_V2_from_V2u32( px_size ),
		                        pxpscale );
	}
	size = lpr_V2_mult_f32( size, scale );

	Lpr_V2 pos = curr_pt;
	Lpr_V2 offset = g->pos_offset_px;
	offset = lpr_V2_mult_f32( offset, scale * pxpscale );
	offset.x += font->glyphs_metrics[glyph_id].left_side_bearing *
	            scale * pxpscale * font_scale;
	Lpr_V2 unrot_offset = offset;
	offset.x = unrot_offset.x * right_dir.x - unrot_offset.y * right_dir.y;
	offset.y = unrot_offset.x * right_dir.y + unrot_offset.y * right_dir.x;
	pos = lpr_V2_add_V2( pos, offset );

	quad_data.pos[0] = pos.x;
	quad_data.pos[1] = pos.y;
	quad_data.size[0] = size.x;
	quad_data.size[1] = size.y;
	if ( !quad_data.size[0] || !quad_data.size[1] )
	{
		return -0xface;
	}
	quad_data.uv_origin[0] = g->bl_uv.x;
	quad_data.uv_origin[1] = g->bl_uv.y;
	quad_data.uv_limits[0] = g->uv_size.w;
	quad_data.uv_limits[1] = g->uv_size.h;
	quad_data.uv_scale[0] = 1;
	quad_data.uv_scale[1] = 1;

	quad_data.rot = glyph_rot_deg;

	lpr_s32 atlas_id = of_size->atlases_of_glyphs[glyph_id];
	Lpr_Texture * texture = &multi_font->atlases[atlas_id];

	quad_data.texture_overflow_x = texture->default_overflow.x;
	quad_data.texture_overflow_y = texture->default_overflow.y;

	quad_data.texture_min_filter = texture->default_min_filter;
	quad_data.texture_mag_filter = texture->default_mag_filter;
	*(lpr_u32*)quad_data.color = color.rgba_b;

	quad_data.stencil_id = stencil_id;

	lpr_s32 ret =
	lpr_add_quad_to_batch( draw_data, batch_id, &quad_data, texture );

	return ret;
}

LPR_DEF
lpr_s32
lpr_add_rotated_glyph_clipped( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                               Lpr_Multi_Font * multi_font,
                               lpr_s32 font_id, lpr_s32 size_id,
                               lpr_s32 glyph_id,
                               Lpr_V2 curr_pt,
                               Lpr_V2 right_dir,
                               float glyph_rot_deg,
                               Lpr_Rgba_u8 color,
                               // NOTE(theGiallo): LPR_ZERO means no clipping
                               Lpr_AA_Rect clipping_rect,
                               float scale,
                               lpr_bool px_perfect,
                               lpr_bool viewport_pxs,
                               lpr_u32 stencil_id )
{
	Lpr_Quad_Data_T_Atlas quad_data = LPR_ZERO_STRUCT;

	Lpr_Font * font = multi_font->fonts + font_id;
	Lpr_Font_Of_Size * of_size = font->of_size + size_id;
	float font_scale = font->scales[size_id];
	Lpr_Glyph_In_Atlas * g = of_size->glyphs_in_atlas + glyph_id;

	Lpr_V2u32 px_size = g->px_size;
	px_size.x *= of_size->oversampling_scale.x;
	px_size.y *= of_size->oversampling_scale.y;
	Lpr_V2 size = lpr_V2_from_V2u32( px_size );
	float pxpscale = 1.0f;
	if ( px_perfect )
	{
		lpr_u32 px_in_w = viewport_pxs ? draw_data->viewport_size.w :
		                                 draw_data->window_size.w;
		pxpscale = draw_data->batches[batch_id].camera_world_span.w /
		            (float)px_in_w;
		size = lpr_V2_mult_f32( lpr_V2_from_V2u32( px_size ),
		                        pxpscale );
	}
	size = lpr_V2_mult_f32( size, scale );

	Lpr_V2 pos = curr_pt;
	Lpr_V2 offset = g->pos_offset_px;
	offset = lpr_V2_mult_f32( offset, scale * pxpscale );
	offset.x += font->glyphs_metrics[glyph_id].left_side_bearing *
	            scale * pxpscale * font_scale;
	Lpr_V2 unrot_offset = offset;
	offset.x = unrot_offset.x * right_dir.x - unrot_offset.y * right_dir.y;
	offset.y = unrot_offset.x * right_dir.y + unrot_offset.y * right_dir.x;
	pos = lpr_V2_add_V2( pos, offset );

	quad_data.pos[0] = pos.x;
	quad_data.pos[1] = pos.y;
	quad_data.size[0] = size.x;
	quad_data.size[1] = size.y;
	if ( !quad_data.size[0] || !quad_data.size[1] )
	{
		return -0xface;
	}
	quad_data.uv_origin[0] = g->bl_uv.x;
	quad_data.uv_origin[1] = g->bl_uv.y;
	quad_data.uv_limits[0] = g->uv_size.w;
	quad_data.uv_limits[1] = g->uv_size.h;
	quad_data.uv_scale[0] = 1;
	quad_data.uv_scale[1] = 1;

	quad_data.rot = glyph_rot_deg;

	Lpr_V2 h_size = lpr_V2_mult_f32( size, 0.5f );
	float rr = lpr_V2_sqlength( h_size );
	Lpr_V2 p = lpr_V2_abs( lpr_V2_sub_V2( pos, clipping_rect.pos ) );
	Lpr_V2 d = lpr_V2_sub_V2( p, lpr_V2_mult_f32( clipping_rect.size, 0.5f ) );
	d.x = lpr_max( d.x, 0.0f );
	d.y = lpr_max( d.y, 0.0f );
	float sqdist = lpr_V2_sqlength( d );
	if ( sqdist > rr )
	{
		return LPR_CLIPPED_OUT;
	}

	lpr_s32 atlas_id = of_size->atlases_of_glyphs[glyph_id];
	Lpr_Texture * texture = &multi_font->atlases[atlas_id];

	quad_data.texture_overflow_x = texture->default_overflow.x;
	quad_data.texture_overflow_y = texture->default_overflow.y;

	quad_data.texture_min_filter = texture->default_min_filter;
	quad_data.texture_mag_filter = texture->default_mag_filter;
	*(lpr_u32*)quad_data.color = color.rgba_b;

	quad_data.stencil_id = stencil_id;

	lpr_s32 ret =
	lpr_add_quad_to_batch( draw_data, batch_id, &quad_data, texture );

	return ret;
}

LPR_DEF
lpr_s32
lpr_add_utf8_char( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                   Lpr_Multi_Font * multi_font, lpr_s32 font_id,
                   lpr_s32 size_id,
                   Lpr_UTF8_Fullchar fullchar,
                   Lpr_V2 curr_pt, Lpr_Rgba_u8 color,
                   float scale, lpr_bool px_perfect, lpr_bool viewport_pxs,
                   lpr_u32 stencil_id,
                   lpr_s32 * glyph_id )
{
	Lpr_Font * font = multi_font->fonts + font_id;
	Lpr_Font_Of_Size * of_size = font->of_size + size_id;
	*glyph_id = lpr_glyph_id_from_fullchar( of_size, fullchar );

	lpr_s32 ret =
	lpr_add_glyph( draw_data, batch_id, multi_font, font_id, size_id,
	               *glyph_id, curr_pt, color, scale, px_perfect,
	               viewport_pxs, stencil_id );

	return ret;
}

LPR_DEF
lpr_s32
lpr_add_kerned_glyph( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                      Lpr_Multi_Font * multi_font, lpr_s32 font_id,
                      lpr_s32 size_id, lpr_s32 glyph_id,
                      Lpr_V2 curr_pt, lpr_s32 prev_glyph_id,
                      Lpr_Rgba_u8 color, float scale,
                      lpr_bool px_perfect, lpr_bool viewport_pxs,
                      lpr_u32 stencil_id, float * kerning )
{
	Lpr_Font * font = multi_font->fonts + font_id;
	float tot_scale = 1.0f;
	if ( px_perfect )
	{
		lpr_u32 px_in_w = viewport_pxs ? draw_data->viewport_size.w :
		                                 draw_data->window_size.w;
		tot_scale = draw_data->batches[batch_id].camera_world_span.w /
		            (float)px_in_w;
	}
	tot_scale = scale * font->scales[size_id];
	*kerning = tot_scale *
	           (float)font->kerning_table[prev_glyph_id][glyph_id];

	curr_pt.x += *kerning;

	lpr_s32 ret =
	lpr_add_glyph( draw_data, batch_id, multi_font, font_id, size_id,
	               glyph_id, curr_pt, color, scale, px_perfect,
	               viewport_pxs, stencil_id );
	return ret;
}

LPR_DEF
lpr_s32
lpr_add_kerned_utf8_char( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                          Lpr_Multi_Font * multi_font, lpr_s32 font_id,
                          lpr_s32 size_id, Lpr_UTF8_Fullchar fullchar,
                          Lpr_V2 curr_pt,
                          lpr_s32 prev_glyph_id, lpr_s32 * glyph_id,
                          Lpr_Rgba_u8 color, float scale,
                          lpr_bool px_perfect, lpr_bool viewport_pxs,
                          lpr_u32 stencil_id, float * kerning )
{
	Lpr_Font * font = multi_font->fonts + font_id;
	Lpr_Font_Of_Size * of_size = font->of_size + size_id;
	*glyph_id = lpr_glyph_id_from_fullchar( of_size, fullchar );

	lpr_s32 ret =
	lpr_add_kerned_glyph( draw_data, batch_id, multi_font, font_id,
	                      size_id, *glyph_id, curr_pt, prev_glyph_id,
	                      color, scale, px_perfect, viewport_pxs, stencil_id, kerning );
	return ret;
}

LPR_DEF
lpr_bool
lpr_add_multiline_text( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                        Lpr_Multi_Font * multi_font, lpr_s32 font_id,
                        lpr_s32 size_id,
                        Lpr_V2 starting_pt,
                        const char * utf8_string, lpr_u32 str_bytes_size,
                        Lpr_Rgba_u8 color,
                        const lpr_bool set_z, const lpr_u16 z,
                        float scale = 1.0f,
                        lpr_bool px_perfect = false, lpr_bool viewport_pxs = true,
                        lpr_u32 stencil_id = LPR_STENCIL_NONE,
                        lpr_s32 * glyph_id = NULL,
                        Lpr_AA_Rect * bounding_box = NULL, Lpr_V2 * ending_pt = NULL,
                        Lpr_Sprites_Span * sprites_span = NULL )
{
	Lpr_Font * font = multi_font->fonts + font_id;
	Lpr_Font_Of_Size * of_size = font->of_size + size_id;
	float font_scale = font->scales[size_id];

	Lpr_Sprites_Span span = LPR_ZERO_STRUCT;
	span.first = draw_data->batches[batch_id].quads_count;

	Lpr_V2 tot_scale = {1.0f,1.0f};
	if ( px_perfect )
	{
		lpr_u32 px_in_w = viewport_pxs ? draw_data->viewport_size.w :
		                                 draw_data->window_size.w;
		tot_scale = lpr_V2_mult_f32(
		               tot_scale,
		               draw_data->batches[batch_id].camera_world_span.w /
		               (float)px_in_w );
	}
	tot_scale = lpr_V2_mult_f32( tot_scale, scale * font_scale );

	float line_height =
	   tot_scale.y *
	   (float) ( font->metrics.ascent - font->metrics.descent +
	           font->metrics.line_gap );
	Lpr_V2 curr_pt = starting_pt;
	lpr_s32 curr_glyph_id,
	    prev_glyph_id = 0;
	lpr_u8 * curr_utf8_ptr = (lpr_u8*)utf8_string;
	float max_x = starting_pt.x;
	// NOTE(theGiallo): for tabular alignment
	lpr_u32 col = 0;

	for (;;)
	{
		if ( !curr_utf8_ptr )
		{
			break;
		}
		Lpr_UTF8_Fullchar fullchar = lpr_utf8_full_char( curr_utf8_ptr );
		if ( !fullchar.all )
		{
			break;
		}
		curr_utf8_ptr =
		lpr_utf8_next_char_ptr( curr_utf8_ptr, (lpr_u8*)utf8_string,
		                        str_bytes_size );
		if ( !curr_utf8_ptr )
		{
			// NOTE(theGiallo): this means fullchar is not valid or outside the
			// string end
			break;
		}

		if ( fullchar.subchars[0] == '\n' )
		{
			if ( curr_pt.x > max_x )
			{
				max_x = curr_pt.x;
			}
			curr_pt.x = starting_pt.x;
			curr_pt.y -= line_height;
			col = 0;

			continue;
		}
		if ( fullchar.subchars[0] == '\r' )
		{
			continue;
		}

		lpr_s32 count = 1;
		if ( fullchar.subchars[0] == '\t' )
		{
			count = 8 - ( col % 8 );
			fullchar.subchars[0] = ' ';
		}

		for ( lpr_s32 i = 0; i != count; ++i )
		{
			curr_glyph_id = lpr_glyph_id_from_fullchar( of_size, fullchar );
			float kerning =
			   tot_scale.x *
			   (float)font->kerning_table[prev_glyph_id][curr_glyph_id];
			curr_pt.x += kerning;

			lpr_s32
			i_ret =
			lpr_add_glyph(
			   draw_data, batch_id, multi_font, font_id, size_id,
			   curr_glyph_id, curr_pt, color, scale,
			   px_perfect, viewport_pxs, stencil_id );
			if ( i_ret < 0 && i_ret != -0xface )
			{
				lpr_log_err( "failed to add kerned utf8 char to rendering [batch_id=%u]", batch_id );
				return lpr_false;
			}
			if ( set_z && i_ret != -0xface )
			{
				lpr_set_instance_z( draw_data, batch_id, i_ret, z );
			}
			curr_pt.x +=
			   tot_scale.x *
			   (float)font->glyphs_metrics[curr_glyph_id].advance_width;

			prev_glyph_id = curr_glyph_id;
			++col;
		}
	}

	if ( curr_pt.x > max_x )
	{
		max_x = curr_pt.x;
	}

	if (  glyph_id )
	{
		*glyph_id = prev_glyph_id;
	}
	if ( bounding_box )
	{
		bounding_box->pos.x = starting_pt.x; // TODO(theGiallo, 2016-03-10): should we add the min left bearing?
		bounding_box->pos.y = curr_pt.y +
		                      tot_scale.y * (float)font->metrics.descent;
		bounding_box->size.w = max_x - starting_pt.x;
		// TODO(theGiallo, 2016-03-11): should we keep track of max ascent and descent?
		bounding_box->size.h =
		   starting_pt.y - curr_pt.y +
		   tot_scale.y * (float)(-font->metrics.descent + font->metrics.ascent);
		bounding_box->pos = lpr_V2_add_V2( bounding_box->pos,
		                                   lpr_V2_mult_f32( bounding_box->size,
		                                                    0.5f ) );
	}
	if ( ending_pt )
	{
		*ending_pt = curr_pt;
	}

	if ( sprites_span )
	{
		span.count = draw_data->batches[batch_id].quads_count - span.first;
		*sprites_span = span;
	}

	return lpr_true;
}

LPR_DEF
lpr_bool
lpr_add_multiline_rotated_text( Lpr_Draw_Data * draw_data,
                                lpr_u32 batch_id,
                                Lpr_Multi_Font * multi_font, lpr_s32 font_id,
                                lpr_s32 size_id,
                                Lpr_V2 starting_pt,
                                const char * utf8_string,
                                lpr_u32 str_bytes_size,
                                Lpr_Rgba_u8 color,
                                const lpr_bool set_z, const lpr_u16 z,
                                float scale = 1.0f, float rot_deg = 0.0f,
                                lpr_bool px_perfect = false, lpr_bool viewport_pxs = true,
                                lpr_bool camera_aligned = false,
                                lpr_u32 stencil_id = LPR_STENCIL_NONE,
                                lpr_s32 * glyph_id = NULL,
                                Lpr_Rot_Rect * bounding_box = NULL,
                                Lpr_V2 * ending_pt = NULL,
                                Lpr_Sprites_Span * sprites_span = NULL )
{
	Lpr_Font * font = multi_font->fonts + font_id;
	Lpr_Font_Of_Size * of_size = font->of_size + size_id;
	float font_scale = font->scales[size_id];

	Lpr_Sprites_Span span = LPR_ZERO_STRUCT;
	span.first = draw_data->batches[batch_id].quads_count;

	if ( camera_aligned )
	{
		rot_deg += draw_data->batches[batch_id].camera_angle;
	}

	Lpr_V2 tot_scale = {1.0f,1.0f};
	if ( px_perfect )
	{
		lpr_u32 px_in_w = viewport_pxs ? draw_data->viewport_size.w :
		                                 draw_data->window_size.w;
		tot_scale = lpr_V2_mult_f32(
		               tot_scale,
		               draw_data->batches[batch_id].camera_world_span.w /
		               (float)px_in_w );
	}
	tot_scale = lpr_V2_mult_f32( tot_scale, scale * font_scale );

	float line_height =
	   tot_scale.y *
	   (float)( font->metrics.ascent - font->metrics.descent +
	          font->metrics.line_gap );
	Lpr_V2 curr_pt = starting_pt;
	Lpr_V2 right_dir = { lpr_cos_deg( rot_deg ), lpr_sin_deg( rot_deg ) };
	Lpr_V2 down_dir = {  right_dir.y,
	                -right_dir.x };
	Lpr_V2 down_1_line = lpr_V2_mult_f32( down_dir, line_height );
	Lpr_V2 previous_line_start = starting_pt;
	lpr_s32 curr_glyph_id,
	    prev_glyph_id = 0;
	lpr_u8 * curr_utf8_ptr = (lpr_u8*)utf8_string;
	float max_x = 0;
	float unrot_x = max_x;
	// NOTE(theGiallo): for tabular alignment
	lpr_u32 col = 0;
	lpr_u32 row = 0;

	for (;;)
	{
		if ( !curr_utf8_ptr )
		{
			break;
		}
		Lpr_UTF8_Fullchar fullchar = lpr_utf8_full_char( curr_utf8_ptr );
		if ( !fullchar.all )
		{
			break;
		}
		curr_utf8_ptr =
		lpr_utf8_next_char_ptr( curr_utf8_ptr, (lpr_u8*)utf8_string,
		                        str_bytes_size );
		if ( !curr_utf8_ptr )
		{
			// NOTE(theGiallo): this means fullchar is not valid or outside the
			// string end
			break;
		}
		if ( fullchar.subchars[0] == '\n' )
		{
			if ( unrot_x > max_x )
			{
				max_x = unrot_x;
			}
			previous_line_start = lpr_V2_add_V2( previous_line_start,
			                                     down_1_line );
			curr_pt = previous_line_start;
			unrot_x = 0;
			col = 0;
			++row;

			continue;
		}
		if ( fullchar.subchars[0] == '\r' )
		{
			continue;
		}

		lpr_s32 count = 1;
		if ( fullchar.subchars[0] == '\t' )
		{
			count = 8 - ( col % 8 );
			fullchar.subchars[0] = ' ';
		}

		for ( lpr_s32 i = 0; i != count; ++i )
		{
			curr_glyph_id = lpr_glyph_id_from_fullchar( of_size, fullchar );
			float kerning = tot_scale.x *
			              (float)font->kerning_table[prev_glyph_id][curr_glyph_id];
			curr_pt = lpr_V2_add_V2( curr_pt,
			                         lpr_V2_mult_f32( right_dir, kerning ) );
			unrot_x += kerning;

			lpr_s32
			i_ret =
			lpr_add_rotated_glyph(
			   draw_data, batch_id, multi_font, font_id, size_id,
			   curr_glyph_id, curr_pt, right_dir, rot_deg,
			   color, scale,
			   px_perfect, viewport_pxs, stencil_id );
			if ( i_ret < 0 && i_ret != -0xface )
			{
				lpr_log_err( "failed to add kerned utf8 char to rendering [batch_id=%u]", batch_id );
				return lpr_false;
			}
			if ( set_z && i_ret != -0xface )
			{
				lpr_set_instance_z( draw_data, batch_id, i_ret, z );
			}
			float adv = tot_scale.x *
			          (float)font->glyphs_metrics[curr_glyph_id].advance_width;
			curr_pt = lpr_V2_add_V2( curr_pt,
			                         lpr_V2_mult_f32( right_dir, adv ) );
			unrot_x += adv;

			prev_glyph_id = curr_glyph_id;
			++col;
		}
	}

	if ( unrot_x > max_x )
	{
		max_x = unrot_x;
	}

	if ( glyph_id )
	{
		*glyph_id = prev_glyph_id;
	}

	if ( bounding_box )
	{
		// TODO(theGiallo, 2016-03-10): should we add the min left bearing?
		float start_to_bottom = row * line_height -
		                      tot_scale.y * (float)font->metrics.descent;
		bounding_box->pos = lpr_V2_add_V2( starting_pt,
		                                   lpr_V2_mult_f32( down_dir,
		                                                    start_to_bottom) );
		bounding_box->size.w = max_x;
		// TODO(theGiallo, 2016-03-11): should we keep track of max ascent and descent?
		bounding_box->size.h =
		   start_to_bottom +
		   tot_scale.y * (float)font->metrics.ascent;
		bounding_box->pos = lpr_V2_add_V2( bounding_box->pos,
		                                   lpr_V2_mult_f32(
		                                      right_dir,
		                                      bounding_box->size.w * 0.5f ) );
		bounding_box->pos = lpr_V2_add_V2( bounding_box->pos,
		                                   lpr_V2_mult_f32(
		                                      down_dir,
		                                      -bounding_box->size.h * 0.5f ) );
		bounding_box->rot_deg = rot_deg;
	}

	if ( ending_pt )
	{
		*ending_pt = curr_pt;
	}

	if ( sprites_span )
	{
		span.count = draw_data->batches[batch_id].quads_count - span.first;
		*sprites_span = span;
	}

	return lpr_true;
}

LPR_DEF
lpr_bool
lpr_add_multiline_text_in_rect( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                                Lpr_Multi_Font * multi_font, lpr_s32 font_id,
                                lpr_s32 size_id,
                                Lpr_AA_Rect rect,
                                const char * utf8_string,
                                lpr_u32 str_bytes_size,
                                Lpr_Rgba_u8 color,
                                const lpr_bool set_z, const lpr_u16 z,
                                float scale = 1.0f,
                                lpr_bool px_perfect = false, lpr_bool viewport_pxs = true,
                                lpr_u32 stencil_id = LPR_STENCIL_NONE,
                                lpr_s32 * glyph_id = NULL,
                                Lpr_AA_Rect * aa_bb = NULL, Lpr_V2 * ending_pt = NULL,
                                Lpr_Sprites_Span * sprites_span = NULL )
{
	Lpr_Font * font = multi_font->fonts + font_id;

	Lpr_V2 pos = lpr_V2_add_V2( rect.pos,
	                            lpr_V2_mult_f32( lpr_V2( -rect.size.x,
	                                                          rect.size.y),
	                                             0.5f ) );
	pos.y -= scale * font->metrics.ascent * font->scales[size_id];

	lpr_bool ret =
	lpr_add_multiline_text( draw_data, batch_id, multi_font, font_id, size_id,
	                        pos, utf8_string, str_bytes_size, color, set_z, z,
	                        scale, px_perfect, viewport_pxs, stencil_id,
	                        glyph_id, aa_bb, ending_pt, sprites_span );

	return ret;
}

LPR_DEF
lpr_bool
lpr_add_multiline_text_in_rot_rect( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                                    Lpr_Multi_Font * multi_font,
                                    lpr_s32 font_id,
                                    lpr_s32 size_id,
                                    Lpr_Rot_Rect rect,
                                    const char * utf8_string,
                                    lpr_u32 str_bytes_size,
                                    Lpr_Rgba_u8 color,
                                    const lpr_bool set_z, const lpr_u16 z,
                                    float scale = 1.0f,
                                    lpr_bool px_perfect = false, lpr_bool viewport_pxs = true,
                                    lpr_bool camera_aligned = false,
                                    lpr_u32 stencil_id = LPR_STENCIL_NONE,
                                    lpr_s32 * glyph_id = NULL, Lpr_Rot_Rect * rot_bb = NULL,
                                    Lpr_V2 * ending_pt = NULL,
                                    Lpr_Sprites_Span * sprites_span = NULL )
{
	Lpr_Font * font = multi_font->fonts + font_id;

	float tot_scale = scale;
	if ( px_perfect )
	{
		lpr_u32 px_in_w = viewport_pxs ? draw_data->viewport_size.w :
		                                 draw_data->window_size.w;
		tot_scale *= draw_data->batches[batch_id].camera_world_span.w /
		         (float)px_in_w;
	}
	Lpr_V2 pos = lpr_V2_mult_f32( lpr_V2(-rect.size.x, rect.size.y),
	                                    0.5f );
	pos.y -= tot_scale * font->metrics.ascent * font->scales[size_id];
	pos = lpr_V2_add_V2( rect.pos,
	                     lpr_V2_rotated_deg( pos, rect.rot_deg ) );

	lpr_bool ret =
	lpr_add_multiline_rotated_text( draw_data, batch_id,
	                                multi_font, font_id, size_id,
	                                pos, utf8_string, str_bytes_size, color,
	                                set_z, z,
	                                scale, rect.rot_deg,
	                                px_perfect, viewport_pxs,
	                                camera_aligned,
	                                stencil_id,
	                                glyph_id, rot_bb, ending_pt, sprites_span );

	return ret;
}

LPR_DEF
lpr_bool
lpr_add_multiline_text_in_rect_aligned( Lpr_Draw_Data * draw_data,
                                        lpr_u32 batch_id,
                                        Lpr_Multi_Font * multi_font,
                                        lpr_s32 font_id,
                                        lpr_s32 size_id,
                                        Lpr_AA_Rect rect,
                                        const char * utf8_string,
                                        lpr_u32 str_bytes_size,
                                        const Lpr_Txt_HAlign h_alignment,
                                        const Lpr_Txt_VAlign v_alignment,
                                        Lpr_Rgba_u8 color,
                                        const lpr_bool set_z, const lpr_u16 z,
                                        float scale = 1.0f, lpr_bool px_perfect = false,
                                        lpr_bool viewport_pxs = true,
                                        lpr_u32 stencil_id = LPR_STENCIL_NONE,
                                        lpr_s32 * glyph_id = NULL,
                                        Lpr_AA_Rect * aa_bb = NULL,
                                        Lpr_V2 * ending_pt = NULL,
                                        Lpr_Sprites_Span * sprites_span = NULL )

{
	Lpr_Font * font = multi_font->fonts + font_id;
	Lpr_Font_Of_Size * of_size = font->of_size + size_id;
	float font_scale = font->scales[size_id];

	Lpr_Sprites_Span span = LPR_ZERO_STRUCT;
	span.first = draw_data->batches[batch_id].quads_count;

	float tot_scale = scale;
	float pxpscale = 1.0f;
	Lpr_V2 tot_font_scale = {1.0f,1.0f};
	if ( px_perfect )
	{
		lpr_u32 px_in_w = viewport_pxs ? draw_data->viewport_size.w :
		                                 draw_data->window_size.w;
		pxpscale = draw_data->batches[batch_id].camera_world_span.w /
		           (float)px_in_w;
		tot_scale *= pxpscale;
	}
	tot_font_scale = lpr_V2_mult_f32( tot_font_scale, tot_scale * font_scale );

	float line_height =
	   tot_font_scale.y *
	   (float) ( font->metrics.ascent - font->metrics.descent +
	           font->metrics.line_gap );

	Lpr_V2 starting_pt = lpr_V2_add_V2( rect.pos,
	                     lpr_V2_mult_f32( lpr_V2(-rect.size.x, rect.size.y),
	                                      0.5f ) );

	if ( v_alignment != LPR_TXT_VALIGN_TOP )
	{
		lpr_u32 lines_count = lpr_lines_count( utf8_string );
		switch ( v_alignment )
		{
			case LPR_TXT_VALIGN_BOTTOM:
				starting_pt.y -=
				   rect.size.h - ( lines_count - 1 ) * line_height +
				   font->metrics.descent * tot_font_scale.y;
				break;
			case LPR_TXT_VALIGN_MIDDLE:
				starting_pt.y -=
				   ( rect.size.h - lines_count * line_height +
				     font->metrics.line_gap * tot_font_scale.y ) * 0.5f +
				     font->metrics.ascent * tot_font_scale.y;
				break;
			default:
				LPR_ILLEGAL_PATH();
				break;
		}
	} else
	{
		starting_pt.y -= tot_font_scale.y * font->metrics.ascent;
	}

	Lpr_V2 curr_pt = starting_pt;
	lpr_s32 curr_glyph_id,
	    prev_glyph_id = 0;
	lpr_u8 * curr_utf8_ptr = (lpr_u8*)utf8_string;

	float max_x = starting_pt.x;
	float min_x = LPR_FLT_MAX;

	// NOTE(theGiallo): for tabular alignment
	lpr_u32 col = 0;

	lpr_u32 remaining_string_byte_size = str_bytes_size;

	float actual_line_length = 0.0f;
	{
		float line_length = 0;
		//if ( h_alignment != LPR_TXT_HALIGN_LEFT )
		{
			line_length = tot_scale *
			              lpr_kerned_line_length_px(
			                 multi_font, font_id, size_id,
			                 (const char*)curr_utf8_ptr,
			                 remaining_string_byte_size );
		}
		switch ( h_alignment )
		{
			case LPR_TXT_HALIGN_CENTERED:
				curr_pt.x = starting_pt.x + ( rect.size.w - line_length ) /
				                            2.0f;
				break;
			case LPR_TXT_HALIGN_RIGHT:
				curr_pt.x = starting_pt.x + ( rect.size.w - line_length );
				break;
			case LPR_TXT_HALIGN_LEFT:
				curr_pt.x = starting_pt.x;
				break;
			default:
				LPR_ILLEGAL_PATH();
				break;
		}
		if ( curr_pt.x < min_x )
		{
			min_x = curr_pt.x;
		}
		if ( h_alignment != LPR_TXT_HALIGN_LEFT )
		{
			Lpr_UTF8_Fullchar next_fullchar =
			   lpr_utf8_full_char( (lpr_u8*)curr_utf8_ptr );
			lpr_s32 glyph_id = lpr_glyph_id_from_fullchar( of_size, next_fullchar );
			float lsb = font->glyphs_metrics[glyph_id].left_side_bearing *
			            tot_font_scale.x;
			curr_pt.x -= lsb;
			actual_line_length = -lsb;

		#if 0
			lpr_s32 id =
			lpr_lpr_add_colored_rect(
			   draw_data, batch_id,
			   lpr_V2_add_V2( curr_pt, lpr_V2(line_length * 0.5f + lsb * 2.0f,
			                                  1) ),
			   lpr_V2(line_length, 2.7),
			   Lpr_Rgba_u8{255,0,0, 150} );
			lpr_set_instance_z( draw_data, batch_id, id, z );

			id =
			lpr_lpr_add_colored_rect(
			   draw_data, batch_id,
			   lpr_V2_add_V2(
			      curr_pt,
			      lpr_V2(-(rect.size.w - line_length) * 0.5f + lsb, 1) ),
			   lpr_V2((rect.size.w - line_length), 2),
			   lpr_C_colors_u8.green );
			lpr_set_instance_z( draw_data, batch_id, id, z )
		#endif
		}
	}

	for (;;)
	{
		if ( !curr_utf8_ptr )
		{
			break;
		}
		Lpr_UTF8_Fullchar fullchar = lpr_utf8_full_char( curr_utf8_ptr );
		if ( !fullchar.all )
		{
			break;
		}
		{
			lpr_u8 * old_ptr = curr_utf8_ptr;
			curr_utf8_ptr =
			lpr_utf8_next_char_ptr( curr_utf8_ptr, (lpr_u8*)utf8_string,
			                        str_bytes_size );
			if ( !curr_utf8_ptr )
			{
				// NOTE(theGiallo): this means fullchar is not valid or outside the
				// string end
				break;
			}
			remaining_string_byte_size -= curr_utf8_ptr - old_ptr;
		}
		if ( fullchar.subchars[0] == '\n' )
		{
		#if 0
			lpr_s32 i_ret =
			lpr_lpr_add_colored_rect( draw_data, batch_id,
								 lpr_V2_sub_V2( curr_pt, lpr_V2( actual_line_length * 0.5f, -0.5f ) ),
								 lpr_V2( actual_line_length, 1),
								 {0,255,255,155} );
			lpr_set_instance_z( draw_data, batch_id, i_ret, z );
		#endif
			if ( curr_pt.x > max_x )
			{
				max_x = curr_pt.x;
			}
			float line_length = 0;
			//if ( h_alignment != LPR_TXT_HALIGN_LEFT )
			{
				line_length = tot_scale *
				              lpr_kerned_line_length_px(
				                 multi_font, font_id, size_id,
				                 (const char*)curr_utf8_ptr,
				                 remaining_string_byte_size );
			}
			switch ( h_alignment )
			{
				case LPR_TXT_HALIGN_CENTERED:
					curr_pt.x = starting_pt.x + ( rect.size.w - line_length ) /
					                            2.0f;
					break;
				case LPR_TXT_HALIGN_RIGHT:
					curr_pt.x = starting_pt.x + ( rect.size.w - line_length );
					break;
				case LPR_TXT_HALIGN_LEFT:
					curr_pt.x = starting_pt.x;
					break;
				default:
					LPR_ILLEGAL_PATH();
					break;
			}
			curr_pt.y -= line_height;
			if ( curr_pt.x < min_x )
			{
				min_x = curr_pt.x;
			}

			if ( h_alignment != LPR_TXT_HALIGN_LEFT )
			{
				Lpr_UTF8_Fullchar next_fullchar =
				   lpr_utf8_full_char( (lpr_u8*)curr_utf8_ptr );
				lpr_s32 glyph_id =
				   lpr_glyph_id_from_fullchar( of_size, next_fullchar );
				float lsb = font->glyphs_metrics[glyph_id].left_side_bearing *
				            tot_font_scale.x;
				curr_pt.x -= lsb;
				actual_line_length = -lsb;

			#if 0
				lpr_s32 id =
				lpr_lpr_add_colored_rect(
				   draw_data, batch_id,
				   lpr_V2_add_V2( curr_pt, lpr_V2(line_length * 0.5f + lsb,
				                                  1) ),
				   lpr_V2(line_length, 2.7),
				   Lpr_Rgba_u8{255,0,0, 150} );
				lpr_set_instance_z( draw_data, batch_id, id, z );

				id =
				lpr_lpr_add_colored_rect(
				   draw_data, batch_id,
				   lpr_V2_add_V2(
				      curr_pt,
				      lpr_V2(-(rect.size.w - line_length) * 0.5f + lsb, 1) ),
				   lpr_V2((rect.size.w - line_length), 2),
				   lpr_C_colors_u8.green );
				lpr_set_instance_z( draw_data, batch_id, id, z );
			#endif
				actual_line_length = -lsb;
			}
			col = 0;

			prev_glyph_id = 0;
			//prev_glyph_id = lpr_glyph_id_from_fullchar( of_size, fullchar );
			continue;
		}
		if ( fullchar.subchars[0] == '\r' )
		{
			continue;
		}

		lpr_s32 count = 1;
		if ( fullchar.subchars[0] == '\t' )
		{
			count = 8 - ( col % 8 );
			fullchar.subchars[0] = ' ';
		}

		curr_glyph_id = lpr_glyph_id_from_fullchar( of_size, fullchar );
		for ( lpr_s32 i = 0; i != count; ++i )
		{
			float kerning =
			   tot_font_scale.x *
			   (float)font->kerning_table[prev_glyph_id][curr_glyph_id];
			curr_pt.x += kerning;
			actual_line_length += kerning;

			lpr_s32
			i_ret =
			lpr_add_glyph(
			   draw_data, batch_id, multi_font, font_id, size_id,
			   curr_glyph_id, curr_pt, color, scale,
			   px_perfect, viewport_pxs, stencil_id );
			if ( i_ret < 0 && i_ret != -0xface )
			{
				lpr_log_err( "failed to add kerned utf8 char to rendering [batch_id=%u]", batch_id );
				return lpr_false;
			}
			if ( set_z && i_ret != -0xface )
			{
				lpr_set_instance_z( draw_data, batch_id, i_ret, z );
			}
			curr_pt.x +=
			   tot_font_scale.x *
			   (float)font->glyphs_metrics[curr_glyph_id].advance_width;
			actual_line_length +=
			   tot_font_scale.x *
			   (float)font->glyphs_metrics[curr_glyph_id].advance_width;

			prev_glyph_id = curr_glyph_id;
			++col;
		}
	}
	#if 0
	lpr_s32 i_ret =
	lpr_lpr_add_colored_rect( draw_data, batch_id,
	                      lpr_V2_sub_V2( curr_pt,
	                                     lpr_V2( actual_line_length * 0.5f,
	                                             -0.5f ) ),
	                      lpr_V2( actual_line_length, 1),
	                      {0,255,255,155} );
	lpr_set_instance_z( draw_data, batch_id, i_ret, z );
	#endif

	if ( curr_pt.x > max_x )
	{
		max_x = curr_pt.x;
	}

	if ( glyph_id )
	{
		*glyph_id = prev_glyph_id;
	}

	if ( aa_bb )
	{
		aa_bb->pos.x = min_x;
		aa_bb->pos.y = curr_pt.y + tot_font_scale.y * (float)font->metrics.descent;
		aa_bb->size.w = max_x - min_x;
		// TODO(theGiallo, 2016-03-11): should we keep track of max ascent and descent?
		aa_bb->size.h =
		   starting_pt.y - curr_pt.y +
		   tot_font_scale.y *
		   (float)(-font->metrics.descent + font->metrics.ascent);
		aa_bb->pos = lpr_V2_add_V2( aa_bb->pos,
		                                   lpr_V2_mult_f32( aa_bb->size,
		                                                    0.5f ) );
	}

	if ( ending_pt )
	{
		*ending_pt = curr_pt;
	}

	if ( sprites_span )
	{
		span.count = draw_data->batches[batch_id].quads_count - span.first;
		*sprites_span = span;
	}

	return lpr_true;
}

LPR_DEF
lpr_bool
lpr_add_multiline_text_in_rot_rect_aligned( Lpr_Draw_Data * draw_data,
                                            lpr_u32 batch_id,
                                            Lpr_Multi_Font * multi_font,
                                            lpr_s32 font_id,
                                            lpr_s32 size_id,
                                            Lpr_Rot_Rect rect,
                                            const char * utf8_string,
                                            lpr_u32 str_bytes_size,
                                            const Lpr_Txt_HAlign h_alignment,
                                            const Lpr_Txt_VAlign v_alignment,
                                            Lpr_Rgba_u8 color,
                                            const lpr_bool set_z,
                                            const lpr_u16 z,
                                            float scale = 1.0f, lpr_bool px_perfect = false,
                                            lpr_bool viewport_pxs = true,
                                            lpr_bool camera_aligned = false,
                                            lpr_u32 stencil_id = LPR_STENCIL_NONE,
                                            lpr_s32 * glyph_id = NULL,
                                            Lpr_Rot_Rect * bounding_box = NULL,
                                            Lpr_V2 * ending_pt = NULL,
                                            Lpr_Sprites_Span * sprites_span = NULL )
{
	Lpr_Font * font = multi_font->fonts + font_id;
	Lpr_Font_Of_Size * of_size = font->of_size + size_id;
	float font_scale = font->scales[size_id];

	Lpr_Sprites_Span span = LPR_ZERO_STRUCT;
	span.first = draw_data->batches[batch_id].quads_count;

	if ( camera_aligned )
	{
		rect.rot_deg += draw_data->batches[batch_id].camera_angle;
	}

	float tot_scale = scale;
	float pxpscale = 1.0f;
	Lpr_V2 tot_font_scale = {1.0f,1.0f};
	if ( px_perfect )
	{
		lpr_u32 px_in_w = viewport_pxs ? draw_data->viewport_size.w :
		                                 draw_data->window_size.w;
		pxpscale = draw_data->batches[batch_id].camera_world_span.w /
		           (float)px_in_w;
		tot_scale *= pxpscale;
	}
	tot_font_scale = lpr_V2_mult_f32( tot_font_scale, tot_scale * font_scale );
	float line_height =
	   tot_font_scale.y *
	   (float)( font->metrics.ascent - font->metrics.descent +
	          font->metrics.line_gap );

	Lpr_V2 starting_pt = lpr_V2_mult_f32( lpr_V2(-rect.size.x, rect.size.y),
	                                      0.5f );

	if ( v_alignment != LPR_TXT_VALIGN_TOP )
	{
		lpr_u32 lines_count = lpr_lines_count( utf8_string );
		switch ( v_alignment )
		{
			case LPR_TXT_VALIGN_BOTTOM:
				starting_pt.y -=
				   rect.size.h - ( lines_count - 1 ) * line_height +
				   font->metrics.descent * tot_font_scale.y;
				break;
			case LPR_TXT_VALIGN_MIDDLE:
				starting_pt.y -=
				   ( rect.size.h - lines_count * line_height +
				     font->metrics.line_gap * tot_font_scale.y ) * 0.5f +
				     font->metrics.ascent * tot_font_scale.y;
				break;
			default:
				LPR_ILLEGAL_PATH();
				break;
		}
	} else
	{
		starting_pt.y -= tot_font_scale.y * font->metrics.ascent;
	}

	starting_pt = lpr_V2_add_V2( rect.pos,
	                             lpr_V2_rotated_deg( starting_pt,
	                                                 rect.rot_deg ) );

	Lpr_V2 curr_pt = starting_pt;
	Lpr_V2 right_dir = { lpr_cos_deg( rect.rot_deg ),
	                     lpr_sin_deg( rect.rot_deg ) };
	Lpr_V2 down_dir = {  right_dir.y,
	                -right_dir.x };
	Lpr_V2 down_1_line = lpr_V2_mult_f32( down_dir, line_height );
	Lpr_V2 previous_line_start = starting_pt;
	lpr_s32 curr_glyph_id,
	    prev_glyph_id = 0;
	lpr_u8 * curr_utf8_ptr = (lpr_u8*)utf8_string;
	float max_x = 0;
	float min_x = rect.size.w;
	float unrot_x = max_x;
	// NOTE(theGiallo): for tabular alignment
	lpr_u32 col = 0;
	lpr_u32 row = 0;

	lpr_u32 remaining_string_byte_size = str_bytes_size;

	float actual_line_length = 0.0f;
	{
		float line_length = 0;
		//if ( h_alignment != LPR_TXT_HALIGN_LEFT )
		{
			line_length = tot_scale *
			              lpr_kerned_line_length_px(
			                 multi_font, font_id, size_id,
			                 (const char*)curr_utf8_ptr,
			                 remaining_string_byte_size );
		}
		switch ( h_alignment )
		{
			case LPR_TXT_HALIGN_CENTERED:
				unrot_x = ( rect.size.w - line_length ) / 2.0f;
				curr_pt = lpr_V2_add_V2( curr_pt,
				                         lpr_V2_mult_f32(
				                            right_dir,
				                            unrot_x ) );
				break;
			case LPR_TXT_HALIGN_RIGHT:
				unrot_x = rect.size.w - line_length;
				curr_pt = lpr_V2_add_V2( curr_pt,
				                         lpr_V2_mult_f32(
				                            right_dir,
				                            unrot_x ) );
				break;
			case LPR_TXT_HALIGN_LEFT:
				unrot_x = 0;
				break;
			default:
				LPR_ILLEGAL_PATH();
				break;
		}
		if ( unrot_x < min_x )
		{
			min_x = unrot_x;
		}

		if ( h_alignment != LPR_TXT_HALIGN_LEFT )
		{
			Lpr_UTF8_Fullchar next_fullchar =
			   lpr_utf8_full_char( (lpr_u8*)curr_utf8_ptr );
			lpr_s32 glyph_id = lpr_glyph_id_from_fullchar( of_size, next_fullchar );
			float lsb = font->glyphs_metrics[glyph_id].left_side_bearing *
			          tot_font_scale.x;
			curr_pt = lpr_V2_sub_V2( curr_pt,
			                         lpr_V2_mult_f32( right_dir, lsb ) );
			actual_line_length = -lsb;
		}
	}


	for (;;)
	{
		if ( !curr_utf8_ptr )
		{
			break;
		}
		Lpr_UTF8_Fullchar fullchar = lpr_utf8_full_char( curr_utf8_ptr );
		if ( !fullchar.all )
		{
			break;
		}
		{
			lpr_u8 * old_ptr = curr_utf8_ptr;
			curr_utf8_ptr =
			lpr_utf8_next_char_ptr( curr_utf8_ptr, (lpr_u8*)utf8_string,
			                        str_bytes_size );
			if ( !curr_utf8_ptr )
			{
				// NOTE(theGiallo): this means fullchar is not valid or outside the
				// string end
				break;
			}
			remaining_string_byte_size -= curr_utf8_ptr - old_ptr;
		}
		if ( fullchar.subchars[0] == '\n' )
		{
			if ( unrot_x > max_x )
			{
				max_x = unrot_x;
			}
			previous_line_start = lpr_V2_add_V2( previous_line_start,
			                                     down_1_line );
			curr_pt = previous_line_start;
			float line_length = 0;
			//if ( h_alignment != LPR_TXT_HALIGN_LEFT )
			{
				line_length = tot_scale *
				              lpr_kerned_line_length_px(
				                 multi_font, font_id, size_id,
				                 (const char*)curr_utf8_ptr,
				                 remaining_string_byte_size );
			}
			switch ( h_alignment )
			{
				case LPR_TXT_HALIGN_CENTERED:
					unrot_x = ( rect.size.w - line_length ) / 2.0f;
					curr_pt = lpr_V2_add_V2( curr_pt,
					                         lpr_V2_mult_f32(
					                            right_dir,
					                            unrot_x ) );
					break;
				case LPR_TXT_HALIGN_RIGHT:
					unrot_x = rect.size.w - line_length;
					curr_pt = lpr_V2_add_V2( curr_pt,
					                         lpr_V2_mult_f32(
					                            right_dir,
					                            unrot_x ) );
					break;
				case LPR_TXT_HALIGN_LEFT:
					unrot_x = 0;
					break;
				default:
					LPR_ILLEGAL_PATH();
					break;
			}
			if ( unrot_x < min_x )
			{
				min_x = unrot_x;
			}
			Lpr_UTF8_Fullchar next_fullchar =
			   lpr_utf8_full_char( (lpr_u8*)curr_utf8_ptr );
			lpr_s32 glyph_id = lpr_glyph_id_from_fullchar( of_size, next_fullchar );
			float lsb = font->glyphs_metrics[glyph_id].left_side_bearing *
			          tot_font_scale.x;
			curr_pt = lpr_V2_sub_V2( curr_pt,
			                         lpr_V2_mult_f32( right_dir, lsb ) );
			actual_line_length = -lsb;

			col = 0;
			++row;

			continue;
		}
		if ( fullchar.subchars[0] == '\r' )
		{
			continue;
		}

		lpr_s32 count = 1;
		if ( fullchar.subchars[0] == '\t' )
		{
			count = 8 - ( col % 8 );
			fullchar.subchars[0] = ' ';
		}

		for ( lpr_s32 i = 0; i != count; ++i )
		{
			curr_glyph_id = lpr_glyph_id_from_fullchar( of_size, fullchar );
			float kerning =
			   tot_font_scale.x *
			   (float)font->kerning_table[prev_glyph_id][curr_glyph_id];
			curr_pt = lpr_V2_add_V2( curr_pt,
			                         lpr_V2_mult_f32( right_dir, kerning ) );
			unrot_x += kerning;

			lpr_s32
			i_ret =
			lpr_add_rotated_glyph(
			   draw_data, batch_id, multi_font, font_id, size_id,
			   curr_glyph_id, curr_pt, right_dir, rect.rot_deg,
			   color, scale,
			   px_perfect, viewport_pxs, stencil_id );
			if ( i_ret < 0 && i_ret != -0xface )
			{
				lpr_log_err( "failed to add kerned utf8 char to rendering [batch_id=%u]", batch_id );
				return lpr_false;
			}
			if ( set_z && i_ret != -0xface )
			{
				lpr_set_instance_z( draw_data, batch_id, i_ret, z );
			}
			float adv = tot_font_scale.x *
			          (float)font->glyphs_metrics[curr_glyph_id].advance_width;
			curr_pt = lpr_V2_add_V2( curr_pt,
			                         lpr_V2_mult_f32( right_dir, adv ) );
			unrot_x += adv;

			prev_glyph_id = curr_glyph_id;
			++col;
		}
	}

	if ( unrot_x > max_x )
	{
		max_x = unrot_x;
	}

	if ( glyph_id )
	{
		*glyph_id = prev_glyph_id;
	}

	if ( bounding_box )
	{
		float start_to_bottom = row * line_height -
		                      tot_font_scale.y * (float)font->metrics.descent;
		bounding_box->pos = lpr_V2_add_V2( starting_pt,
		                                   lpr_V2_mult_f32( down_dir,
		                                                    start_to_bottom) );
		bounding_box->pos = lpr_V2_add_V2( bounding_box->pos,
		                                   lpr_V2_mult_f32( right_dir,
		                                                    min_x) );
		bounding_box->size.w = max_x - min_x;
		// TODO(theGiallo, 2016-03-11): should we keep track of max ascent and descent?
		bounding_box->size.h =
		   start_to_bottom +
		   tot_font_scale.y * (float)font->metrics.ascent;
		bounding_box->pos = lpr_V2_add_V2( bounding_box->pos,
		                                   lpr_V2_mult_f32(
		                                      right_dir,
		                                      bounding_box->size.w * 0.5f ) );
		bounding_box->pos = lpr_V2_add_V2( bounding_box->pos,
		                                   lpr_V2_mult_f32(
		                                      down_dir,
		                                      -bounding_box->size.h * 0.5f ) );
		bounding_box->rot_deg = rect.rot_deg;
	}

	if ( ending_pt )
	{
		*ending_pt = curr_pt;
	}

	if ( sprites_span )
	{
		span.count = draw_data->batches[batch_id].quads_count - span.first;
		*sprites_span = span;
	}

	return lpr_true;
}

LPR_DEF
lpr_bool
lpr_add_multiline_text_in_rot_rect_aligned_wrap(
   Lpr_Draw_Data * draw_data,
   lpr_u32 batch_id,
   Lpr_Multi_Font * multi_font,
   lpr_s32 font_id,
   lpr_s32 size_id,
   Lpr_Rot_Rect rect,
   const char * utf8_string,
   lpr_u32 str_bytes_size,
   const Lpr_Txt_HAlign h_alignment,
   const Lpr_Txt_VAlign v_alignment,
   const Lpr_Txt_Wrap wrap,
   Lpr_Rgba_u8 color,
   const lpr_bool set_z,
   const lpr_u16 z,
   float scale = 1.0f,
   lpr_bool px_perfect = false,
   lpr_bool viewport_pxs = true,
   lpr_bool camera_aligned = false,
   lpr_u32 stencil_id = LPR_STENCIL_NONE,
   lpr_s32 * glyph_id = NULL,
   Lpr_Rot_Rect * bounding_box = NULL,
   Lpr_V2 * ending_pt = NULL,
   Lpr_Sprites_Span * sprites_span = NULL )
{
	Lpr_Font * font = multi_font->fonts + font_id;
	Lpr_Font_Of_Size * of_size = font->of_size + size_id;
	float font_scale = font->scales[size_id];

	Lpr_Sprites_Span span = LPR_ZERO_STRUCT;
	span.first = draw_data->batches[batch_id].quads_count;

	if ( camera_aligned )
	{
		rect.rot_deg += draw_data->batches[batch_id].camera_angle;
	}

	float tot_scale = scale;
	float pxpscale = 1.0f;
	Lpr_V2 tot_font_scale = {1.0f,1.0f};
	if ( px_perfect )
	{
		lpr_u32 px_in_w = viewport_pxs ? draw_data->viewport_size.w :
		                                 draw_data->window_size.w;
		pxpscale = draw_data->batches[batch_id].camera_world_span.w /
		           (float)px_in_w;
	}
	tot_font_scale = lpr_V2_mult_f32( tot_font_scale, tot_scale * font_scale );

	float line_height =
	   tot_font_scale.y *
	   (float)( font->metrics.ascent - font->metrics.descent +
	          font->metrics.line_gap );

	float world_to_font_px_scale = 1.0f / tot_scale;
	float max_px_width = rect.size.w * world_to_font_px_scale;

	Lpr_V2 starting_pt = lpr_V2_mult_f32(
	                        lpr_V2(-rect.size.x, rect.size.y), 0.5f );

	if ( v_alignment != LPR_TXT_VALIGN_TOP )
	{
		lpr_u32 lines_count =
		   lpr_lines_count_wrap( multi_font, font_id, size_id, utf8_string,
		                         str_bytes_size, wrap, max_px_width, lpr_true );
		switch ( v_alignment )
		{
			case LPR_TXT_VALIGN_BOTTOM:
				starting_pt.y -=
				   rect.size.h - ( lines_count - 1 ) * line_height +
				   font->metrics.descent * tot_font_scale.y;
				break;
			case LPR_TXT_VALIGN_MIDDLE:
				starting_pt.y -=
				   ( rect.size.h - lines_count * line_height +
				     font->metrics.line_gap * tot_font_scale.y ) * 0.5f +
				     font->metrics.ascent * tot_font_scale.y;
				break;
			default:
				LPR_ILLEGAL_PATH();
				break;
		}
	} else
	{
		starting_pt.y -= tot_font_scale.y * font->metrics.ascent;
	}

	starting_pt = lpr_V2_add_V2( rect.pos,
	                             lpr_V2_rotated_deg( starting_pt,
	                                                 rect.rot_deg ) );

	Lpr_V2 curr_pt = starting_pt;
	Lpr_V2 right_dir = { lpr_cos_deg( rect.rot_deg ),
	                     lpr_sin_deg( rect.rot_deg ) };
	Lpr_V2 down_dir = {  right_dir.y,
	                -right_dir.x };
	Lpr_V2 down_1_line = lpr_V2_mult_f32( down_dir, line_height );
	Lpr_V2 previous_line_start = starting_pt;
	lpr_s32 curr_glyph_id,
	    prev_glyph_id = 0;
	lpr_u8 * curr_utf8_ptr = (lpr_u8*)utf8_string;
	lpr_u8 * next_utf8_ptr = NULL;
	float max_x = 0;
	float min_x = rect.size.w;
	float unrot_x = max_x;
	// NOTE(theGiallo): for tabular alignment
	lpr_u32 col = 0;
	lpr_u32 row = 0;
	lpr_u32 num_chars_to_draw = 0;

	lpr_u32 remaining_string_byte_size = str_bytes_size;

	float actual_line_length = 0.0f;
	{
		float line_length = 0;
		const char * foo = NULL;
		line_length = tot_scale *
		              lpr_line_length_px_wrap(
		                 multi_font, font_id, size_id,
		                 (const char*)curr_utf8_ptr,
		                 remaining_string_byte_size,
		                 wrap, max_px_width,
		                 lpr_true, &num_chars_to_draw, &foo );
		switch ( h_alignment )
		{
			case LPR_TXT_HALIGN_CENTERED:
				unrot_x = ( rect.size.w - line_length ) / 2.0f;
				curr_pt = lpr_V2_add_V2( curr_pt,
				                         lpr_V2_mult_f32(
				                            right_dir,
				                            unrot_x ) );
				break;
			case LPR_TXT_HALIGN_RIGHT:
				unrot_x = rect.size.w - line_length;
				curr_pt = lpr_V2_add_V2( curr_pt,
				                         lpr_V2_mult_f32(
				                            right_dir,
				                            unrot_x ) );
				break;
			case LPR_TXT_HALIGN_LEFT:
				unrot_x = 0;
				break;
			default:
				LPR_ILLEGAL_PATH();
				break;
		}
		if ( unrot_x < min_x )
		{
			min_x = unrot_x;
		}

		if ( h_alignment != LPR_TXT_HALIGN_LEFT )
		{
			Lpr_UTF8_Fullchar next_fullchar =
			   lpr_utf8_full_char( (lpr_u8*)curr_utf8_ptr );
			lpr_s32 glyph_id = lpr_glyph_id_from_fullchar( of_size, next_fullchar );
			float lsb = font->glyphs_metrics[glyph_id].left_side_bearing *
			          tot_font_scale.x;
			curr_pt = lpr_V2_sub_V2( curr_pt,
			                         lpr_V2_mult_f32( right_dir, lsb ) );
			actual_line_length = -lsb;
		}
	}


	for (;;)
	{
		if ( !curr_utf8_ptr )
		{
			break;
		}
		Lpr_UTF8_Fullchar fullchar = lpr_utf8_full_char( curr_utf8_ptr );
		if ( !fullchar.all )
		{
			break;
		}
		next_utf8_ptr =
		lpr_utf8_next_char_ptr( curr_utf8_ptr, (lpr_u8*)utf8_string,
		                        str_bytes_size );
		if ( !next_utf8_ptr )
		{
			// NOTE(theGiallo): this means fullchar is not valid or outside the
			// string end
			break;
		}


		if ( !num_chars_to_draw )
		{
			if ( unrot_x > max_x )
			{
				max_x = unrot_x;
			}
			previous_line_start = lpr_V2_add_V2( previous_line_start,
			                                     down_1_line );
			curr_pt = previous_line_start;
			float line_length = 0;
			const char * foo = NULL;
			line_length = tot_scale *
			              lpr_line_length_px_wrap(
			                 multi_font, font_id, size_id,
			                 (const char*)curr_utf8_ptr,
			                 remaining_string_byte_size,
			                 wrap, rect.size.w * world_to_font_px_scale,
			                 lpr_true, &num_chars_to_draw, &foo );

			switch ( h_alignment )
			{
				case LPR_TXT_HALIGN_CENTERED:
					unrot_x = ( rect.size.w - line_length ) / 2.0f;
					curr_pt = lpr_V2_add_V2( curr_pt,
					                         lpr_V2_mult_f32(
					                            right_dir,
					                            unrot_x ) );
					break;
				case LPR_TXT_HALIGN_RIGHT:
					unrot_x = rect.size.w - line_length;
					curr_pt = lpr_V2_add_V2( curr_pt,
					                         lpr_V2_mult_f32(
					                            right_dir,
					                            unrot_x ) );
					break;
				case LPR_TXT_HALIGN_LEFT:
					unrot_x = 0;
					break;
				default:
					LPR_ILLEGAL_PATH();
					break;
			}
			if ( unrot_x < min_x )
			{
				min_x = unrot_x;
			}
			Lpr_UTF8_Fullchar next_fullchar =
			   lpr_utf8_full_char( (lpr_u8*)curr_utf8_ptr );
			lpr_s32 glyph_id = lpr_glyph_id_from_fullchar( of_size, next_fullchar );
			float lsb = font->glyphs_metrics[glyph_id].left_side_bearing *
			          tot_font_scale.x;
			curr_pt = lpr_V2_sub_V2( curr_pt,
			                         lpr_V2_mult_f32( right_dir, lsb ) );
			actual_line_length = -lsb;

			++row;
		}

		{
			remaining_string_byte_size -= next_utf8_ptr - curr_utf8_ptr;
			curr_utf8_ptr = next_utf8_ptr;
		}

		--num_chars_to_draw;

		if ( fullchar.subchars[0] == '\n' )
		{
			col = 0;
		}
		if ( fullchar.subchars[0] == '\n' ||
		     fullchar.subchars[0] == '\r' )
		{
			continue;
		}

		lpr_s32 count = 1;
		if ( fullchar.subchars[0] == '\t' )
		{
			count = 8 - ( col % 8 );
			fullchar.subchars[0] = ' ';
		}

		for ( lpr_s32 i = 0; i != count; ++i )
		{
			curr_glyph_id = lpr_glyph_id_from_fullchar( of_size, fullchar );
			float kerning =
			   tot_font_scale.x *
			   (float)font->kerning_table[prev_glyph_id][curr_glyph_id];
			curr_pt = lpr_V2_add_V2( curr_pt,
			                         lpr_V2_mult_f32( right_dir, kerning ) );
			unrot_x += kerning;

			lpr_s32
			i_ret =
			lpr_add_rotated_glyph(
			   draw_data, batch_id, multi_font, font_id, size_id,
			   curr_glyph_id, curr_pt, right_dir, rect.rot_deg,
			   color, scale,
			   px_perfect, viewport_pxs, stencil_id );
			if ( i_ret < 0 && i_ret != -0xface )
			{
				lpr_log_err( "failed to add kerned utf8 char to rendering [batch_id=%u]", batch_id );
				return lpr_false;
			}
			if ( set_z && i_ret != -0xface )
			{
				lpr_set_instance_z( draw_data, batch_id, i_ret, z );
			}
			float adv = tot_font_scale.x *
			          (float)font->glyphs_metrics[curr_glyph_id].advance_width;
			curr_pt = lpr_V2_add_V2( curr_pt,
			                         lpr_V2_mult_f32( right_dir, adv ) );
			unrot_x += adv;

			prev_glyph_id = curr_glyph_id;
			++col;
		}
	}

	if ( unrot_x > max_x )
	{
		max_x = unrot_x;
	}

	if ( glyph_id )
	{
		*glyph_id = prev_glyph_id;
	}

	if ( bounding_box )
	{
		float start_to_bottom = row * line_height -
		                      tot_font_scale.y * (float)font->metrics.descent;
		bounding_box->pos = lpr_V2_add_V2( starting_pt,
		                                   lpr_V2_mult_f32( down_dir,
		                                                    start_to_bottom) );
		bounding_box->pos = lpr_V2_add_V2( bounding_box->pos,
		                                   lpr_V2_mult_f32( right_dir,
		                                                    min_x) );
		bounding_box->size.w = max_x - min_x;
		// TODO(theGiallo, 2016-03-11): should we keep track of max ascent and descent?
		bounding_box->size.h =
		   start_to_bottom +
		   tot_font_scale.y * (float)font->metrics.ascent;
		bounding_box->pos = lpr_V2_add_V2( bounding_box->pos,
		                                   lpr_V2_mult_f32(
		                                      right_dir,
		                                      bounding_box->size.w * 0.5f ) );
		bounding_box->pos = lpr_V2_add_V2( bounding_box->pos,
		                                   lpr_V2_mult_f32(
		                                      down_dir,
		                                      -bounding_box->size.h * 0.5f ) );
		bounding_box->rot_deg = rect.rot_deg;
	}

	if ( ending_pt )
	{
		*ending_pt = curr_pt;
	}

	if ( sprites_span )
	{
		span.count = draw_data->batches[batch_id].quads_count - span.first;
		*sprites_span = span;
	}

	return lpr_true;
}

LPR_DEF
lpr_bool
lpr_add_multiline_text_in_rot_rect_aligned_wrap_clipped(
   Lpr_Draw_Data * draw_data,
   lpr_u32 batch_id,
   Lpr_Multi_Font * multi_font,
   lpr_s32 font_id,
   lpr_s32 size_id,
   Lpr_Rot_Rect rect,
   const char * utf8_string,
   lpr_u32 str_bytes_size,
   const Lpr_Txt_HAlign h_alignment,
   const Lpr_Txt_VAlign v_alignment,
   const Lpr_Txt_Wrap wrap,
   Lpr_Rgba_u8 color,
   // NOTE(theGiallo): LPR_ZERO means no clipping
   Lpr_AA_Rect clipping_rect,
   const lpr_bool set_z,
   const lpr_u16 z,
   float scale = 1.0f,
   lpr_bool px_perfect = false,
   lpr_bool viewport_pxs = true,
   lpr_bool camera_aligned = false,
   lpr_u32 stencil_id = LPR_STENCIL_NONE,
   lpr_s32 * glyph_id = NULL,
   Lpr_Rot_Rect * bounding_box = NULL,
   Lpr_V2 * ending_pt = NULL,
   Lpr_Sprites_Span * sprites_span = NULL )
{
	Lpr_Font * font = multi_font->fonts + font_id;
	Lpr_Font_Of_Size * of_size = font->of_size + size_id;
	float font_scale = font->scales[size_id];

	Lpr_Sprites_Span span = LPR_ZERO_STRUCT;
	span.first = draw_data->batches[batch_id].quads_count;

	if ( camera_aligned )
	{
		rect.rot_deg += draw_data->batches[batch_id].camera_angle;
	}

	float tot_scale = scale;
	float pxpscale = 1.0f;
	Lpr_V2 tot_font_scale = {1.0f,1.0f};
	if ( px_perfect )
	{
		lpr_u32 px_in_w = viewport_pxs ? draw_data->viewport_size.w :
		                                 draw_data->window_size.w;
		pxpscale = draw_data->batches[batch_id].camera_world_span.w /
		           (float)px_in_w;
	}
	tot_font_scale = lpr_V2_mult_f32( tot_font_scale, tot_scale * font_scale );

	float line_height =
	   tot_font_scale.y *
	   (float)( font->metrics.ascent - font->metrics.descent +
	          font->metrics.line_gap );

	float world_to_font_px_scale = 1.0f / tot_scale;
	float max_px_width = rect.size.w * world_to_font_px_scale;

	Lpr_V2 starting_pt = lpr_V2_mult_f32(
	                        lpr_V2(-rect.size.x, rect.size.y), 0.5f );

	if ( v_alignment != LPR_TXT_VALIGN_TOP )
	{
		lpr_u32 lines_count =
		   lpr_lines_count_wrap( multi_font, font_id, size_id, utf8_string,
		                         str_bytes_size, wrap, max_px_width, lpr_true );
		switch ( v_alignment )
		{
			case LPR_TXT_VALIGN_BOTTOM:
				starting_pt.y -=
				   rect.size.h - ( lines_count - 1 ) * line_height +
				   font->metrics.descent * tot_font_scale.y;
				break;
			case LPR_TXT_VALIGN_MIDDLE:
				starting_pt.y -=
				   ( rect.size.h - lines_count * line_height +
				     font->metrics.line_gap * tot_font_scale.y ) * 0.5f +
				     font->metrics.ascent * tot_font_scale.y;
				break;
			default:
				LPR_ILLEGAL_PATH();
				break;
		}
	} else
	{
		starting_pt.y -= tot_font_scale.y * font->metrics.ascent;
	}

	starting_pt = lpr_V2_add_V2( rect.pos,
	                             lpr_V2_rotated_deg( starting_pt,
	                                                 rect.rot_deg ) );

	Lpr_V2 curr_pt = starting_pt;
	Lpr_V2 right_dir = { lpr_cos_deg( rect.rot_deg ),
	                     lpr_sin_deg( rect.rot_deg ) };
	Lpr_V2 down_dir = {  right_dir.y,
	                -right_dir.x };
	Lpr_V2 down_1_line = lpr_V2_mult_f32( down_dir, line_height );
	Lpr_V2 previous_line_start = starting_pt;
	lpr_s32 curr_glyph_id,
	    prev_glyph_id = 0;
	lpr_u8 * curr_utf8_ptr = (lpr_u8*)utf8_string;
	lpr_u8 * next_utf8_ptr = NULL;
	float max_x = 0;
	float min_x = rect.size.w;
	float unrot_x = max_x;
	// NOTE(theGiallo): for tabular alignment
	lpr_u32 col = 0;
	lpr_u32 row = 0;
	lpr_u32 num_chars_to_draw = 0;

	lpr_u32 remaining_string_byte_size = str_bytes_size;

	float actual_line_length = 0.0f;
	{
		float line_length = 0;
		const char * foo = NULL;
		line_length = tot_scale *
		              lpr_line_length_px_wrap(
		                 multi_font, font_id, size_id,
		                 (const char*)curr_utf8_ptr,
		                 remaining_string_byte_size,
		                 wrap, max_px_width,
		                 lpr_true, &num_chars_to_draw, &foo );
		switch ( h_alignment )
		{
			case LPR_TXT_HALIGN_CENTERED:
				unrot_x = ( rect.size.w - line_length ) / 2.0f;
				curr_pt = lpr_V2_add_V2( curr_pt,
				                         lpr_V2_mult_f32(
				                            right_dir,
				                            unrot_x ) );
				break;
			case LPR_TXT_HALIGN_RIGHT:
				unrot_x = rect.size.w - line_length;
				curr_pt = lpr_V2_add_V2( curr_pt,
				                         lpr_V2_mult_f32(
				                            right_dir,
				                            unrot_x ) );
				break;
			case LPR_TXT_HALIGN_LEFT:
				unrot_x = 0;
				break;
			default:
				LPR_ILLEGAL_PATH();
				break;
		}
		if ( unrot_x < min_x )
		{
			min_x = unrot_x;
		}

		if ( h_alignment != LPR_TXT_HALIGN_LEFT )
		{
			Lpr_UTF8_Fullchar next_fullchar =
			   lpr_utf8_full_char( (lpr_u8*)curr_utf8_ptr );
			lpr_s32 glyph_id = lpr_glyph_id_from_fullchar( of_size, next_fullchar );
			float lsb = font->glyphs_metrics[glyph_id].left_side_bearing *
			          tot_font_scale.x;
			curr_pt = lpr_V2_sub_V2( curr_pt,
			                         lpr_V2_mult_f32( right_dir, lsb ) );
			actual_line_length = -lsb;
		}
	}


	for (;;)
	{
		if ( !curr_utf8_ptr )
		{
			break;
		}
		Lpr_UTF8_Fullchar fullchar = lpr_utf8_full_char( curr_utf8_ptr );
		if ( !fullchar.all )
		{
			break;
		}
		next_utf8_ptr =
		lpr_utf8_next_char_ptr( curr_utf8_ptr, (lpr_u8*)utf8_string,
		                        str_bytes_size );
		if ( !next_utf8_ptr )
		{
			// NOTE(theGiallo): this means fullchar is not valid or outside the
			// string end
			break;
		}


		if ( !num_chars_to_draw )
		{
			if ( unrot_x > max_x )
			{
				max_x = unrot_x;
			}
			previous_line_start = lpr_V2_add_V2( previous_line_start,
			                                     down_1_line );
			curr_pt = previous_line_start;
			float line_length = 0;
			const char * foo = NULL;
			line_length = tot_scale *
			              lpr_line_length_px_wrap(
			                 multi_font, font_id, size_id,
			                 (const char*)curr_utf8_ptr,
			                 remaining_string_byte_size,
			                 wrap, rect.size.w * world_to_font_px_scale,
			                 lpr_true, &num_chars_to_draw, &foo );

			switch ( h_alignment )
			{
				case LPR_TXT_HALIGN_CENTERED:
					unrot_x = ( rect.size.w - line_length ) / 2.0f;
					curr_pt = lpr_V2_add_V2( curr_pt,
					                         lpr_V2_mult_f32(
					                            right_dir,
					                            unrot_x ) );
					break;
				case LPR_TXT_HALIGN_RIGHT:
					unrot_x = rect.size.w - line_length;
					curr_pt = lpr_V2_add_V2( curr_pt,
					                         lpr_V2_mult_f32(
					                            right_dir,
					                            unrot_x ) );
					break;
				case LPR_TXT_HALIGN_LEFT:
					unrot_x = 0;
					break;
				default:
					LPR_ILLEGAL_PATH();
					break;
			}
			if ( unrot_x < min_x )
			{
				min_x = unrot_x;
			}
			Lpr_UTF8_Fullchar next_fullchar =
			   lpr_utf8_full_char( (lpr_u8*)curr_utf8_ptr );
			lpr_s32 glyph_id = lpr_glyph_id_from_fullchar( of_size, next_fullchar );
			float lsb = font->glyphs_metrics[glyph_id].left_side_bearing *
			          tot_font_scale.x;
			curr_pt = lpr_V2_sub_V2( curr_pt,
			                         lpr_V2_mult_f32( right_dir, lsb ) );
			actual_line_length = -lsb;

			++row;
		}

		{
			remaining_string_byte_size -= next_utf8_ptr - curr_utf8_ptr;
			curr_utf8_ptr = next_utf8_ptr;
		}

		--num_chars_to_draw;

		if ( fullchar.subchars[0] == '\n' )
		{
			col = 0;
		}
		if ( fullchar.subchars[0] == '\n' ||
		     fullchar.subchars[0] == '\r' )
		{
			continue;
		}

		lpr_s32 count = 1;
		if ( fullchar.subchars[0] == '\t' )
		{
			count = 8 - ( col % 8 );
			fullchar.subchars[0] = ' ';
		}

		for ( lpr_s32 i = 0; i != count; ++i )
		{
			curr_glyph_id = lpr_glyph_id_from_fullchar( of_size, fullchar );
			float kerning =
			   tot_font_scale.x *
			   (float)font->kerning_table[prev_glyph_id][curr_glyph_id];
			curr_pt = lpr_V2_add_V2( curr_pt,
			                         lpr_V2_mult_f32( right_dir, kerning ) );
			unrot_x += kerning;

			lpr_s32
			i_ret =
			lpr_add_rotated_glyph_clipped(
			   draw_data, batch_id, multi_font, font_id, size_id,
			   curr_glyph_id, curr_pt, right_dir, rect.rot_deg,
			   color, clipping_rect, scale,
			   px_perfect, viewport_pxs, stencil_id );
			if ( i_ret < 0 && i_ret != -0xface && i_ret != LPR_CLIPPED_OUT )
			{
				lpr_log_err( "failed to add kerned utf8 char to rendering [batch_id=%u]", batch_id );
				return lpr_false;
			}
			if ( set_z && i_ret != -0xface && i_ret != LPR_CLIPPED_OUT )
			{
				lpr_set_instance_z( draw_data, batch_id, i_ret, z );
			}
			float adv = tot_font_scale.x *
			          (float)font->glyphs_metrics[curr_glyph_id].advance_width;
			curr_pt = lpr_V2_add_V2( curr_pt,
			                         lpr_V2_mult_f32( right_dir, adv ) );
			unrot_x += adv;

			prev_glyph_id = curr_glyph_id;
			++col;
		}
	}

	if ( unrot_x > max_x )
	{
		max_x = unrot_x;
	}

	if ( glyph_id )
	{
		*glyph_id = prev_glyph_id;
	}

	if ( bounding_box )
	{
		float start_to_bottom = row * line_height -
		                      tot_font_scale.y * (float)font->metrics.descent;
		bounding_box->pos = lpr_V2_add_V2( starting_pt,
		                                   lpr_V2_mult_f32( down_dir,
		                                                    start_to_bottom) );
		bounding_box->pos = lpr_V2_add_V2( bounding_box->pos,
		                                   lpr_V2_mult_f32( right_dir,
		                                                    min_x) );
		bounding_box->size.w = max_x - min_x;
		// TODO(theGiallo, 2016-03-11): should we keep track of max ascent and descent?
		bounding_box->size.h =
		   start_to_bottom +
		   tot_font_scale.y * (float)font->metrics.ascent;
		bounding_box->pos = lpr_V2_add_V2( bounding_box->pos,
		                                   lpr_V2_mult_f32(
		                                      right_dir,
		                                      bounding_box->size.w * 0.5f ) );
		bounding_box->pos = lpr_V2_add_V2( bounding_box->pos,
		                                   lpr_V2_mult_f32(
		                                      down_dir,
		                                      -bounding_box->size.h * 0.5f ) );
		bounding_box->rot_deg = rect.rot_deg;
	}

	if ( ending_pt )
	{
		*ending_pt = curr_pt;
	}

	if ( sprites_span )
	{
		span.count = draw_data->batches[batch_id].quads_count - span.first;
		*sprites_span = span;
	}

	return lpr_true;
}

LPR_DEF
lpr_bool
lpr_move_sprites( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                  Lpr_Sprites_Span sprites_span, Lpr_V2 displacement )
{
	lpr_bool ret = true;
	if ( (lpr_u32)draw_data->batches[batch_id].quads_count < sprites_span.first + sprites_span.count )
	{
		ret = false;
	}
	for ( lpr_u32 i = sprites_span.first;
	      ret && i != sprites_span.first + sprites_span.count;
	      ++i )
	{
		draw_data->batches[batch_id].instances_data[i].pos[0] += displacement.x;
		draw_data->batches[batch_id].instances_data[i].pos[1] += displacement.y;
	}

	return ret;
}

LPR_DEF
lpr_bool
lpr_change_color_of_sprites( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                             Lpr_Sprites_Span sprites_span, Lpr_Rgba_u8 color )
{
	lpr_bool ret = true;
	if ( (lpr_u32)draw_data->batches[batch_id].quads_count < sprites_span.first + sprites_span.count )
	{
		ret = false;
	}
	for ( lpr_u32 i = sprites_span.first;
	      ret && i != sprites_span.first + sprites_span.count;
	      ++i )
	{
		for ( int j =0; j!=4; ++j )
		{
			draw_data->batches[batch_id].instances_data[i].color[j] = color.rgba[j];
		}
	}

	return ret;
}

LPR_DEF
lpr_bool
lpr_change_color_and_move_sprites( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                                   Lpr_Sprites_Span sprites_span,
                                   Lpr_Rgba_u8 color, Lpr_V2 displacement )
{
	lpr_bool ret = true;
	if ( (lpr_u32)draw_data->batches[batch_id].quads_count < sprites_span.first + sprites_span.count )
	{
		ret = false;
	}
	for ( lpr_u32 i = sprites_span.first;
	      ret && i != sprites_span.first + sprites_span.count;
	      ++i )
	{
		for ( int j =0; j!=4; ++j )
		{
			draw_data->batches[batch_id].instances_data[i].color[j] = color.rgba[j];
		}
		draw_data->batches[batch_id].instances_data[i].pos[0] += displacement.x;
		draw_data->batches[batch_id].instances_data[i].pos[1] += displacement.y;
	}

	return ret;
}

// font API
///////////////////////////////////////////////////////////////////////////////

LPR_DEF
u8
lpr_new_stencil( Lpr_Draw_Data * draw_data, lpr_u32 batch_id )
{
	u8 ret = LPR_STENCIL_NONE;
	Lpr_Batch_Data * batch = draw_data->batches + batch_id;
	if ( batch->stencils_count != LPR_STENCIL_NONE )
	{
		ret = batch->stencils_count++;
	}

	return ret;
}

LPR_DEF
void
lpr_stencil_clear_all( const Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                       bool value )
{
	Lpr_Batch_Data * batch = draw_data->batches + batch_id;
	GLuint color_value[4];
	GLuint v = value ? 0xFFFFFFFF : 0x0;
	for ( int i=0; i!=4; ++i )
	{
		color_value[i] = v;
	}
	for ( int i=0; i!=LPR_STENCILS_TEXTURES_COUNT; ++i )
	{
		glBindFramebuffer( GL_FRAMEBUFFER, batch->stencil_fbos[0][i].fbo );
		glClearBufferuiv( GL_COLOR, 0, color_value );
	}
}

LPR_DEF
void
lpr_stencil_clear( const Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                   u8 stencil_id, bool value )
{
	Lpr_Batch_Data * batch = draw_data->batches + batch_id;

	Lpr_AA_Rect aa_rect = LPR_ZERO_STRUCT;

	Lpr_V2u32 ws = draw_data->window_size;
	aa_rect.size.x =
	aa_rect.size.y = 2.0;

	Lpr_Mx3 old_VP = batch->VP;
	batch->VP = lpr_Mx3_make_identity();

	lpr_stencil_draw_aa_rect(
	   draw_data, batch_id, stencil_id, aa_rect,
	   value?LPR_STENCIL_DRAW_MODE_SET_ONE:LPR_STENCIL_DRAW_MODE_SET_ZERO,
	   LPR_STENCIL_NONE );

	batch->VP = old_VP;
}

LPR_DEF
void
lpr_stencil_draw_quad(
   const Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
   u8 target_stencil, Lpr_V2 * vertex_buffer, // [4*2]
   Lpr_Texture const * texture, lpr_u32 draw_mode,
   u8 applied_stencil )
{
	lpr_assert( target_stencil != LPR_STENCIL_NONE );
	lpr_assert( ( draw_mode & LPR_STENCIL_DRAW_MODE_LOGIC_MASK ) != 0x0 );
	lpr_assert( ( ( draw_mode & LPR_STENCIL_DRAW_MODE_TX_MASK ) == 0x0 && !texture ) ||
	            ( ( draw_mode & LPR_STENCIL_DRAW_MODE_TX_MASK ) != 0x0 && texture  ) );

	Lpr_Batch_Data * batch = draw_data->batches + batch_id;

	lpr_u32 applied_stencil_32 = applied_stencil;
	lpr_u32 applied_stencil_tex_id = 0;
	lpr_u32 target_stencil_32  = target_stencil;
	lpr_u32 target_stencil_tex_id = 0;
	if ( applied_stencil > 127 )
	{
		applied_stencil_tex_id = 1;
		applied_stencil_32 -= 128;
	}
	if ( applied_stencil == LPR_STENCIL_NONE )
	{
		applied_stencil_32 = 255;
	}
	if ( target_stencil > 127 )
	{
		target_stencil_tex_id = 1;
		target_stencil_32 -= 128;
	}

	lpr_u32 stencil_pingpong = ( applied_stencil == target_stencil ) ? 1 : 0;

	glBindFramebuffer( GL_FRAMEBUFFER, batch->stencil_fbos[0][target_stencil_tex_id].fbo );
	glReadBuffer( GL_COLOR_ATTACHMENT0 );
	glBindTexture(     GL_TEXTURE_2D,  batch->stencil_fbos[1][target_stencil_tex_id].fb_texture );
	glCopyTexImage2D( GL_TEXTURE_2D, // target,
	                  0,             // level
	                  GL_RGBA32UI,   // internal format
	                  0, 0,          // bottom left
	                  draw_data->window_size.w, draw_data->window_size.h, // size
	                  0              // border
	                );
	glBindTexture( GL_TEXTURE_2D, 0 );


	glBindFramebuffer( GL_FRAMEBUFFER, batch->stencil_fbos[0][target_stencil_tex_id].fbo );
	glDrawBuffer( GL_COLOR_ATTACHMENT0 );
	glViewport( 0, 0, draw_data->window_size.w, draw_data->window_size.h );

	GLint drawFboId = -1, readFboId = -1;
	glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &drawFboId);
	glGetIntegerv(GL_READ_FRAMEBUFFER_BINDING, &readFboId);
	lpr_assert( drawFboId == (GLint)batch->stencil_fbos[0][target_stencil_tex_id].fbo );
	lpr_assert( readFboId == (GLint)batch->stencil_fbos[0][target_stencil_tex_id].fbo );

	const Lpr_Draw_Data::Stencil_Shader_Data * shader =
	   applied_stencil == LPR_STENCIL_NONE ?
	   &draw_data->stencil_shader_no_stencils : &draw_data->stencil_shader_stencils;

	glUseProgram( shader->program );

	glUniformMatrix3fv( shader->VP_loc, 1, GL_FALSE,
	                    batch->VP.array );

	// TODO(theGiallo, 2016-12-03): send the color already built as glUniform4uiv
	glUniform1ui( shader->draw_mode_loc, draw_mode );
	glUniform1ui( shader->target_stencil_id_loc, target_stencil_32 );

	GLuint textures_used = 0;
	glActiveTexture( GL_TEXTURE0 + textures_used );
	glBindTexture( GL_TEXTURE_2D, batch->stencil_fbos[1][target_stencil_tex_id].fb_texture );
	glUniform1i( shader->target_stencil_loc, textures_used );
	++textures_used;

	if ( applied_stencil != LPR_STENCIL_NONE )
	{
		glUniform1ui( shader->stencil_id_loc, applied_stencil_32 );

		glActiveTexture( GL_TEXTURE0 + textures_used );
		glBindTexture( GL_TEXTURE_2D, batch->stencil_fbos[stencil_pingpong][applied_stencil_tex_id].fb_texture );
		glUniform1i( shader->stencil_loc, textures_used );
		++textures_used;
	}

	if ( texture )
	{
		glActiveTexture( GL_TEXTURE0 + textures_used );
		glBindTexture( GL_TEXTURE_2D, texture->gl_texture );
		glUniform1i( shader->texture_loc, textures_used );
		++textures_used;
	}

	glBindBuffer( GL_ARRAY_BUFFER, draw_data->stencil_draw_vb );
	glBufferData( GL_ARRAY_BUFFER,         // target
	              sizeof( Lpr_V2 ) * 8, // size
	              NULL,                    // data
	              GL_STREAM_DRAW );        // usage
	glBufferData( GL_ARRAY_BUFFER,         // target
	              sizeof( Lpr_V2 ) * 8, // size
	              vertex_buffer,           // data
	              GL_STREAM_DRAW );        // usage

	GLuint stride = sizeof( Lpr_V2 ) * 2;
	glEnableVertexAttribArray( shader->vertex_loc );
	glVertexAttribPointer( shader->vertex_loc, // location
	                       2,        // size
	                       GL_FLOAT, // type
	                       GL_FALSE, // normalized?
	                       stride,   // stride
	                       (void*)0  // array buffer offset
	                     );
	glEnableVertexAttribArray( shader->uv_loc );
	glVertexAttribPointer( shader->uv_loc, // location
	                       2,        // size
	                       GL_FLOAT, // type
	                       GL_FALSE, // normalized?
	                       stride,   // stride
	                       (void*)sizeof(Lpr_V2) // array buffer offset
	                     );

	glColorMask( GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE );
	glDisable( GL_STENCIL_TEST );
	glDisable( GL_SCISSOR_TEST );
	glDisable( GL_DEPTH_TEST );
	glDepthMask( GL_FALSE );
	glDisable( GL_BLEND );
	glDisable( GL_DITHER );
	glDisable( GL_MULTISAMPLE );
	glEnable( GL_DITHER );
	glEnable( GL_MULTISAMPLE );

	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER,  draw_data->stencil_draw_ib );
	glDrawElements( GL_TRIANGLES,     // mode
	                6,                // size
	                GL_UNSIGNED_BYTE, // type
	                0 );              // indices

	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	glBindTexture( GL_TEXTURE_2D, 0 );
	glDisableVertexAttribArray( shader->vertex_loc );
	glDisableVertexAttribArray( shader->uv_loc );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glUseProgram( 0 );
	glBindFramebuffer( GL_FRAMEBUFFER, 0 );
}

LPR_DEF
void
lpr_stencil_draw_aa_rect(
   const Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
   u8 target_stencil, Lpr_AA_Rect aa_rect, Lpr_Stencil_Draw_Mode draw_mode,
   u8 applied_stencil )
{
	lpr_assert( target_stencil != LPR_STENCIL_NONE );
	lpr_assert( ( draw_mode & LPR_STENCIL_DRAW_MODE_LOGIC_MASK ) != 0x0 );

	Lpr_Batch_Data * batch = draw_data->batches + batch_id;

	Lpr_V2 h_size = lpr_V2_mult_f32( aa_rect.size, 0.5f );
	Lpr_V2 vertex_buffer[4*2] = {
	   lpr_V2_add_V2( aa_rect.pos, h_size                        ), Lpr_V2{1.0,1.0},
	   lpr_V2_add_V2( aa_rect.pos, Lpr_V2{  h_size.x, -h_size.y} ), Lpr_V2{1.0,0},
	   lpr_V2_add_V2( aa_rect.pos, Lpr_V2{ -h_size.x, -h_size.y} ), Lpr_V2{0,0},
	   lpr_V2_add_V2( aa_rect.pos, Lpr_V2{ -h_size.x,  h_size.y} ), Lpr_V2{0,1.0},
	};

	lpr_stencil_draw_quad(
	   draw_data, batch_id, target_stencil, vertex_buffer,
	   NULL, draw_mode, applied_stencil );
}

LPR_DEF
void
lpr_stencil_draw_rot_rect(
   const Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
   u8 target_stencil, Lpr_Rot_Rect rot_rect, Lpr_Stencil_Draw_Mode draw_mode,
   u8 applied_stencil, bool camera_aligned )
{
	lpr_assert( target_stencil != LPR_STENCIL_NONE );

	const Lpr_Batch_Data * batch = draw_data->batches + batch_id;
	float angle_deg = rot_rect.rot_deg;
	if ( camera_aligned )
	{
		angle_deg += batch->camera_angle;
	}
	Lpr_V2 h_size = lpr_V2_mult_f32( rot_rect.size, 0.5f );
	Lpr_V2 vertex_buffer[4*2] = {
	   lpr_V2_add_V2(
	      lpr_V2_rotated_deg( h_size, angle_deg ),
	      rot_rect.pos ),
	   Lpr_V2{1.0,1.0},
	   lpr_V2_add_V2(
	      lpr_V2_rotated_deg(
	         Lpr_V2{  h_size.x, -h_size.y},
	         angle_deg ),
	      rot_rect.pos ),
	   Lpr_V2{1.0,0},
	   lpr_V2_add_V2(
	      lpr_V2_rotated_deg(
	         Lpr_V2{ -h_size.x, -h_size.y},
	         angle_deg ),
	      rot_rect.pos ),
	   Lpr_V2{0,0},
	   lpr_V2_add_V2(
	      lpr_V2_rotated_deg(
	         Lpr_V2{ -h_size.x,  h_size.y},
	         angle_deg ),
	      rot_rect.pos ),
	   Lpr_V2{0,1.0},
	};

	lpr_stencil_draw_quad(
	   draw_data, batch_id, target_stencil, vertex_buffer,
	   NULL, draw_mode, applied_stencil );
}

LPR_DEF
void
lpr_stencil_draw_sprite_w_size(
   const Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
   u8 target_stencil,
   Lpr_Texture const * texture, Lpr_Rot_Rect rot_rect,
   Lpr_Stencil_Draw_Mode draw_mode_logic,
   Lpr_Stencil_Draw_Mode draw_mode_tex,
   u8 applied_stencil, bool camera_aligned
   )
{
	lpr_assert( target_stencil != LPR_STENCIL_NONE );
	lpr_assert( ( draw_mode_logic & LPR_STENCIL_DRAW_MODE_TX_MASK    ) == 0 );
	lpr_assert( ( draw_mode_tex   & LPR_STENCIL_DRAW_MODE_LOGIC_MASK ) == 0 );
	lpr_assert( texture );

	const Lpr_Batch_Data * batch = draw_data->batches + batch_id;
	float angle_deg = rot_rect.rot_deg;
	if ( camera_aligned )
	{
		angle_deg += batch->camera_angle;
	}
	Lpr_V2 h_size = lpr_V2_mult_f32( rot_rect.size, 0.5f );
	Lpr_V2 vertex_buffer[4*2] = {
	   lpr_V2_add_V2(
	      lpr_V2_rotated_deg( h_size, angle_deg ),
	      rot_rect.pos ),
	   Lpr_V2{1.0,1.0},
	   lpr_V2_add_V2(
	      lpr_V2_rotated_deg(
	         Lpr_V2{  h_size.x, -h_size.y},
	         angle_deg ),
	      rot_rect.pos ),
	   Lpr_V2{1.0,0},
	   lpr_V2_add_V2(
	      lpr_V2_rotated_deg(
	         Lpr_V2{ -h_size.x, -h_size.y},
	         angle_deg ),
	      rot_rect.pos ),
	   Lpr_V2{0,0},
	   lpr_V2_add_V2(
	      lpr_V2_rotated_deg(
	         Lpr_V2{ -h_size.x,  h_size.y},
	         angle_deg ),
	      rot_rect.pos ),
	   Lpr_V2{0,1.0},
	};

	lpr_stencil_draw_quad(
	   draw_data, batch_id, target_stencil, vertex_buffer,
	   texture, draw_mode_logic | draw_mode_tex, applied_stencil );
}

LPR_DEF
void
lpr_stencil_draw_sprite_w_scale(
   const Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
   u8 target_stencil,
   Lpr_Texture const * texture, Lpr_V2 pos,
   float scale, float rot_deg,
   Lpr_Stencil_Draw_Mode draw_mode_logic,
   Lpr_Stencil_Draw_Mode draw_mode_tex,
   u8 applied_stencil, bool camera_aligned
   )
{
	lpr_assert( target_stencil != LPR_STENCIL_NONE );
	lpr_assert( ( draw_mode_logic & LPR_STENCIL_DRAW_MODE_TX_MASK    ) == 0 );
	lpr_assert( ( draw_mode_tex   & LPR_STENCIL_DRAW_MODE_LOGIC_MASK ) == 0 );
	lpr_assert( texture );

	const Lpr_Batch_Data * batch = draw_data->batches + batch_id;
	float angle_deg = rot_deg;
	if ( camera_aligned )
	{
		angle_deg += batch->camera_angle;
	}
	Lpr_V2 h_size = lpr_V2_mult_f32( lpr_V2_from_V2u32( texture->size ), scale * 0.5f );
	Lpr_V2 vertex_buffer[4*2] = {
	   lpr_V2_add_V2(
	      lpr_V2_rotated_deg( h_size, angle_deg ),
	      pos ),
	   Lpr_V2{1.0,1.0},
	   lpr_V2_add_V2(
	      lpr_V2_rotated_deg(
	         Lpr_V2{  h_size.x, -h_size.y},
	         angle_deg ),
	      pos ),
	   Lpr_V2{1.0,0},
	   lpr_V2_add_V2(
	      lpr_V2_rotated_deg(
	         Lpr_V2{ -h_size.x, -h_size.y},
	         angle_deg ),
	      pos ),
	   Lpr_V2{0,0},
	   lpr_V2_add_V2(
	      lpr_V2_rotated_deg(
	         Lpr_V2{ -h_size.x,  h_size.y},
	         angle_deg ),
	      pos ),
	   Lpr_V2{0,1.0},
	};

	lpr_stencil_draw_quad(
	   draw_data, batch_id, target_stencil, vertex_buffer,
	   texture, draw_mode_logic | draw_mode_tex, applied_stencil );
}

// NOTE(theGiallo): instance_id is the value returned by a add* function
LPR_DEF
void
lpr_set_instance_stencil_id( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                             lpr_u16 instance_id, lpr_u8 stencil_id )
{
	lpr_assert_m( batch_id < draw_data->batches_count,
	              "batch_id must be less than batches count!" );

	Lpr_Batch_Data * batch = draw_data->batches + batch_id;

	lpr_assert_m( instance_id < batch->quads_count,
	              "instance_id must be less than quads count!" );

	batch->instances_data[instance_id].stencil_id = stencil_id;

}

// NOTE(theGiallo): instance_id is the value returned by a add* function
LPR_DEF
void
lpr_set_instance_z( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                    lpr_u16 instance_id, lpr_u16 z )
{
	lpr_assert_m( batch_id < draw_data->batches_count,
	              "batch_id must be less than batches count!" );

	Lpr_Batch_Data * batch = draw_data->batches + batch_id;

	lpr_assert_m( instance_id < batch->quads_count,
	              "instance_id must be less than quads count!" );

	batch->z[instance_id] = z;
	batch->ids[instance_id] = instance_id;
}

// NOTE(theGiallo): instance_id is the value returned by a add* function
LPR_DEF
void
lpr_set_instance_blend_mode( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                             lpr_u16 instance_id, Lpr_Blend_Mode blend_mode )
{
	lpr_assert_m( batch_id < draw_data->batches_count,
	              "batch_id must be less than batches count!" );

	Lpr_Batch_Data * batch = draw_data->batches + batch_id;

	lpr_assert_m( instance_id < batch->quads_count,
	              "instance_id must be less than quads count!" );

	batch->has_many_blend_modes = lpr_true;
	batch->blend_modes[instance_id] = (lpr_u8)blend_mode;
}

LPR_DEF
void
lpr_reset_all_batches( Lpr_Draw_Data * draw_data )
{
	const int batches_count = draw_data->batches_count;
	for ( int i = 0; i != batches_count; ++i )
	{
		lpr_reset_batch( draw_data->batches + i );
	}
}

LPR_DEF
void
lpr_set_batch_camera( Lpr_Draw_Data * draw_data, lpr_u32 batch_id,
                      Lpr_V2 camera_pos, float camera_angle,
                      Lpr_V2 vanishing_point, Lpr_V2 camera_world_span )
{
	lpr_assert_m( batch_id < draw_data->batches_count,
	              "batch_id must be less than batches count!" );
	lpr_assert_m( vanishing_point.x >= 0, "vanishing_point values must be in [0,1]" );
	lpr_assert_m( vanishing_point.x <= 1, "vanishing_point values must be in [0,1]" );
	lpr_assert_m( vanishing_point.y >= 0, "vanishing_point values must be in [0,1]" );
	lpr_assert_m( vanishing_point.y <= 1, "vanishing_point values must be in [0,1]" );

	Lpr_Batch_Data * batch = draw_data->batches + batch_id;
	Lpr_Mx3 T, R, V, P;
	lpr_Mx3_orthographic( &P, vanishing_point, camera_world_span );
	lpr_Mx3_translation( &T, lpr_V2( -camera_pos.x, -camera_pos.y ) );
	lpr_Mx3_rotation_deg( &R , -camera_angle );
	lpr_Mx3_product( &R, &T, &V );
	lpr_Mx3_product( &P, &V, &batch->VP );
	batch->camera_pos        = camera_pos;
	batch->camera_angle      = camera_angle;
	batch->vanishing_point   = vanishing_point;
	batch->camera_world_span = camera_world_span;
}

LPR_DEF
void
lpr_clear_framebuffer_zero( const Lpr_Draw_Data * draw_data,
                            const Lpr_Rgba_f32 * clear_color )
{
	glBindFramebuffer( GL_FRAMEBUFFER, 0 );
	glViewport( 0, 0, draw_data->window_size.w, draw_data->window_size.h );
	glClearColor( clear_color->r, clear_color->g, clear_color->b, clear_color->a );
	glClear( GL_COLOR_BUFFER_BIT );
}

// NOTE(theGiallo): binds the linear framebuffer or the default one, and sets
// the viewport to the whole framebuffer
LPR_DEF
void
lpr_set_draw_target_default_full( Lpr_Draw_Data * draw_data  )
{
	lpr_set_draw_target_default( draw_data,
	                             lpr_V2u32( 0, 0 ),
	                             draw_data->window_size );
}

// NOTE(theGiallo): binds the linear framebuffer or the default one, and sets
// the viewport as given
LPR_DEF
void
lpr_set_draw_target_default( Lpr_Draw_Data * draw_data,
                             Lpr_V2u32 bl, Lpr_V2u32 size )
{
	if ( ! draw_data->framebuffer_is_sRGB )
	{
		glBindFramebuffer( GL_FRAMEBUFFER, draw_data->linear_fbo.fbo );
	} else
	{
		glBindFramebuffer( GL_FRAMEBUFFER, 0 );
	}

	draw_data->viewport_origin = bl;
	draw_data->viewport_size   = size;
	glViewport( bl.x, bl.y, size.w, size.h );
}

LPR_DEF
void
lpr_prep_batch_drawing( const Lpr_Draw_Data * draw_data )
{
	glDepthMask( GL_FALSE );

	if ( ! draw_data->framebuffer_is_sRGB )
	{
		// NOTE(theGiallo): this is to be able to compose on the default
		// framebuffer, incase there was something there, and preserves the clear
		// color of lpr_clear_framebuffer_zero
		glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
	}

	glClear( GL_COLOR_BUFFER_BIT );
	glDisable( GL_DEPTH_TEST );
	#if 0
	/* TODO(theGiallo, 2016/01/10): with multiple blend modes this had to be
	moved to lpr_draw_batch. In this way it's called many times. Is this so
	bad? It's called once per batch if all batches are in default blend mode. :\
	*/
	glEnable( GL_BLEND );
	// NOTE(theGiallo): this is for premultiplied alpha
	glBlendFunc( GL_ONE, GL_ONE_MINUS_SRC_ALPHA );
	#endif
}

LPR_DEF
void
lpr_sort_batch( Lpr_Draw_Data * draw_data, lpr_u32 batch_id )
{
	lpr_assert_m( batch_id < draw_data->batches_count,
	              "batch_id must be less than batches count!" );
	Lpr_Batch_Data * batch = draw_data->batches + batch_id;

	// NOTE(theGiallo): init ids to 1..q_count
	const lpr_u32 q_count = batch->quads_count;
	for ( lpr_u32 i = 0; i < q_count; ++i )
	{
		batch->ids[i] = (lpr_u16) i;
	}

	// NOTE(theGiallo): according to benchmarks this is faster than quick sort
	// iterative for the 16 bit case
	#if 1
	lpr_radix_sort_mc_16( batch->z, batch->ids, batch->tmp, q_count );
	#else
	lpr_radix_sort_16( batch->z, batch->ids, q_count );
	#endif

	lpr_reorder_16_8( batch->ids, batch->blend_modes, q_count );

	/* TODO(theGiallo, 2016/01/07): sort groups with the same z according to
	blend_mode
	NOTE(theGiallo): is this sane? is sane to have a case with many sprites with
	the same z but different blend modes? If so is sane to reorder them for blend
	mode instead of keeping original insertion order?
	*/

	lpr_reorder_16_s( batch->ids, batch->instances_data, q_count,
	                  sizeof(batch->instances_data[0]) );
}

LPR_DEF
void
lpr_draw_batch( const Lpr_Draw_Data * draw_data, lpr_u32 batch_id )
{
	lpr_assert_m( batch_id < draw_data->batches_count,
	              "batch_id must be less than batches count!" );
	Lpr_Batch_Data const * batch = draw_data->batches + batch_id;

	const Lpr_Draw_Data::Instanced_Shader_Data * shader_data =
	   batch->stencils_enabled ? &draw_data->stencils : &draw_data->no_stencils;

	glUseProgram( shader_data->quad_program );

	// NOTE(theGiallo): bind all the used textures
	lpr_assert_m( (int)batch->textures_count <= lpr_C_max_textures,
	              "batch textures must be less than lpr_C_max_textures!" );
	const GLuint textures_count = batch->textures_count;
	for ( GLuint i=0; i != textures_count; ++i )
	{
		glActiveTexture( GL_TEXTURE0 + i );
		glBindTexture( GL_TEXTURE_2D, batch->textures[i] );
		glUniform1i( shader_data->textures_locs[i], i );
	}
	if ( batch->stencils_enabled )
	{
		for ( GLuint i=0; i != LPR_STENCILS_TEXTURES_COUNT; ++i )
		{
			glActiveTexture( GL_TEXTURE0 + textures_count + i );
			glBindTexture( GL_TEXTURE_2D, batch->stencil_fbos[0][i].fb_texture );
			glUniform1i( shader_data->stencils_locs[i], textures_count + i );
		}
	}

	const int pos_dim = 2;
	const int uv_dim  = 2;
	const int stride  = pos_dim + uv_dim;

	glBindBuffer( GL_ARRAY_BUFFER, draw_data->quad_vb );

	// NOTE(theGiallo): uniform data
	glUniformMatrix3fv( shader_data->VP_loc, 1, GL_FALSE, batch->VP.array );

	// NOTE(theGiallo): per vertex data
	glEnableVertexAttribArray( shader_data->va_vertex_loc );
	glVertexAttribPointer( shader_data->va_vertex_loc, // location
	                       pos_dim,                    // size
	                       GL_FLOAT,                   // type
	                       GL_FALSE,                   // normalized?
	                       stride * sizeof( GLfloat ), // stride
	                       (void*)0                    // array buffer offset
	                     );

	glEnableVertexAttribArray( shader_data->va_uv_loc );
	glVertexAttribPointer( shader_data->va_uv_loc,                // location
	                       uv_dim,                                // size
	                       GL_FLOAT,                              // type
	                       GL_FALSE,                              // normalized?
	                       stride * sizeof( GLfloat ),            // stride
	                       (void*)( pos_dim * sizeof( GLfloat ) ) // array buffer
	                                                              // offset
	                     );

	// NOTE(theGiallo): per instance data
	// NOTE(theGiallo): ask for a new buffer (every time probably the driver won't
	// reallocate it, but will keep the old one)
	glBindBuffer( GL_ARRAY_BUFFER, batch->per_instance_vb );
	glBufferData( GL_ARRAY_BUFFER,
	              sizeof(batch->instances_data[0]) * batch->quads_count,
	              NULL, GL_STREAM_DRAW );
	// NOTE(theGiallo): fill the new buffer
	glBufferSubData( GL_ARRAY_BUFFER, 0,
	                 sizeof(batch->instances_data[0]) * batch->quads_count,
	                 batch->instances_data );
	// NOTE(theGiallo): use the new buffer
	glBindBuffer( GL_ARRAY_BUFFER, batch->per_instance_vb );
	uint8_t * offset = 0;
	const GLsizei pi_stride = sizeof( Lpr_Quad_Data_T_Atlas );
	Lpr_Quad_Data_Loc const * const
	per_instance_data_loc = &shader_data->per_instance_data_loc;
	glEnableVertexAttribArray( per_instance_data_loc->pos );
	glVertexAttribDivisor( per_instance_data_loc->pos, 1 );
	glVertexAttribPointer( per_instance_data_loc->pos, // location
	                       2,                          // size
	                       GL_FLOAT,                   // type
	                       GL_FALSE,                   // normalized?
	                       pi_stride,                  // stride
	                       (void*)( offset )           // array buffer offset
	                     );
	offset += 2 * sizeof( GLfloat );

	glEnableVertexAttribArray( per_instance_data_loc->size );
	glVertexAttribDivisor( per_instance_data_loc->size, 1 );
	glVertexAttribPointer( per_instance_data_loc->size, // location
	                       2,                           // size
	                       GL_FLOAT,                    // type
	                       GL_FALSE,                    // normalized?
	                       pi_stride,                   // stride
	                       (void*)( offset )            // array buffer offset
	                     );
	offset += 2 * sizeof( GLfloat );

	glEnableVertexAttribArray( per_instance_data_loc->rot );
	glVertexAttribDivisor( per_instance_data_loc->rot, 1 );
	glVertexAttribPointer( per_instance_data_loc->rot, // location
	                       1,                          // size
	                       GL_FLOAT,                   // type
	                       GL_FALSE,                   // normalized?
	                       pi_stride,                  // stride
	                       (void*)( offset )           // array buffer offset
	                     );
	offset += sizeof( GLfloat );

	// TODO(theGiallo, 2015/12/10): try if using 4 GL_BYTE with float conversion
	// is faster
	glEnableVertexAttribArray( per_instance_data_loc->color );
	glVertexAttribDivisor( per_instance_data_loc->color, 1 );
	glVertexAttribIPointer( per_instance_data_loc->color, // location
	                        1,                            // size
	                        GL_UNSIGNED_INT,              // type
	                        pi_stride,                    // stride
	                        (void*)( offset )             // array buffer offset
	                      );
	offset += 4 * sizeof( GLbyte );

	glEnableVertexAttribArray( per_instance_data_loc->texture_id );
	glVertexAttribDivisor( per_instance_data_loc->texture_id, 1 );
	glVertexAttribIPointer( per_instance_data_loc->texture_id, // location
	                        1,                                 // size
	                        GL_UNSIGNED_BYTE,                  // type
	                        pi_stride,                         // stride
	                        (void*)( offset )                  // array buffer offset
	                      );
	offset += sizeof( GLubyte );

	if ( batch->stencils_enabled )
	{
		glEnableVertexAttribArray( per_instance_data_loc->stencil_id );
		glVertexAttribDivisor( per_instance_data_loc->stencil_id, 1 );
		glVertexAttribIPointer( per_instance_data_loc->stencil_id, // location
		                        1,                                 // size
		                        GL_UNSIGNED_BYTE,                  // type
		                        pi_stride,                         // stride
		                        (void*)( offset )                  // array buffer offset
		                      );
	}
	offset += sizeof( GLubyte );
	offset += sizeof( GLushort );

	glEnableVertexAttribArray( per_instance_data_loc->uv_offset );
	glVertexAttribDivisor( per_instance_data_loc->uv_offset, 1 );
	glVertexAttribPointer( per_instance_data_loc->uv_offset, // location
	                       2,                                // size
	                       GL_FLOAT,                         // type
	                       GL_FALSE,                         // normalized?
	                       pi_stride,                        // stride
	                       (void*)( offset )                 // array buffer offset
	                     );
	offset += 2 * sizeof( GLfloat );

	glEnableVertexAttribArray( per_instance_data_loc->uv_scale );
	glVertexAttribDivisor( per_instance_data_loc->uv_scale, 1 );
	glVertexAttribPointer( per_instance_data_loc->uv_scale, // location
	                       2,                               // size
	                       GL_FLOAT,                        // type
	                       GL_FALSE,                        // normalized?
	                       pi_stride,                       // stride
	                       (void*)( offset )                // array buffer offset
	                     );
	offset += 2 * sizeof( GLfloat );

	glEnableVertexAttribArray( per_instance_data_loc->uv_origin );
	glVertexAttribDivisor( per_instance_data_loc->uv_origin, 1 );
	glVertexAttribPointer( per_instance_data_loc->uv_origin, // location
	                       2,                                // size
	                       GL_FLOAT,                         // type
	                       GL_FALSE,                         // normalized?
	                       pi_stride,                        // stride
	                       (void*)( offset )                 // array buffer offset
	                     );
	offset += 2 * sizeof( GLfloat );

	glEnableVertexAttribArray( per_instance_data_loc->uv_limits );
	glVertexAttribDivisor( per_instance_data_loc->uv_limits, 1 );
	glVertexAttribPointer( per_instance_data_loc->uv_limits, // location
	                       2,                                // size
	                       GL_FLOAT,                         // type
	                       GL_FALSE,                         // normalized?
	                       pi_stride,                        // stride
	                       (void*)( offset )                 // array buffer offset
	                     );
	offset += 2 * sizeof( GLfloat );

	glEnableVertexAttribArray( per_instance_data_loc->texture_overflow_x );
	glVertexAttribDivisor( per_instance_data_loc->texture_overflow_x, 1 );
	glVertexAttribIPointer( per_instance_data_loc->texture_overflow_x, // location
	                        1,                                         // size
	                        GL_UNSIGNED_BYTE,                          // type
	                        pi_stride,                                 // stride
	                        (void*)( offset )                          // array buffer offset
	                      );
	offset += sizeof( GLubyte );

	glEnableVertexAttribArray( per_instance_data_loc->texture_overflow_y );
	glVertexAttribDivisor( per_instance_data_loc->texture_overflow_y, 1 );
	glVertexAttribIPointer( per_instance_data_loc->texture_overflow_y, // location
	                        1,                                         // size
	                        GL_UNSIGNED_BYTE,                          // type
	                        pi_stride,                                 // stride
	                        (void*)( offset )                          // array buffer offset
	                      );
	offset += sizeof( GLubyte );

	glEnableVertexAttribArray( per_instance_data_loc->texture_min_filter );
	glVertexAttribDivisor( per_instance_data_loc->texture_min_filter, 1 );
	glVertexAttribIPointer( per_instance_data_loc->texture_min_filter, // location
	                        1,                                         // size
	                        GL_UNSIGNED_BYTE,                          // type
	                        pi_stride,                                 // stride
	                        (void*)( offset )                          // array buffer offset
	                      );
	offset += sizeof( GLubyte );

	glEnableVertexAttribArray( per_instance_data_loc->texture_mag_filter );
	glVertexAttribDivisor( per_instance_data_loc->texture_mag_filter, 1 );
	glVertexAttribIPointer( per_instance_data_loc->texture_mag_filter, // location
	                        1,                                         // size
	                        GL_UNSIGNED_BYTE,                          // type
	                        pi_stride,                                 // stride
	                        (void*)( offset )                          // array buffer offset
	                      );
	offset += sizeof( GLubyte );

	// NOTE(theGiallo): actual draw
	if ( batch->has_many_blend_modes )
	{
		const GLsizei indices_count = 4;
		const GLsizei q_count = batch->quads_count;
		for ( GLint start = 0, count = 0; start < q_count; start += count )
		{
			lpr_u8 blend_mode = batch->blend_modes[start];
			// NOTE(theGiallo): find how many consecutive instances with the same
			// blend mode there are
			for ( count = 0;
			      start + count < q_count &&
			      batch->blend_modes[start + count] == blend_mode;
			      ++count );

			// NOTE(theGiallo): set the blend mode
			if ( blend_mode == LPR_BLEND_BLEND )
			{
				glEnable( GL_BLEND );
				// NOTE(theGiallo): this is for premultiplied alpha
				glBlendFunc( GL_ONE, GL_ONE_MINUS_SRC_ALPHA );
				glBlendEquation( GL_FUNC_ADD );
			} else
			if ( blend_mode == LPR_BLEND_MULTIPLY_1_2 )
			{
				glEnable( GL_BLEND );
				glBlendFuncSeparate( GL_DST_COLOR, GL_ONE, GL_ZERO, GL_ONE );
				glBlendEquation( GL_FUNC_ADD );
			} else
			if ( blend_mode == LPR_BLEND_MULTIPLY_0_1 )
			{
				glEnable( GL_BLEND );
				glBlendFuncSeparate( GL_DST_COLOR, GL_ZERO, GL_ZERO, GL_ONE );
				glBlendEquation( GL_FUNC_ADD );
			} else
			{
				// TODO(theGiallo, 2016/01/10): implement custom blend modes
				LPR_ILLEGAL_PATH();
			}

			// NOTE(theGiallo): draw
			glDrawArraysInstancedBaseInstance( GL_TRIANGLE_STRIP, 0, indices_count,
			                                   count, start );
		}
	} else
	{
		const lpr_u8 blend_mode = batch->single_blend_mode;
		if ( blend_mode == LPR_BLEND_BLEND )
		{
			glEnable( GL_BLEND );
			// NOTE(theGiallo): this is for premultiplied alpha
			glBlendFunc( GL_ONE, GL_ONE_MINUS_SRC_ALPHA );
			glBlendEquation( GL_FUNC_ADD );
		} else
		if ( blend_mode == LPR_BLEND_MULTIPLY_1_2 )
		{
			glEnable( GL_BLEND );
			glBlendFuncSeparate( GL_DST_COLOR, GL_ONE, GL_ZERO, GL_ONE );
			glBlendEquation( GL_FUNC_ADD );
		} else
		if ( blend_mode == LPR_BLEND_MULTIPLY_0_1 )
		{
			glEnable( GL_BLEND );
			glBlendFuncSeparate( GL_DST_COLOR, GL_ZERO, GL_ZERO, GL_ONE );
			glBlendEquation( GL_FUNC_ADD );
		} else
		{
			// TODO(theGiallo, 2016/01/10): implement custom blend modes
			LPR_ILLEGAL_PATH();
		}
		const GLint start = 0;
		const GLsizei indices_count = 4;
		glDrawArraysInstanced( GL_TRIANGLE_STRIP, start, indices_count,
		                       batch->quads_count );
	}

	// NOTE(theGiallo): clean opengl state
	glDisableVertexAttribArray( shader_data->va_vertex_loc );
	glDisableVertexAttribArray( shader_data->va_uv_loc );
	glDisableVertexAttribArray( per_instance_data_loc->pos );
	glDisableVertexAttribArray( per_instance_data_loc->size );
	glDisableVertexAttribArray( per_instance_data_loc->rot );
	glDisableVertexAttribArray( per_instance_data_loc->color );
	glDisableVertexAttribArray( per_instance_data_loc->texture_id );
	if ( batch->stencils_enabled )
	{
		glDisableVertexAttribArray( per_instance_data_loc->stencil_id );
	}
	glDisableVertexAttribArray( per_instance_data_loc->uv_offset );
	glDisableVertexAttribArray( per_instance_data_loc->uv_scale );
	glDisableVertexAttribArray( per_instance_data_loc->uv_origin );
	glDisableVertexAttribArray( per_instance_data_loc->uv_limits );
	glDisableVertexAttribArray( per_instance_data_loc->texture_overflow_x );
	glDisableVertexAttribArray( per_instance_data_loc->texture_overflow_y );
	glDisableVertexAttribArray( per_instance_data_loc->texture_min_filter );
	glDisableVertexAttribArray( per_instance_data_loc->texture_mag_filter );

	glVertexAttribDivisor( per_instance_data_loc->pos,                 0 );
	glVertexAttribDivisor( per_instance_data_loc->size,                0 );
	glVertexAttribDivisor( per_instance_data_loc->rot,                 0 );
	glVertexAttribDivisor( per_instance_data_loc->color,               0 );
	glVertexAttribDivisor( per_instance_data_loc->texture_id,          0 );
	glVertexAttribDivisor( per_instance_data_loc->stencil_id,          0 );
	glVertexAttribDivisor( per_instance_data_loc->uv_offset,           0 );
	glVertexAttribDivisor( per_instance_data_loc->uv_scale,            0 );
	glVertexAttribDivisor( per_instance_data_loc->uv_origin,           0 );
	glVertexAttribDivisor( per_instance_data_loc->uv_limits,           0 );
	glVertexAttribDivisor( per_instance_data_loc->texture_overflow_x,  0 );
	glVertexAttribDivisor( per_instance_data_loc->texture_overflow_y,  0 );
	glVertexAttribDivisor( per_instance_data_loc->texture_min_filter,  0 );
	glVertexAttribDivisor( per_instance_data_loc->texture_mag_filter,  0 );

	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glBindTexture( GL_TEXTURE_2D, 0 );

	glUseProgram( 0 );
}

LPR_DEF
void
lpr_draw_all_batches( const Lpr_Draw_Data * draw_data )
{
	const int batches_count = draw_data->batches_count;
	for ( int i = 0; i != batches_count; ++i )
	{
		lpr_draw_batch( draw_data, i );
	}
}

LPR_DEF
void
lpr_draw_all_batches_sorted( Lpr_Draw_Data * draw_data )
{
	const int batches_count = draw_data->batches_count;
	for ( int i = 0; i != batches_count; ++i )
	{
		lpr_sort_batch( draw_data, i );
		lpr_draw_batch( draw_data, i );
	}
}

LPR_DEF
void
lpr_finalize_draw( const Lpr_Draw_Data * draw_data )
{
	if ( ! draw_data->framebuffer_is_sRGB )
	{
		// NOTE(theGiallo): draw the framebuffer with gamma correction

		glBindFramebuffer( GL_FRAMEBUFFER, 0 );

		glUseProgram( draw_data->gamma_correction_program );

		// TODO(theGiallo, 2016/01/06): we could use a scissors test to the
		// viewport area, it could be faster. Drawbacks?
		glViewport( 0, 0, draw_data->window_size.w, draw_data->window_size.h );

		// NOTE(theGiallo): set texture uniform
		glActiveTexture( GL_TEXTURE0 );
		glBindTexture( GL_TEXTURE_2D, draw_data->linear_fbo.fb_texture );
		glUniform1i( draw_data->gc_linear_fb_texture_loc, 0 );

		// NOTE(theGiallo): render quad
		glEnableVertexAttribArray( draw_data->gc_vertex_loc );
		glBindBuffer( GL_ARRAY_BUFFER, draw_data->fullscreen_quad_vb );
		glVertexAttribPointer( draw_data->gc_vertex_loc,    // location
		                       2,                           // size
		                       GL_FLOAT,                    // type
		                       GL_FALSE,                    // normalized?
		                       4 * sizeof( GLfloat ),       // stride
		                       (void*)0                     // array buffer offset
		                     );
		glEnableVertexAttribArray( draw_data->gc_uv_loc );
		// glBindBuffer(GL_ARRAY_BUFFER, quad_vb);
		glVertexAttribPointer( draw_data->gc_uv_loc,           // location
		                       2,                              // size
		                       GL_FLOAT,                       // type
		                       GL_FALSE,                       // normalized?
		                       4 * sizeof( GLfloat ),          // stride
		                       (void*)(2 * sizeof( GLfloat ) ) // array buffer offset
		                     );

		glEnable( GL_BLEND );
		// NOTE(theGiallo): this is for premultiplied alpha
		glBlendFunc( GL_ONE, GL_ONE_MINUS_SRC_ALPHA );
		glBlendEquation( GL_FUNC_ADD );

		// NOTE(theGiallo): from vertex 0; 4 vertices -> 1 quad
		glDrawArrays( GL_TRIANGLE_STRIP, 0, 4 );

		glDisableVertexAttribArray( draw_data->gc_vertex_loc );
		glDisableVertexAttribArray( draw_data->gc_uv_loc );
		glActiveTexture( GL_TEXTURE0 );
		glBindTexture( GL_TEXTURE_2D, 0 );
	}
}

LPR_DEF
lpr_bool
lpr_resize( Lpr_Draw_Data * draw_data, Lpr_V2u32 size )
{
	lpr_bool ret = true;

	if ( draw_data->window_size.w == size.w &&
	     draw_data->window_size.h == size.h )
	{
		return lpr_true;
	}

	draw_data->window_size = size;

	if ( !draw_data->framebuffer_is_sRGB )
	{
		ret = lpr_create_linear_fb( draw_data );
	}

	lpr_create_stencils_fbs_for_batches( draw_data );

	return ret;
}

LPR_DEF
lpr_bool
lpr_create_linear_fb(  Lpr_Draw_Data * draw_data )
{
	GLint gl_width  = draw_data->window_size.w,
	      gl_height = draw_data->window_size.h;

	if ( ! glIsFramebuffer( draw_data->linear_fbo.fbo ) )
	{
		glGenFramebuffers(  1, &draw_data->linear_fbo.fbo );
	}
	glBindFramebuffer( GL_FRAMEBUFFER, draw_data->linear_fbo.fbo );
	if ( ! glIsRenderbuffer( draw_data->linear_fbo.color_rt ) )
	{
		glGenRenderbuffers( 1, &draw_data->linear_fbo.color_rt );
	}
	glBindRenderbuffer(    GL_RENDERBUFFER, draw_data->linear_fbo.color_rt );
	glRenderbufferStorage( GL_RENDERBUFFER, GL_RGBA16F, gl_width, gl_height );
	glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
	                           GL_RENDERBUFFER, draw_data->linear_fbo.color_rt );

	// glGenRenderbuffers( 1, &draw_data->linear_depth_rt );
	// glBindRenderbuffer(    GL_RENDERBUFFER, draw_data->linear_depth_rt );
	// glRenderbufferStorage( GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, gl_width, gl_height );
	// glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
	//                            GL_RENDERBUFFER, draw_data->linear_depth_rt );

	// NOTE(theGiallo): Generate and bind the OGL texture for diffuse
	if ( ! glIsTexture( draw_data->linear_fbo.fb_texture ) )
	{
		glGenTextures( 1, &draw_data->linear_fbo.fb_texture );
	}
	glBindTexture( GL_TEXTURE_2D, draw_data->linear_fbo.fb_texture );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA16F, gl_width, gl_height, 0,
	              GL_RGBA, GL_HALF_FLOAT, NULL );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	// NOTE(theGiallo): Attach the texture to the FBO
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
	                        GL_TEXTURE_2D, draw_data->linear_fbo.fb_texture, 0 );

	// NOTE(theGiallo): Check if all worked fine and unbind the FBO
	if( !lpr_check_and_log_framebuffer_status() )
	{
		lpr_log_err( "Something failed during FBO initialization. FBO is not complete." );
		// TODO(theGiallo, 2015/09/28): proper clean
		glDeleteFramebuffers( 1, &draw_data->linear_fbo.fbo );
		glDeleteRenderbuffers( 1, &draw_data->linear_fbo.color_rt );
		// glDeleteRenderbuffers( 1, &draw_data->linear_depth_rt );
		// SDL_Quit(); TODO(theGiallo, 2015/12/04): this is bad, but how to manage it? Return lpr_false?
		LPR_ILLEGAL_PATH();
		return lpr_false;
	}
	lpr_log_framebuffer_info();

	// NOTE(theGiallo): OpenGL cleanup
	glBindTexture( GL_TEXTURE_2D, 0 );
	glBindRenderbuffer( GL_RENDERBUFFER, 0 );
	glBindFramebuffer( GL_FRAMEBUFFER, 0 );

	return lpr_true;
}

LPR_DEF
lpr_bool
lpr_create_program_from_files( GLuint * gl_program,
                               const char * vertex_shader_file_path,
                               const char * fragment_shader_file_path,
                               void * alloc_context )
{
	char * vertex_shader_text   = NULL,
	     * fragment_shader_text = NULL;

	lpr_s64 read;
	read =
	lpr_read_binary_file( vertex_shader_file_path,
	                        (void**)&vertex_shader_text, 0, alloc_context );
	if ( !lpr_is_IO_OK( read ) )
	{
		lpr_log_err( "Error reading file \"%s\": %s",
		             vertex_shader_file_path,
		             Lpr_File_Sys_Error_strings[-read] );
		return lpr_false;
	}
	GLint vertex_shader_text_length = read;

	read =
	lpr_read_binary_file( fragment_shader_file_path,
	                        (void**)&fragment_shader_text, 0, alloc_context );
	if ( !lpr_is_IO_OK( read ) )
	{
		lpr_log_err( "Error reading file \"%s\": %s",
		         fragment_shader_file_path,
		         Lpr_File_Sys_Error_strings[-read] );
		return lpr_false;
	}
	GLint fragment_shader_text_length = read;

	lpr_bool ret =
	lpr_create_program_from_text(
	   gl_program,
	   vertex_shader_text,   vertex_shader_text_length,
	   fragment_shader_text, fragment_shader_text_length );

	lpr_free( vertex_shader_text,   alloc_context );
	lpr_free( fragment_shader_text, alloc_context );

	return ret;
}

LPR_DEF
lpr_bool
lpr_create_program_from_text( GLuint *     gl_program,
                              const char * vertex_shader_text,
                              GLint        vertex_shader_text_length,
                              const char * fragment_shader_text,
                              GLint        fragment_shader_text_length )
{

	*gl_program = glCreateProgram( );
	GLuint gl_p = *gl_program;

	GLuint gl_shader_vertex,
	       gl_shader_fragment;

	gl_shader_vertex   = glCreateShader( GL_VERTEX_SHADER   );
	gl_shader_fragment = glCreateShader( GL_FRAGMENT_SHADER );
	if ( !gl_shader_vertex || !gl_shader_fragment )
	{
		lpr_log_err( "Error creating shader objects." );

		glDeleteProgram( gl_p );

		glDetachShader( gl_p, gl_shader_vertex );
		glDeleteShader( gl_shader_vertex );

		glDetachShader( gl_p, gl_shader_fragment );
		glDeleteShader( gl_shader_fragment );
		return lpr_false;
	}

	// NOTE(theGiallo): compile and attach vertex shader
	glShaderSource( gl_shader_vertex,                    // shader
	                1,                                   // count
	                (const GLchar**)&vertex_shader_text, // string
	                &vertex_shader_text_length );        // length
	glCompileShader( gl_shader_vertex );

	// NOTE(theGiallo): check vertex shader compilation
	GLint compiled = GL_FALSE;
	int info_log_length = 0;
	glGetShaderiv( gl_shader_vertex, GL_COMPILE_STATUS,  &compiled );
	glGetShaderiv( gl_shader_vertex, GL_INFO_LOG_LENGTH, &info_log_length );
	if ( info_log_length > 0 )
	{
		char shader_info_log[info_log_length+1];
		glGetShaderInfoLog( gl_shader_vertex, info_log_length, NULL,
		                    shader_info_log );
		lpr_log_info( "\nVertex shader compilation infos (%dB):\n" \
		              "----------------------------------------\n" \
		              "%s\n" \
		              "----------------------------------------\n",
		              info_log_length,
		              shader_info_log );
	}
	if ( compiled == GL_FALSE )
	{
		lpr_log_err( "Error compiling vertex shader." );

		glDeleteProgram( gl_p );

		glDetachShader( gl_p, gl_shader_vertex );
		glDeleteShader( gl_shader_vertex );

		glDetachShader( gl_p, gl_shader_fragment );
		glDeleteShader( gl_shader_fragment );
		return lpr_false;
	}
	// NOTE(theGiallo): compilation successfull => attach vertex shader
	glAttachShader( gl_p, gl_shader_vertex );


	// NOTE(theGiallo): compile and attach fragment shader
	glShaderSource( gl_shader_fragment,                    // shader
	                1,                                     // count
	                (const GLchar**)&fragment_shader_text, // string
	                &fragment_shader_text_length );        // length
	glCompileShader( gl_shader_fragment );

	// NOTE(theGiallo): check fragment shader compilation
	compiled = GL_FALSE;
	info_log_length = 0;
	glGetShaderiv( gl_shader_fragment, GL_COMPILE_STATUS,  &compiled );
	glGetShaderiv( gl_shader_fragment, GL_INFO_LOG_LENGTH, &info_log_length );
	if ( info_log_length > 0 )
	{
		char shader_info_log[info_log_length+1];
		glGetShaderInfoLog( gl_shader_fragment, info_log_length, NULL,
		                    shader_info_log );
		lpr_log_info( "\nFragment shader compilation infos (%dB):\n" \
		              "----------------------------------------\n" \
		              "%s\n" \
		              "----------------------------------------\n",
		              info_log_length,
		              shader_info_log );
	}
	if ( compiled == GL_FALSE )
	{
		lpr_log_err( "Error compiling vertex shader." );

		glDeleteProgram( gl_p );

		glDetachShader( gl_p, gl_shader_vertex );
		glDeleteShader( gl_shader_vertex );

		glDetachShader( gl_p, gl_shader_fragment );
		glDeleteShader( gl_shader_fragment );
		return lpr_false;
	}
	// NOTE(theGiallo): compilation successfull => attach fragment shader
	glAttachShader( gl_p, gl_shader_fragment );


	// NOTE(theGiallo): link the program
	glLinkProgram( gl_p );

	// NOTE(theGiallo): check the program linking
	{
		GLint link_status = GL_FALSE;
		int info_log_length;
		glGetProgramiv(gl_p, GL_INFO_LOG_LENGTH, &info_log_length);
		if ( info_log_length > 0 )
		{
			char program_info_log[info_log_length+1];
			glGetProgramInfoLog( gl_p, info_log_length, NULL,
			                     program_info_log );
			lpr_log_info( "\nProgram linking infos:\n\n%s\n", program_info_log );
		}
		glGetProgramiv( gl_p, GL_LINK_STATUS, &link_status );
		if ( link_status == GL_FALSE )
		{
			lpr_log_err( "Error linking the program." );

			glDeleteProgram( gl_p );

			glDetachShader( gl_p, gl_shader_vertex );
			glDeleteShader( gl_shader_vertex );

			glDetachShader( gl_p, gl_shader_fragment );
			glDeleteShader( gl_shader_fragment );
			return lpr_false;
		}
	}

	glValidateProgram( gl_p );

	// NOTE(theGiallo): check the program validation
	{
		GLint validation_status = GL_FALSE;
		int info_log_length;
		glGetProgramiv(gl_p, GL_INFO_LOG_LENGTH, &info_log_length);
		if ( info_log_length > 0 )
		{
			char program_info_log[info_log_length+1];
			glGetProgramInfoLog( gl_p, info_log_length, NULL,
			                     program_info_log );
			lpr_log_info( "\nProgram validation infos:\n\n%s\n", program_info_log );
		}
		glGetProgramiv( gl_p, GL_VALIDATE_STATUS, &validation_status );
		if ( validation_status == GL_FALSE )
		{
			lpr_log_err( "Error validating the program." );

			glDeleteProgram( gl_p );

			glDetachShader( gl_p, gl_shader_vertex );
			glDeleteShader( gl_shader_vertex );

			glDetachShader( gl_p, gl_shader_fragment );
			glDeleteShader( gl_shader_fragment );
			return lpr_false;
		}
	}

	// NOTE(theGiallo): we will never modify or reuse the shader objects, so we can detach
	// and delete them.
	glDetachShader( gl_p, gl_shader_vertex );
	glDeleteShader( gl_shader_vertex );

	glDetachShader( gl_p, gl_shader_fragment );
	glDeleteShader( gl_shader_fragment );

	return lpr_true;
}

LPR_DEF
lpr_bool
lpr_create_texture( void * raw, Lpr_V2u32 size, Lpr_Img_Fmt format,
                    lpr_bool raw_is_sRGB, lpr_bool carthesian,
                    lpr_bool has_premultiplied_alpha,
                    Lpr_Texture * texture
                    ,
                    Lpr_Texture_Filter min_filter,
                    Lpr_Texture_Filter mag_filter,
                    Lpr_Wrap wrap_x,
                    Lpr_Wrap wrap_y,
                    Lpr_Scale_Filter mipmap_filter,
                    Lpr_V2 atlas_size_in_frames,
                    void * alloc_context
                     )
{
	(void) alloc_context;

	lpr_assert_m( mag_filter != LPR_MIPMAP_NEAREST,
	              "magnification filter can only be nearest or linear!" );
	lpr_assert_m( mag_filter != LPR_MIPMAP_LINEAR,
	              "magnification filter can only be nearest or linear!" );
	lpr_assert_m( mag_filter != LPR_MIPMAP_BILINEAR,
	              "magnification filter can only be nearest or linear!" );
	lpr_assert_m( mag_filter != LPR_MIPMAP_TRILINEAR,
	              "magnification filter can only be nearest or linear!" );

	if ( !carthesian )
	{
		switch ( format )
		{
			case LPR_RGBA8:
				lpr_flip_RGBA8_image_vertically( raw, size,
				                                 !has_premultiplied_alpha,
				                                 raw_is_sRGB );
				break;
			case LPR_RGB8:
				lpr_flip_RGB8_image_vertically( raw, size );
				break;
			case LPR_GRAYSCALE8:
			case LPR_GRAYSCALE8_ALPHA:
				lpr_flip_GRAYSCALE8_image_vertically( raw, size );
				break;
			default:
				lpr_log_err( "not supported yet" );
				LPR_ILLEGAL_PATH();
				break;
		}
	} else
	if ( !has_premultiplied_alpha )
	{
		if ( format == LPR_RGBA8  )
		{
			lpr_premultiply_alpha_of_RGBA8_image( raw, size, raw_is_sRGB );
		}
	}

	if ( !glIsTexture( texture->gl_texture ) )
	{
		glGenTextures( 1, &texture->gl_texture );
		if ( !texture->gl_texture )
		{
			lpr_log_err( "OpenGL error while loading texture! (glGenTextures)" );
			return lpr_false;
		}
	}
	GLint gl_internal_format = GL_SRGB8_ALPHA8;
	GLenum gl_format = GL_RGBA;
	GLenum gl_type = GL_UNSIGNED_BYTE;
	switch ( format )
	{
		case LPR_RGBA8:
			gl_format = GL_RGBA;
			gl_type = GL_UNSIGNED_BYTE;
			break;
		case LPR_RGB8:
			gl_format = GL_RGB;
			gl_type = GL_UNSIGNED_BYTE;
			break;
		case LPR_GRAYSCALE8:
		case LPR_GRAYSCALE8_ALPHA:
			gl_format = GL_RED;
			gl_type = GL_UNSIGNED_BYTE;
			break;
		default:
			LPR_ILLEGAL_PATH();
			break;
	}
	if ( format == LPR_RGBA8 && raw_is_sRGB )
	{
		gl_internal_format = GL_SRGB8_ALPHA8;
	} else
	if ( format == LPR_RGBA8 && !raw_is_sRGB )
	{
		gl_internal_format = GL_RGBA8;
	} else
	if ( format == LPR_RGB8 && raw_is_sRGB )
	{
		gl_internal_format = GL_SRGB8;
	} else
	if ( format == LPR_RGB8 && !raw_is_sRGB )
	{
		gl_internal_format = GL_RGB8;
	} else
	if ( ( format == LPR_GRAYSCALE8 || format == LPR_GRAYSCALE8_ALPHA ) && !raw_is_sRGB )
	{
		gl_internal_format = GL_R8;
	} else
	{
		LPR_ILLEGAL_PATH();
	}
	glBindTexture( GL_TEXTURE_2D, texture->gl_texture );
	glTexImage2D( GL_TEXTURE_2D, 0, gl_internal_format, size.w, size.h,
	              0, gl_format, gl_type, raw );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
	                 lpr_C_texture_filter_gltexparameter[min_filter] );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
	                 lpr_C_texture_filter_gltexparameter[mag_filter] );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
	                 lpr_C_wrap_gltexparameter[wrap_x] );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
	                 lpr_C_wrap_gltexparameter[wrap_y] );
	if ( format == LPR_GRAYSCALE8 )
	{
		GLint swizzle_mask[] = {GL_RED, GL_RED, GL_RED, GL_ONE};
		glTexParameteriv( GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_RGBA, swizzle_mask );
	}
	if ( format == LPR_GRAYSCALE8_ALPHA )
	{
		GLint swizzle_mask[] = {GL_RED, GL_RED, GL_RED, GL_RED};
		glTexParameteriv( GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_RGBA, swizzle_mask );
	}

	texture->default_overflow.x = wrap_x;
	texture->default_overflow.y = wrap_y;
	texture->default_min_filter = min_filter;
	texture->default_mag_filter = mag_filter;


	if ( min_filter == LPR_MIPMAP_NEAREST ||
	     min_filter == LPR_MIPMAP_LINEAR ||
	     min_filter == LPR_MIPMAP_BILINEAR ||
	     min_filter == LPR_MIPMAP_TRILINEAR
	   )
	{
		const int BYTES_PER_PIXEL = lpr_C_img_fmt_bytes_per_pixel[format];
		Lpr_V2u32 mip_size = size;
		mip_size.x /= 2;
		mip_size.y /= 2;
		void * mipmap_to_free = lpr_calloc( mip_size.x * mip_size.y * BYTES_PER_PIXEL + 3, 1, alloc_context );
		lpr_assert_m( mipmap_to_free, "calloc returned NULL!" );
		void * mipmap = (void*)( ( (lpr_u64)mipmap_to_free + 3 ) & ( ~0x3L ) ); // NOTE(theGiallo): aligning to 4 bytes
		//lpr_log_dbg( "mipmap = 0x%016x", mipmap );
		lpr_u32 level = 0;
		// TODO(theGiallo, 2015/12/21): WORKING on this
		// TODO(theGiallo, 2015/12/22): this works with powers of 2. need to adapt to boxes of decimal size
		// TODO(theGiallo, 2015/12/27): unused mipmap_filter

		if ( format == LPR_RGBA8 )
		{
			while ( mip_size.w >= atlas_size_in_frames.w &&
			        mip_size.h >= atlas_size_in_frames.h )
			{
				++level;
				//lpr_log_dbg( "generating mipmap level %u", level );

				Lpr_V2u32 box_size = {size.x / mip_size.x, size.y / mip_size.y};
				lpr_u32 box_area = box_size.x * box_size.y;
				//lpr_log_dbg( "    box size = %3u x %3u ", box_size.x, box_size.y );
				for ( lpr_u32 mmy = 0; mmy < mip_size.y; ++mmy )
				{
					for ( lpr_u32 mmx = 0; mmx < mip_size.x; ++mmx )
					{
						Lpr_Rgba_f32 pixel = LPR_ZERO_STRUCT;
						for ( lpr_u32 box_y = 0; box_y < box_size.y; ++box_y )
						{
							for ( lpr_u32 box_x = 0; box_x < box_size.x; ++box_x )
							{
								Lpr_Rgba_u8 a_pixel = ((Lpr_Rgba_u8*)raw)
								                [( mmy * box_size.y + box_y ) * size.x +
								                 mmx * box_size.x + box_x];
								if ( raw_is_sRGB )
								{
									pixel.r += lpr_pow( a_pixel.r / 255.0f, 2.2f );
									pixel.g += lpr_pow( a_pixel.g / 255.0f, 2.2f );
									pixel.b += lpr_pow( a_pixel.b / 255.0f, 2.2f );
									pixel.a += lpr_pow( a_pixel.a / 255.0f, 2.2f );
								} else
								{
									pixel.r += a_pixel.r;
									pixel.g += a_pixel.g;
									pixel.b += a_pixel.b;
									pixel.a += a_pixel.a;
								}
							}
						}
						if ( raw_is_sRGB )
						{
							pixel.r = lpr_pow( lpr_clamp( pixel.r / box_area, 0.0f, 1.0f ),
							                   1.0f / 2.2f ) * 255.0f;
							pixel.g = lpr_pow( lpr_clamp( pixel.g / box_area, 0.0f, 1.0f ),
							                   1.0f / 2.2f ) * 255.0f;
							pixel.b = lpr_pow( lpr_clamp( pixel.b / box_area, 0.0f, 1.0f ),
							                   1.0f / 2.2f ) * 255.0f;
							pixel.a = lpr_pow( lpr_clamp( pixel.a / box_area, 0.0f, 1.0f ),
							                   1.0f / 2.2f ) * 255.0f;
						} else
						{
							pixel.r = lpr_clamp( (int)lpr_round( pixel.r / box_area ),
							                     0, 255 );
							pixel.g = lpr_clamp( (int)lpr_round( pixel.g / box_area ),
							                     0, 255 );
							pixel.b = lpr_clamp( (int)lpr_round( pixel.b / box_area ),
							                     0, 255 );
							pixel.a = lpr_clamp( (int)lpr_round( pixel.a / box_area ),
							                     0, 255 );
						}
						Lpr_Rgba_u8 * mm_px = (Lpr_Rgba_u8*)mipmap +
						                      ( mmy * mip_size.x + mmx );
						mm_px->r = (lpr_u8)pixel.r;
						mm_px->g = (lpr_u8)pixel.g;
						mm_px->b = (lpr_u8)pixel.b;
						mm_px->a = (lpr_u8)pixel.a;
					}
				}

				//lpr_log_dbg( "    mip size = %3u x %3u ", mip_size.x, mip_size.y );
				glTexImage2D( GL_TEXTURE_2D, level, gl_internal_format,
				              mip_size.w, mip_size.h,
				              0, gl_format, gl_type, mipmap );
				mip_size.x /= 2;
				mip_size.y /= 2;
			}
		} else
		if ( format == LPR_RGB8 )
		{
			while ( mip_size.w >= atlas_size_in_frames.w &&
			        mip_size.h >= atlas_size_in_frames.h )
			{
				++level;
				//lpr_log_dbg( "generating mipmap level %u", level );

				Lpr_V2u32 box_size = {size.x / mip_size.x, size.y / mip_size.y};
				lpr_u32 box_area = box_size.x * box_size.y;
				//lpr_log_dbg( "    box size = %3u x %3u ", box_size.x, box_size.y );
				for ( lpr_u32 mmy = 0; mmy < mip_size.y; ++mmy )
				{
					for ( lpr_u32 mmx = 0; mmx < mip_size.x; ++mmx )
					{
						Lpr_Rgb_f32 pixel = LPR_ZERO_STRUCT;
						for ( lpr_u32 box_y = 0; box_y < box_size.y; ++box_y )
						{
							for ( lpr_u32 box_x = 0; box_x < box_size.x; ++box_x )
							{
								Lpr_Rgb_u8 a_pixel =
								   ((Lpr_Rgb_u8*)raw)
								   [( mmy * box_size.y + box_y ) * size.x +
								   mmx * box_size.x + box_x];
								if ( raw_is_sRGB )
								{
									pixel.r += lpr_pow( a_pixel.r / 255.0f, 2.2f );
									pixel.g += lpr_pow( a_pixel.g / 255.0f, 2.2f );
									pixel.b += lpr_pow( a_pixel.b / 255.0f, 2.2f );
								} else
								{
									pixel.r += a_pixel.r;
									pixel.g += a_pixel.g;
									pixel.b += a_pixel.b;
								}
							}
						}
						if ( raw_is_sRGB )
						{
							pixel.r = lpr_pow( lpr_clamp( pixel.r / box_area, 0.0f, 1.0f ),
							                   1.0f / 2.2f ) * 255.0f;
							pixel.g = lpr_pow( lpr_clamp( pixel.g / box_area, 0.0f, 1.0f ),
							                   1.0f / 2.2f ) * 255.0f;
							pixel.b = lpr_pow( lpr_clamp( pixel.b / box_area, 0.0f, 1.0f ),
							                   1.0f / 2.2f ) * 255.0f;
						} else
						{
							pixel.r = lpr_clamp( (int)lpr_round( pixel.r / box_area ),
							                     0, 255 );
							pixel.g = lpr_clamp( (int)lpr_round( pixel.g / box_area ),
							                     0, 255 );
							pixel.b = lpr_clamp( (int)lpr_round( pixel.b / box_area ),
							                     0, 255 );
						}
						Lpr_Rgb_u8 * mm_px = (Lpr_Rgb_u8*)mipmap +
						                     ( mmy * mip_size.x + mmx );
						mm_px->r = (lpr_u8)pixel.r;
						mm_px->g = (lpr_u8)pixel.g;
						mm_px->b = (lpr_u8)pixel.b;
					}
				}

				//lpr_log_dbg( "    mip size = %3u x %3u ", mip_size.x, mip_size.y );
				glTexImage2D( GL_TEXTURE_2D, level, gl_internal_format,
				              mip_size.w, mip_size.h,
				              0, gl_format, gl_type, mipmap );
				mip_size.x /= 2;
				mip_size.y /= 2;
			}
		} else
		if ( format == LPR_GRAYSCALE8 || format == LPR_GRAYSCALE8_ALPHA )
		{
			while ( mip_size.w >= atlas_size_in_frames.w &&
			        mip_size.h >= atlas_size_in_frames.h )
			{
				++level;
				//lpr_log_dbg( "generating mipmap level %u", level );

				Lpr_V2u32 box_size = {size.x / mip_size.x, size.y / mip_size.y};
				lpr_u32 box_area = box_size.x * box_size.y;
				//lpr_log_dbg( "    box size = %3u x %3u ", box_size.x, box_size.y );
				for ( lpr_u32 mmy = 0; mmy < mip_size.y; ++mmy )
				{
					for ( lpr_u32 mmx = 0; mmx < mip_size.x; ++mmx )
					{
						lpr_f32 pixel = LPR_ZERO_STRUCT;
						for ( lpr_u32 box_y = 0; box_y < box_size.y; ++box_y )
						{
							for ( lpr_u32 box_x = 0; box_x < box_size.x; ++box_x )
							{
								lpr_u8 a_pixel =
								   ((lpr_u8*)raw)
								   [( mmy * box_size.y + box_y ) * size.x +
								   mmx * box_size.x + box_x];
								if ( raw_is_sRGB )
								{
									pixel += lpr_pow( a_pixel / 255.0f, 2.2f );
								} else
								{
									pixel += a_pixel;
								}
							}
						}
						if ( raw_is_sRGB )
						{
							pixel = lpr_pow( lpr_clamp( pixel / box_area, 0.0f, 1.0f ),
							                 1.0f / 2.2f ) * 255.0f;
						} else
						{
							pixel = lpr_clamp( (int)lpr_round( pixel / box_area ),
							                   0, 255 );
						}
						lpr_u8 * mm_px = (lpr_u8*)mipmap +
						                 ( mmy * mip_size.x + mmx );
						*mm_px = (lpr_u8)pixel;
					}
				}

				//lpr_log_dbg( "    mip size = %3u x %3u ", mip_size.x, mip_size.y );
				glTexImage2D( GL_TEXTURE_2D, level, gl_internal_format,
				              mip_size.w, mip_size.h,
				              0, gl_format, gl_type, mipmap );
				mip_size.x /= 2;
				mip_size.y /= 2;
			}
		} else
		{
			LPR_ILLEGAL_PATH();
		}
		//lpr_log_dbg( "generated %u mipmap levels", level );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0 );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, level );
		lpr_free( mipmap_to_free, alloc_context );
	}

	// NOTE(theGiallo): cleanup opengl state
	glBindTexture( GL_TEXTURE_2D, 0 );

	texture->size = size;

	return lpr_true;
}

LPR_DEF
void
lpr_flip_RGBA8_image_vertically( void * raw, Lpr_V2u32 size,
                                 lpr_bool premultiply_alpha,
                                 lpr_bool is_sRGB )
{
	const lpr_u32 hh = size.h / 2;
	const lpr_u32 BYTES_PER_PIXEL = lpr_C_img_fmt_bytes_per_pixel[LPR_RGBA8];
	const lpr_u32 BYTES_PER_ROW = size.w * BYTES_PER_PIXEL;
	lpr_u8 * image = (lpr_u8*)raw;
	lpr_u8 * bottom = image + BYTES_PER_ROW * ( size.h - 1 );
	lpr_u8 * top = image;
	for ( lpr_u32 y = 0; y != hh; ++y )
	{
		lpr_u8 row_buf[BYTES_PER_ROW];
		lpr_memcpy( row_buf, top,     BYTES_PER_ROW );
		lpr_memcpy( top,     bottom,  BYTES_PER_ROW );
		lpr_memcpy( bottom,  row_buf, BYTES_PER_ROW );
		if ( premultiply_alpha )
		{
			for ( lpr_u32 x = 0; x != BYTES_PER_ROW; x += BYTES_PER_PIXEL )
			{
				float top_alpha    = (float)top   [x+3] / 255.0f;
				float bottom_alpha = (float)bottom[x+3] / 255.0f;
				if ( is_sRGB )
				{
					top_alpha    = lpr_pow( top_alpha,    1.0f/2.2f );
					bottom_alpha = lpr_pow( bottom_alpha, 1.0f/2.2f );
				}
				for ( lpr_u32 c = 0; c != 3; ++c )
				{
					top   [x+c] = ( top   [x+c] * top_alpha    ) + 0.5f;
					bottom[x+c] = ( bottom[x+c] * bottom_alpha ) + 0.5f;
				}
			}
		}
		bottom -= BYTES_PER_ROW;
		top    += BYTES_PER_ROW;
	}

	if ( premultiply_alpha && hh * 2 != size.h )
	// NOTE: center row
	{
		for ( lpr_u32 x = 0; x != BYTES_PER_ROW; x += BYTES_PER_PIXEL )
		{
			float top_alpha    = (float)top   [x+3] / 255.0f;
			if ( is_sRGB )
			{
				top_alpha    = lpr_pow( top_alpha,    1.0f/2.2f );
			}
			for ( lpr_u32 c = 0; c != 3; ++c )
			{
				top   [x+c] = ( top   [x+c] * top_alpha    ) + 0.5f;
			}
		}
	}
}

LPR_DEF
void
lpr_flip_RGB8_image_vertically( void * raw, Lpr_V2u32 size )
{
	const int hh = size.h / 2;
	const int BYTES_PER_PIXEL = lpr_C_img_fmt_bytes_per_pixel[LPR_RGB8];
	const int BYTES_PER_ROW = size.w * BYTES_PER_PIXEL;
	lpr_u8 * image = (lpr_u8*)raw;
	lpr_u8 * bottom = image + BYTES_PER_ROW * ( size.h - 1 );
	lpr_u8 * top = image;
	for ( int y = 0; y != hh; ++y )
	{
		lpr_u8 row_buf[BYTES_PER_ROW];
		lpr_memcpy( row_buf, top,     BYTES_PER_ROW );
		lpr_memcpy( top,     bottom,  BYTES_PER_ROW );
		lpr_memcpy( bottom,  row_buf, BYTES_PER_ROW );
		bottom -= BYTES_PER_ROW;
		top    += BYTES_PER_ROW;
	}
}

LPR_DEF
void
lpr_flip_GRAYSCALE8_image_vertically( void * raw, Lpr_V2u32 size )
{
	const int hh = size.h / 2;
	const int BYTES_PER_PIXEL = lpr_C_img_fmt_bytes_per_pixel[LPR_GRAYSCALE8];
	const int BYTES_PER_ROW = size.w * BYTES_PER_PIXEL;
	lpr_u8 * image = (lpr_u8*)raw;
	lpr_u8 * bottom = image + BYTES_PER_ROW * ( size.h - 1 );
	lpr_u8 * top = image;
	for ( int y = 0; y != hh; ++y )
	{
		lpr_u8 row_buf[BYTES_PER_ROW];
		lpr_memcpy( row_buf, top,     BYTES_PER_ROW );
		lpr_memcpy( top,     bottom,  BYTES_PER_ROW );
		lpr_memcpy( bottom,  row_buf, BYTES_PER_ROW );
		bottom -= BYTES_PER_ROW;
		top    += BYTES_PER_ROW;
	}
}

LPR_DEF
void
lpr_premultiply_alpha_of_RGBA8_image( void * raw, Lpr_V2u32 size,
                                      lpr_bool is_sRGB )
{
	const lpr_u32 BYTES_PER_PIXEL = lpr_C_img_fmt_bytes_per_pixel[LPR_RGBA8];
	const lpr_u32 BYTES_PER_ROW = size.w * BYTES_PER_PIXEL;
	lpr_u8 * image = (lpr_u8*)raw;
	lpr_u8 * top = image;
	for ( lpr_u32 y = 0; y != size.h; ++y )
	{
		for ( lpr_u32 x = 0; x != BYTES_PER_ROW; x += BYTES_PER_PIXEL )
		{
			float top_alpha = (float)top[x+3] / 255.0f;
			if ( is_sRGB )
			{
				top_alpha = lpr_pow( top_alpha, 1.0f/2.2f );
			}
			for ( lpr_u32 c = 0; c != 3; ++c )
			{
				top[x+c] = ( top[x+c] * top_alpha ) + 0.5f;
			}
		}
		top += BYTES_PER_ROW;
	}
}

LPR_DEF
GLuint
lpr_create_quad_vb()
{
	GLuint quad_vb = 0;
	const float _X_ = 0.5f, _Y_ = 0.5f;
	const GLfloat quad_va_data[] = { // position       uv
	                                    -_X_, -_Y_,    0.0f, 0.0f,
	                                     _X_, -_Y_,    1.0f, 0.0f,
	                                    -_X_,  _Y_,    0.0f, 1.0f,
	                                     _X_,  _Y_,    1.0f, 1.0f,
	                                };

	glGenBuffers( 1, &quad_vb );
	glBindBuffer( GL_ARRAY_BUFFER, quad_vb );
	glBufferData( GL_ARRAY_BUFFER, sizeof( quad_va_data ),
	              quad_va_data,    GL_STATIC_DRAW );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	return quad_vb;
}

GLuint
_lpr_create_per_instance_vb( GLsizei total_size, void * data )
{
	GLuint gl_buf = 0;
	glGenBuffers( 1, &gl_buf );
	if ( gl_buf == 0 )
	{
		lpr_log_err( "vertices buffer not created!" );
		return 0;
	}

	glBindBuffer( GL_ARRAY_BUFFER, gl_buf );
	glBufferData( GL_ARRAY_BUFFER,  // target
	              total_size,       // size
	              data,             // data
	              GL_STREAM_DRAW ); // usage
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	return gl_buf;
}

// NOTE(theGiallo): texture locs must be large enough to contain MAX_TEXTURES
LPR_DEF
lpr_bool
lpr_get_uniform_texture_array_location( GLuint program,
                                        const char * sampler_name,
                                        lpr_u32 count,
                                        GLuint * textures_locs,
                                        GLuint * textures_count )
{
	char u_name[512];
	lpr_u32 i;
	for ( i = 0; i != count; ++i )
	{
		snprintf( u_name, sizeof(u_name), "%s[%d]", sampler_name, i );
		int ret =
		glGetUniformLocation( program, u_name );
		if ( ret != -1 )
		{
			textures_locs[i] = ret;
		} else
		{
			break;
		}
	}

	*textures_count = i;
	return i;
}

LPR_DEF
lpr_bool
lpr_init_draw_data( Lpr_Draw_Data * draw_data, int batches_count,
                    void * alloc_context )
{
	// NOTE(theGiallo): IMPORTANT you have to create a VertexArray and bind it!!!
	glGenVertexArrays( 1, &draw_data->vertex_array_ID );
	glBindVertexArray( draw_data->vertex_array_ID );

	lpr_bool b_ret;
	lpr_s32  i_ret;

	if ( !draw_data->framebuffer_is_sRGB )
	{
		b_ret =
		#if LPR_DEV_SHADERS_FROM_FILES
		lpr_create_program_from_files( &draw_data->gamma_correction_program,
		                               LPR_SHADERS_FOLDER_PATH "gamma_correction.vert",
		                               LPR_SHADERS_FOLDER_PATH "gamma_correction.frag",
		                               alloc_context );
		#else
		lpr_create_program_from_text(
		   &draw_data->debug.gamma_correction_program,
		   LPR_GAMMA_CORRECTION_VERTEX_SHADER_TEXT,
		   sizeof(LPR_GAMMA_CORRECTION_VERTEX_SHADER_TEXT),
		   LPR_GAMMA_CORRECTION_FRAGMENT_SHADER_TEXT,
		   sizeof(LPR_GAMMA_CORRECTION_FRAGMENT_SHADER_TEXT) );
		#endif
		if ( !b_ret )
		{
			lpr_log_err( "Could not create the gamma_correction program!" );
			return lpr_false; // TODO(theGiallo, 2015/09/24): cleanup call?
		}

		// TODO(theGiallo, 2015/12/04): check if this is a good thing
		// NOTE: quad already ready for rendering, with no transformation
		const float _X_ = 1.0f, _Y_ = 1.0f;
		const GLfloat quad_va_data[] = { // position       uv
		                                    -_X_, -_Y_,    0.0f, 0.0f,
		                                     _X_, -_Y_,    1.0f, 0.0f,
		                                    -_X_,  _Y_,    0.0f, 1.0f,
		                                     _X_,  _Y_,    1.0f, 1.0f,
		                                };

		glGenBuffers( 1, &draw_data->fullscreen_quad_vb );
		glBindBuffer( GL_ARRAY_BUFFER, draw_data->fullscreen_quad_vb );
		glBufferData( GL_ARRAY_BUFFER, sizeof( quad_va_data ),
		              quad_va_data,    GL_STATIC_DRAW );
		glBindBuffer( GL_ARRAY_BUFFER, 0 );

		i_ret = glGetAttribLocation( draw_data->gamma_correction_program, "vertex" );
		if ( i_ret != -1 )
		{
			draw_data->gc_vertex_loc = i_ret;
		} else
		{
			lpr_log_err( "no vertex attribute vertex in gamma_correction shader" );
			return lpr_false;
		}

		i_ret = glGetAttribLocation( draw_data->gamma_correction_program, "uv" );
		if ( i_ret != -1 )
		{
			draw_data->gc_uv_loc = i_ret;
		} else
		{
			lpr_log_err( "no vertex attribute uv in gamma_correction shader" );
			return lpr_false;
		}


		// NOTE(theGiallo): we create a frame buffer object so we can work linearly,
		// before end we blit correcting gamma.
		lpr_bool res = lpr_create_linear_fb( draw_data );
		if ( ! res )
		{
			return lpr_false;
		}
	}

	draw_data->quad_vb = lpr_create_quad_vb();
	if ( !draw_data->quad_vb )
	{
		lpr_log_err( "Could not create the quad array buffer!" );
		return lpr_false; //TODO(theGiallo, 2015/09/24): cleanup call?
	}

	for ( lpr_s32 i = 0; i!=2; ++i )
	{
		if ( i )
		{
			lpr_log_dbg( "loading stencil shader program" );
		} else
		{
			lpr_log_dbg( "loading no-stencil shader program" );
		}
		Lpr_Draw_Data::Instanced_Shader_Data * shader_data =
		   i ? &draw_data->stencils : &draw_data->no_stencils;
		b_ret =
		#if LPR_DEV_SHADERS_FROM_FILES
		lpr_create_program_from_files(
		   &shader_data->quad_program,
		   i ? LPR_SHADERS_FOLDER_PATH "stencil_quad_atlas.vert" :
		       LPR_SHADERS_FOLDER_PATH "quad_atlas.vert",
		   i ? LPR_SHADERS_FOLDER_PATH "stencil_quad_atlas.frag" :
		       LPR_SHADERS_FOLDER_PATH "quad_atlas.frag",
		       alloc_context );
		#else
		lpr_create_program_from_text(
		   &shader_data->debug.quad_program,
		   i ? LPR_SHADER_DEFINE_STENCILS_ENABLED
		       LPR_QUAD_ATLAS_VERTEX_SHADER_TEXT :
		       LPR_QUAD_ATLAS_VERTEX_SHADER_TEXT,
		   i ? sizeof(LPR_SHADER_DEFINE_STENCILS_ENABLED) +
		       sizeof(LPR_QUAD_ATLAS_VERTEX_SHADER_TEXT) :
		       sizeof(LPR_QUAD_ATLAS_VERTEX_SHADER_TEXT),
		   i ? LPR_SHADER_DEFINE_STENCILS_ENABLED ?
		       LPR_QUAD_ATLAS_FRAGMENT_SHADER_TEXT :
		       LPR_QUAD_ATLAS_FRAGMENT_SHADER_TEXT,
		   i ? sizeof(LPR_SHADER_DEFINE_STENCILS_ENABLED) +
		       sizeof(LPR_QUAD_ATLAS_FRAGMENT_SHADER_TEXT) :
		       sizeof(LPR_QUAD_ATLAS_FRAGMENT_SHADER_TEXT)
		   );
		#endif
		if ( !b_ret )
		{
			lpr_log_err( "Could not create the %s quad program!",
			             i ? "stencils enabled" : "stencils disabled" );
			return lpr_false; //TODO(theGiallo, 2015/09/24): cleanup call?
		}

		glUseProgram( shader_data->quad_program );

		GLint i_ret;
		i_ret = glGetAttribLocation( shader_data->quad_program, "vertex" );
		if ( i_ret != -1 )
		{
			shader_data->va_vertex_loc = i_ret;
		} else
		{
			lpr_log_err( "no vertex attribute vertex in quad_atlas shader" );
			return lpr_false;
		}

		i_ret = glGetAttribLocation( shader_data->quad_program, "uv" );
		if ( i_ret != -1 )
		{
			shader_data->va_uv_loc = i_ret;
		} else
		{
			lpr_log_err( "no vertex attribute uv in quad_atlas shader" );
			return lpr_false;
		}

		i_ret = glGetUniformLocation( shader_data->quad_program, "VP" );
		if ( i_ret != -1 )
		{
			shader_data->VP_loc = i_ret;
		} else
		{
			lpr_log_err( "no uniform VP in quad_atlas shader" );
			return lpr_false;
		}


		#define LPR_LOAD_ATTRIB_LOC( name ) \
		i_ret = glGetAttribLocation( shader_data->quad_program, "quad_data." LPR_TOSTRING( name ) );\
		if ( i_ret != -1 )\
		{\
			shader_data->per_instance_data_loc.name = i_ret;\
		} else\
		{\
			lpr_log_err( "no vertex attribute quad_data." LPR_TOSTRING( name ) " in quad_atlas shader" );\
			return lpr_false;\
		}

		LPR_LOAD_ATTRIB_LOC( pos                );
		LPR_LOAD_ATTRIB_LOC( size               );
		LPR_LOAD_ATTRIB_LOC( rot                );
		LPR_LOAD_ATTRIB_LOC( color              );
		LPR_LOAD_ATTRIB_LOC( texture_id         );
		if ( i )
		{
			LPR_LOAD_ATTRIB_LOC( stencil_id         );
		}
		LPR_LOAD_ATTRIB_LOC( uv_offset          );
		LPR_LOAD_ATTRIB_LOC( uv_scale           );
		LPR_LOAD_ATTRIB_LOC( uv_origin          );
		LPR_LOAD_ATTRIB_LOC( uv_limits          );
		LPR_LOAD_ATTRIB_LOC( texture_overflow_x );
		LPR_LOAD_ATTRIB_LOC( texture_overflow_y );
		LPR_LOAD_ATTRIB_LOC( texture_min_filter );
		LPR_LOAD_ATTRIB_LOC( texture_mag_filter );

		#undef LPR_LOAD_ATTRIB_LOC

		b_ret =
		lpr_get_uniform_texture_array_location(
		   shader_data->quad_program, "textures", lpr_C_max_textures,
		   shader_data->textures_locs, &shader_data->max_textures_count );
		if ( !b_ret )
		{
			lpr_log_err( "no \"textures[0]\" in this shader program!" );
			return lpr_false;
		}

		if ( i )
		{
			b_ret =
			lpr_get_uniform_texture_array_location(
			   shader_data->quad_program, "stencils",
			   lpr_C_stencils_textures_count,
			   shader_data->stencils_locs, &shader_data->max_textures_count );
			if ( !b_ret )
			{
				lpr_log_err( "no \"stencils[0]\" in this shader program!" );
				return lpr_false;
			}
		}
	}

	// NOTE(theGiallo): stencil draw programs
	for ( lpr_s32 i = 0; i!=2; ++i )
	{
		Lpr_Draw_Data::Stencil_Shader_Data * shader_data =
		   i ? &draw_data->stencil_shader_stencils :
		       &draw_data->stencil_shader_no_stencils;
		b_ret =
		#if LPR_DEV_SHADERS_FROM_FILES
		lpr_create_program_from_files(
		   &shader_data->program,
		   i ? LPR_SHADERS_FOLDER_PATH "stencil_draw_stencil.vert" :
		       LPR_SHADERS_FOLDER_PATH "stencil_draw_no_stencil.vert",
		   i ? LPR_SHADERS_FOLDER_PATH "stencil_draw_stencil.frag" :
		       LPR_SHADERS_FOLDER_PATH "stencil_draw_no_stencil.frag",
		   alloc_context );
		#else
		lpr_create_program_from_text(
		   &shader_data->program,
		   i ? LPR_SHADER_DEFINE_STENCILS_ENABLED
		       LPR_STENCIL_DRAW_VERTEX_SHADER_TEXT :
		       LPR_STENCIL_DRAW_VERTEX_SHADER_TEXT,
		   i ? sizeof(LPR_SHADER_DEFINE_STENCILS_ENABLED) +
		       sizeof(LPR_STENCIL_DRAW_VERTEX_SHADER_TEXT) :
		       sizeof(LPR_STENCIL_DRAW_VERTEX_SHADER_TEXT),
		   i ? LPR_SHADER_DEFINE_STENCILS_ENABLED
		       LPR_STENCIL_DRAW_FRAGMENT_SHADER_TEXT :
		       LPR_STENCIL_DRAW_FRAGMENT_SHADER_TEXT,
		   i ? sizeof(LPR_SHADER_DEFINE_STENCILS_ENABLED) +
		       sizeof(LPR_STENCIL_DRAW_FRAGMENT_SHADER_TEXT) :
		       sizeof(LPR_STENCIL_DRAW_FRAGMENT_SHADER_TEXT)
		   );
		#endif
		if ( !b_ret )
		{
			lpr_log_err( "Could not create the %s stencil draw program!",
			             i ? "stencils enabled" : "stencils disabled" );
			return lpr_false; //TODO(theGiallo, 2015/09/24): cleanup call?
		}

		#define LPR_LOAD_ATTRIB_LOC( name ) \
		i_ret = glGetAttribLocation( shader_data->program, LPR_TOSTRING( name ) );\
		if ( i_ret != -1 )\
		{\
			shader_data->name##_loc = i_ret;\
		} else\
		{\
			lpr_log_err( "no vertex attribute " LPR_TOSTRING( name )\
			             " in stencil draw shader %s",\
			             i ? "stencil" : "no stencil" );\
			return lpr_false;\
		}

		#define LPR_LOAD_UNIF_LOC( name ) \
		i_ret = glGetUniformLocation( shader_data->program, LPR_TOSTRING( name ) );\
		if ( i_ret != -1 )\
		{\
			shader_data->name##_loc = i_ret;\
		} else\
		{\
			lpr_log_err( "no uniform '" LPR_TOSTRING( name )\
			             "' in stencil draw shader %s",\
			             i ? "stencil" : "no stencil" );\
			return lpr_false;\
		}

		LPR_LOAD_ATTRIB_LOC( vertex            );
		LPR_LOAD_ATTRIB_LOC( uv                );
		LPR_LOAD_UNIF_LOC( texture           );
		if ( i )
		{
			LPR_LOAD_UNIF_LOC( stencil           );
			LPR_LOAD_UNIF_LOC( stencil_id        );
		}
		LPR_LOAD_UNIF_LOC( draw_mode         );
		LPR_LOAD_UNIF_LOC( target_stencil_id );
		LPR_LOAD_UNIF_LOC( target_stencil    );
		LPR_LOAD_UNIF_LOC( VP                );

		#undef LPR_LOAD_UNIF_LOC
		#undef LPR_LOAD_ATTRIB_LOC

		glGenBuffers( 1, &draw_data->stencil_draw_vb );
		lpr_assert_m( draw_data->stencil_draw_vb,
		   "glGenBuffers( 1, &debug.lines_vb ); didn't create a buffer!" );

		#if 0
		glBindBuffer( GL_ARRAY_BUFFER, draw_data->stencil_draw_vb );
		glBufferData( GL_ARRAY_BUFFER,   // target
		              sizeof( V2 ) * 8 , // size
		              NULL,              // data
		              GL_STREAM_DRAW );  // usage
		#endif

		glGenBuffers( 1, &draw_data->stencil_draw_ib );
		lpr_assert_m( draw_data->stencil_draw_ib,
		   "glGenBuffers( 1, &debug.lines_ib ); didn't create a buffer!" );

		{
			GLubyte b[6] = { 0, 3, 1, 2, 1, 3 };
			lpr_memcpy( draw_data->stencil_draw_index_buffer, b, sizeof(GLubyte[6]) );
		}

		glBindBuffer( GL_ARRAY_BUFFER, draw_data->stencil_draw_ib );
		glBufferData( GL_ARRAY_BUFFER,                      // target
		              sizeof( GLubyte ) * 6,                // size
		              draw_data->stencil_draw_index_buffer, // data
		              GL_STATIC_DRAW );                     // usage
	}

	#if LPR_DEBUG_LINES
		glGenBuffers( 1, &draw_data->debug.lines_vb );
		lpr_assert_m( draw_data->debug.lines_vb,
		   "glGenBuffers( 1, &draw_data->debug.lines_vb ); didn't create a buffer!" );

		glBindBuffer( GL_ARRAY_BUFFER, draw_data->debug.lines_vb );
		glBufferData( GL_ARRAY_BUFFER,                    // target
		              sizeof( draw_data->debug.line_vertices ), // size
		              draw_data->debug.line_vertices,     // data
		              GL_STREAM_DRAW );                   // usage
		b_ret =
		#if LPR_DEV_SHADERS_FROM_FILES
		lpr_create_program_from_files( &draw_data->debug.lines_program,
		                      LPR_SHADERS_FOLDER_PATH "2d_debug_segments.vert",
		                      LPR_SHADERS_FOLDER_PATH "2d_debug_segments.frag",
		                      alloc_context );
		#else
		lpr_create_program_from_text(
		   &draw_data->debug.lines_program,
		   LPR_2D_DEBUG_SEGMENTS_VERTEX_SHADER_TEXT,
		   sizeof(LPR_2D_DEBUG_SEGMENTS_VERTEX_SHADER_TEXT),
		   LPR_2D_DEBUG_SEGMENTS_FRAGMENT_SHADER_TEXT,
		   sizeof(LPR_2D_DEBUG_SEGMENTS_FRAGMENT_SHADER_TEXT) );
		#endif
		if ( !b_ret )
		{
			lpr_log_err( "Could not create the lines program!" );
			return lpr_false; //TODO(theGiallo, 2015/09/24): cleanup call?
		}
		glBindBuffer( GL_ARRAY_BUFFER, 0 );

		i_ret = glGetAttribLocation( draw_data->debug.lines_program, "vertex" );
		if ( i_ret != -1 )
		{
			draw_data->debug.vertex_loc = i_ret;
		} else
		{
			lpr_log_err( "no vertex attribute vertex in debug lines program shader" );
			return lpr_false;
		}
		i_ret = glGetAttribLocation( draw_data->debug.lines_program, "color" );
		if ( i_ret != -1 )
		{
			draw_data->debug.color_loc = i_ret;
		} else
		{
			lpr_log_err( "no color attribute vertex in debug lines program shader" );
			return lpr_false;
		}

		i_ret = glGetUniformLocation( draw_data->debug.lines_program, "VP" );
		if ( i_ret != -1 )
		{
			draw_data->debug.VP_loc = i_ret;
		} else
		{
			lpr_log_err( "no uniform VP in debug lines shader" );
			return lpr_false;
		}
	#endif

	lpr_create_batches( draw_data, batches_count, alloc_context );
	lpr_reset_all_batches( draw_data );
	return lpr_true;
}
LPR_DEF
lpr_bool
lpr_create_batches( Lpr_Draw_Data * draw_data, int batches_count,
                    void * alloc_context )
{
	(void)alloc_context; // NOTE(theGiallo): prevents unused_parameter warning

	draw_data->batches =
	(Lpr_Batch_Data*) lpr_calloc( sizeof(Lpr_Batch_Data),batches_count,
	                              alloc_context );
	if ( !draw_data->batches )
	{
		lpr_log_err( "Could not allocate batches! Requested %d batches (%lu B)",
		             batches_count, sizeof(Lpr_Batch_Data) * batches_count );
		return lpr_false;
	}

	Lpr_Batch_Data * const batches = draw_data->batches;

	for ( int i = 0; i != batches_count; ++i )
	{
		batches[i].per_instance_vb =
		lpr_create_per_instance_vb( batches[i].instances_data );
		if ( !batches[i].per_instance_vb )
		{
			lpr_log_err( "Could not create the quad per instance array buffer!" );
			return lpr_false;
		}
	}
	draw_data->batches_count = batches_count;
	return lpr_true;
}

LPR_DEF
void
lpr_create_stencils_fbs_for_batches( Lpr_Draw_Data * draw_data )
{
	for ( lpr_u32 batch_id = 0; batch_id!= draw_data->batches_count; ++batch_id )
	{
		if ( draw_data->batches[batch_id].manage_stencils_rt )
		{
			lpr_create_stencils_fbs( draw_data, batch_id );
		}
	}
}

lpr_bool
lpr_create_stencils_fbs( Lpr_Draw_Data * draw_data, lpr_u32 batch_id )
{
	GLint gl_width  = draw_data->window_size.w,
	      gl_height = draw_data->window_size.h;

	Lpr_Batch_Data * batch = draw_data->batches + batch_id;
	for ( lpr_s32 j = 0; j!=2; ++j )
	{
		for ( lpr_s32 i = 0; i!=LPR_STENCILS_TEXTURES_COUNT; ++i )
		{
			Lpr_FBO_Texture_Color * fbo = batch->stencil_fbos[j] + i;
			if ( ! glIsFramebuffer( fbo->fbo ) )
			{
				glGenFramebuffers(  1, &fbo->fbo );
			}
			glBindFramebuffer( GL_FRAMEBUFFER, fbo->fbo );
			if ( ! glIsRenderbuffer( fbo->color_rt ) )
			{
				glGenRenderbuffers( 1, &fbo->color_rt );
			}
			glBindRenderbuffer(    GL_RENDERBUFFER, fbo->color_rt );
			glRenderbufferStorage( GL_RENDERBUFFER, GL_RGBA32UI, gl_width, gl_height );
			glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
			                           GL_RENDERBUFFER, fbo->color_rt );

			// NOTE(theGiallo): Generate and bind the OGL texture for diffuse
			if ( ! glIsTexture( fbo->fb_texture ) )
			{
				glGenTextures( 1, &fbo->fb_texture );
			}
			glBindTexture( GL_TEXTURE_2D, fbo->fb_texture );
			glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA32UI, gl_width, gl_height, 0,
			              GL_RGBA_INTEGER, GL_UNSIGNED_INT, NULL );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
			// NOTE(theGiallo): Attach the texture to the FBO
			glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
			                        GL_TEXTURE_2D, fbo->fb_texture, 0 );

			glDrawBuffer( GL_COLOR_ATTACHMENT0 );

			// NOTE(theGiallo): Check if all worked fine and unbind the FBO
			if( !lpr_check_and_log_framebuffer_status() )
			{
				lpr_log_err( "Something failed during FBO initialization. FBO is not complete." );
				// TODO(theGiallo, 2015/09/28): proper clean
				glDeleteFramebuffers( 1, &fbo->fbo );
				glDeleteRenderbuffers( 1, &fbo->color_rt );
				// SDL_Quit(); TODO(theGiallo, 2015/12/04): this is bad, but how to manage it? Return lpr_false?
				LPR_ILLEGAL_PATH();
				return lpr_false;
			}
			lpr_log_framebuffer_info();
		}
	}

	// NOTE(theGiallo): OpenGL cleanup
	glBindTexture( GL_TEXTURE_2D, 0 );
	glBindFramebuffer( GL_FRAMEBUFFER, 0 );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	return lpr_true;
}

////////////////////////////////////////////////////////////////////////////////
// misc

LPR_DEF
void
lpr_reorder_16_s( lpr_u16 * ids, void * data, lpr_u32 count,
                  lpr_u32 elem_size )
{
	lpr_u8 v[elem_size];
	lpr_u8 moved[LPR_BITNSLOTS( 0xFFFF )] = LPR_ZERO_STRUCT;
	for ( lpr_u32 i = 0; i < count; ++i )
	{
		if ( LPR_BITTEST( moved, i ) )
		{
			continue;
		}

		lpr_memcpy( v, (lpr_u8*)data + i * elem_size, elem_size );
		for ( lpr_u16 j = i; ; )
		{
			lpr_u16 next = ids[j];
			if ( next == i )
			{
				lpr_memcpy( (lpr_u8*)data + j * elem_size, v, elem_size );
				LPR_BITSET( moved, j );
				break;
			}
			lpr_memcpy( (lpr_u8*)data + j * elem_size, (lpr_u8*)data + next * elem_size, elem_size );
			LPR_BITSET( moved, j );
			j = next;
		}
	}
}
LPR_DEF
void
lpr_reorder_16_8( lpr_u16 * ids, lpr_u8 * data, lpr_u32 count )
{
	lpr_u8 v;
	lpr_u8 moved[LPR_BITNSLOTS( 0xFFFF )] = LPR_ZERO_STRUCT;
	for ( lpr_u32 i = 0; i < count; ++i )
	{
		if ( LPR_BITTEST( moved, i ) )
		{
			continue;
		}

		v = data[i];
		for ( lpr_u16 j = i; ; )
		{
			lpr_u16 next = ids[j];
			if ( next == i )
			{
				data[j] = v;
				LPR_BITSET( moved, j );
				break;
			}
			data[j] = data[next];
			LPR_BITSET( moved, j );
			j = next;
		}
	}
}
LPR_DEF
void
lpr_reorder_16( lpr_u16 * ids, lpr_u16 * data, lpr_u32 count )
{
	lpr_u16 v;
	lpr_u8 moved[LPR_BITNSLOTS( 0xFFFF )] = LPR_ZERO_STRUCT;
	for ( lpr_u32 i = 0; i < count; ++i )
	{
		if ( LPR_BITTEST( moved, i ) )
		{
			continue;
		}

		v = data[i];
		for ( lpr_u16 j = i; ; )
		{
			lpr_u16 next = ids[j];
			if ( next == i )
			{
				data[j] = v;
				LPR_BITSET( moved, j );
				break;
			}
			data[j] = data[next];
			LPR_BITSET( moved, j );
			j = next;
		}
	}
}

LPR_DEF
void
lpr_reorder_32( lpr_u32 * ids, lpr_u32 * data, lpr_u32 count )
{
	lpr_u32 v;
	lpr_u8 moved[LPR_BITNSLOTS( count )];
	// NOTE(theGiallo): it's dynamic, so we cant initialize it
	lpr_memset( moved, 0, LPR_BITNSLOTS( count ) * LPR_CHAR_BIT );
	for ( lpr_u32 i = 0; i < count; ++i )
	{
		if ( LPR_BITTEST( moved, i ) )
		{
			continue;
		}

		v = data[i];
		for ( lpr_u32 j = i; ; )
		{
			lpr_u32 next = ids[j];
			if ( next == i )
			{
				data[j] = v;
				LPR_BITSET( moved, j );
				break;
			}
			data[j] = data[next];
			LPR_BITSET( moved, j );
			j = next;
		}
	}
}

LPR_DEF
void
lpr_radix_sort_mc_16( lpr_u16 * keys, lpr_u16 * ids, lpr_u16 * tmp,
                      lpr_u32 count )
{
	lpr_u32 firsts[2][256] = LPR_ZERO_STRUCT;

	// NOTE(theGiallo): count buckets occurrencies
	for ( lpr_u32 i = 0; i != count; ++i )
	{
		++firsts[0][( keys[i]      ) & 0xFF];
		++firsts[1][( keys[i] >> 8 ) & 0xFF];
	}

	// NOTE(theGiallo): calculate the position of the last element of every bucket
	lpr_u32 counts[2] = {0,0};
	for ( lpr_u32 i = 0; i != 256; ++i )
	{
		lpr_u32 v[2];
		v[0] = firsts[0][i];
		v[1] = firsts[1][i];
		firsts[0][i] = counts[0];
		firsts[1][i] = counts[1];
		counts[0] += v[0];
		counts[1] += v[1];
	}

	for ( lpr_u8 b = 0; b != 2; ++b )
	{
		for ( lpr_u32 i = 0; i != count; ++i )
		{
			lpr_assert_m( ids[ i ] < count, "ids[ i ] = %d i=%d " LPR_HERE_COLOR() "\n",
			              ids[ i ], i );

			lpr_u8 id = ( keys[ ids[i] ] >> ( 8 * b ) ) & 0xFF;

			lpr_assert_m( firsts[b][ id ] < count,
			              "firsts[b][ id ] = %d b=%d id=%d " LPR_HERE_COLOR() "\n",
			              firsts[b][ id ], b, id );

			tmp[ firsts[b][ id ]++ ] = ids[i];
		}
		lpr_u16 * swp = tmp;
		          tmp = ids;
		          ids = swp;
	}
}

LPR_DEF
void
lpr_radix_sort_mc_32( lpr_u32 * keys, lpr_u32 * ids, lpr_u32 * tmp,
                      lpr_u32 count )
{
	lpr_u32 firsts[4][256] = LPR_ZERO_STRUCT;

	// NOTE(theGiallo): count buckets occurrencies
	for ( lpr_u32 i = 0; i != count; ++i )
	{
		++firsts[0][( keys[i]       ) & 0xFF];
		++firsts[1][( keys[i] >> 8  ) & 0xFF];
		++firsts[2][( keys[i] >> 16 ) & 0xFF];
		++firsts[3][( keys[i] >> 24 ) & 0xFF];
	}

	// NOTE(theGiallo): calculate the position of the last element of every bucket
	lpr_u32 counts[4] = LPR_ZERO_STRUCT;
	for ( lpr_u32 i = 0; i != 256; ++i )
	{
		lpr_u32 v[4];
		v[0] = firsts[0][i];
		v[1] = firsts[1][i];
		v[2] = firsts[2][i];
		v[3] = firsts[3][i];
		firsts[0][i] = counts[0];
		firsts[1][i] = counts[1];
		firsts[2][i] = counts[2];
		firsts[3][i] = counts[3];
		counts[0] += v[0];
		counts[1] += v[1];
		counts[2] += v[2];
		counts[3] += v[3];
	}

	for ( lpr_u8 b=0; b!=4; ++b )
	{
		for ( lpr_u32 i = 0; i != count; ++i )
		{
			lpr_u8 id = ( keys[ ids[i] ] >> ( 8 * b ) ) & 0xFF;
			tmp[ firsts[b][ id ]++ ] = ids[i];
		}
		lpr_u32 * swp = tmp;
		          tmp = ids;
		          ids = swp;
	}
}

LPR_DEF
void
lpr_radix_sort_mc_32_16( lpr_u32 * keys, lpr_u16 * ids, lpr_u16 * tmp,
                         lpr_u32 count )
{
	lpr_u32 firsts[4][256] = LPR_ZERO_STRUCT;

	// NOTE(theGiallo): count buckets occurrencies
	for ( lpr_u32 i = 0; i != count; ++i )
	{
		++firsts[0][( keys[i]       ) & 0xFF];
		++firsts[1][( keys[i] >> 8  ) & 0xFF];
		++firsts[2][( keys[i] >> 16 ) & 0xFF];
		++firsts[3][( keys[i] >> 24 ) & 0xFF];
	}

	// NOTE(theGiallo): calculate the position of the last element of every bucket
	lpr_u32 counts[4] = LPR_ZERO_STRUCT;
	for ( lpr_u32 i = 0; i != 256; ++i )
	{
		lpr_u32 v[4];
		v[0] = firsts[0][i];
		v[1] = firsts[1][i];
		v[2] = firsts[2][i];
		v[3] = firsts[3][i];
		firsts[0][i] = counts[0];
		firsts[1][i] = counts[1];
		firsts[2][i] = counts[2];
		firsts[3][i] = counts[3];
		counts[0] += v[0];
		counts[1] += v[1];
		counts[2] += v[2];
		counts[3] += v[3];
	}

	for ( lpr_u8 b=0; b!=4; ++b )
	{
		for ( lpr_u32 i = 0; i != count; ++i )
		{
			lpr_u8 id = ( keys[ ids[i] ] >> ( 8 * b ) ) & 0xFF;
			tmp[ firsts[b][ id ]++ ] = ids[i];
		}
		lpr_u16 * swp = tmp;
		          tmp = ids;
		          ids = swp;
	}
}

LPR_DEF
void
lpr_radix_sort_mc_32_keys_only( lpr_u32 * keys, lpr_u32 * tmp, lpr_u32 count )
{
	lpr_u32 firsts[4][256] = LPR_ZERO_STRUCT;

	// NOTE(theGiallo): count buckets occurrencies
	for ( lpr_u32 i = 0; i != count; ++i )
	{
		++firsts[0][( keys[i]       ) & 0xFF];
		++firsts[1][( keys[i] >> 8  ) & 0xFF];
		++firsts[2][( keys[i] >> 16 ) & 0xFF];
		++firsts[3][( keys[i] >> 24 ) & 0xFF];
	}

	// NOTE(theGiallo): calculate the position of the last element of every bucket
	lpr_u32 counts[4] = LPR_ZERO_STRUCT;
	for ( lpr_u32 i = 0; i != 256; ++i )
	{
		lpr_u32 v[4];
		v[0] = firsts[0][i];
		v[1] = firsts[1][i];
		v[2] = firsts[2][i];
		v[3] = firsts[3][i];
		firsts[0][i] = counts[0];
		firsts[1][i] = counts[1];
		firsts[2][i] = counts[2];
		firsts[3][i] = counts[3];
		counts[0] += v[0];
		counts[1] += v[1];
		counts[2] += v[2];
		counts[3] += v[3];
	}

	for ( lpr_u8 b=0; b!=4; ++b )
	{
		for ( lpr_u32 i = 0; i != count; ++i )
		{
			lpr_u8 id = ( keys[i] >> ( 8 * b ) ) & 0xFF;
			tmp[ firsts[b][ id ]++ ] = keys[i];
		}
		lpr_u32 * swp = tmp;
		          tmp = keys;
		         keys = swp;
	}
}

LPR_DEF
void
lpr_insertion_sort_16( lpr_u16 * keys, lpr_u16 * data, lpr_u32 count )
{
	for ( lpr_u32 i = 1; i < count; ++i )
	{
		lpr_u16 k = keys[i];
		lpr_u16 d = data[i];
		if ( keys[i-1] > k )
		{
			lpr_u32 j = i;
			do
			{
				keys[j] = keys[j-1];
				data[j] = data[j-1];
			} while ( --j > 0 && keys[j-1] > k );
			keys[j] = k;
			data[j] = d;
		}
	}
}

LPR_DEF
void
lpr_insertion_sort_32( lpr_u32 * keys, lpr_u32 * data, lpr_u32 count )
{
	for ( lpr_u32 i = 1; i < count; ++i )
	{
		lpr_u32 k = keys[i];
		lpr_u32 d = data[i];
		if ( keys[i-1] > k )
		{
			lpr_u32 j = i;
			do
			{
				keys[j] = keys[j-1];
				data[j] = data[j-1];
			} while ( --j > 0 && keys[j-1] > k );
			keys[j] = k;
			data[j] = d;
		}
	}
}

LPR_DEF
void
lpr_radix_sort_bucket_32( lpr_u32 * keys, lpr_u32 * data, lpr_u32 count,
                          lpr_u8 shift )
{
	if ( count <= LPR_INSERTION_SORT_32_IS_BETTER_MAX_COUNT )
	{
		lpr_insertion_sort_32( keys, data, count );
		return;
	}

	lpr_u32 last[256] = LPR_ZERO_STRUCT,
	        pointer[256];

	for ( lpr_u32 i = 0; i != count; ++i )
	{
		++last[( keys[i] >> shift ) & 0xFF];
	}

	pointer[0] = 0;

	for ( lpr_u32 i = 1; i != 256; ++i )
	{
		pointer[i] = last[i-1];
		last[i] += last[i-1];
	}

	for ( lpr_u32 i = 0; i != 256; ++i )
	{
		lpr_u32 value, data_value;
		lpr_u8 j;
		while ( pointer[i] != last[i] )
		{
			value = keys[pointer[i]];
			data_value = data[pointer[i]];
			j = ( value >> shift ) & 0xFF;
			while ( i != j )
			{
				     lpr_u32 tmp = keys[pointer[j]];
				keys[pointer[j]] = value;
				           value = tmp;

				             tmp = data[pointer[j]];
				data[pointer[j]] = data_value;
				      data_value = tmp;

				++pointer[j];

				j = ( value >> shift ) & 0xFF;
			}
			keys[pointer[j]] = value;
			data[pointer[j]] = data_value;
			++pointer[j];
		}
	}

	if ( shift )
	{
		shift -= 8;

		for ( lpr_u32 i = 0; i < 256; ++i )
		{
			lpr_u32 new_start = i ? last[i-1] : 0;
			lpr_u32 new_count = i ? last[i] - last[i-1] : last[0];
			if ( new_count )
			{
				lpr_radix_sort_bucket_32( keys + new_start, data + new_start, new_count, shift );
			}
		}
	}
}

LPR_DEF
void
lpr_radix_sort_32( lpr_u32 * keys, lpr_u32 * data, lpr_u32 count )
{
	lpr_radix_sort_bucket_32( keys, data, count, 24 );
}


LPR_DEF
void
lpr_radix_sort_bucket_16( lpr_u16 * keys, lpr_u16 * data, lpr_u32 count,
                          lpr_u8 shift )
{
	if ( count <= LPR_INSERTION_SORT_16_IS_BETTER_MAX_COUNT )
	{
		lpr_insertion_sort_16( keys, data, count );
		return;
	}

	lpr_u32 last[256] = LPR_ZERO_STRUCT,
	          pointer[256];

	for ( lpr_u32 i = 0; i != count; ++i )
	{
		++last[( keys[i] >> shift ) & 0xFF];
	}

	pointer[0] = 0;

	for ( lpr_u32 i = 1; i != 256; ++i )
	{
		pointer[i] = last[i-1];
		last[i] += last[i-1];
	}

	for ( lpr_u16 i = 0; i != 256; ++i )
	{
		lpr_u16 value, data_value;
		lpr_u8 j;
		while ( pointer[i] != last[i] )
		{
			value = keys[pointer[i]];
			data_value = data[pointer[i]];
			j = ( value >> shift ) & 0xFF;
			while ( i != j )
			{
				     lpr_u16 tmp = keys[pointer[j]];
				keys[pointer[j]] = value;
				           value = tmp;

				             tmp = data[pointer[j]];
				data[pointer[j]] = data_value;
				      data_value = tmp;

				++pointer[j];

				j = ( value >> shift ) & 0xFF;
			}
			keys[pointer[j]] = value;
			data[pointer[j]] = data_value;
			++pointer[j];
		}
	}

	if ( shift )
	{
		shift -= 8;

		for ( lpr_u16 i = 0; i < 256; ++i )
		{
			lpr_u32 new_start = i ? last[i-1] : 0;
			lpr_u32 new_count = i ? last[i] - last[i-1] : last[0];
			if ( new_count )
			{
				lpr_radix_sort_bucket_16( keys + new_start, data + new_start, new_count, shift );
			}
		}
	}
}

LPR_DEF
void
lpr_radix_sort_16( lpr_u16 * keys, lpr_u16 * data, lpr_u32 count )
{
	lpr_radix_sort_bucket_16( keys, data, count, 8 );
}

LPR_DEF
void
lpr_quicksort_iterative_32( lpr_u32 * keys, lpr_u32 * data, lpr_u32 count )
{
	lpr_u32 left = 0,
	          stack[LPR_MAX_STACK_QSIT],
	          pos = 0;
	lpr_u64 seeds[2] = { 2873467L, 2379425723943L };
	for (;;)
	{
		for ( ; left + 1 < count; count++ )
		{
			if ( pos == LPR_MAX_STACK_QSIT )
			{
				pos = 0;
				count = stack[pos]; // NOTE(theGiallo): reset on stack overflow
			}
			lpr_u32 rnd = ( lpr_xorshift128plus( seeds ) >> 32 ) & LPR_MAX_U32;
			// NOTE(theGiallo): pick random pivot
			lpr_u32 pivot = keys[left + rnd % ( count - left )];
			stack[pos++] = count; // NOTE(theGiallo): sort right part later

			// NOTE(theGiallo): partitioning
			for ( lpr_u32 right = left - 1; ; )
			{
				// NOTE(theGiallo): look for greater element
				while ( keys[++right] < pivot );
				// NOTE(theGiallo): look for smaller element
				while ( pivot < keys[--count] );

				if ( right >= count )
				{
					break; // NOTE(theGiallo):  partition point found?
				}
				  lpr_u32 tmp = keys[right];
				  keys[right] = keys[count];
				  keys[count] = tmp;
				          tmp = data[right];
				  data[right] = data[count];
				  data[count] = tmp;
			}
			// NOTE(theGiallo): partitioned, continue left part
		}
		if ( !pos)
		{
			break; // NOTE(theGiallo): stack empty?
		}
		left = count; // NOTE(theGiallo): left to right is sorted
		count = stack[--pos]; // NOTE(theGiallo): get next range to sort
	}
}

LPR_DEF
void
lpr_quicksort_iterative_16( lpr_u16 * keys, lpr_u16 * data, lpr_u32 count )
{
	lpr_u32 left = 0,
	    stack[LPR_MAX_STACK_QSIT],
	    pos = 0;
	lpr_u64 seeds[2] = { 2873467L, 2379425723943L };
	for (;;)
	{
		for ( ; left + 1 < count; count++ )
		{
			if ( pos == LPR_MAX_STACK_QSIT )
			{
				pos = 0;
				count = stack[pos]; // NOTE(theGiallo): reset on stack overflow
			}
			lpr_u16 rnd = lpr_xorshift128plus( seeds ) & LPR_MAX_U16;
			// NOTE(theGiallo): pick random pivot
			lpr_u32 pivot = keys[left + rnd % ( count - left )];
			stack[pos++] = count; // NOTE(theGiallo): sort right part later

			// NOTE(theGiallo): partitioning
			for ( lpr_u32 right = left - 1; ; )
			{
				// NOTE(theGiallo): look for greater element
				while ( keys[++right] < pivot );
				// NOTE(theGiallo): look for smaller element
				while ( pivot < keys[--count] );

				if ( right >= count )
				{
					break; // NOTE(theGiallo):  partition point found?
				}
				  lpr_u16 tmp = keys[right];
				  keys[right] = keys[count];
				  keys[count] = tmp;
				          tmp = data[right];
				  data[right] = data[count];
				  data[count] = tmp;
			}
			// NOTE(theGiallo): partitioned, continue left part
		}
		if ( !pos)
		{
			break; // NOTE(theGiallo): stack empty?
		}
		left = count; // NOTE(theGiallo): left to right is sorted
		count = stack[--pos]; // NOTE(theGiallo): get next range to sort
	}
}

LPR_DEF
lpr_u64
lpr_xorshift128plus( lpr_u64 seeds[2] )
{
	lpr_u64 x       = seeds[0];
	lpr_u64 const y = seeds[1];
	seeds[0] = y;
	x ^= x << 23; // a
	x ^= x >> 17; // b
	x ^= y ^ (y >> 26); // c
	seeds[1] = x;
	return x + y;
}

LPR_DEF
lpr_u8
lpr_u8_hash_32( lpr_s32 v )
{
	// NOTE(theGiallo): Pearson hashing
	lpr_u8 ret = 0;

	for ( lpr_u8 i=0; i!=4; ++i )
	{
		lpr_u8 c = 0xFF & ( v >> (8*i) );
		ret = lpr_C_pearson_table[ret ^ c];
	}

	return ret;
}

LPR_DEF
lpr_u32
lpr_u32_hash_32( lpr_s32 v )
{
	// NOTE(theGiallo): Pearson hashing
	lpr_u32 ret = 0;

	lpr_u8 c0 = v >> 24;
	for ( lpr_s32 j = 0; j < 4; ++j )
	{
		lpr_u8 h = lpr_C_pearson_table[(c0 + j) % 256];
		for ( lpr_s32 i = 1; i < 4; ++i )
		{
			lpr_u8 c = ( v >> ( 24 - 8 * i ) ) & 0xFF;
			h = lpr_C_pearson_table[h ^ c];
		}
		ret |=  h << ( 8 * j );
	}

	return ret;
}

// misc
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// file I/O

const char *
Lpr_File_Sys_Error_strings[lpr_File_Sys_Errors_count] =
   {LPR_IO_ERRORS_TABLE(LPR_EXPAND_AS_STRING_TABLE_3)};

lpr_s64
lpr_read_binary_file( const char * file_path, void ** out_mem, int out_mem_size, void * alloc_context )
{
	(void) alloc_context; // NOTE(theGiallo): removes the unused parameter warning
	FILE * fp;

	fp = fopen( file_path, "r" );
	if (!fp)
	{
		LPR_PERROR_CLEAN( "readBinaryFile " LPR_HERE() );
		return -LPR_IO_CANT_OPEN_FILE;
	}

	// NOTE(theGiallo): get file size in Bytes
	fseek( fp, 0, SEEK_END );
	long int bytes_size = ftell( fp );
	rewind( fp );

	if ( *out_mem && out_mem_size < bytes_size )
	{
		fclose( fp );
		return -LPR_IO_OUT_MEM_TOO_SHORT;
	}

	if ( !*out_mem )
	{
		*out_mem = lpr_calloc( bytes_size, 1, alloc_context );
		if ( !out_mem )
		{
			fclose( fp );
			return -LPR_IO_CANT_MALLOC;
		}
	}

	int bytes_read =
	fread( *out_mem, 1, bytes_size, fp );
	if ( bytes_read != bytes_size )
	{
		fclose( fp );
		return -LPR_IO_READING_ERROR;
	}

	fclose( fp );
	return bytes_read;
}

// file I/O
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// UTF-8

/*
   NOTE(theGiallo):
     returns the pointer into the given UTF-8 string to the character next to
     the given one.
     IMPORTANT: assumes the given one is a valid UTF-8 char position.
     IMPORTANT: does not check if inside_a_string is really inside.

     Returns NULL on not valid inside_a_string (not a valid 1st byte) or if
     inside_a_string character ends after buffer end, or if the string is not
     null terminated and it's ended.
     Returns base_string + bytes_size if inside_a_string character was the last
     one.
 */
LPR_DEF
lpr_u8 *
lpr_utf8_next_char_ptr( const lpr_u8 * const inside_a_string,
                        const lpr_u8 * const base_string, lpr_u64 bytes_size )
{
	lpr_assert( base_string );
	lpr_assert( inside_a_string );
	lpr_assert( base_string <= inside_a_string );
	lpr_assert( (lpr_u64)( inside_a_string - base_string ) <= bytes_size );

	lpr_u8 first = inside_a_string[0];
	const lpr_u8 * const end = base_string + bytes_size;
	const lpr_s32 bytes_after = end - inside_a_string - 1;
	for ( lpr_s32 i = 0;
	      i != 4 && i != bytes_after + 1; // NOTE(theGiallo): +1 so it can return end
	      ++i )
	{
		if ( LPR_UTF8_COMPARE_LEFT_BYTES( lpr_C_utf8_key_mask_1st_byte[i], first,
		                                  lpr_C_utf8_key_mask_1st_byte_left_bits_count[i] ) )
		{
			return (lpr_u8*)inside_a_string + i + 1;
		}
	}

	return NULL;
}

/*
   NOTE(theGiallo):
   returns the pointer into the given UTF-8 string to the character next to the
   given one.
   Do not assumes the given one is a valid UTF-8 char position.
   inside_a_string can be inside a full character.
   IMPORTANT: does not check if inside_a_string is really inside.
   IMPORTANT: does not check if the returned pointed subchar is a valid 1st.
 */
LPR_DEF
lpr_u8 *
lpr_utf8_first_next_char_ptr( const lpr_u8 * inside_a_string,
                              const lpr_u8 * base_string, lpr_u64 bytes_size )
{
	lpr_assert( base_string );
	lpr_assert( inside_a_string );
	lpr_assert( base_string <= inside_a_string );
	lpr_assert( (lpr_u64)( inside_a_string - base_string ) < bytes_size );

	++inside_a_string;

	for ( const lpr_u8 * const end =
	      lpr_min( inside_a_string + 4, base_string + bytes_size );
	      inside_a_string < end;
	      ++inside_a_string )
	{
		// NOTE: this is faster than check for is_1st_subchar,
		// but in this way the function does not validate the string
		if ( ! lpr_utf8_is_mb_subchar( inside_a_string[0] ) )
		{
			lpr_assert( lpr_utf8_is_1st_subchar( inside_a_string[0] ) );

			return lpr_utf8_next_char_ptr( inside_a_string, base_string, bytes_size );
		}
	}

	return NULL;
}

/*
   NOTE(theGiallo):
   size is the byte size of the string buffer.
   Returns the number of full characters of the string.
 */
LPR_DEF
lpr_s32
lpr_utf8_char_count( const lpr_u8 * base_string, lpr_u64 bytes_size, const lpr_bool null_terminated )
{
	lpr_u32 count = 0;
	const lpr_u8 * const end = base_string + bytes_size;
	const lpr_u8 * inside_string = base_string;
	while ( inside_string < end && (!null_terminated || inside_string[0]) )
	{
		inside_string = lpr_utf8_next_char_ptr( inside_string, base_string, bytes_size) ;

		if ( !inside_string )
		{
			return -1;
		}
		++count;
	}

	return count;
}

LPR_DEF
Lpr_UTF8_Fullchar
lpr_utf8_full_char( const lpr_u8 * first )
{
	Lpr_UTF8_Fullchar res = LPR_ZERO_STRUCT;
	for ( int i = 0;
	      i != 4;
	      ++i )
	{
		if ( LPR_UTF8_COMPARE_LEFT_BYTES( lpr_C_utf8_key_mask_1st_byte[i], *first,
		                                  lpr_C_utf8_key_mask_1st_byte_left_bits_count[i] ) )
		{
			lpr_memcpy( &res, first, i+1 );
			break;
		}
	}

	return res;
}

LPR_DEF
void
lpr_utf8_print_char( const Lpr_UTF8_Fullchar fullchar, FILE * out )
{
	for ( lpr_s32 i = 0; i != 4; ++i )
	{
		if ( fullchar.subchars[i] || i==0 )
		{
			lpr_putc( fullchar.subchars[i], out );
		} else
		{
			break;
		}
	}
}

LPR_DEF
lpr_bool
lpr_utf8_is_1st_subchar( lpr_u8 subchar )
{
	lpr_bool ret =
	LPR_UTF8_COMPARE_LEFT_BYTES( lpr_C_utf8_key_mask_1st_byte[0], subchar,
	                             lpr_C_utf8_key_mask_1st_byte_left_bits_count[0] ) ||
	LPR_UTF8_COMPARE_LEFT_BYTES( lpr_C_utf8_key_mask_1st_byte[1], subchar,
	                             lpr_C_utf8_key_mask_1st_byte_left_bits_count[1] ) ||
	LPR_UTF8_COMPARE_LEFT_BYTES( lpr_C_utf8_key_mask_1st_byte[2], subchar,
	                             lpr_C_utf8_key_mask_1st_byte_left_bits_count[2] ) ||
	LPR_UTF8_COMPARE_LEFT_BYTES( lpr_C_utf8_key_mask_1st_byte[3], subchar,
	                             lpr_C_utf8_key_mask_1st_byte_left_bits_count[3] );

	return ret;
}

LPR_DEF
lpr_bool
lpr_utf8_is_mb_subchar( lpr_u8 subchar )
{
	lpr_bool ret =
	LPR_UTF8_COMPARE_LEFT_BYTES( lpr_C_utf8_key_mask_following_bytes, subchar,
	                             lpr_C_utf8_key_mask_following_left_bits_count );

	 return ret;
}

LPR_DEF
lpr_u32
lpr_utf8_fullchar_to_ucs4( Lpr_UTF8_Fullchar fullchar )
{
	lpr_u32 ret = 0;

	int count = 0;
	for ( int i = 0;
	      i != 4;
	      ++i )
	{
		if ( LPR_UTF8_COMPARE_LEFT_BYTES( lpr_C_utf8_key_mask_1st_byte[i], fullchar.subchars[0],
		                                  lpr_C_utf8_key_mask_1st_byte_left_bits_count[i] ) )
		{
			count = i+1;
		}
	}

	int shift = 0;
	for ( int i=0;
	      i != count;
	      ++i )
	{
		lpr_u8 id = count-1-i;
		lpr_u8 mask = id ? lpr_C_utf8_key_mask_following_bytes : lpr_C_utf8_key_mask_1st_byte[count-1];
		ret |= ( fullchar.subchars[id] & (~mask) ) << shift;
		shift += id ? lpr_C_utf8_key_mask_following_left_bits_count : lpr_C_utf8_key_mask_1st_byte_left_bits_count[count-1];
	}

	return ret;
}

LPR_DEF
Lpr_UTF8_Fullchar
lpr_utf8_fullchar_from_ucs4( lpr_u32 codepoint )
{
	Lpr_UTF8_Fullchar ret = LPR_ZERO_STRUCT;

	lpr_s32 bytes_count;
	if ( codepoint < 0x7F )
	{
		bytes_count = 1;
	} else
	if ( codepoint < 0x7FF )
	{
		bytes_count = 2;
	} else
	if ( codepoint < 0xFFFF )
	{
		bytes_count = 3;
	} else
	if ( codepoint < 0x1FFFF )
	{
		bytes_count = 4;
	} else
	{
		lpr_log_err( "Lepre UTF-8 chars are max 4 bytes! :[" );
		ret.all = 0xFFFFFFFF;
		return ret;
	}

	lpr_u32 shift = 0;
	for ( int i = 0;
	      i != bytes_count;
	      ++i )
	{
		lpr_u8 b_id = bytes_count - i - 1;
		lpr_u8 utf8_key  = b_id ? lpr_C_utf8_key_mask_following_bytes  : lpr_C_utf8_key_mask_1st_byte[bytes_count-1];
		lpr_u8 bits_mask = b_id ? lpr_C_utf8_bits_mask_following_bytes : lpr_C_utf8_bits_mask_1st_byte[bytes_count-1];
		ret.subchars[b_id] = utf8_key | ( ( codepoint >> shift ) & bits_mask );
		// NOTE(theGiallo): increment has meaning only until the last iteration
		// shift += b_id ? lpr_C_utf8_key_mask_following_left_bits_count : lpr_C_utf8_key_mask_1st_byte_left_bits_count[bytes_count-1];
		shift += lpr_C_utf8_key_mask_following_left_bits_count;
	}

	return ret;
}

// UTF-8
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// OpenGL debug

void
lpr_log_framebuffer_info()
{
	lpr_log_info( "\n===== FBO STATUS =====\n" );

	// NOTE(theGiallo): print max # of colorbuffers supported by FBO
	int color_buffer_count = 0;
	glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &color_buffer_count);
	lpr_log_info( "Max Number of Color Buffer Attachment Points: %d",
	              color_buffer_count );

	int object_type;
	int object_id;
	char string_buf[2048];

	// NOTE(theGiallo): print info of the colorbuffer attachable image
	for ( int i = 0; i != color_buffer_count; ++i )
	{
		glGetFramebufferAttachmentParameteriv( GL_FRAMEBUFFER,
		                                       GL_COLOR_ATTACHMENT0+i,
		                                       GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE,
		                                       &object_type );
		if ( object_type != GL_NONE )
		{
			glGetFramebufferAttachmentParameteriv( GL_FRAMEBUFFER,
			                                       GL_COLOR_ATTACHMENT0+i,
			                                       GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME,
			                                       &object_id );

			lpr_log_info( "Color Attachment %d: ", i );
			if ( object_type == GL_TEXTURE )
			{
				lpr_log_info( "\tGL_TEXTURE, %s",
				              lpr_get_texture_parameters(
				                 object_id,
				                 string_buf,
				                 sizeof( string_buf ) ) );
			} else
			if ( object_type == GL_RENDERBUFFER )
			{
				lpr_log_info( "\tGL_RENDERBUFFER, %s",
				                lpr_get_renderbuffer_parameters(
				                   object_id,
				                   string_buf,
				                   sizeof( string_buf ) ) );
			}
		}
	}

	// NOTE(theGiallo): print info of the depthbuffer attachable image
	glGetFramebufferAttachmentParameteriv( GL_FRAMEBUFFER,
	                                       GL_DEPTH_ATTACHMENT,
	                                       GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE,
	                                       &object_type );
	if ( object_type != GL_NONE )
	{
		glGetFramebufferAttachmentParameteriv( GL_FRAMEBUFFER,
		                                       GL_DEPTH_ATTACHMENT,
		                                       GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME,
		                                       &object_id );

		lpr_log_info( "Depth Attachment: " );
		switch(object_type)
		{
			case GL_TEXTURE:
				lpr_log_info( "\tGL_TEXTURE, %s",
				                lpr_get_texture_parameters(
				                   object_id,
				                   string_buf,
				                   sizeof( string_buf ) ) );
				break;
			case GL_RENDERBUFFER:
				lpr_log_info( "\tGL_RENDERBUFFER, %s",
				                lpr_get_renderbuffer_parameters(
				                   object_id,
				                   string_buf,
				                   sizeof( string_buf ) ) );
				break;
		}
	}

	// NOTE(theGiallo): print info of the stencilbuffer attachable image
	glGetFramebufferAttachmentParameteriv( GL_FRAMEBUFFER,
	                                       GL_STENCIL_ATTACHMENT,
	                                       GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE,
	                                       &object_type );
	if ( object_type != GL_NONE )
	{
		glGetFramebufferAttachmentParameteriv( GL_FRAMEBUFFER,
		                                       GL_STENCIL_ATTACHMENT,
		                                       GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME,
		                                       &object_id );

		lpr_log_info( "Stencil Attachment: " );
		switch( object_type )
		{
			case GL_TEXTURE:
				lpr_log_info( "\tGL_TEXTURE, %s",
				                lpr_get_texture_parameters(
				                   object_id,
				                   string_buf,
				                   sizeof( string_buf ) ) );
				break;
			case GL_RENDERBUFFER:
				lpr_log_info( "tGL_RENDERBUFFER, %s",
				                lpr_get_renderbuffer_parameters(
				                   object_id,
				                   string_buf,
				                   sizeof( string_buf ) ) );
				break;
		}
	}

	lpr_log_info( "\n===== /FBO STATUS =====\n" );
}

char *
lpr_convert_internal_format_to_string( GLenum format, char * buf, size_t buf_size )
{
	const char * format_name;

	switch(format)
	{
	case GL_STENCIL_INDEX: // 0x1901
		format_name = "GL_STENCIL_INDEX";
		break;
	case GL_DEPTH_COMPONENT: // 0x1902
		format_name = "GL_DEPTH_COMPONENT";
		break;
	case GL_ALPHA: // 0x1906
		format_name = "GL_ALPHA";
		break;
	case GL_RGB: // 0x1907
		format_name = "GL_RGB";
		break;
	case GL_RGBA: // 0x1908
		format_name = "GL_RGBA";
		break;
	case GL_LUMINANCE: // 0x1909
		format_name = "GL_LUMINANCE";
		break;
	case GL_LUMINANCE_ALPHA: // 0x190A
		format_name = "GL_LUMINANCE_ALPHA";
		break;
	case GL_R3_G3_B2: // 0x2A10
		format_name = "GL_R3_G3_B2";
		break;
	case GL_ALPHA4: // 0x803B
		format_name = "GL_ALPHA4";
		break;
	case GL_ALPHA8: // 0x803C
		format_name = "GL_ALPHA8";
		break;
	case GL_ALPHA12: // 0x803D
		format_name = "GL_ALPHA12";
		break;
	case GL_ALPHA16: // 0x803E
		format_name = "GL_ALPHA16";
		break;
	case GL_LUMINANCE4: // 0x803F
		format_name = "GL_LUMINANCE4";
		break;
	case GL_LUMINANCE8: // 0x8040
		format_name = "GL_LUMINANCE8";
		break;
	case GL_LUMINANCE12: // 0x8041
		format_name = "GL_LUMINANCE12";
		break;
	case GL_LUMINANCE16: // 0x8042
		format_name = "GL_LUMINANCE16";
		break;
	case GL_LUMINANCE4_ALPHA4: // 0x8043
		format_name = "GL_LUMINANCE4_ALPHA4";
		break;
	case GL_LUMINANCE6_ALPHA2: // 0x8044
		format_name = "GL_LUMINANCE6_ALPHA2";
		break;
	case GL_LUMINANCE8_ALPHA8: // 0x8045
		format_name = "GL_LUMINANCE8_ALPHA8";
		break;
	case GL_LUMINANCE12_ALPHA4: // 0x8046
		format_name = "GL_LUMINANCE12_ALPHA4";
		break;
	case GL_LUMINANCE12_ALPHA12: // 0x8047
		format_name = "GL_LUMINANCE12_ALPHA12";
		break;
	case GL_LUMINANCE16_ALPHA16: // 0x8048
		format_name = "GL_LUMINANCE16_ALPHA16";
		break;
	case GL_INTENSITY: // 0x8049
		format_name = "GL_INTENSITY";
		break;
	case GL_INTENSITY4: // 0x804A
		format_name = "GL_INTENSITY4";
		break;
	case GL_INTENSITY8: // 0x804B
		format_name = "GL_INTENSITY8";
		break;
	case GL_INTENSITY12: // 0x804C
		format_name = "GL_INTENSITY12";
		break;
	case GL_INTENSITY16: // 0x804D
		format_name = "GL_INTENSITY16";
		break;
	case GL_RGB4: // 0x804F
		format_name = "GL_RGB4";
		break;
	case GL_RGB5: // 0x8050
		format_name = "GL_RGB5";
		break;
	case GL_RGB8: // 0x8051
		format_name = "GL_RGB8";
		break;
	case GL_RGB10: // 0x8052
		format_name = "GL_RGB10";
		break;
	case GL_RGB12: // 0x8053
		format_name = "GL_RGB12";
		break;
	case GL_RGB16: // 0x8054
		format_name = "GL_RGB16";
		break;
	case GL_RGBA2: // 0x8055
		format_name = "GL_RGBA2";
		break;
	case GL_RGBA4: // 0x8056
		format_name = "GL_RGBA4";
		break;
	case GL_RGB5_A1: // 0x8057
		format_name = "GL_RGB5_A1";
		break;
	case GL_RGBA8: // 0x8058
		format_name = "GL_RGBA8";
		break;
	case GL_RGB10_A2: // 0x8059
		format_name = "GL_RGB10_A2";
		break;
	case GL_RGBA12: // 0x805A
		format_name = "GL_RGBA12";
		break;
	case GL_RGBA16: // 0x805B
		format_name = "GL_RGBA16";
		break;
	case GL_DEPTH_COMPONENT16: // 0x81A5
		format_name = "GL_DEPTH_COMPONENT16";
		break;
	case GL_DEPTH_COMPONENT24: // 0x81A6
		format_name = "GL_DEPTH_COMPONENT24";
		break;
	case GL_DEPTH_COMPONENT32: // 0x81A7
		format_name = "GL_DEPTH_COMPONENT32";
		break;
	case GL_DEPTH_STENCIL: // 0x84F9
		format_name = "GL_DEPTH_STENCIL";
		break;
	case GL_RGBA32F: // 0x8814
		format_name = "GL_RGBA32F";
		break;
	case GL_RGB32F: // 0x8815
		format_name = "GL_RGB32F";
		break;
	case GL_RGBA16F: // 0x881A
		format_name = "GL_RGBA16F";
		break;
	case GL_RGB16F: // 0x881B
		format_name = "GL_RGB16F";
		break;
	case GL_RGBA32UI: // 0x8D70
		format_name = "GL_RGBAUI32";
		break;
	case GL_DEPTH24_STENCIL8: // 0x88F0
		format_name = "GL_DEPTH24_STENCIL8";
		break;
	case GL_SRGB: // 0x8C40
		format_name = "GL_SRGB";
		break;
	case GL_SRGB8: // 0x8C41
		format_name = "GL_SRGB8";
		break;
	case GL_SRGB_ALPHA: // 0x8C42
		format_name = "GL_SRGB_ALPHA";
		break;
	case GL_SRGB8_ALPHA8: // 0x8C43
		format_name = "GL_SRGB8_ALPHA8";
		break;
	case GL_COMPRESSED_SRGB: // 0x8C48
		format_name = "GL_COMPRESSED_SRGB";
		break;
	case GL_COMPRESSED_SRGB_ALPHA: // 0x8C49
		format_name = "GL_COMPRESSED_SRGB_ALPHA";
		break;
	case GL_COMPRESSED_SLUMINANCE: // 0x8C4A
		format_name = "GL_COMPRESSED_SLUMINANCE";
		break;
	case GL_COMPRESSED_SLUMINANCE_ALPHA: // 0x8C4B
		format_name = "GL_COMPRESSED_SLUMINANCE_ALPHA";
		break;

	default:
		snprintf( buf, buf_size, "Unknown Format ( 0x%x )", format );
		return buf;
	}

	snprintf( buf, buf_size, "%s", format_name );
	return buf;
}

char *
lpr_get_texture_parameters( GLuint id, char * buf, size_t buf_size )
{
	if ( glIsTexture( id ) == GL_FALSE )
	{
		snprintf( buf, buf_size, "Not texture object" );
		return buf;
	}

	int width, height, format;
	char format_name[1024];
	glBindTexture( GL_TEXTURE_2D, id );
	glGetTexLevelParameteriv( GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &width);
	glGetTexLevelParameteriv( GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &height);
	glGetTexLevelParameteriv( GL_TEXTURE_2D, 0, GL_TEXTURE_INTERNAL_FORMAT,
	                          &format );
	glBindTexture( GL_TEXTURE_2D, 0 );

	lpr_convert_internal_format_to_string( format, format_name,
	                                         sizeof(format_name) );

	snprintf( buf, buf_size, "%d x %d, %s", width, height, format_name );

	return buf;
}

char *
lpr_get_renderbuffer_parameters( GLuint id, char * buf, size_t buf_size )
{
	if ( glIsRenderbuffer(id) == GL_FALSE )
	{
		snprintf( buf, buf_size, "Not Renderbuffer object" );
		return buf;
	}

	int width, height, format;
	char format_name[1024];
	glBindRenderbuffer( GL_RENDERBUFFER, id );
	glGetRenderbufferParameteriv( GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width );
	glGetRenderbufferParameteriv( GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height );
	glGetRenderbufferParameteriv( GL_RENDERBUFFER, GL_RENDERBUFFER_INTERNAL_FORMAT,
	                              &format );
	glBindRenderbuffer( GL_RENDERBUFFER, 0 );

	lpr_convert_internal_format_to_string( format, format_name, sizeof(format_name) );

	snprintf( buf, buf_size, "%d x %d, %s", width, height, format_name );

	return buf;
}

// OpenGL debug
////////////////////////////////////////////////////////////////////////////////

// NOTE(theGiallo): LPR_IMPLEMENTATION end
#endif
