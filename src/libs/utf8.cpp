#include "utf8.h"
#include "macro_tools.h"
#include "tgmath.h"

/*
   NOTE(theGiallo):
     returns the pointer into the given UTF-8 string to the character next to
     the given one.
     IMPORTANT: assumes the given one is a valid UTF-8 char position.
     IMPORTANT: does not check if inside_a_string is really inside.

     Returns NULL on not valid inside_a_string (not a valid 1st byte) or if
     inside_a_string character ends after buffer end, or if the string is not
     null terminated and it's ended.
     Returns base_string + bytes_size if inside_a_string character was the last
     one.
 */
u8 *
utf8_next_char_ptr( const u8 * const inside_a_string,
                    const u8 * const base_string, u64 bytes_size )
{
	tg_assert( base_string );
	tg_assert( inside_a_string );
	tg_assert( base_string <= inside_a_string );
	tg_assert( (u64)( inside_a_string - base_string ) <= bytes_size );

	u8 first = inside_a_string[0];
	const u8 * const end = base_string + bytes_size;
	const s32 bytes_after = end - inside_a_string - 1;
	for ( s32 i = 0;
	      i != 4 && i != bytes_after + 1; // NOTE(theGiallo): +1 so it can return end
	      ++i )
	{
		if ( UTF8_COMPARE_LEFT_BYTES( C_utf8_key_mask_1st_byte[i], first,
		                              C_utf8_key_mask_1st_byte_left_bits_count[i] ) )
		{
			return (u8*)inside_a_string + i + 1;
		}
	}

	return NULL;
}

/*
   NOTE(theGiallo):
   returns the pointer into the given UTF-8 string to the character next to the
   given one.
   Do not assumes the given one is a valid UTF-8 char position.
   inside_a_string can be inside a full character.
   IMPORTANT: does not check if inside_a_string is really inside.
   IMPORTANT: does not check if the returned pointed subchar is a valid 1st.
 */
u8 *
utf8_first_next_char_ptr( const u8 * inside_a_string,
                          const u8 * base_string, u64 bytes_size )
{
	tg_assert( base_string );
	tg_assert( inside_a_string );
	tg_assert( base_string <= inside_a_string );
	tg_assert( (u64)( inside_a_string - base_string ) < bytes_size );

	++inside_a_string;

	for ( const u8 * const end =
	      (u8 *) min( s64( inside_a_string + 4 ), s64( base_string + bytes_size ) );
	      inside_a_string < end;
	      ++inside_a_string )
	{
		// NOTE: this is faster than check for is_1st_subchar,
		// but in this way the function does not validate the string
		if ( ! utf8_is_mb_subchar( inside_a_string[0] ) )
		{
			tg_assert( utf8_is_1st_subchar( inside_a_string[0] ) );

			return utf8_next_char_ptr( inside_a_string, base_string, bytes_size );
		}
	}

	return NULL;
}

/*
   NOTE(theGiallo):
   size is the byte size of the string buffer.
   Returns the number of full characters of the string.
 */
s32
utf8_char_count( const u8 * base_string, u64 bytes_size, const bool null_terminated )
{
	u32 count = 0;
	const u8 * const end = base_string + bytes_size;
	const u8 * inside_string = base_string;
	while ( inside_string < end && (!null_terminated || inside_string[0]) )
	{
		inside_string = utf8_next_char_ptr( inside_string, base_string, bytes_size) ;

		if ( !inside_string )
		{
			return -1;
		}
		++count;
	}

	return count;
}

UTF8_Fullchar
utf8_full_char( const u8 * first )
{
	UTF8_Fullchar res = {};
	for ( int i = 0;
	      i != 4;
	      ++i )
	{
		if ( UTF8_COMPARE_LEFT_BYTES( C_utf8_key_mask_1st_byte[i], *first,
		                              C_utf8_key_mask_1st_byte_left_bits_count[i] ) )
		{
			memcpy( &res, first, i+1 );
			break;
		}
	}

	return res;
}

void
utf8_print_char( const UTF8_Fullchar fullchar, FILE * out )
{
	for ( s32 i = 0; i != 4; ++i )
	{
		if ( fullchar.subchars[i] || i==0 )
		{
			putc( fullchar.subchars[i], out );
		} else
		{
			break;
		}
	}
}

bool
utf8_is_1st_subchar( u8 subchar )
{
	bool ret =
	UTF8_COMPARE_LEFT_BYTES( C_utf8_key_mask_1st_byte[0], subchar,
	                         C_utf8_key_mask_1st_byte_left_bits_count[0] ) ||
	UTF8_COMPARE_LEFT_BYTES( C_utf8_key_mask_1st_byte[1], subchar,
	                         C_utf8_key_mask_1st_byte_left_bits_count[1] ) ||
	UTF8_COMPARE_LEFT_BYTES( C_utf8_key_mask_1st_byte[2], subchar,
	                         C_utf8_key_mask_1st_byte_left_bits_count[2] ) ||
	UTF8_COMPARE_LEFT_BYTES( C_utf8_key_mask_1st_byte[3], subchar,
	                         C_utf8_key_mask_1st_byte_left_bits_count[3] );

	return ret;
}

bool
utf8_is_mb_subchar( u8 subchar )
{
	bool ret =
	UTF8_COMPARE_LEFT_BYTES( C_utf8_key_mask_following_bytes, subchar,
	                         C_utf8_key_mask_following_left_bits_count );

	 return ret;
}

u32
utf8_fullchar_to_ucs4( UTF8_Fullchar fullchar )
{
	u32 ret = 0;

	int count = 0;
	for ( int i = 0;
	      i != 4;
	      ++i )
	{
		if ( UTF8_COMPARE_LEFT_BYTES( C_utf8_key_mask_1st_byte[i], fullchar.subchars[0],
		                              C_utf8_key_mask_1st_byte_left_bits_count[i] ) )
		{
			count = i+1;
		}
	}

	int shift = 0;
	for ( int i=0;
	      i != count;
	      ++i )
	{
		u8 id = count-1-i;
		u8 mask = id ? C_utf8_key_mask_following_bytes : C_utf8_key_mask_1st_byte[count-1];
		ret |= ( fullchar.subchars[id] & (~mask) ) << shift;
		shift += id ? C_utf8_key_mask_following_left_bits_count : C_utf8_key_mask_1st_byte_left_bits_count[count-1];
	}

	return ret;
}

UTF8_Fullchar
utf8_fullchar_from_ucs4( u32 codepoint )
{
	UTF8_Fullchar ret = {};

	s32 bytes_count;
	if ( codepoint < 0x7F )
	{
		bytes_count = 1;
	} else
	if ( codepoint < 0x7FF )
	{
		bytes_count = 2;
	} else
	if ( codepoint < 0xFFFF )
	{
		bytes_count = 3;
	} else
	if ( codepoint < 0x1FFFF )
	{
		bytes_count = 4;
	} else
	{
		log_err( "Lepre UTF-8 chars are max 4 bytes! :[" );
		ret.all = 0xFFFFFFFF;
		return ret;
	}

	u32 shift = 0;
	for ( int i = 0;
	      i != bytes_count;
	      ++i )
	{
		u8 b_id = bytes_count - i - 1;
		u8 utf8_key  = b_id ? C_utf8_key_mask_following_bytes  : C_utf8_key_mask_1st_byte[bytes_count-1];
		u8 bits_mask = b_id ? C_utf8_bits_mask_following_bytes : C_utf8_bits_mask_1st_byte[bytes_count-1];
		ret.subchars[b_id] = utf8_key | ( ( codepoint >> shift ) & bits_mask );
		// NOTE(theGiallo): increment has meaning only until the last iteration
		// shift += b_id ? C_utf8_key_mask_following_left_bits_count : C_utf8_key_mask_1st_byte_left_bits_count[bytes_count-1];
		shift += C_utf8_key_mask_following_left_bits_count;
	}

	return ret;
}


