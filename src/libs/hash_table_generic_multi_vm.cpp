#include "hash_table_generic_multi_vm.h"
#include "utility.h"

#define LIST_VM_EMIT_BODY 1
#define LIST_VM_BODY_ONLY 1

#define LIST_VM_TYPE Hash_Table_Generic_Multi_VM_Element
#define LIST_VM_TYPE_SHORT HTGMVM_Element
#include "list_vm.inc.h"

#define LIST_VM_TYPE Hash_Table_Generic_Multi_VM_Multi_Element
#define LIST_VM_TYPE_SHORT HTGMVM_M_Element
#include "list_vm.inc.h"

#undef LIST_VM_EMIT_BODY
#undef LIST_VM_BODY_ONLY


#define ELEMENT_MATCH_HASH_AND_KEY(element,the_hash,the_key,key_size) \
	( (element).hash == (the_hash) \
	  && ( (key_size) == -1 \
	     ? string_equal( (u8*)(element).key, (u8*)(the_key) ) \
	     : memcmp( (element).key, (the_key), (key_size) ) == 0 ) )

#define HASH_KEY(ht,key) \
	if ( (ht)->key_size == -1 ) \
	{ \
		hash = u64_FNV1a( (char*)(key) ); \
	} else \
	{ \
		hash = u64_FNV1a( (key), (ht)->key_size ); \
	}

bool
hash_table_generic_multi_vm_init( Hash_Table_Generic_Multi_VM * ht,
                            s64 key_size,
                            u64 max_capacity,
                            u32 table_length,
                            HTGMVM_M_Element_List_VM * table,
                            Pool_Allocator_Generic_VM * element_list_nodes_pool_p,
                            Pool_Allocator_Generic_VM * m_element_list_nodes_pool_p )
{
	tg_assert( ht );
	tg_assert( key_size );
	tg_assert( table_length );

	bool ret = false;

	ht->key_size = key_size;
	ht->table_length = table_length;

	ht->table = table;
	ht->element_list_nodes_pool_p = element_list_nodes_pool_p;

	u64 table_size_bytes = sizeof( HTGMVM_Element_List_VM ) * table_length;
	ht->table_is_extern = table;
	if ( !table )
	{
		table = (HTGMVM_M_Element_List_VM*)sys_mem_reserve( table_size_bytes );
		if ( table )
		{
			if ( !sys_mem_commit( table, table_size_bytes ) )
			{
				sys_mem_free( table, table_size_bytes );
				table = 0;
				return ret;
			}
		} else
		{
			return ret;
		}
		ht->table = table;
	}
	memset( ht->table, 0x0, table_size_bytes );

	Pool_Allocator_Generic_VM * ht_element_list_nodes_pool_p = element_list_nodes_pool_p;
	if ( !element_list_nodes_pool_p )
	{
		ht->element_list_nodes_pool = {};
		// NOTE(theGiallo): here max_capacity is correct because the lists are not resident
		pool_allocator_generic_vm_init( &ht->element_list_nodes_pool,
		                                sizeof(HTGMVM_Element_List_VM_Node),
		                                max_capacity, -1 );

		ht_element_list_nodes_pool_p = &ht->element_list_nodes_pool;
	}
	Pool_Allocator_Generic_VM * ht_m_element_list_nodes_pool_p = m_element_list_nodes_pool_p;
	if ( !m_element_list_nodes_pool_p )
	{
		ht->m_element_list_nodes_pool = {};
		// NOTE(theGiallo): here max_capacity is correct because the lists are not resident
		pool_allocator_generic_vm_init( &ht->m_element_list_nodes_pool,
		                                sizeof(HTGMVM_M_Element_List_VM_Node),
		                                max_capacity, -1 );

		ht_m_element_list_nodes_pool_p = &ht->m_element_list_nodes_pool;
	}
	ht->occupancy = 0;
	for ( u64 i = 0; i != table_length; ++i )
	{
		list_vm_init( ht->table + i, ht_m_element_list_nodes_pool_p );
	}
	ret = true;

	return ret;
}

void
hash_table_generic_multi_vm_destroy( Hash_Table_Generic_Multi_VM * ht )
{
	tg_assert( ht );

	ht->occupancy = 0;

	if ( ht->table_is_extern )
	{
		sys_mem_free( ht->table, sizeof( HTGMVM_M_Element_List_VM ) * ht->table_length );
	}

	if ( !ht->element_list_nodes_pool_p )
	{
		pool_allocator_generic_vm_destroy( &ht->element_list_nodes_pool );
	} else
	{
		for ( u32 i = 0; i != ht->table_length; ++i )
		{
			for ( HTGMVM_M_Element_List_VM_Node * m_node = ht->table[i].first;
			      m_node;
			      m_node = m_node->next )
			{
				list_vm_remove_all( &m_node->element.list );
			}
		}
	}

	if ( !ht->m_element_list_nodes_pool_p )
	{
		pool_allocator_generic_vm_destroy( &ht->m_element_list_nodes_pool );
	} else
	{
		for ( u32 i = 0; i != ht->table_length; ++i )
		{
			list_vm_remove_all( ht->table + i );
		}
	}
}

inline
bool
_hash_table_generic_multi_vm_insert_new_value( Hash_Table_Generic_Multi_VM * ht, Hash_Table_Generic_Multi_VM_Element e )
{
	bool ret = false;

	u32 index = e.hash % ht->table_length;

	HTGMVM_M_Element_List_VM * mel = ht->table + index;

	Hash_Table_Generic_Multi_VM_Multi_Element me = { .hash = e.hash, .key = e.key, .list = {} };
	Pool_Allocator_Generic_VM * pool_p = ht->element_list_nodes_pool_p ? ht->element_list_nodes_pool_p : &ht->element_list_nodes_pool;
	list_vm_init( &me.list, pool_p );
	bool b_res;
	b_res = list_vm_push_first( &me.list, &e );
	if ( b_res )
	{
		b_res = list_vm_push_last( mel, &me );
		if ( !b_res )
		{
			list_vm_remove_first( &me.list );
		} else
		{
			ret = true;
		}
	}

	return ret;
}

bool
hash_table_generic_multi_vm_insert( Hash_Table_Generic_Multi_VM * ht, void * key, void * value )
{
	tg_assert( ht );
	tg_assert( key );

	bool ret;

	u64 hash;
	HASH_KEY( ht, key );

	Hash_Table_Generic_Multi_VM_Element e = { .hash = hash, .key = key, .value = value };

	u32 index = hash % ht->table_length;

	HTGMVM_M_Element_List_VM * mel = ht->table + index;
	bool inserted = false;
	for ( HTGMVM_M_Element_List_VM_Node * m_node = mel->first;
	      m_node;
	      m_node = m_node->next )
	{
		if ( ELEMENT_MATCH_HASH_AND_KEY( m_node->element, hash, key, ht->key_size ) )
		{
			ret = list_vm_push_last( &m_node->element.list, &e );
			inserted = true;
		}
	}
	if ( !inserted )
	{
		ret = _hash_table_generic_multi_vm_insert_new_value( ht, e );
	}

	if ( ret )
	{
		++ht->occupancy;
	}

	return ret;
}

bool
hash_table_generic_multi_vm_insert_unique_or_get_all( Hash_Table_Generic_Multi_VM * ht,
                                                      void * key,
                                                      void * value,
                                                      HTGMVM_Element_List_VM ** out_values_list )
{
	tg_assert( ht );
	tg_assert( key );

	bool ret = false;

	u64 hash;
	HASH_KEY( ht, key );

	Hash_Table_Generic_Multi_VM_Element e = { .hash = hash, .key = key, .value = value };

	u32 index = hash % ht->table_length;

	HTGMVM_M_Element_List_VM * m_list = ht->table + index;
	bool already_present = false;
	for ( HTGMVM_M_Element_List_VM_Node * m_node = m_list->first;
	      m_node;
	      m_node = m_node->next )
	{
		if ( ELEMENT_MATCH_HASH_AND_KEY( m_node->element, hash, key, ht->key_size ) )
		{
			already_present = true;
			*out_values_list = &m_node->element.list;
			break;
		}
	}
	if ( !already_present )
	{
		ret = _hash_table_generic_multi_vm_insert_new_value( ht, e );
		if ( ret )
		{
			++ht->occupancy;
		}
		*out_values_list = 0;
	}

	return ret;
}

bool
hash_table_generic_multi_vm_insert_unique( Hash_Table_Generic_Multi_VM * ht, void * key, void * value )
{
	tg_assert( ht );
	tg_assert( key );

	bool ret = false;

	u64 hash;
	HASH_KEY( ht, key );

	Hash_Table_Generic_Multi_VM_Element e = { .hash = hash, .key = key, .value = value };

	u32 index = hash % ht->table_length;

	HTGMVM_M_Element_List_VM * m_list = ht->table + index;
	bool already_present = false;
	for ( HTGMVM_M_Element_List_VM_Node * m_node = m_list->first;
	      m_node;
	      m_node = m_node->next )
	{
		if ( ELEMENT_MATCH_HASH_AND_KEY( m_node->element, hash, key, ht->key_size ) )
		{
			already_present = true;
			break;
		}
	}
	if ( !already_present )
	{
		ret = _hash_table_generic_multi_vm_insert_new_value( ht, e );
		if ( ret )
		{
			++ht->occupancy;
		}
	}

	return ret;
}

bool
hash_table_generic_multi_vm_get_all( Hash_Table_Generic_Multi_VM * ht, void * key, HTGMVM_Element_List_VM ** out_values_list )
{
	tg_assert( ht );
	tg_assert( key );

	bool ret = false;

	u64 hash;
	HASH_KEY( ht, key );

	u32 index = hash % ht->table_length;

	HTGMVM_M_Element_List_VM * m_list = ht->table + index;
	for ( HTGMVM_M_Element_List_VM_Node * m_node = m_list->first;
	      m_node;
	      m_node = m_node->next )
	{
		if ( ELEMENT_MATCH_HASH_AND_KEY( m_node->element, hash, key, ht->key_size ) )
		{
			ret = true;
			*out_values_list = &m_node->element.list;
			break;
		}
	}
	if ( !ret )
	{
		*out_values_list = 0;
	}

	return ret;
}

bool
hash_table_generic_multi_vm_remove_and_get_all( Hash_Table_Generic_Multi_VM * ht, void * key, HTGMVM_Element_List_VM * out_values_list )
{
	tg_assert( ht );
	tg_assert( key );

	bool ret = false;

	u64 hash;
	HASH_KEY( ht, key );

	u32 index = hash % ht->table_length;

	HTGMVM_M_Element_List_VM * m_list = ht->table + index;
	for ( HTGMVM_M_Element_List_VM_Node *      m_node = m_list->first,
	                                    * prev_m_node = 0;
	      m_node;
	      prev_m_node = m_node,
	      m_node = m_node->next )
	{
		if ( ELEMENT_MATCH_HASH_AND_KEY( m_node->element, hash, key, ht->key_size ) )
		{
			ret = true;
			*out_values_list = m_node->element.list;
			ht->occupancy -= m_node->element.list.length;
			bool b_res;
			if ( prev_m_node )
			{
				b_res =
				list_vm_remove_next( m_list, prev_m_node );
			} else
			{
				b_res =
				list_vm_remove_first( m_list );
			}
			tg_assert( b_res );
			break;
		}
	}
	if ( !ret )
	{
		*out_values_list = {};
	}

	return ret;
}

bool
hash_table_generic_multi_vm_remove_all( Hash_Table_Generic_Multi_VM * ht, void * key )
{
	tg_assert( ht );
	tg_assert( key );

	bool ret = false;

	u64 hash;
	HASH_KEY( ht, key );

	u32 index = hash % ht->table_length;

	HTGMVM_M_Element_List_VM * m_list = ht->table + index;
	for ( HTGMVM_M_Element_List_VM_Node *      m_node = m_list->first,
	                                    * prev_m_node = 0;
	      m_node;
	      prev_m_node = m_node,
	      m_node = m_node->next )
	{
		if ( ELEMENT_MATCH_HASH_AND_KEY( m_node->element, hash, key, ht->key_size ) )
		{
			ret = true;
			bool b_res;

			ht->occupancy -= m_node->element.list.length;
			list_vm_remove_all( &m_node->element.list );

			if ( prev_m_node )
			{
				b_res =
				list_vm_remove_next( m_list, prev_m_node );
			} else
			{
				b_res =
				list_vm_remove_first( m_list );
			}
			tg_assert( b_res );
			break;
		}
	}

	return ret;
}

#undef ELEMENT_MATCH_HASH_AND_KEY
#undef HASH_KEY
