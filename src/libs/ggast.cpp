#include <list>
#include <vector>
#include <string.h>
#include <stdarg.h>
#include "basic_types.h"
#include "tgmath.h"
#include "utility.h"
#include "macro_tools.h"
#include "system.h"
#include "tgstring.h"
#include "tgparse.h"

template<typename T>
using list = std::list<T>;

template<typename T>
using vector = std::vector<T>;

template<typename T>
bool
insert_in_list_at_pos( list<T> * l, s32 pos, T * el )
{
	bool ret = false;

	auto it = l->begin();
	for ( it = l->begin(); it != l->end() && pos != 0; ++it, --pos )
	{}
	if ( pos == 0 )
	{
		l->insert( it, el );
		ret = true;
	}

	return ret;
}

////////////////////////////////////////////////////////////////////////////////

namespace GenGen::ast
{

using namespace tg;


#define AST_MAX_ARRAY_LENGTH 1024

// NOTE(theGiallo): should we remove `double`?
#define AST_TYPE_DEF(ENTRY) \
	ENTRY(FLOAT , "float" , f32   ) \
	ENTRY(INT   , "int"   , s32   ) \
	ENTRY(UINT  , "uint"  , u32   ) \
	ENTRY(BOOL  , "bool"  , bool  ) \
	ENTRY(VEC2  , "vec2"  , V2    ) \
	ENTRY(VEC3  , "vec3"  , V3    ) \
	ENTRY(VEC4  , "vec4"  , V4    ) \
	ENTRY(MAT2  , "mat2"  , Mat2  ) \
	ENTRY(MAT3  , "mat3"  , Mat3  ) \
	ENTRY(MAT4  , "mat4"  , Mat4  ) \
	ENTRY(MAT23 , "mat2x3", Mat23 ) \
	ENTRY(MAT24 , "mat2x4", Mat24 ) \
	ENTRY(MAT32 , "mat3x2", Mat32 ) \
	ENTRY(MAT34 , "mat3x4", Mat34 ) \
	ENTRY(MAT42 , "mat4x2", Mat42 ) \
	ENTRY(MAT43 , "mat4x3", Mat43 ) \

// ENTRY(DOUBLE, "double") \

#define AST_TYPE_ENUM_DEF_ENUM( e, str, cpp_t ) e,
#define AST_TYPE_ENUM_DEF_ENUM_TO_ARR( e ) ARRAY_##e
#define AST_TYPE_ENUM_DEF_ENUM_ARR( e, str, cpp_t ) AST_TYPE_ENUM_DEF_ENUM_TO_ARR( e ),
#define AST_TYPE_ENUM_DEF_STR( e, str, cpp_t ) TOSTRING( e ),
#define AST_TYPE_ENUM_DEF_STR_ARR( e, str, cpp_t ) TOSTRING( AST_TYPE_ENUM_DEF_ENUM_TO_ARR( e ) ),
#define AST_TYPE_ENUM_DEF_STR_CODE( e, str, cpp_t ) str,

enum class
AST_Type : u8
{
	AST_TYPE_DEF( AST_TYPE_ENUM_DEF_ENUM )
	_BASIC_COUNT,
	_ARRAY_BEGIN = _BASIC_COUNT,
	__ARRAY_BEGIN_BACKSTEP = _ARRAY_BEGIN - 1,
	AST_TYPE_DEF( AST_TYPE_ENUM_DEF_ENUM_ARR )
	//ARRAY_ELEMENT,
	//---
	_COUNT
};
const char * AST_Type_code_strings[] = {
	AST_TYPE_DEF( AST_TYPE_ENUM_DEF_STR_CODE )
	AST_TYPE_DEF( AST_TYPE_ENUM_DEF_STR_CODE )
};
const char * AST_Type_strings[] = {
	AST_TYPE_DEF( AST_TYPE_ENUM_DEF_STR )
	AST_TYPE_DEF( AST_TYPE_ENUM_DEF_STR_ARR )
};
#undef AST_TYPE_ENUM_DEF
#undef AST_TYPE_ENUM_DEF_ENUM
#undef AST_TYPE_ENUM_DEF_ENUM_ARR

inline
bool
AST_TYPE_IS_ARRAY( AST_Type type )
{
	return ( (type) >= AST_Type::_ARRAY_BEGIN );
}

struct
AST_Variable_Ref
{
	// NOTE(theGiallo): declarations are AST_Var_Decls             stored in each AST_Function

	// NOTE(theGiallo): if is constant the type and decl_index are relative to AST_Program
	u32 is_constant :  1;
	// NOTE(theGiallo): this is relative to an AST_Function
	u32 type        :  7;
	u32 decl_index  :  8; // NOTE(theGiallo): index of per type declaration
	//u32 array_element_index : 16;
};
using AST_Var_Ref = AST_Variable_Ref;
struct
AST_Variable
{
	AST_Type type;
	union
	{
		struct
		{
			//AST_Type array_type;
			u16      length; // NOTE(theGiallo): this length is constant, cannot be a variable
		} array;
		// NOTE(theGiallo): array elements would refer to a variable for the index, so we avoid them for now
		// they will be implemented via multiple funcalls
		//struct
		//{
		//    AST_Var_Ref array;
		//    AST_Var_Ref index;
		//} array_element;
	};
};

#define AST_NODE_TYPE_ENUM_DEF(ENTRY) \
	ENTRY(IF_ELSE) \
	ENTRY(WHILE) \
	ENTRY(FOR_EACH) \
	ENTRY(FOR_EACH_MANY) \
	ENTRY(FOR_RANGE) \
	ENTRY(FUNCTION_CALL) \
	ENTRY(ARRAY_ACCESS) \

#define AST_NODE_TYPE_ENUM_DEF_ENUM( e ) e,
#define AST_NODE_TYPE_ENUM_DEF_STR( e ) TOSTRING( e ),
#define AST_NODE_TYPE_ENUM_DEF_TO_STRING( e ) case AST_Node_Type::e: to_string_##e( str, context, node ); break;

enum
class
AST_Node_Type : u8
{
	AST_NODE_TYPE_ENUM_DEF(AST_NODE_TYPE_ENUM_DEF_ENUM)
	//---
	_COUNT
};
const char * AST_Node_Type_strings[] = {
	AST_NODE_TYPE_ENUM_DEF(AST_NODE_TYPE_ENUM_DEF_STR)
};
//#undef AST_NODE_TYPE_ENUM_DEF
#undef AST_NODE_TYPE_ENUM_DEF_ENUM
#undef AST_NODE_TYPE_ENUM_DEF_ENUM_ARR

struct
AST_Node
{
	AST_Node_Type type;
};
struct
AST_Block
{
	vector<AST_Node*>    nodes;
};
struct
AST_Variable_Declarations
{
	vector<AST_Variable> variables_declarations_per_type[ (u32)AST_Type::_COUNT ]; // NOTE(theGiallo): [type][decl_index]
};
using AST_Var_Decls = AST_Variable_Declarations;
enum
class
AST_Function_Type
{
	AST,
	NATIVE,
	//---
	_COUNT
};
struct
AST_Function
{
	AST_Function_Type   type;
	// NOTE(theGiallo): in/out parameters references are relative to the declarations
	// of the function: `declarations`. `declarations` has to be maintained in sync with the parameters.
	vector<AST_Var_Ref> in_parameters;
	vector<AST_Var_Ref> inout_parameters;
	AST_Var_Decls       declarations;
};
struct
AST_Function_AST
{
	AST_Function ast_function;
	AST_Block    block;

	inline
	void
	init()
	{
		ast_function.type = AST_Function_Type::AST;
	}
};
struct
AST_Function_Native
{
	AST_Function ast_function;
	String       name;
	String       declaration_text;

	inline
	void
	init()
	{
		ast_function.type = AST_Function_Type::NATIVE;
	}
};
struct
AST_Node_If_Else
{
	AST_Node    node;
	AST_Var_Ref condition;
	AST_Block   if_block;
	AST_Block   else_block;

	inline
	void
	init()
	{
		node.type = AST_Node_Type::IF_ELSE;
	}
};
struct
AST_Node_While
{
	AST_Node    node;
	AST_Var_Ref condition;
	AST_Block   block;

	inline void init()
	{
		node.type = AST_Node_Type::WHILE;
	}
};
struct
AST_Node_For_Each
{
	AST_Node    node;
	AST_Var_Ref array;
	// NOTE(theGiallo): at each loop a copy of the index is written in there
	AST_Var_Ref copy_of_index;
	//AST_Variable     array_element;
	AST_Block   block;

	inline void init()
	{
		node.type = AST_Node_Type::FOR_EACH;
	}
};

// NOTE(theGiallo): to manage the index and the accessed arrays I think the only viable solution is
// to build a variables "dictionary" while navigating the AST.
//
// Another option is to write the index into a normal variable, and forcibly write it at every loop
// iteration. Array access should be constructed and would not be available pre-made.

// NOTE(theGiallo): array over many arrays with 1 single for loop, accessing all the arrays with the
// index at the same time. Arrays are parallel, not orthogonal.
//
// Does this make sense? I think not.
//
// We already have the for_each and it would have the same outcome. The only difference would be the
// possible min operator on the arrays lengths. E.g. iterating over A[10] and B[3] would mean a 3 long
// for loop. Changing later the lengths of the arrays could change the length of the loop or not.
struct
AST_Node_For_Each_Many
{
	AST_Node            node;
	vector<AST_Var_Ref> arrays;
	AST_Var_Ref         copy_of_index;
	//vector<AST_Variable>     array_elements;
	AST_Block           block;

	inline void init()
	{
		node.type = AST_Node_Type::FOR_EACH_MANY;
	}
};
struct
AST_Function_ID
{
	// NOTE(theGiallo): relative to AST_Program.functions
	s32 decl_index;
};
struct
AST_Node_Function_Call
{
	AST_Node            node;
	AST_Function_ID     function;
	vector<AST_Var_Ref> in_parameters;
	vector<AST_Var_Ref> inout_parameters;

	inline void init()
	{
		node.type = AST_Node_Type::FUNCTION_CALL;
	}
};
struct
AST_Node_Array_Access
{
	enum class
	Type
	{
		READ,
		WRITE
	};
	AST_Node    node;
	AST_Var_Ref array;
	AST_Var_Ref index;
	AST_Var_Ref var;
	Type        type;

	inline void init()
	{
		node.type = AST_Node_Type::ARRAY_ACCESS;
	}
};

template<typename T>
void
initializer_to_string( T value, String * str );
void
initializer_to_string( f32 v, String * str )
{
	str->add( "%e", v );
}
void
initializer_to_string( s32 v, String * str )
{
	str->add( "%d", v );
}
void
initializer_to_string( u32 v, String * str )
{
	str->add( "%u", v );
}
void
initializer_to_string( bool v, String * str )
{
	str->add( "%s", BOOL_TO_S( v ) );
}

void
initializer_to_string( V2 v, String * str )
{
	str->add( "vec2( %e, %e )", v.x, v.y );
}
void
initializer_to_string( V3 v, String * str )
{
	str->add( "vec3( %e, %e, %e )", v.x, v.y, v.z );
}
void
initializer_to_string( V4 v, String * str )
{
	str->add( "vec4( %e, %e, %e, %e )", v.x, v.y, v.z, v.w );
}

// NOTE(theGiallo): matnxm: A matrix with n columns and m rows.
// OpenGL uses column-major matrices, which is standard for mathematics users. Example: mat3x4.
// https://www.khronos.org/opengl/wiki/Data_Type_(GLSL)#Matrices
template<s32 COLS, s32 ROWS>
void
initializer_to_string( Mx<f32,COLS,ROWS> mat, String * str )
{
	const s32 cols_count = COLS;
	const s32 rows_count = ROWS;
	if ( cols_count == rows_count )
	{
		str->add( "mat%d( ", cols_count );
	} else
	{
		str->add( "mat%dx%d( ", cols_count, rows_count );
	}
	for_0M( row, rows_count )
	for_0M( col, cols_count )
	{
		if ( row == 0 )
		{
			str->add( "      " );
		}

		str->add( "%e", mat[col][row] );

		if ( col != cols_count - 1 && row != rows_count - 1 )
		{
			str->add( ", " );
		} else
		if ( col == cols_count - 1 && row != rows_count - 1 )
		{
			str->add( ",\n" );
		} else
		{
			str->add( "\n" );
		}
	}
	str->add( ");" );
}

template<typename T, AST_Type t>
void
initializer_to_string( vector<T> arr, String * str )
{
	str->add( "%s[%d](\n", AST_Type_code_strings[s32(t)], arr.size() );
	for_0M( i, arr.size() )
	{
		str->add( "   " );
		initializer_to_string( arr[i], str );
		if ( i != i__count - 1 )
		{
			str->add( ",\n" );
		}
	}
	str->add( ")" );
}

struct
AST_Value
{
	AST_Type type;

	typedef void To_String( AST_Value * self, String * str );
	To_String * _initializer_to_string;

	void
	initializer_to_string( String * str )
	{
		if ( _initializer_to_string )
		{
			_initializer_to_string( this, str );
		}
	}
};

// NOTE(theGiallo): we could make this struct a template and use `using` to create the types with the macro,
// but it would be the same and the more template we use the more slow it is to compile
#define AST_TYPE_VALUE_STRUCT(AST_TYPE, GLSL_T_STR, CPP_TYPE) \
struct \
GLUE(AST_Value_,AST_TYPE) \
{ \
	AST_Value ast_value; \
	CPP_TYPE value; \
	void \
	init() \
	{ \
		ast_value.type = AST_Type::AST_TYPE; \
		ast_value._initializer_to_string = initializer_to_string; \
	} \
	static \
	void \
	initializer_to_string( AST_Value * _self, String * str ) \
	{ \
		auto * self = (GLUE(AST_Value_,AST_TYPE)*)_self; \
		ast::initializer_to_string( self->value, str ); \
	} \
}; \

#define AST_TYPE_VALUE_STRUCT_ARR(AST_TYPE,GLSL_T_STR,CPP_TYPE) AST_TYPE_VALUE_STRUCT( AST_TYPE_ENUM_DEF_ENUM_TO_ARR(AST_TYPE), AST_TYPE_ENUM_DEF_STR_ARR(GLSL_T_STR), vector<CPP_TYPE> )

AST_TYPE_DEF( AST_TYPE_VALUE_STRUCT )
AST_TYPE_DEF( AST_TYPE_VALUE_STRUCT_ARR )

#undef AST_TYPE_VALUE_STRUCT_ARR
#undef AST_TYPE_VALUE_STRUCT

struct
AST_Program
{
	AST_Var_Decls             constants;
	vector<String>            constants_names [s32(AST_Type::_COUNT)]; // NOTE(theGiallo): [type][decl_index]
	vector<AST_Value*>        constants_values[s32(AST_Type::_COUNT)]; // NOTE(theGiallo): [type][decl_index]
	vector<AST_Function *>    functions;
	AST_Function            * main_function;
};

struct
AST_Context
{
	AST_Program      * program;
	AST_Function_AST * function_containing_the_block;
	AST_Block        * the_block;
	u64                seeds[2];

	const char * func_name;
	s32          indent_lvl;
};

enum
AST_Block_Opt
{
	DONT_PRINT_CURLY = 0,
	PRINT_CURLY
};

enum
AST_Missing
{
	NONE = 0,
	ARRAY_VARIABLE,
	INT_VARIABLE,
	VAR,
};


AST_Function *
AST_fun_ref_from_id( AST_Context * context, AST_Function_ID id )
{
	AST_Function * ret = context->program->functions[id.decl_index];
	return ret;
}

AST_Variable
dereference_variable( AST_Context * context, AST_Var_Ref var_ref )
{
	auto func_ast = context->function_containing_the_block;
	AST_Var_Decls * decls = var_ref.is_constant ? &context->program->constants : &func_ast->ast_function.declarations;
	AST_Variable ret = decls->variables_declarations_per_type[var_ref.type][var_ref.decl_index];
	return ret;
}

AST_Type
random_array_type( AST_Context * context )
{
	AST_Type ret;
	ret = AST_Type( randu_between( context->seeds, u32(AST_Type::_ARRAY_BEGIN), u32(AST_Type::_COUNT) - 1 ) );
	return ret;
}
AST_Type
random_non_array_type( AST_Context * context )
{
	AST_Type ret;
	ret = AST_Type( randu_between( context->seeds, u32(0), u32(AST_Type::__ARRAY_BEGIN_BACKSTEP) ) );
	return ret;
}
AST_Type
random_type( AST_Context * context )
{
	AST_Type ret;
	ret = AST_Type( randu_between( context->seeds, u32(0), u32(AST_Type::_COUNT) - 1 ) );
	return ret;
}
bool
ast_type_from_string( const char * str, AST_Type * out_type )
{
	bool ret = false;
	for_0M( i, s32( AST_Type::_COUNT ) )
	{
		ret = string_equal( str, AST_Type_code_strings[i] );
		if ( ret )
		{
			*out_type = AST_Type(i);
			break;
		}
	}
	return ret;
}
AST_Type
get_array_base_type( AST_Type array_type )
{
	AST_Type ret = AST_Type( s32(array_type) - s32(AST_Type::_ARRAY_BEGIN) );
	return ret;
}

bool
declarations_have_type( AST_Var_Decls * decl, u32 decl_type )
{
	bool ret =
	   decl->variables_declarations_per_type[ decl_type ].size() > 0;
	//|| ( decl_type < (u32)AST_Type::ARRAY
	//  && decl->variables_declarations_per_type[ decl_type + (u32)AST_Type::ARRAY ].size() > 0);
	return ret;
}
enum class
Allow_Constants : bool
{
	DISALLOW = false,
	ALLOW    = true,
};
bool
parameters_are_assignable( AST_Context         * context,
                           vector<AST_Var_Ref> * parameters,
                           Allow_Constants       allow_constants )
{
	AST_Var_Decls * const_declarations = & context->program->constants;
	AST_Var_Decls * fun_declarations   = & context->function_containing_the_block->ast_function.declarations;
	bool ret = true;
	for ( auto it = parameters->begin(), end = parameters->end(); it != end; ++it )
	{
		ret = ( (bool)allow_constants && declarations_have_type( const_declarations, it->type ) )
		   || declarations_have_type( fun_declarations, it->type );
		if ( ! ret )
		{
			return ret;
		}
	}
	return ret;
}
bool
random_assign( AST_Context         * context,
               vector<AST_Var_Ref> * parameters_decl_ref,
               //AST_Var_Decls       * parameters_decl,
               vector<AST_Var_Ref> * parameters_to_assign,
               Allow_Constants       allow_constants )
{
	tg_assert( parameters_decl_ref->size() == parameters_to_assign->size() );

	bool ret = false;

	//for ( s32 i = 0, i_count = parameters_decl_ref->size(); i != i_count; ++i )
	s32 p_i = 0;
	for ( auto it  = parameters_decl_ref->begin(),
	           end = parameters_decl_ref->end();
	      it != end;
	      ++it, ++p_i )
	{
		auto non_constants = context->function_containing_the_block->ast_function.declarations.variables_declarations_per_type;
		auto     constants = context->program->constants.variables_declarations_per_type;
		// parameters_decl->variables_declarations_per_type[it->type].size()
		s32 total_available_count = non_constants[ it->type ].size();
		s32 non_constants_count = total_available_count;
		if ( (bool)allow_constants )
		{
			total_available_count += (u32)constants[ it->type ].size();
		}
		if ( ! total_available_count )
		{
			return ret;
		}
		s32 chosen_index = (s32)randu_between( context->seeds, 0, total_available_count - 1 );
		auto decls = non_constants;
		bool is_constant = chosen_index >= non_constants_count;
		if ( is_constant )
		{
			chosen_index -= non_constants_count;
			decls = constants;
		}
		AST_Variable var = decls[it->type][chosen_index];
		(*parameters_to_assign)[p_i].decl_index  = chosen_index;
		(*parameters_to_assign)[p_i].is_constant = is_constant;
		(*parameters_to_assign)[p_i].type        = (u8) var.type;
	}

	ret = true;
	return ret;
}
AST_Node_Function_Call *
make_function_call( AST_Context * context, AST_Function_ID func_id )
{
	AST_Node_Function_Call * ret = new AST_Node_Function_Call();

	vector<AST_Function*> & funcs = context->program->functions;
	AST_Function * f = funcs[func_id.decl_index];
	ret->init();
	ret->function = func_id;
	ret->in_parameters.resize( f->in_parameters.size() );
	ret->inout_parameters.resize( f->inout_parameters.size() );
	bool b_res;
	b_res = random_assign( context, & f->in_parameters,    /*& f->declarations,*/ & ret->in_parameters,    Allow_Constants::ALLOW    );
	if ( b_res )
	{
		b_res = random_assign( context, & f->inout_parameters, /*& f->declarations,*/ & ret->inout_parameters, Allow_Constants::DISALLOW );
	}
	if ( ! b_res )
	{
		//ret->in_parameters.clear();
		//ret->inout_parameters.clear();
		delete ret;
		ret = 0;
		return ret;
	}

	return ret;
}
bool
get_callable_function( AST_Context *context, AST_Node_Function_Call **out_fun_call )
{
	bool ret = false;

	vector<AST_Function*> & funcs = context->program->functions;
	vector<s32> callable_funct_indices = vector<s32>();
	callable_funct_indices.reserve( funcs.size() );
	for_0M( i, funcs.size() )
	{
		if ( parameters_are_assignable( context, & funcs[i]->in_parameters,    Allow_Constants::ALLOW    )
		  && parameters_are_assignable( context, & funcs[i]->inout_parameters, Allow_Constants::DISALLOW ) )
		{
			callable_funct_indices.push_back( i );
		}
	}

	if ( callable_funct_indices.size() == 0 )
	{
		return ret;
	}

	s32 cfi = (s32)randu_between( context->seeds, 0, callable_funct_indices.size() - 1 );
	AST_Function_ID func_id = {};
	func_id.decl_index = callable_funct_indices[cfi];

	*out_fun_call = make_function_call(context, func_id);
	tg_assert( out_fun_call );
	ret = true;

	return ret;
}
bool
get_variable_of_type( AST_Context * context, AST_Type type, AST_Var_Ref * out_res, Allow_Constants allow_constants )
{
	tg_assert( context );
	tg_assert( out_res );

	bool ret = false;
	AST_Var_Decls * decls   = &context->function_containing_the_block->ast_function.declarations;
	auto * type_decls       = & decls->variables_declarations_per_type[(s32)type];
	auto * type_decls_const = & context->program->constants.variables_declarations_per_type[(s32)type];
	s32 non_const_count = type_decls->size();
	s32 count = non_const_count;
	if ( allow_constants == Allow_Constants::ALLOW )
	{
		count += type_decls_const->size();
	}
	if ( count == 0 )
	{
		return ret;
	}
	s32 chosen_index = (s32)randu_between( context->seeds, 0, count - 1 );
	bool is_constant = chosen_index >= non_const_count;
	s32 chosen_relative_index = is_constant
	                          ? chosen_index - non_const_count
	                          : chosen_index;
	out_res->decl_index = chosen_relative_index;
	out_res->type = (u32)type;
	out_res->is_constant = is_constant;
	ret = true;
	return ret;
}
bool
get_array_variable( AST_Context * context,
                    AST_Var_Ref * out_res,
                    Allow_Constants allow_constants,
                    bool constraint_has_var_of_same_type = false,
                    AST_Missing * missing = 0,
                    AST_Type * var_type = 0 )
{
	tg_assert( context );
	tg_assert( out_res );

	bool ret = false;

	AST_Var_Decls * decls       = &context->function_containing_the_block->ast_function.declarations;
	AST_Var_Decls * decls_const = &context->program->constants;

	bool at_least_one_missing_var = false;

	s32 types_count = (s32)AST_Type::_ARRAY_BEGIN;
	s32 total_vars_count             = 0;
	s32 cumulative_counts_up_to_type       [(s32)AST_Type::_ARRAY_BEGIN] = {};
	s32 total_non_const_no_var_count = 0;
	s32 cumulative_counts_up_to_type_no_var[(s32)AST_Type::_ARRAY_BEGIN] = {};

	for ( s32 i = 0; i != types_count; ++i )
	{
		s32 t = i + types_count;
		s32 c = decls->variables_declarations_per_type[t].size();
		if ( c > 0 )
		{
			s32 c_same_type = decls->variables_declarations_per_type[i].size();
			if ( ! constraint_has_var_of_same_type || c_same_type > 0 )
			{
				total_vars_count += c;
			} else
			{
				total_non_const_no_var_count += c;
				at_least_one_missing_var = true;
			}
		}
		cumulative_counts_up_to_type       [i] = total_vars_count;
		cumulative_counts_up_to_type_no_var[i] = total_non_const_no_var_count;
	}

	s32 total_non_const_count = total_vars_count;

	s32 total_const_count        = 0;
	s32 cumulative_counts_up_to_type_const       [(s32)AST_Type::_ARRAY_BEGIN] = {};
	s32 total_const_no_var_count = 0;
	s32 cumulative_counts_up_to_type_const_no_var[(s32)AST_Type::_ARRAY_BEGIN] = {};

	if ( allow_constants == Allow_Constants::ALLOW )
	{
		for ( s32 i = 0; i != types_count; ++i )
		{
			s32 t = i + types_count;
			s32 c = decls_const->variables_declarations_per_type[t].size();
			if ( c > 0 )
			{
				s32 c_same_type = decls->variables_declarations_per_type[i].size();
				if ( ! constraint_has_var_of_same_type || c_same_type > 0 )
				{
					total_vars_count  += c;
					total_const_count += c;
				} else
				{
					total_const_no_var_count += c;
					at_least_one_missing_var = true;
				}
			}
			cumulative_counts_up_to_type_const       [i] = total_const_count;
			cumulative_counts_up_to_type_const_no_var[i] = total_const_no_var_count;
		}
	}

	auto random_choose =
	   [&context]( s32  * non_const_counts,
	               s32  * const_counts,
	               s32    total_non_const_count,
	               s32    total_vars_count,
	               s32  * chosen_decl_index,
	               s32  * chosen_type,
	               bool * is_constant )
	{
		s32 types_count = (s32)AST_Type::_ARRAY_BEGIN;
		s32 chosen_index = (s32)randu_between( context->seeds, 0, total_vars_count - 1 );

		* is_constant = chosen_index >= total_non_const_count;
		s32 chosen_relative_index = *is_constant
		                          ? chosen_index - total_non_const_count
		                          : chosen_index;
		s32 * cumulative_counts = *is_constant ? const_counts : non_const_counts;

		*chosen_decl_index = -1;
		*chosen_type = -1;
		for ( s32 i = 0; i != types_count; ++i )
		{
			if ( chosen_relative_index < cumulative_counts[i] )
			{
				*chosen_type = i + types_count;
				s32 base_counts = i == 0 ? 0 : cumulative_counts[i - 1];
				*chosen_decl_index = chosen_relative_index - base_counts;
				break;
			}
		}
		tg_assert( *chosen_decl_index >= 0 );
		tg_assert( *chosen_type >= 0 );
	};

	s32 chosen_decl_index;
	s32 chosen_type;
	bool is_constant;

	if ( total_vars_count == 0 )
	{
		if ( at_least_one_missing_var )
		{
			if ( missing ) * missing = AST_Missing::VAR;

			if ( var_type )
			{
				random_choose( cumulative_counts_up_to_type_no_var,
				               cumulative_counts_up_to_type_const_no_var,
				               total_non_const_no_var_count,
				               total_non_const_no_var_count + total_const_no_var_count,
				               &chosen_decl_index,
				               &chosen_type,
				               &is_constant );
				*var_type = get_array_base_type( AST_Type( chosen_type ) );
			}
		} else
		{
			if ( missing ) * missing = AST_Missing::ARRAY_VARIABLE;
		}

		return ret;
	}

	random_choose( cumulative_counts_up_to_type, cumulative_counts_up_to_type_const,
	               total_non_const_count, total_vars_count,
	               &chosen_decl_index, &chosen_type, &is_constant );

	if ( is_constant )
	{
		tg_assert( s32(context->program->constants.variables_declarations_per_type[chosen_type].size()) > chosen_decl_index );
	} else
	{
		tg_assert( s32(context->function_containing_the_block->ast_function.declarations.variables_declarations_per_type[chosen_type].size()) > chosen_decl_index );
	}
	out_res->decl_index  = chosen_decl_index;
	out_res->type        = (u32)chosen_type;
	out_res->is_constant = is_constant;
	ret = true;
	return ret;
}
// TODO(theGiallo, 2020-11-25): add/get/set constant value
AST_Var_Ref
add_constant( AST_Context * context, AST_Variable v, String name = {} )
{
	AST_Var_Ref ret = {};
	ret.type = u32(v.type);
	ret.decl_index = context->program->constants.variables_declarations_per_type[s32(v.type)].size();
	ret.is_constant = true;

	context->program->constants.variables_declarations_per_type[s32(v.type)].push_back( v );
	context->program->constants_names                          [s32(v.type)].push_back( name );
	return ret;
}
AST_Var_Ref
add_variable( AST_Context * context, AST_Function * f, AST_Type type )
{
	AST_Var_Ref ret;
	AST_Var_Decls    * decls = & f->declarations;

	AST_Variable var = {};
	var.type = type;
	if ( AST_TYPE_IS_ARRAY( type ) )
	{
		var.array.length = randu_between( context->seeds, 0, AST_MAX_ARRAY_LENGTH );
	}
	ret.decl_index  = decls->variables_declarations_per_type[(s32)type].size();
	ret.is_constant = false;
	ret.type        = (u32)type;
	decls->variables_declarations_per_type[(s32)type].push_back( var );

	return ret;
}
AST_Var_Ref
add_variable( AST_Context * context, AST_Type type )
{
	return add_variable( context, &context->function_containing_the_block->ast_function, type );
}
void
add_variable( AST_Context * context, AST_Function * f, AST_Var_Ref * out_var_ref )
{
	AST_Type type = random_type( context );
	if ( AST_TYPE_IS_ARRAY( type ) )
	{
		type = random_type( context );
	}
	*out_var_ref = add_variable( context, f, type );
}
void
add_variable( AST_Context * context, AST_Var_Ref * out_var_ref )
{
	return add_variable( context, &context->function_containing_the_block->ast_function, out_var_ref );
}
void
add_in_parameter_to_function( AST_Context * context, AST_Function * f, AST_Type type )
{
	AST_Var_Ref vr = add_variable( context, f, type );
	f->in_parameters.push_back( vr );
}
void
add_inout_parameter_to_function( AST_Context * context, AST_Function * f, AST_Type type )
{
	AST_Var_Ref vr = add_variable( context, f, type );
	f->inout_parameters.push_back( vr );
}
void
add_in_parameter_to_function( AST_Context * context, AST_Type type )
{
	add_in_parameter_to_function( context, &context->function_containing_the_block->ast_function, type );
}
void
add_inout_parameter_to_function( AST_Context * context, AST_Type type )
{
	add_inout_parameter_to_function( context, &context->function_containing_the_block->ast_function, type );
}
void
make_function_callable( AST_Context * context, AST_Function * fun )
{
	AST_Function_AST * c_fun  = context->function_containing_the_block;
	AST_Var_Decls    & decls  = c_fun->ast_function.declarations;
	AST_Var_Decls    & consts = context->program->constants;
	for_0M( in_i, fun->in_parameters.size() )
	{
		u32 type = fun->in_parameters[in_i].type;
		if ( decls .variables_declarations_per_type[type].size() == 0
		  || consts.variables_declarations_per_type[type].size() == 0 )
		{
			// TODO(theGiallo): maybe randomly choose between adding a const or a variable
			AST_Var_Ref v = add_variable( context, (AST_Type)type );
			(void)v;
		}
	}
	for_0M( inout_i, fun->inout_parameters.size() )
	{
		u32 type = fun->inout_parameters[inout_i].type;
		if ( decls.variables_declarations_per_type[type].size() == 0 )
		{
			AST_Var_Ref v = add_variable( context, (AST_Type)type );
			(void)v;
		}
	}
}
AST_Function_ID
get_random_function_id( AST_Context * context )
{
	AST_Function_ID ret;
	vector<AST_Function*> & funcs = context->program->functions;
	if ( (u32)funcs.size() == 0 )
	{
		ret.decl_index = -1;
		return ret;
	}
	s32 chosen_index = randu_between( context->seeds, 0, (u32)funcs.size() - 1 );
	ret.decl_index = chosen_index;
	return ret;
}

s32
random_node_index( AST_Context * context )
{
	s32 ret;
	vector<AST_Node*> * nodes = & context->the_block->nodes;
	u32 s = nodes->size();
	ret = s == 0 ? 0 : randu_between( context->seeds, 0, s - 1 );
	return ret;
}
AST_Block *
random_block( AST_Context * context )
{
	AST_Block * ret = 0;

	struct Stack_Status
	{
		AST_Block * block;
		u32         node_index : 30;
		u32         node_part  :  2;
	};

	vector<AST_Block*> all_blocks  = vector<AST_Block*>();
	vector<Stack_Status> stack     = vector<Stack_Status>();

	Stack_Status ss = { .block = context->the_block };
	stack.push_back( ss );

	while ( stack.size() != 0 )
	{
		Stack_Status & ss = stack[stack.size() - 1];
		vector<AST_Node *> & nodes = ss.block->nodes;
		s32 next_i = ss.node_index;
		AST_Block * b = 0;
		for_mM( i, ss.node_index, s32( nodes.size() ) )
		{
			ss.node_index = next_i;
			AST_Node * node = nodes[i];
			switch ( node->type )
			{
				case AST_Node_Type::ARRAY_ACCESS:
					break;
				case AST_Node_Type::FOR_EACH:
					b = &((AST_Node_For_Each*)node)->block;
					break;
				case AST_Node_Type::FOR_EACH_MANY:
					b = &((AST_Node_For_Each_Many*)node)->block;
					break;
				case AST_Node_Type::FOR_RANGE:
					//b = &((AST_Node_For_Range*)node)->block;
					break;
				case AST_Node_Type::FUNCTION_CALL:
					break;
				case AST_Node_Type::IF_ELSE:
					if ( ss.node_part == 0 )
					{
						b = &((AST_Node_If_Else*)node)->if_block;
					} else
					{
						b = &((AST_Node_If_Else*)node)->else_block;
					}
					++ss.node_part;
					break;
				case AST_Node_Type::WHILE:
					b = &((AST_Node_While*)node)->block;
					break;
				case AST_Node_Type::_COUNT:
					break;
			}

			if ( b )
			{
				ss.node_index = ss.node_part == 1 ? i : i + 1;
				break;
			}
		}

		if ( b )
		{
			Stack_Status nss = { .block = b };
			stack.push_back( nss );
		} else
		{
			all_blocks.push_back( ss.block );
			stack.pop_back();
		}
	}

	u32 bi = randu_between( context->seeds, 0, all_blocks.size() - 1 );
	ret = all_blocks[bi];

	//log_dbg( "found %u blocks, chose %u", u32( all_blocks.size() ), bi );

	return ret;
}

// NOTE(theGiallo): the given block is assumed to be contained in the function space
bool
add_callable_function_call_to_block( AST_Context * context, s32 * out_index )
{
	bool ret = false;

	vector<AST_Node*> * nodes = & context->the_block->nodes;
	s32 node_index = random_node_index( context );

	AST_Node_Function_Call * fun_call_node;
	ret = get_callable_function( context, &fun_call_node );
	if ( ! ret )
	{
		return ret;
	}

	AST_Node * new_node = (AST_Node*)fun_call_node;
	nodes->insert( nodes->begin() + node_index, new_node );
	*out_index = node_index;
	//insert_in_list_at_pos( nodes, node_index, &new_node );

	return ret;
}
bool
add_function_call_to_block( AST_Context * context, s32 * out_index )
{
	bool ret = false;
	ret = add_callable_function_call_to_block( context, out_index );
	if ( ! ret )
	{
		AST_Function_ID func_id = get_random_function_id( context );
		if ( func_id.decl_index < 0 )
		{
			*out_index = -1;
			return ret;
		}
		AST_Function * func = AST_fun_ref_from_id( context, func_id );
		make_function_callable( context, func );
		ret = add_callable_function_call_to_block( context, out_index );
		tg_assert( ret );
	}
	return ret;
}
bool
add_array_access_to_block( AST_Context * context,
                           AST_Node_Array_Access::Type access_type,
                           s32 * out_index,
                           AST_Missing * missing = 0,
                           AST_Type * var_type = 0 )
{
	bool ret = false;

	AST_Var_Ref arr_v_ref   = {};
	AST_Var_Ref index_v_ref = {};
	AST_Var_Ref var_v_ref   = {};

	Allow_Constants array_allow_const = access_type == AST_Node_Array_Access::Type::READ  ? Allow_Constants::ALLOW : Allow_Constants::DISALLOW;
	Allow_Constants var_allow_const   = access_type == AST_Node_Array_Access::Type::WRITE ? Allow_Constants::ALLOW : Allow_Constants::DISALLOW;

	bool b_res;
	b_res = get_array_variable( context, &arr_v_ref, array_allow_const, true, missing, var_type );
	if ( ! b_res )
	{
		return ret;
	}
	b_res = get_variable_of_type( context, AST_Type::INT, &index_v_ref, Allow_Constants::ALLOW );
	if ( ! b_res )
	{
		if ( missing ) *missing = AST_Missing::INT_VARIABLE;
		return ret;
	}
	b_res = get_variable_of_type( context, get_array_base_type( AST_Type( arr_v_ref.type ) ), &var_v_ref, var_allow_const );

	if ( ! b_res )
	{
		if ( missing ) *missing = AST_Missing::VAR;
		return ret;
	}

	AST_Node_Array_Access * node = new AST_Node_Array_Access();
	node->init();

	node->array = arr_v_ref;
	node->index = index_v_ref;
	node->var   = var_v_ref;
	node->type  = access_type;

	vector<AST_Node*> * nodes = & context->the_block->nodes;
	s32 node_index = random_node_index( context );

	AST_Node * new_node = (AST_Node*)node;
	nodes->insert( nodes->begin() + node_index, new_node );
	*out_index = node_index;

	ret = true;

	return ret;
}
bool
add_if_else( AST_Context * context, s32 * out_index )
{
	bool ret = false;
	AST_Var_Ref condition_var = {};
	ret = get_variable_of_type( context, AST_Type::BOOL, &condition_var, Allow_Constants::DISALLOW );
	if ( ! ret )
	{
		return ret;
	}
	AST_Node_If_Else * ifelse_node = new AST_Node_If_Else();
	ifelse_node->init();
	ifelse_node->condition = condition_var;
	//ifelse_node->if_block
	//ifelse_node->else_block

	vector<AST_Node*> * nodes = & context->the_block->nodes;
	s32 node_index = random_node_index( context );
	nodes->insert( nodes->begin() + node_index, (AST_Node*)ifelse_node );
	*out_index = node_index;
	ret = true;

	return ret;
}
bool
add_while( AST_Context * context, s32 * out_index )
{
	bool ret = false;
	AST_Var_Ref condition_var = {};
	ret = get_variable_of_type( context, AST_Type::BOOL, &condition_var, Allow_Constants::DISALLOW );
	if ( ! ret )
	{
		return ret;
	}
	AST_Node_While * while_node = new AST_Node_While();
	while_node->init();
	while_node->condition = condition_var;
	//while_node->block

	vector<AST_Node*> * nodes = & context->the_block->nodes;
	s32 node_index = random_node_index( context );
	nodes->insert( nodes->begin() + node_index, (AST_Node*)while_node );
	*out_index = node_index;
	ret = true;

	return ret;
}
bool
add_for_each( AST_Context * context, s32 * out_index, AST_Missing * missing = 0 )
{
	bool ret = false;
	AST_Var_Ref array_var = {};
	ret = get_array_variable( context, &array_var, Allow_Constants::ALLOW );
	if ( ! ret )
	{
		if ( missing != 0 )
		{
			*missing = AST_Missing::ARRAY_VARIABLE;
		}
		return ret;
	}
	AST_Var_Ref copy_of_index_var = {};
	ret = get_variable_of_type( context, AST_Type::INT, &copy_of_index_var, Allow_Constants::DISALLOW );
	if ( ! ret )
	{
		if ( missing != 0 )
		{
			*missing = AST_Missing::INT_VARIABLE;
		}
		return ret;
	}

	AST_Node_For_Each * for_each_node = new AST_Node_For_Each();
	for_each_node->init();
	for_each_node->array = array_var;
	for_each_node->copy_of_index = copy_of_index_var;
	//for_each_node->block

	vector<AST_Node*> * nodes = & context->the_block->nodes;
	s32 node_index = random_node_index( context );
	nodes->insert( nodes->begin() + node_index, (AST_Node*)for_each_node );
	*out_index = node_index;
	ret = true;

	return ret;
}

void
add_if_else_forced( AST_Context * context, s32 * out_index )
{
	bool b_res = add_if_else( context, out_index );
	if ( ! b_res )
	{
		AST_Var_Ref vr = add_variable( context, AST_Type::BOOL );
		(void)vr;
		b_res = add_if_else( context, out_index );
		tg_assert( b_res );
	}
}

void
add_for_each_forced( AST_Context * context, s32 * out_index )
{
	bool b_res;
	do
	{
		AST_Missing missing = AST_Missing::NONE;
		b_res = add_for_each( context, out_index, &missing );
		if ( ! b_res )
		{
			switch ( missing )
			{
				case AST_Missing::ARRAY_VARIABLE:
				{
					AST_Type type  = random_array_type( context );
					AST_Var_Ref vr = add_variable( context, type );
					AST_Variable v = dereference_variable( context, vr );
					log_dbg( "vr.is_constant = %s", BOOL_TO_S(vr.is_constant) );
					log_dbg( "length = %u", u32(v.array.length) );
					log_dbg( "type = %u %u %s", u32(v.type), u32(vr.type), AST_Type_strings[vr.type] );
					break;
				}
				case AST_Missing::INT_VARIABLE:
				{
					AST_Var_Ref vr = add_variable( context, AST_Type::INT );
					(void)vr;
					break;
				}
				case AST_Missing::NONE:
					break;
				case AST_Missing::VAR:
					ILLEGAL_PATH();
					break;
			}
		}
	} while ( ! b_res );
}

void
add_while_forced( AST_Context * context, s32 * out_index )
{
	bool b_res = add_while( context, out_index );
	if ( ! b_res )
	{
		AST_Var_Ref vr = add_variable( context, AST_Type::BOOL );
		(void)vr;
		b_res = add_while( context, out_index );
		tg_assert( b_res );
	}
}
void
add_array_access_forced( AST_Context * context, AST_Node_Array_Access::Type access_type, s32 * out_index )
{
	AST_Missing missing = {};
	AST_Type vt = {};
	bool b_res;
	do
	{
		b_res = add_array_access_to_block( context, access_type, out_index, &missing, &vt );
		switch ( missing )
		{
			case AST_Missing::ARRAY_VARIABLE:
			{
				AST_Type arr_type = random_array_type( context );
				AST_Var_Ref vr = add_variable( context, arr_type );
				(void)vr;
				break;
			}
			case AST_Missing::INT_VARIABLE:
			{
				AST_Var_Ref vr = add_variable( context, AST_Type::INT );
				(void)vr;
				break;
			}
				break;
			case AST_Missing::VAR:
			{
				AST_Var_Ref vr = add_variable( context, vt );
				(void)vr;
				break;
			}
				break;
			case AST_Missing::NONE:
				break;
		}
	} while ( ! b_res );
}

void
fun_name( AST_Context * context, String * str, AST_Function_ID f_id )
{
	AST_Function * f = AST_fun_ref_from_id( context, f_id );
	if ( f->type == AST_Function_Type::AST )
	{
		str->add( "fun_%03d", f_id.decl_index );
	} else
	if ( f->type == AST_Function_Type::NATIVE )
	{
		AST_Function_Native * nf = (AST_Function_Native*)f;
		str->add( nf->name.str );
	} else
	{
		ILLEGAL_PATH();
	}
}
void
var_name( String * str, const char * func_name, AST_Type t, u32 decl_index )
{
	const char * t_name = AST_Type_strings[(s32)t];
	str->add( "%s_%s_%u", func_name, t_name, decl_index );
}
void
var_name( String * str, AST_Context * context, AST_Var_Ref vr )
{
	if ( vr.is_constant )
	{
		const char * name = context->program->constants_names[vr.type][vr.decl_index].str;
		if ( name )
		{
			str->add( name );
		} else
		{
			var_name( str, "const", AST_Type(vr.type), vr.decl_index );
		}
	} else
	{
		var_name( str, context->func_name, AST_Type(vr.type), vr.decl_index );
	}
}
void
var_initializer( String * str, AST_Context * context, AST_Var_Ref vr )
{
	if ( vr.is_constant )
	{
		context->program->constants_values[vr.type][vr.decl_index]->initializer_to_string( str );
	} else
	{
		// NOTE(theGiallo): we don't have initializers for non-constants for now
	}
}
String
random_index_name( AST_Context * context )
{
	String ret = {};
	u64 r = xorshift128plus( context->seeds );
	ret.add( "_%x", r );
	return ret;
}
void
to_string( String * str, AST_Context * context, const AST_Node * node );
void
to_string( String * str, AST_Context * context, const AST_Block * block, AST_Block_Opt opt = AST_Block_Opt::PRINT_CURLY )
{
	s32 & indent_lvl = context->indent_lvl;

	if ( opt == AST_Block_Opt::PRINT_CURLY )
	{
		str->indent( context->indent_lvl );
		str->add( "{\n" );
		++indent_lvl;
	}

	for ( AST_Node * n : block->nodes )
	{
		str->indent( indent_lvl );
		#if 0
		const char * t_name = AST_Node_Type_strings[s32(n->type)];
		str->add( "// %s\n", t_name );
		#else
		// NOTE(theGiallo): this causes a SIGSEV in vsnprintf
		str->add( "// %s\n", AST_Node_Type_strings[s32(n->type)] );
		#endif
		to_string( str, context, n );
	}

	if ( opt == AST_Block_Opt::PRINT_CURLY )
	{
		--indent_lvl;
		str->indent( indent_lvl );
		str->add( "}\n" );
	}
}
void
to_string_IF_ELSE( String * str, AST_Context * context, const AST_Node * node )
{
	auto n = (AST_Node_If_Else*)node;
	String str_name = {};
	var_name( &str_name, context, n->condition );
	str->indent( context->indent_lvl );
	str->add( "if ( %s )\n", str_name.str );
	to_string( str, context, &n->if_block );
	str->indent( context->indent_lvl );
	str->add( "else\n" );
	to_string( str, context, &n->else_block );
}
void
to_string_WHILE( String * str, AST_Context * context, const AST_Node * node )
{
	auto n = (AST_Node_While*)node;

	String it_name = random_index_name( context);

	str->indent( context->indent_lvl );
	str->add( "for ( int %s; %s < %s &&", it_name.str, it_name.str, "1000" );
	var_name( str, context, n->condition );
	str->add( "; ++%s )\n", it_name.str );
	to_string( str, context, &n->block );

	it_name.free();
}
void
to_string_FOR_EACH( String * str, AST_Context * context, const AST_Node * node )
{
	auto n = (AST_Node_For_Each*)node;
	AST_Variable v_arr = dereference_variable( context, n->array );
	u16 arr_len = v_arr.array.length;
	//AST_Variable v_index_copy = dereference_variable( context, n->copy_of_index );
	String array_name = {};
	String copy_of_index_name = {};
	String index_name = random_index_name( context );
	var_name( &array_name, context, n->array );
	var_name( &copy_of_index_name, context, n->copy_of_index );
	str->indent( context->indent_lvl );
	str->add( "for ( int %s = 0; %s != %u && ( %s = %s, true ); ++%s )\n",
	          index_name.str,
	          index_name.str,
	          u32(arr_len),
	          copy_of_index_name.str,
	          index_name.str,
	          index_name.str );
	to_string( str, context, &n->block );
}
void
to_string_FOR_EACH_MANY( String * str, AST_Context * context, const AST_Node * node )
{
	auto n = (AST_Node_For_Each_Many*)node;
	log_err( "no for each many implemented" );
}
void
to_string_FOR_RANGE( String * str, AST_Context * context, const AST_Node * node )
{
	// auto n = (AST_Node_For_Range*)node;
	const char * msg = "for range not implemented";
	log_err( "%s", msg );
	str->add( msg );
}
void
to_string_FUNCTION_CALL( String * str, AST_Context * context, const AST_Node * node )
{
	auto n = (AST_Node_Function_Call*)node;

	AST_Function * f = AST_fun_ref_from_id( context, n->function );
	//str->indent( context->indent_lvl );
	//str->add( "void\n" );
	//str->indent( context->indent_lvl );
	s32 f_name_length = str->occupancy;
	str->indent( context->indent_lvl );
	fun_name( context, str, n->function );
	f_name_length = str->occupancy - f_name_length;
	str->add( "( " );
	for_0M( i,  s32(n->in_parameters.size()) )
	{
		if ( i != 0 )
		{
			str->add( ", " );
		}
		var_name( str, context, n->in_parameters[i] );
	}
	for_0M ( i,  s32(n->inout_parameters.size()) )
	{
		str->add( ", " );
		var_name( str, context, n->inout_parameters[i] );
	}
	str->add( " );\n" );
}
void
to_string_ARRAY_ACCESS( String * str, AST_Context * context, const AST_Node * node )
{
	auto n = (AST_Node_Array_Access*)node;

	//AST_Variable v_arr   = dereference_variable( context, n->array );
	//AST_Variable v_index = dereference_variable( context, n->index );
	//AST_Variable v_var   = dereference_variable( context, n->var   );
	//u16 arr_len = v_arr.array.length;

	String array_name = {};
	String index_name = {};
	String _var_name  = {};
	var_name( &array_name, context, n->array );
	var_name( &index_name, context, n->index );
	var_name( &_var_name,  context, n->var   );

	str->indent( context->indent_lvl );

	if ( n->type == AST_Node_Array_Access::Type::READ )
	{
		str->add( "%s = %s[%s];\n",
		          _var_name .str,
		          array_name.str,
		          index_name.str );
	} else
	if ( n->type == AST_Node_Array_Access::Type::WRITE )
	{
		str->add( "%s[%s] = %s;\n",
		          array_name.str,
		          index_name.str,
		          _var_name .str );
	}
}
void
to_string( String * str, AST_Context * context, const AST_Node * node )
{
	switch ( node->type )
	{
		AST_NODE_TYPE_ENUM_DEF( AST_NODE_TYPE_ENUM_DEF_TO_STRING )
		default:
			tg_assert( false );
	}
}
void
to_string_variable_decl( String * str, AST_Context * context, AST_Var_Ref vr )
{
	String str_name = {};
	var_name( &str_name, context, vr );
	if ( AST_TYPE_IS_ARRAY( AST_Type(vr.type) ) )
	{
		AST_Variable v = context->function_containing_the_block->ast_function.declarations.variables_declarations_per_type[vr.type][vr.decl_index];
		str->add( "%s %s[%d]", AST_Type_code_strings[vr.type], str_name.str, v.array.length );
	} else
	{
		str->add( "%s %s", AST_Type_code_strings[vr.type], str_name.str );
	}
	str_name.free();
}
void
to_string_fun_decl( String * str, AST_Context * context )
{
	AST_Function_AST * func_ast = context->function_containing_the_block;
	AST_Function     * func     = &func_ast->ast_function;
	const char * name = context->func_name;

	if ( func->type == AST_Function_Type::NATIVE )
	{
		return;
	}

	str->add( "void\n%s( ", name );
	String str_name = {};
	str_name.init();
	auto str_pars = [str, context]( vector<AST_Var_Ref> parameters, char * pio )
	{
		s32 i = 0;
		bool last = strcmp( pio, "inout" ) == 0;
		for ( AST_Var_Ref p : parameters )
		{
			str->add( "%s ", pio );
			to_string_variable_decl( str, context, p );
			if ( i == (s32)parameters.size() - 1 && last )
			{
				str->add( " " );
			} else
			{
				str->add( ", " );
			}
		}
	};
	auto is_par = [func] ( AST_Type type, u32 decl_index )
	{
		bool ret = false;
		auto is_par = [type,decl_index]( vector<AST_Var_Ref> pars )
		{
			bool ret = false;
			for ( AST_Var_Ref p : pars )
			{
				ret = p.decl_index == decl_index && p.type == (u32)type;
				if ( ret )
				{
					return ret;
				}
			}
			return ret;
		};
		ret = is_par( func->in_parameters ) || is_par( func->inout_parameters );
		return ret;
	};

	str_pars( func->in_parameters, "in" );
	str_pars( func->inout_parameters, "inout" );
	str->add( ")\n" );
	str->add( "{\n" );
	s32 & indent_lvl = context->indent_lvl;
	++indent_lvl;
	for_0M( t, (s32)AST_Type::_COUNT )
	{
		vector<AST_Variable> decls = func->declarations.variables_declarations_per_type[t];
		for_0M ( di, decls.size() )
		{
			AST_Variable v = decls[di];
			if ( is_par( v.type, di ) )
			{
				continue;
			}
			//const char * type_str = AST_Type_code_strings[t];
			//var_name( &str_name, name, (AST_Type)t, di );
			AST_Var_Ref vr = {};
			vr.decl_index = di;
			vr.is_constant = false;
			vr.type = t;
			str->indent( indent_lvl );
			to_string_variable_decl( str, context, vr );
			//str->add( "%s %s;// t:%u\n", type_str, str_name.str, u32(t) );
			str->add( "; // t:%u\n", u32(t) );
			str_name.clear();
		}
	}

	to_string( str, context, &func_ast->block, AST_Block_Opt::DONT_PRINT_CURLY );

	--indent_lvl;
	str->add( "}\n" );

	str_name.free();
}
void
to_string_fun_decl( String * str, AST_Context * context, AST_Function_ID fun_id, const char * func_name = 0 )
{
	AST_Function * f = AST_fun_ref_from_id( context, fun_id );
	if ( f->type != AST_Function_Type::AST )
	{
		if ( f->type == AST_Function_Type::NATIVE )
		{
			AST_Function_Native * nf = (AST_Function_Native*)f;
			if ( nf->declaration_text.occupancy )
			{
				str->add( nf->declaration_text.str );
				str->add( "\n" );
			}
		}
		return;
	}

	AST_Function_AST * func_ast = (AST_Function_AST*)f;
	context->function_containing_the_block = func_ast;

	String fn_str = {};
	if ( func_name == 0 )
	{
		fun_name( context, &fn_str, fun_id );
		context->func_name = fn_str.str;
	} else
	{
		context->func_name = func_name;
	}

	to_string_fun_decl( str, context );

	if ( func_name == 0 )
	{
		fn_str.free();
	}
}
void
to_string_const_decl( String * str, AST_Context * context, AST_Var_Ref vr )
{
	if ( context->program->constants_names[vr.type][vr.decl_index].str )
	{
		return;
	}
	str->indent( context->indent_lvl );
	str->add( "%s ", AST_Type_code_strings[vr.type] );
	var_name( str, context, vr );
	str->add( " = " );
	var_initializer( str, context, vr );
	str->add( ";\n" );
}
String
to_string( AST_Context * context, const char * main_func_name = "mainImage" )
{
	String ret = {};

	context->indent_lvl = 0;

	for_0M( t, s32(AST_Type::_COUNT) )
	for_0M( i, context->program->constants_names[t].size() )
	{
		to_string_const_decl( &ret, context, AST_Var_Ref{ .is_constant = true, .type = u32(t), .decl_index = u32(i) } );
	}

	for_0M( i, context->program->functions.size() )
	{
		to_string_fun_decl( &ret, context, (AST_Function_ID){ .decl_index = i } );
	}

	context->function_containing_the_block = (AST_Function_AST*)context->program->main_function;
	context->func_name = main_func_name;
	to_string_fun_decl( &ret, context );

	return ret;
}

bool
parse_uniform( AST_Context * context, const char * text, const char * & out_next_text, AST_Variable & out_var, String & out_name )
{
	bool ret = false;

	const char * fun_start_text = text;

	out_next_text = text;

	s32 s32_type;
	parse::consume_blanks( text );
	if ( ! parse::consume_any( text, AST_Type_code_strings, s32(AST_Type::_BASIC_COUNT), &s32_type ) )
	{
		return ret;
	}

	AST_Type type = AST_Type( s32_type );

	s32 blanks_count = parse::consume_blanks( text );

	if ( ! blanks_count )
	{
		return ret;
	}

	const char * name_start;
	s32 name_length;
	if ( ! parse::consume_c_name( text, &name_start, &name_length ) )
	{
		return ret;
	}

	parse::consume_blanks( text );

	const char * arr_length_start = 0;
	s32 arr_length_length;
	if ( parse::consume( text, "[" ) )
	{
		parse::consume_blanks( text );
		arr_length_start = text;

		const char * arr_length_past_end;
		if ( ! parse::until( text, ']', true ) )
		{
			arr_length_past_end = text;
			parse::consume_blanks( text );
			if ( ! parse::consume( text, "]" ) )
			{
				return ret;
			}
		} else
		{
			++text;
			arr_length_past_end = text;
		}

		arr_length_length = s32( arr_length_past_end - arr_length_start );
	}

	parse::consume_blanks( text );
	if ( ! parse::consume( text, ";" ) )
	{
		return ret;
	}

	s32 array_length = 0;
	bool is_array = arr_length_start != 0;
	if ( is_array )
	{
		type = AST_Type( s32(type) + s32(AST_Type::_ARRAY_BEGIN) );
		s32 scanned = sscanf( arr_length_start, "%d", &array_length );
		if ( scanned != 1 )
		{
			return ret;
		}
	}


	out_next_text = text;

	out_var = {};
	out_var.type = type;
	out_var.array.length = array_length;
	out_name = {};
	out_name.add_substrig( name_start, name_length );

	ret = true;

	return ret;
}

bool
parse_native_function( AST_Context * context, const char * text, const char * & out_next_text, AST_Function_Native & out_fun )
{
	bool ret = false;

	//out_fun = {};
	//out_fun.init();

	const char * fun_start_text = text;

	out_next_text = text;

	auto consume = [&text]( const char * str )
	{
		return parse::consume( text, str );
	};
	auto until = [&text]( char c, bool allow_blanks = true )
	{
		return parse::until( text, c, allow_blanks );
	};
	auto until_one = [&text]( const char * chars, bool allow_blanks = true )
	{
		return parse::until_one( text, chars, allow_blanks );
	};
	auto consume_blanks = [&text]()
	{
		parse::consume_blanks( text );
	};

	bool b_res;

	consume_blanks();
	b_res = consume( "void" );
	if ( ! b_res )
	{
		//if ( text[0] )
		//{
		//	log_err( "expected \"void\" at %ld", text - fun_start_text );
		//}
		return ret;
	}
	consume_blanks();

	{
		const char * name_start = text;
		b_res = until( '(', false );
		if ( ! b_res )
		{
			log_err( "expected '(' at %ld", text - fun_start_text );
			return ret;
		}
		out_fun.name.clear();
		out_fun.name.add_substrig( name_start, text - name_start );
	}

	consume_blanks();
	b_res = consume( "(" );
	if ( ! b_res )
	{
		log_err( "expected '(' at %ld", text - fun_start_text );
		return ret;
	}
	consume_blanks();

	enum Par_Type
	{
		NONE,
		IN,
		INOUT
	};

	String p_type_str = {};

	s32 parameters_parsed_count = 0;
	for (;;)
	{
		Par_Type parameter_type = Par_Type::NONE;
		if ( consume( "in " ) )
		{
			parameter_type = Par_Type::IN;
		} else
		if ( consume( "inout " ) )
		{
			parameter_type = Par_Type::INOUT;
		}

		if ( parameter_type != Par_Type::NONE )
		{
			consume_blanks();

			const char * start = text;
			// NOTE(theGiallo): type
			b_res = until( '\0', false );
			if ( ! b_res )
			{
				log_err( "expected more text at %ld", text - fun_start_text );
				goto end;
			}

			p_type_str.clear();
			p_type_str.add_substrig( start, text - start );
			AST_Type p_type;
			b_res = ast_type_from_string( p_type_str.str, &p_type );
			if ( !b_res )
			{
				log_err( "parameter type '%s' not recognized at %ld", p_type_str.str, text - fun_start_text );
				goto end;
			}

			consume_blanks();
			// NOTE(theGiallo): name
			b_res = until_one( ",)", false );
			if ( ! b_res )
			{
				log_err( "expected parameter name followed by , or ) at %ld", text - fun_start_text );
				goto end;
			}

			if ( parameter_type == Par_Type::IN )
			{
				add_in_parameter_to_function( context, &out_fun.ast_function, p_type );
			} else
			{
				add_inout_parameter_to_function( context, &out_fun.ast_function, p_type );
			}

			++parameters_parsed_count;

			consume_blanks();
			if ( consume( "," ) )
			{
				consume_blanks();
				continue;
			} else
			if ( consume( ")" ) )
			{
				break;
			} else
			{
				log_err( "expected , or ) at %ld", text - fun_start_text );
				goto end;
			}
		} else
		{
			if ( parameters_parsed_count )
			{
				log_err( "could not reach end of parameters at %ld", text - fun_start_text );
				goto end;
			} else
			{
				break;
			}
		}
	}

	consume_blanks();
	if ( consume( "{" ) )
	{
		s32 blocks_count = 1;
		while ( blocks_count != 0 )
		{
			b_res = until_one( "{}", true );
			if ( ! b_res )
			{
				goto end;
			}
			if ( consume( "{" ) )
			{
				++blocks_count;
			} else
			if ( consume( "}" ) )
			{
				--blocks_count;
			} else
			{
				goto end;
			}
		}

		out_fun.declaration_text.clear();
		out_fun.declaration_text.add_substrig( fun_start_text, text - fun_start_text );
	} else
	if ( consume( ";" ) )
	{
		out_fun.declaration_text.clear();
	} else
	{
		log_err( "expecting { or ; after parameters at %ld", text - fun_start_text );
		goto end;
	}

	out_next_text = text;
	ret = true;

	end:
	p_type_str.free();

	return ret;
}

} // GenGen::ast
