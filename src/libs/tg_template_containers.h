#ifndef _TG_TEMPLATE_CONTAINERS_H_
#define _TG_TEMPLATE_CONTAINERS_H_ 1

#include "basic_types.h"
#include "macro_tools.h"
#include "system.h"

namespace tg
{
	template<typename T>
	struct
	Array_VM
	{
		s64 length;
		T * array;

		static Array_VM make( s64 length )
		{
			Array_VM ret = {};
			ret.init( length );
			return ret;
		}

		bool
		init( s64 length )
		{
			bool ret = false;
			this->length = length;
			s32 Bs = bytes_size();
			this->length = 0;
			array = (T*)sys_mem_reserve( Bs );
			if ( array == SYS_MEM_FAILED )
			{
				log_err( "failed to allocate %d Bytes", Bs );
				array = NULL;
				return ret;
			}
			ret = sys_mem_commit( array, Bs );
			if ( ! ret )
			{
				log_err( "failed to commit memory %d", Bs );
				destroy();
				array = NULL;
				return ret;
			}
			this->length = length;
			return ret;
		}

		bool
		destroy()
		{
			bool ret = array == NULL;
			if ( ! ret )
			{
				// NOTE(theGiallo): not sure here if to set array to NULL anyway or not
				ret = sys_mem_uncommit( array, bytes_size() );
				if ( ret )
				{
					array = NULL;
				}
			}
			return ret;
		}

		inline
		s64
		bytes_size()
		{
			s64 ret = length * sizeof ( T );
			return ret;
		}

		inline
		operator T*()
		{
			return array;
		}
	};
}

#endif /* ifndef _TG_TEMPLATE_CONTAINERS_H_ */
