#ifndef _TG_STRING_H_
#define _TG_STRING_H_ 1

#include <cstdarg>
#include "basic_types.h"
//#include "tgmath.h"
//#include "utility.h"
#include "macro_tools.h"
#include "system.h"

namespace tg
{

struct
String
{
	static const s32 INITIAL_SIZE = 16;
	static const s32 DEFAULT_RESERVED_SIZE = 128 * 1024 * 1024;
	char * str;
	s32    capacity;
	s32    occupancy;
	s32    reserved_size;
	inline
	s32
	free_space()
	{
		return capacity - occupancy;
	}

	inline
	bool
	init( s32 size = INITIAL_SIZE )
	{
		bool ret = false;
		reserved_size = MIN( size, DEFAULT_RESERVED_SIZE );
		str = (char*)sys_mem_reserve( size );
		ret = sys_mem_commit( str, size );
		if ( ! ret )
		{
			free();
		} else
		{
			capacity = ret ? size : 0;
			occupancy = 0;
		}

		return ret;
	}

	inline
	void
	free()
	{
		if ( str && reserved_size )
		{
			sys_mem_free( str, reserved_size );
		}
		zero_struct( *this );
	}

	inline
	void
	clear()
	{
		if ( occupancy > 0 ) memset( str, 0x0, occupancy );
		occupancy = 0;
	}

	inline
	bool
	add( const char * fmt, ... )
	{
		va_list args;
		if ( str == 0 )
		{
			init();
		}
		va_start( args, fmt );
		bool ret = false;
		for (;;)
		{
			s32 fs = free_space();
			va_list _args;
			va_copy( _args, args );
			//va_start( args, fmt );
			s32 written = vsnprintf( str + occupancy, fs, fmt, _args );
			va_end( _args );
			if ( written >= fs )
			{
				bool b_res = ensure_free_space( written + 1 );
				if ( ! b_res )
				{
					goto _return;
				}
			} else
			{
				occupancy += written;
				ret = true;
				break;
			}
		}
		_return:
		va_end( args );
		return ret;
	}

	inline
	bool
	indent( s32 l )
	{
		bool ret = false;
		for_0M( i, l )
		{
			if ( ! add( "\t" ) )
			{
				return ret;
			}
		}
		ret = true;
		return ret;
	}

	inline
	bool
	add_substrig( const char * str, s64 length )
	{
		bool ret = ensure_free_space( length + 1 );

		if ( ret )
		{
			memcpy( this->str + occupancy, str, length );
			this->str[ occupancy + length ] = 0;
			occupancy += length + 1;
		}

		return ret;
	}

	inline
	bool
	ensure_free_space( s32 new_free_space )
	{
		bool ret = false;

		s32 new_capacity = capacity + new_free_space;
		if ( new_capacity <= reserved_size )
		{
			ret = sys_mem_commit( str, new_capacity );
		} else
		{
			s32 new_reserved = new_capacity * 2;
			char * new_str = (char*)sys_mem_reserve( new_reserved );
			if ( ! new_str )
			{
				return ret;
			}
			ret= sys_mem_commit( new_str, new_capacity );
			if ( ! ret )
			{
				return ret;
			}
			memset( new_str + occupancy, 0x0, new_capacity - occupancy );
			memcpy( new_str, str, occupancy );
			if ( ret )
			{
				sys_mem_free( str, reserved_size );
				reserved_size = new_reserved;
				str = new_str;
			}
		}

		if ( ret )
		{
			capacity = new_capacity;
		}

		return ret;
	}

};

} // namespace tg

#endif /* ifndef _TG_STRING_H_  */
