#include "stack_generic_vm.h"
#include "system.h"
#include "macro_tools.h"

bool
stack_generic_vm_init( Stack_Generic_VM * stack, u64 element_size, u64 max_capacity, u64 initial_capacity, f64 capacity_increment_multiplier, u64 capacity_increment_adder )
{
	tg_assert( stack );
	tg_assert( capacity_increment_multiplier >= 1 );
	tg_assert( initial_capacity <= max_capacity );

	bool ret = false;

	u64 page_size = sys_page_size();

	u64 size_to_reserve = ( element_size * max_capacity + page_size - 1 ) / page_size * page_size;
	max_capacity = size_to_reserve / element_size;

	stack->mem = sys_mem_reserve( max_capacity * element_size );

	if ( stack->mem )
	{
		stack->max_capacity = max_capacity;
		stack->element_size = element_size;

		s64 total_size_to_commit = ( element_size * initial_capacity + page_size - 1 ) / page_size * page_size;
		initial_capacity = total_size_to_commit / element_size;

		tg_assert( total_size_to_commit % page_size == 0 );

		bool did_commit = sys_mem_commit( stack->mem, total_size_to_commit );
		if ( !did_commit )
		{
			u64 reserved_size = ( stack->max_capacity * element_size + page_size - 1 ) / page_size * page_size;
			log_err( "failed to commit memory 0x%016lx %ld", (u64)stack->mem, total_size_to_commit );
			sys_mem_free( stack->mem, reserved_size );
			stack->mem = NULL;
		} else
		{
			stack->current_capacity = initial_capacity;
			stack->capacity_increment_adder      = capacity_increment_adder;
			stack->capacity_increment_multiplier = capacity_increment_multiplier;
			stack->occupancy = 0;
			ret = true;
		}
	}

	return ret;
}

bool
stack_generic_vm_reserve_free( Stack_Generic_VM * stack, u64 free_count )
{
	u64 new_capacity_at_least = stack->occupancy + free_count;
	bool ret = stack_generic_vm_reserve( stack, new_capacity_at_least );
	return ret;
}

bool
stack_generic_vm_reserve( Stack_Generic_VM * stack, u64 new_capacity_at_least )
{
	tg_assert( stack );
	tg_assert( stack->occupancy <= stack->current_capacity );
	tg_assert( stack->current_capacity <= stack->max_capacity );

	bool ret = false;
	if ( stack->current_capacity < new_capacity_at_least )
	{
		if ( stack->current_capacity < stack->max_capacity )
		{
			u64 page_size = sys_page_size();

			u64 size_to_commit = ( stack->element_size * new_capacity_at_least + page_size - 1 ) / page_size * page_size;
			new_capacity_at_least = size_to_commit / stack->element_size;
			new_capacity_at_least = new_capacity_at_least > stack->max_capacity ? stack->max_capacity : new_capacity_at_least;
			size_to_commit = ( stack->element_size * new_capacity_at_least + page_size - 1 ) / page_size * page_size;

			tg_assert( size_to_commit % page_size == 0 );

			bool did_commit = sys_mem_commit( stack->mem, size_to_commit );
			if ( !did_commit )
			{
				//u64 reserved_size = ( stack->max_capacity * stack->element_size + page_size - 1 ) / page_size * page_size;
				log_err( "failed to commit memory 0x%016lx %ld", (u64)stack->mem, size_to_commit );
				//sys_mem_free( stack->mem, reserved_size );
				//stack->mem = NULL;
			} else
			{
				stack->current_capacity = new_capacity_at_least;
				ret = true;
			}
		}
	} else
	{
		ret = true;
	}

	return ret;
}

bool
stack_generic_vm_push_space( Stack_Generic_VM * stack, void ** first_space, u64 count )
{
	tg_assert( stack );
	tg_assert( stack->occupancy <= stack->current_capacity );
	tg_assert( stack->current_capacity <= stack->max_capacity );

	bool ret = false;

	ret = stack_generic_vm_reserve_free( stack, count );

	if ( ret && stack->current_capacity >= stack->occupancy + count )
	{
		u8 * target = ( (u8*)stack->mem ) + stack->element_size * stack->occupancy;
		*first_space = target;
		stack->occupancy += count;

		ret = true;
	}

	return ret;
}
bool
stack_generic_vm_push_space( Stack_Generic_VM * stack, void ** space )
{
	tg_assert( stack );
	tg_assert( stack->occupancy <= stack->current_capacity );
	tg_assert( stack->current_capacity <= stack->max_capacity );

	bool ret = false;

	if ( stack->current_capacity == stack->occupancy )
	{
		u64 new_capacity_at_least = stack->current_capacity * stack->capacity_increment_multiplier + stack->capacity_increment_adder;
		bool b_res =
		stack_generic_vm_reserve( stack, new_capacity_at_least );
		(void)b_res;
	}
	if ( stack->current_capacity != stack->occupancy )
	{
		u8 * target = ( (u8*)stack->mem ) + stack->element_size * stack->occupancy;
		*space = target;
		++stack->occupancy;

		ret = true;
	}

	return ret;
}
bool
stack_generic_vm_push( Stack_Generic_VM * stack, void * element )
{
	void * space;
	bool ret = stack_generic_vm_push_space( stack, &space );
	if ( ret )
	{
		memcpy( space, element, stack->element_size );
	}
	return ret;
}

void *
stack_generic_vm_peek( Stack_Generic_VM * stack )
{
	tg_assert( stack );
	tg_assert( stack->occupancy <= stack->current_capacity );
	tg_assert( stack->current_capacity <= stack->max_capacity );

	void * ret = 0;

	if ( stack->occupancy )
	{
		ret = ( (u8*)stack->mem ) + stack->element_size * ( stack->occupancy - 1 );
	}

	return ret;
}

bool
stack_generic_vm_peek( Stack_Generic_VM * stack, void * out_copied_element )
{
	tg_assert( stack );
	tg_assert( stack->occupancy <= stack->current_capacity );
	tg_assert( stack->current_capacity <= stack->max_capacity );
	tg_assert( out_copied_element );

	bool ret = false;

	if ( stack->occupancy )
	{
		void * peeked = ( (u8*)stack->mem ) + stack->element_size * ( stack->occupancy - 1 );
		memcpy( out_copied_element, peeked, stack->element_size );
		ret = true;
	}

	return ret;
}

void *
stack_generic_vm_pop( Stack_Generic_VM * stack )
{
	tg_assert( stack );
	tg_assert( stack->occupancy <= stack->current_capacity );
	tg_assert( stack->current_capacity <= stack->max_capacity );

	void * ret = 0;

	if ( stack->occupancy )
	{
		ret = ( (u8*)stack->mem ) + stack->element_size * ( stack->occupancy - 1 );
		--stack->occupancy;
	}

	return ret;
}

bool
stack_generic_vm_pop( Stack_Generic_VM * stack, void * out_copied_element )
{
	tg_assert( stack );
	tg_assert( stack->occupancy <= stack->current_capacity );
	tg_assert( stack->current_capacity <= stack->max_capacity );
	tg_assert( out_copied_element );

	bool ret = false;

	if ( stack->occupancy )
	{
		void * peeked = ( (u8*)stack->mem ) + stack->element_size * ( stack->occupancy - 1 );
		memcpy( out_copied_element, peeked, stack->element_size );
		--stack->occupancy;
		ret = true;
	}

	return ret;
}

void
stack_generic_vm_destroy( Stack_Generic_VM * stack )
{
	tg_assert( stack );
	tg_assert( stack->occupancy <= stack->current_capacity );
	tg_assert( stack->current_capacity <= stack->max_capacity );

	if ( stack->mem )
	{
		u64 page_size = sys_page_size();
		u64 reserved_size = ( stack->max_capacity * stack->element_size + page_size - 1 ) / page_size * page_size;
		sys_mem_free( stack->mem, reserved_size );
	}
	*stack = {};
}
