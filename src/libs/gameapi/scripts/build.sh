#!/bin/bash

THIS_FILE_PATH=$0
THIS_FILE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


ALL=false
DEBUG=false
DEBUG_OPT=false
RELEASE=false
PROFILE=false
PERF=false
BUILD_MAIN=false
BUILD_GAME_API=false

if [ $# == 0 ]
then
	ALL=true
else

	while [[ $# > 0 ]]
	do
	key="$1"

	case $key in
		-d|--debug)
			DEBUG=true
		;;
		-dopt|--debug-opt)
			DEBUG_OPT=true
		;;
		-r|--release)
			RELEASE=true
		;;
		-p|--gprof)
			PROFILE=true
		;;
		-P|--perf)
			PERF=true
		;;
		-M|--main)
			BUILD_MAIN=true
		;;
		-G|--game)
			BUILD_GAME_API=true
		;;
		*)
			echo "Unknown option "$1
		;;
	esac
	shift
	done;
fi

pwd

source $THIS_FILE_DIR/config.sh
source $THIS_FILE_DIR/builds_suffixes.sh

mkdir $BIN_PATH 2> /dev/null


TIMER='time -p '
# GPP="g++ -fdiagnostics-color=auto"
CXX="clang++ -fuse-ld=gold"

#CXX=$TIMER$GPP
CPP_STD="-std=c++11"

declare -a CPP_SOURCES
#CPP_SOURCES[${#CPP_SOURCES[@]}]=`find src -name \*.cpp | sed -r 's/(.*game_api.*|.*tgmath.cpp)//g' | xargs echo`
CPP_SOURCES[${#CPP_SOURCES[@]}]=`find $THIS_FILE_DIR/../ -name \*.cpp | sed -r 's/(.*game_api.*|.*tgmath.cpp)//g'`
echo "find $THIS_FILE_DIR/../ -name \*.cpp | sed -r 's/(.*game_api.*|.*tgmath.cpp)//g'"

declare -a GAME_API_CPP_SOURCES
#GAME_API_CPP_SOURCES[${#GAME_API_CPP_SOURCES[@]}]=`find src -name \*game_api*.cpp`
# for single compilation unit
GAME_API_CPP_SOURCES[${#GAME_API_CPP_SOURCES[@]}]=$THIS_FILE_DIR/../game_api.cpp
#GAME_API_CPP_SOURCES[${#GAME_API_CPP_SOURCES[@]}]=src/game_api_lodepng.cpp

SDL_CONFIG="$THIS_FILE_DIR/../../SDL2/linux/bin/sdl2-config --prefix=$THIS_FILE_DIR/../../SDL2/linux --exec-prefix=$LIBS_REL_PATH/SDL2/linux"

SDL_INCLUDE="$($SDL_CONFIG --cflags )"
declare -a INCLUDE_DIRS
INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="$THIS_FILE_DIR/../"
INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="$THIS_FILE_DIR/../main/"
INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="$THIS_FILE_DIR/../../"
# INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="src/tgmath/"
INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="$THIS_FILE_DIR/../../kiss_fft130/"
INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="$THIS_FILE_DIR/../../lepre/"
INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="$THIS_FILE_DIR/../../lodepng/"
INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="$THIS_FILE_DIR/../../stb_truetype/"
INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="$THIS_FILE_DIR/../../stb_vorbis/"
#
# INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="./libraries/SDL2_mixer/linux/include/"

declare -a INCLUDE_QUOTE_DIRS
## NOTE: for SDL2_ttf
# INCLUDE_QUOTE_DIRS[${#INCLUDE_QUOTE_DIRS[@]}]="../libraries/SDL2/linux/include/SDL2/"

SDL_LIBS="$($SDL_CONFIG --libs)"
declare -a LIBS_DIRS
# LIBS_DIRS[${#LIBS_DIRS[@]}]="./libraries/SDL2_ttf/linux/lib64/"
# LIBS_DIRS[${#LIBS_DIRS[@]}]="./ibraries/SDL2_mixer/linux/lib64/"

declare -a LIBS
# LIBS[${#LIBS[@]}]=SDL2
# LIBS[${#LIBS[@]}]=SDL2_ttf
# LIBS[${#LIBS[@]}]=SDL2_mixer
#LIBS[${#LIBS[@]}]=GL
#LIBS[${#LIBS[@]}]=GLEW
# LIBS[${#LIBS[@]}]=GLU
LIBS[${#LIBS[@]}]=m

declare -a DEFINES
# DEFINES[${#DEFINES[@]}]="PIPPO=1"
DEFINES[${#DEFINES[@]}]="BIN_PATH=\"$BIN_PATH\""
DEFINES[${#DEFINES[@]}]="WE_LOAD_OUR_GL=1"
DEFINES[${#DEFINES[@]}]="THE_MEMORY_MALLOCED=1"

declare -a CPP_FLAGS
CPP_FLAGS[${#CPP_FLAGS[@]}]='-Wall'
CPP_FLAGS[${#CPP_FLAGS[@]}]='-Wextra'
CPP_FLAGS[${#CPP_FLAGS[@]}]='-fno-exceptions'
# CPP_FLAGS[${#CPP_FLAGS[@]}]=''
CPP_FLAGS[${#CPP_FLAGS[@]}]='-Wno-missing-braces'
CPP_FLAGS[${#CPP_FLAGS[@]}]='-Wfatal-errors'
CPP_FLAGS[${#CPP_FLAGS[@]}]='-rdynamic'

declare -a GAME_API_CPP_FLAGS
GAME_API_CPP_FLAGS[${#GAME_API_CPP_FLAGS[@]}]='-fPIC'
GAME_API_CPP_FLAGS[${#GAME_API_CPP_FLAGS[@]}]='-shared'
GAME_API_CPP_FLAGS[${#GAME_API_CPP_FLAGS[@]}]='-fno-exceptions'
# CPP_FLAGS[${#CPP_FLAGS[@]}]=''

function \
include_quote_dirs()
{
	for p in ${INCLUDE_QUOTE_DIRS[@]}
	do
		printf " -iquote$p "
	done
}

function \
include_dirs()
{
	for p in ${INCLUDE_DIRS[@]}
	do
		printf " -I$p "
	done
}

function \
libs_dirs()
{
	for p in ${LIBS_DIRS[@]}
	do
		printf " -L$p -Wl,-rpath=$p "
	done
}

function \
link_libs()
{
	for l in ${LIBS[@]}
	do
		printf " -l$l "
	done
}

function \
defines()
{
	for d in ${DEFINES[@]}
	do
		printf " -D$d "
	done
}

function \
cpp_flags()
{
	for f in ${CPP_FLAGS[@]}
	do
		printf " $f "
	done
}

function \
game_api_cpp_flags()
{
	for f in ${GAME_API_CPP_FLAGS[@]}
	do
		printf " $f "
	done
}

function \
game_api_cpp_sources()
{
	for f in ${GAME_API_CPP_SOURCES[@]}
	do
		printf " $f "
	done
}

function \
main_flags()
{
	printf " $CPP_STD "
	cpp_flags
	defines
	include_quote_dirs
	include_dirs
	printf " $SDL_INCLUDE "
	libs_dirs
	link_libs
	printf " $SDL_LIBS "
}

function \
game_api_flags()
{
	printf " $CPP_STD "
	cpp_flags
	defines
	include_quote_dirs
	include_dirs
	libs_dirs
	link_libs
	printf " -shared -fPIC "
}

function \
delete_other_dls()
{
	ESC_BP=$(echo $BIN_PATH | sed -e 's/[\/&]/\\&/g')
	ESC_NAME=$( echo $GAME_API_SO_NAME | sed -e 's/[]\/$*.^|[]/\\&/g')
	ESC_SFX=$( echo $SUFFIX | sed -e 's/[]\/$*.^|[]/\\&/g')
	ESC_EXT=$( echo $GAME_API_SO_EXTENSION | sed -e 's/[]\/$*.^|[]/\\&/g')
	MATCH=$ESC_NAME$ESC_SFX'_[0-9]*'$ESC_EXT
	ls -1 $BIN_PATH | grep -e "$MATCH"| \
	sed -sr 's/('$MATCH')/'$ESC_BP'\1/g' | \
	tr '\n' ' ' | \
	xargs trash
}
function \
echo_sep()
{
	echo "----------------------------------------"
}

# NOTE(theGiallo):
# parameters:
#    BUILD_CODENAME
#    SUFFIX
#    CXX_FLAGS_BUILD_SPECIFIC
#    OUTPUT_COMPILATION_DB
function \
build()
{
	COMPILE_DB_FILE_PATH=$THIS_FILE_DIR/../compile_commands.json
	if $OUTPUT_COMPILATION_DB
	then
		echo "["                                                           >  $COMPILE_DB_FILE_PATH
	fi

	echo THIS_FILE_DIR=$THIS_FILE_DIR
	echo SDL_INCLUDE=$SDL_INCLUDE
	echo SDL_LIBS=$SDL_LIBS

	if $BUILD_MAIN
	then
		echo_sep
		echo " Going to compile [$BUILD_CODENAME]"
		echo

		command=" $CXX_FLAGS_BUILD_SPECIFIC -DBIN_SUFFIX=\"$SUFFIX\" "
		command=$command"`main_flags` -Wno-unused-function "
		command=$command"`echo $CPP_SOURCES | xargs echo` -o $BIN_PATH$BIN_NAME$SUFFIX"
		command="$CXX $command"

		echo $command

		$TIMER $command
		ret=$?

		if $OUTPUT_COMPILATION_DB
		then
			for f in $CPP_SOURCES
			do
				echo "{"                                                       >> $COMPILE_DB_FILE_PATH
				echo "	\"directory\":\"`pwd`\","                              >> $COMPILE_DB_FILE_PATH
				echo "	\"command\": \"`echo $command | sed 's/"/\\\\"/g'`\"," >> $COMPILE_DB_FILE_PATH
				echo "	\"file\":\"$f\""                                       >> $COMPILE_DB_FILE_PATH
				echo "},"                                                      >> $COMPILE_DB_FILE_PATH
			done
		fi

		if [[ $ret != 0 ]]
		then
			echo "main $BUILD_CODENAME compilation failed! Terminating build script."
			exit $ret
		else
			echo "main $BUILD_CODENAME compilation succeeded!"
		fi
	fi


	if $BUILD_GAME_API
	then
		echo_sep
		echo " Going to compile game_api [$BUILD_CODENAME]"
		echo

		if pidof $BIN_NAME$SUFFIX > /dev/null
		then
			:
		else
			echo -n "deleting old libraries..."
			delete_other_dls
			echo -e "   done\n"
		fi
		OUTPUT_LIB_NAME=$GAME_API_SO_NAME$SUFFIX"_"$RANDOM$GAME_API_SO_EXTENSION
		OUTPUT_LIB_PATH=$BIN_PATH$OUTPUT_LIB_NAME

		command=" -O0 -ggdb3 -fstack-protector-all -DDEBUG=1 -DDEBUG_GL=1 "
		command=$command"`game_api_flags` -Wno-unused-function "
		command=$command"`game_api_cpp_sources` -o $OUTPUT_LIB_PATH"

		command="$CXX $command"

		$TIMER $command
		ret=$?

		if $OUTPUT_COMPILATION_DB
		then
			#for f in `game_api_cpp_sources`
			for f in `find $THIS_FILE_DIR/../ -name 'game_api*.cpp'`
			#for f in `find src/ -regextype posix-egrep -regex '.*/(game_api.*\.cpp|.*/*\.h)' | grep -v 'platform'`
			do
				echo "{"                                                       >> $COMPILE_DB_FILE_PATH
				echo "	\"directory\":\"`pwd`\","                              >> $COMPILE_DB_FILE_PATH
				echo "	\"command\": \"`echo $command | sed 's/"/\\\\"/g'`\"," >> $COMPILE_DB_FILE_PATH
				echo "	\"file\":\"$f\""                                       >> $COMPILE_DB_FILE_PATH
				echo "},"                                                      >> $COMPILE_DB_FILE_PATH
			done
		fi

		if [[ $ret != 0 ]]
		then
			echo "game_api $BUILD_CODENAME compilation failed! Terminating build script."
			exit $ret
		else
			echo "game_api $BUILD_CODENAME compilation succeeded!"
			#mv $GAME_API_SO_NAME$SUFFIX"_tmp_"$GAME_API_SO_EXTENSION $GAME_API_SO_NAME$SUFFIX$GAME_API_SO_EXTENSION
			echo -n $OUTPUT_LIB_PATH > "$BIN_PATH/last_lib_name"$SUFFIX".txt"
		fi
	fi

	if $OUTPUT_COMPILATION_DB
	then
		echo "]"                                                           >> $COMPILE_DB_FILE_PATH
	fi
}

if $ALL || $DEBUG
then
	SUFFIX=$SUFFIX_DEBUG
	BUILD_CODENAME=$BUILD_CODENAME_DEBUG
	CXX_FLAGS_BUILD_SPECIFIC=$CXX_FLAGS_BUILD_SPECIFIC_DEBUG
	OUTPUT_COMPILATION_DB=[ $BUILD_MAIN && $BUILD_GAME_API ]
	build
fi


if $ALL || $DEBUG_OPT
then
	SUFFIX=$SUFFIX_DEBUG_OPT
	BUILD_CODENAME=$BUILD_CODENAME_DEBUG_OPT
	CXX_FLAGS_BUILD_SPECIFIC=$CXX_FLAGS_BUILD_SPECIFIC_DEBUG_OPT
	OUTPUT_COMPILATION_DB=false
	build
fi

if $ALL || $RELEASE
then
	SUFFIX=$SUFFIX_RELEASE
	BUILD_CODENAME=$BUILD_CODENAME_RELEASE
	CXX_FLAGS_BUILD_SPECIFIC=$CXX_FLAGS_BUILD_SPECIFIC_RELEASE
	OUTPUT_COMPILATION_DB=false
	build
fi

if $ALL || $PROFILE
then
	SUFFIX=$SUFFIX_PROFILE
	BUILD_CODENAME=$BUILD_CODENAME_PROFILE
	CXX_FLAGS_BUILD_SPECIFIC=$CXX_FLAGS_BUILD_SPECIFIC_PROFILE
	OUTPUT_COMPILATION_DB=false
	build
fi

if $ALL || $PERF
then
	SUFFIX=$SUFFIX_PERF
	BUILD_CODENAME=$BUILD_CODENAME_PERF
	CXX_FLAGS_BUILD_SPECIFIC=$CXX_FLAGS_BUILD_SPECIFIC_PERF
	OUTPUT_COMPILATION_DB=false
	build
fi
