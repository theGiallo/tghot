#!/bin/bash

THIS_FILE_PATH=$0
THIS_FILE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


ALL=false
DEBUG=false
RELEASE=false
PROFILE=false

source $THIS_FILE_DIR/config.sh
source $THIS_FILE_DIR/builds_suffixes.sh

if [ $# == 0 ]
then
	ALL=true
else

	while [[ $# > 0 ]]
	do
	key="$1"

	case $key in
		-d|--debug)
			DEBUG=true
			CMD="$BIN_PATH/$BIN_NAME$SUFFIX_DEBUG"
		;;
		-dopt|--debug-opt)
			DEBUG=true
			CMD="$BIN_PATH/$BIN_NAME$SUFFIX_DEBUG_OPT"
		;;
		-r|--release)
			RELEASE=true
			CMD="$BIN_PATH/$BIN_NAME$SUFFIX_RELEASE"
		;;
		-p|--gprof)
			PROFILE=true
			CMD="$BIN_PATH/$BIN_NAME$SUFFIX_PROFILE"
		;;
		-P|--perf)
			PERF=true
			CMD="$BIN_PATH/$BIN_NAME$SUFFIX_PERF"
	;;
		*)
			echo "Unknown option "$1
		;;
	esac
	shift
	done;
fi

$CMD
