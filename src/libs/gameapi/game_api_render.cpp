#include "game_api.h"
#include "basic_types.h"
#include "game_api_utility.h"
#include "tgprof.h"
#include "game_api_render.h"

Lpr_Rgba_u8
palette_pastel[] =
{
	PALETTE_PASTEL( PALETTE_RGBAU8 )
};
const char *
pastel_palette_names[] =
{
	PALETTE_PASTEL( PALETTE_STRING_TABLE )
};

#ifdef DEBUG_GUI_ENABLED
internal
Lpr_Rgba_u8
graph_color( u64 color_shift, const Prof_Section * section )
{
	Lpr_Rgba_u8 ret;

	const Lpr_Rgba_u8 * graph_colors = palette_pastel;
	u32 c_count = palette_pastel_count;

	u64 rc =
	( (u64)section->name ^ (u64)section->function ^
	  (u64)section->file ^ (u64)section->line ^
	  color_shift * 65537);
	{
		// NOTE(theGiallo): bit-folding
		u32 nbits = 5;
		rc = (
		   ( ( 1 << ( nbits + 1 ) ) - 1 ) &
		   (
		        rc ^
		      ( rc >>  nbits       ) ^
		      ( rc >> (nbits * 2 ) ) ^
		      ( rc >> (nbits * 3 ) ) ^
		      ( rc >> (nbits * 4 ) ) ^
		      ( rc >> (nbits * 5 ) ) ^
		      ( rc >> (nbits * 6 ) ) ^
		      ( rc >> (nbits * 7 ) ) ^
		      ( rc >> (nbits * 8 ) ) ^
		      ( rc >> (nbits * 9 ) ) ^
		      ( rc >> (nbits * 10) ) ^
		      ( rc >> (nbits * 11) ) ^
		      ( rc >> (nbits * 12) )
		   ) ) % c_count;
	}

	ret = graph_colors[rc];

	return ret;
}

internal
void
draw_prof_top( Game_Memory * game_mem, s32 window_id,
               Lpr_Draw_Data * draw_data, s32 batch_id,
               u32 frame_id, u32 frames_span )
{
	PROFILE_SECTION();

	Prof_Data * prof = &game_mem->prof;
	Prof_Top_Hash_Table * hash_table =
	   prof->top_hash_tables + frame_id;

	u32 sections_count = hash_table->nodes_mh.total_occupancy;
	Prof_Top_HT_Node nodes[sections_count];
	Prof_Top_HT_Node_MH_2_Iterator it = {};
	Prof_Top_HT_Node * node;
	for ( u32 i = 0; ; ++i )
	{
		node = hash_table->nodes_mh.iterate( &it );
		if ( !node )
		{
			lpr_assert( i == ARRAY_COUNT( nodes ) );
			break;
		}
		memcpy( nodes + i, node, sizeof( *node ) );
	}

	Prof_Drawing_Data * pdd = &game_mem->prof_drawing_data;

	f32 step_size = game_mem->inputs.keyboard_state[PK_LSHIFT] ||
	                game_mem->inputs.keyboard_state[PK_RSHIFT] ?
	                50.0f : 10.0f;
	s32 steps_count =
	( (s32)game_mem->inputs.keyboard_pressions_with_reps[PK_RIGHT] -
	  (s32)game_mem->inputs.keyboard_pressions_with_reps[PK_LEFT] );
	pdd->top.scrolling.x -= steps_count * step_size;
	steps_count =
	( (s32)game_mem->inputs.keyboard_pressions_with_reps[PK_UP] -
	  (s32)game_mem->inputs.keyboard_pressions_with_reps[PK_DOWN] );
	pdd->top.scrolling.y -= steps_count * step_size;

	u16 chain_sorted_ids[sections_count];
	for ( u16 i = 0; i < sections_count; ++i )
	{
		chain_sorted_ids[i] = i;
	}
	for ( s32 sort_id = PROF_TOP_SORTS_COUNT - 1; sort_id >= 0; --sort_id )
	{
		Prof_Top_Sort s = pdd->top.sort_whats[sort_id];
		switch ( s )
		{
			case PROF_TOP_SORT_RECURSIVE_AVG_DURATION:
			case PROF_TOP_SORT_RECURSIVE_MIN_DURATION:
			case PROF_TOP_SORT_RECURSIVE_MAX_DURATION:
			case PROF_TOP_SORT_RECURSIVE_SD_DURATION:
			case PROF_TOP_SORT_RECURSIVE_VAR_DURATION:
			case PROF_TOP_SORT_RECURSIVE_TOT_DURATION:
			case PROF_TOP_SORT_RECURSIVE_AVG_DURATION_PER_FRAME:
			case PROF_TOP_SORT_NON_RECURSIVE_AVG_DURATION:
			case PROF_TOP_SORT_NON_RECURSIVE_MIN_DURATION:
			case PROF_TOP_SORT_NON_RECURSIVE_MAX_DURATION:
			case PROF_TOP_SORT_NON_RECURSIVE_SD_DURATION:
			case PROF_TOP_SORT_NON_RECURSIVE_VAR_DURATION:
			case PROF_TOP_SORT_NON_RECURSIVE_TOT_DURATION:
			case PROF_TOP_SORT_NON_RECURSIVE_AVG_DURATION_PER_FRAME:
				// u64
			{
				u64 keys[sections_count];
				for ( u32 sec_idx = 0; sec_idx < sections_count; ++sec_idx )
				{
					switch ( s )
					{
						case PROF_TOP_SORT_RECURSIVE_AVG_DURATION:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.recursive.avg_duration;
							break;
						case PROF_TOP_SORT_RECURSIVE_MIN_DURATION:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.recursive.min_duration;
							break;
						case PROF_TOP_SORT_RECURSIVE_MAX_DURATION:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.recursive.max_duration;
							break;
						case PROF_TOP_SORT_RECURSIVE_SD_DURATION:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.recursive.sd_duration;
							break;
						case PROF_TOP_SORT_RECURSIVE_VAR_DURATION:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.recursive.var_duration;
							break;
						case PROF_TOP_SORT_RECURSIVE_TOT_DURATION:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.recursive.tot_duration;
							break;
						case PROF_TOP_SORT_RECURSIVE_AVG_DURATION_PER_FRAME:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.recursive.avg_duration_per_frame;
							break;
						case PROF_TOP_SORT_NON_RECURSIVE_AVG_DURATION:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.non_recursive.avg_duration;
							break;
						case PROF_TOP_SORT_NON_RECURSIVE_MIN_DURATION:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.non_recursive.min_duration;
							break;
						case PROF_TOP_SORT_NON_RECURSIVE_MAX_DURATION:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.non_recursive.max_duration;
							break;
						case PROF_TOP_SORT_NON_RECURSIVE_SD_DURATION:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.non_recursive.sd_duration;
							break;
						case PROF_TOP_SORT_NON_RECURSIVE_VAR_DURATION:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.non_recursive.var_duration;
							break;
						case PROF_TOP_SORT_NON_RECURSIVE_TOT_DURATION:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.non_recursive.tot_duration;
							break;
						case PROF_TOP_SORT_NON_RECURSIVE_AVG_DURATION_PER_FRAME:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.non_recursive.avg_duration_per_frame;
							break;
						default:
							LPR_ILLEGAL_PATH();
							break;
					}
				}
				u16 ids[ARRAY_COUNT( chain_sorted_ids )];
				u16 tmp[ARRAY_COUNT( ids )];
				for ( u32 i = 0; i < ARRAY_COUNT( ids ); ++i )
				{
					ids[i] = i;
				}
				radix_sort_mc_64_16( keys, ids, tmp, ARRAY_COUNT( keys ),
				                     pdd->top.sort_orders[sort_id] );
				lpr_reorder_16_s( ids, nodes, ARRAY_COUNT( nodes ), sizeof( nodes[0] ) );
			}
				break;
			case PROF_TOP_SORT_RECURSIVE_CALLS_COUNT_TOT:
			case PROF_TOP_SORT_RECURSIVE_CALLS_PER_FRAME_MIN:
			case PROF_TOP_SORT_RECURSIVE_CALLS_PER_FRAME_MAX:
			case PROF_TOP_SORT_NON_RECURSIVE_CALLS_COUNT_TOT:
			case PROF_TOP_SORT_NON_RECURSIVE_CALLS_PER_FRAME_MIN:
			case PROF_TOP_SORT_NON_RECURSIVE_CALLS_PER_FRAME_MAX:
			case PROF_TOP_SORT_LINE:
			case PROF_TOP_SORT_THREAD_ID:
				//u32
			{
				u32 keys[sections_count];
				for ( u32 sec_idx = 0; sec_idx < sections_count; ++sec_idx )
				{
					switch ( s )
					{
						case PROF_TOP_SORT_RECURSIVE_CALLS_COUNT_TOT:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.recursive.calls_count_tot;
							break;
						case PROF_TOP_SORT_RECURSIVE_CALLS_PER_FRAME_MIN:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.recursive.calls_per_frame_min;
							break;
						case PROF_TOP_SORT_RECURSIVE_CALLS_PER_FRAME_MAX:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.recursive.calls_per_frame_max;
							break;
						case PROF_TOP_SORT_NON_RECURSIVE_CALLS_COUNT_TOT:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.non_recursive.calls_count_tot;
							break;
						case PROF_TOP_SORT_NON_RECURSIVE_CALLS_PER_FRAME_MIN:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.non_recursive.calls_per_frame_min;
							break;
						case PROF_TOP_SORT_NON_RECURSIVE_CALLS_PER_FRAME_MAX:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.non_recursive.calls_per_frame_max;
							break;
						case PROF_TOP_SORT_LINE:
							keys[sec_idx] =
							   nodes[sec_idx].identifier.line;
							break;
						case PROF_TOP_SORT_THREAD_ID:
							keys[sec_idx] =
							   nodes[sec_idx].identifier.thread_id;
							break;
						default:
							LPR_ILLEGAL_PATH();
							break;
					}
				}
				u16 ids[ARRAY_COUNT( chain_sorted_ids )];
				u16 tmp[ARRAY_COUNT( ids )];
				for ( u32 i = 0; i < ARRAY_COUNT( ids ); ++i )
				{
					ids[i] = i;
				}
				radix_sort_mc_32_16( keys, ids, tmp, ARRAY_COUNT( keys ), pdd->top.sort_orders[sort_id] );
				lpr_reorder_16_s( ids, nodes, ARRAY_COUNT( nodes ), sizeof( nodes[0] ) );
			}
				break;
			case PROF_TOP_SORT_RECURSIVE_CALLS_PER_FRAME_AVG:
			case PROF_TOP_SORT_NON_RECURSIVE_CALLS_PER_FRAME_AVG:
				//f32
			{
				f32 keys[sections_count];
				for ( u32 sec_idx = 0; sec_idx < sections_count; ++sec_idx )
				{
					switch ( s )
					{
						case PROF_TOP_SORT_RECURSIVE_CALLS_PER_FRAME_AVG:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.recursive.calls_per_frame_avg;
							break;
						case PROF_TOP_SORT_NON_RECURSIVE_CALLS_PER_FRAME_AVG:
							keys[sec_idx] =
							   nodes[sec_idx].statistics.non_recursive.calls_per_frame_avg;
							break;
						default:
							LPR_ILLEGAL_PATH();
							break;
					}
				}

				u16 ids[ARRAY_COUNT( chain_sorted_ids )];
				for ( u32 i = 0; i < ARRAY_COUNT( ids ); ++i )
				{
					ids[i] = i;
				}
				radix_sort_f32( keys, ARRAY_COUNT( keys ), ids,
				                pdd->top.sort_orders[sort_id], false );
				lpr_reorder_16_s( ids, nodes, ARRAY_COUNT( nodes ), sizeof( nodes[0] ) );
			}
				break;
			case PROF_TOP_SORT_NAME:
			case PROF_TOP_SORT_FUNCTION:
			case PROF_TOP_SORT_FILE:
				//string
			{
				const char * keys[sections_count];
				for ( u32 sec_idx = 0; sec_idx < sections_count; ++sec_idx )
				{
					switch ( s )
					{
						case PROF_TOP_SORT_NAME:
							keys[sec_idx] =
							   nodes[sec_idx].identifier.name;
							break;
						case PROF_TOP_SORT_FUNCTION:
							keys[sec_idx] =
							   nodes[sec_idx].identifier.function;
							break;
						case PROF_TOP_SORT_FILE:
							keys[sec_idx] =
							   nodes[sec_idx].identifier.file;
							break;
						default:
							LPR_ILLEGAL_PATH();
							break;
					}
				}
				u16 ids[ARRAY_COUNT( chain_sorted_ids )];
				for ( u32 i = 0; i < ARRAY_COUNT( ids ); ++i )
				{
					ids[i] = i;
				}
				sort_lexicographically_ids_only_16( keys, ARRAY_COUNT( keys ), pdd->top.sort_orders[sort_id], ids );
				lpr_reorder_16_s( ids, nodes, ARRAY_COUNT( nodes ), sizeof( nodes[0] ) );
			}
				break;
			default:
				LPR_ILLEGAL_PATH();
				break;
		}
	}

	char str_buf[4*1024];

	Lpr_Multi_Font * multi_font = &game_mem->fonts.multi_font;
	u32 dbg_font_id      = game_mem->fonts.debug_font->font_id;
	u32 dbg_font_size_id = game_mem->fonts.debug_font->size_id;

	lpr_s32 glyph_id;
	Lpr_AA_Rect aa_bb;
	Lpr_V2 ending_pt;

	V2u32 window_size = {draw_data->window_size.w, draw_data->window_size.h};
	Lpr_AA_Rect rect;
	Lpr_Rgba_u8 text_color = lpr_C_colors_u8.white;
	u16 z = 10;
	f32 scale = 1.0f;

	V2 mouse_pos = game_mem->inputs.mouse_pos;
	Lpr_AA_Rect sort_orders_rect;
	f32 line_height;
	f32 border = 4.0f;
	f32 double_border = border * 2.0f;
	{
		u32 first_free = 0;
		first_free +=
		snprintf( str_buf + first_free,
		          sizeof( str_buf ) - first_free,
		          "Sorting order\n"
		          "------------------------------------%s",
		          pdd->top.sort_orders_contracted ? "\n" : "" );

		if ( pdd->top.sort_orders_contracted )
		{
			for ( u32 sort_id = 0; sort_id < PROF_TOP_SORTS_COUNT; ++sort_id )
			{
				first_free +=
				snprintf( str_buf + first_free,
				          sizeof( str_buf ) - first_free,
				          "%s %s%s",
				          game_mem->prof_drawing_data.top.sort_orders[sort_id]==SORT_ASCENDANTLY?"△":"▽",
				          prof_top_sorts_name_table[game_mem->prof_drawing_data.top.sort_whats[sort_id]],
				          sort_id == PROF_TOP_SORTS_COUNT - 1 ? "" : "\n" );
			}
		}

		rect.size = lpr_V2_from_V2u32( window_size );
		rect.pos = lpr_V2_mult_f32( rect.size, 0.5f );
		rect.pos.x += DEBUG_GUI_START_X_PX;
		rect.pos.y += -200;

		lpr_add_multiline_text_in_rect( draw_data, batch_id, multi_font,
		                                dbg_font_id, dbg_font_size_id,
		                                rect, str_buf, first_free, text_color,
		                                true, z, scale, false, false,
		                                LPR_STENCIL_NONE,
		                                &glyph_id, &aa_bb, &ending_pt );
		Lpr_V2 size = aa_bb.size;
		size.w += double_border;
		size.h += double_border;
		Lpr_Rgba_u8 bg_color = palette_pastel[palette_pastel_maya_blue];
		bg_color.r >>= 1;
		bg_color.g >>= 1;
		bg_color.b >>= 1;
		bg_color.a = 200;
		u32 id =
		lpr_add_colored_rect( draw_data, batch_id, aa_bb.pos, size,
		                      bg_color, 0.0f );
		lpr_set_instance_z( draw_data, batch_id, id, z - 2  );
		sort_orders_rect.pos = aa_bb.pos;
		sort_orders_rect.size = size;

		u32 header_lines = 2;
		f32 lines_count = header_lines;
		if ( pdd->top.sort_orders_contracted )
		{
			lines_count += PROF_TOP_SORTS_COUNT;
		}
		line_height = aa_bb.size.h / lines_count;
		f32 hard_coded_hacky_line_center_delta_perc = 0.2f;
		f32 hard_coded_hacky_line_center_delta_px = line_height * hard_coded_hacky_line_center_delta_perc;
		V2 min, max;

		min.x = aa_bb.pos.x - size.w * 0.5f;
		max.x = min.x + size.w;
		max.y = aa_bb.pos.y + size.h * 0.5f;
		min.y = max.y - line_height * (f32)header_lines;
		if ( point_in_aabb( mouse_pos, min, max ) &&
		     game_mem->inputs.mouse_clicked_left )
		{
			pdd->top.sort_orders_contracted = !pdd->top.sort_orders_contracted;
		}

		if ( pdd->top.sort_orders_contracted )
		{
			min.x = aa_bb.pos.x - size.w * 0.5f;
			max.x = min.x + size.w;
			max.y = aa_bb.pos.y + size.h * 0.5f - header_lines * line_height -
			        hard_coded_hacky_line_center_delta_px;
			min.y = max.y - line_height * (f32)( PROF_TOP_SORTS_COUNT + 1 );
			if ( point_in_aabb( mouse_pos, min, max ) )
			{
				s32 hover_id = ( -mouse_pos.y + max.y ) / line_height;
				bool real_hover = hover_id != PROF_TOP_SORTS_COUNT;
				V2 hover_area_center =
				   { ( min.x + max.x ) * 0.5f,
				     max.y - ( hover_id + 0.5f ) * line_height };
				if ( real_hover && game_mem->inputs.mouse_clicked_right )
				{
					Sort_Order * so = game_mem->prof_drawing_data.top.sort_orders + hover_id;
					if ( *so == SORT_ASCENDANTLY )
					{
						*so = SORT_DESCENDANTLY;
					} else
					{
						*so = SORT_ASCENDANTLY;
					}
				}
				if ( real_hover && game_mem->inputs.mouse_button_left_pressed )
				{
					game_mem->prof_drawing_data.top.dragging_id = hover_id;
					game_mem->prof_drawing_data.top.is_dragging = true;
					game_mem->prof_drawing_data.top.dragging_mouse_pos_rel_to_center =
					   mouse_pos - hover_area_center;
				} else
				if ( game_mem->prof_drawing_data.top.is_dragging )
				{
					s32 old_id = game_mem->prof_drawing_data.top.dragging_id;
					if ( !game_mem->inputs.mouse_button_left_down )
					{
						if ( hover_id != old_id && ( hover_id - 1 ) != old_id )
						{
							Sort_Order    old_sort_order = game_mem->prof_drawing_data.top.sort_orders[old_id];
							Prof_Top_Sort old_sort_what  = game_mem->prof_drawing_data.top.sort_whats [old_id];
							s32 target_id;
							if ( hover_id < old_id )
							{
								target_id = hover_id;
								for ( s32 i = old_id; i > target_id; --i )
								{
									game_mem->prof_drawing_data.top.sort_orders[i] =
									game_mem->prof_drawing_data.top.sort_orders[i-1];
									game_mem->prof_drawing_data.top.sort_whats[i] =
									game_mem->prof_drawing_data.top.sort_whats[i-1];
								}
							} else
							{
								target_id = hover_id - 1;
								for ( s32 i = old_id; i < target_id; ++i )
								{
									game_mem->prof_drawing_data.top.sort_orders[i] =
									game_mem->prof_drawing_data.top.sort_orders[i+1];
									game_mem->prof_drawing_data.top.sort_whats[i] =
									game_mem->prof_drawing_data.top.sort_whats[i+1];
								}
							}
							game_mem->prof_drawing_data.top.sort_orders[target_id] = old_sort_order;
							game_mem->prof_drawing_data.top.sort_whats [target_id] = old_sort_what;
						}

						game_mem->prof_drawing_data.top.dragging_id = -1;
						game_mem->prof_drawing_data.top.is_dragging = false;
					}
				}

				if ( game_mem->prof_drawing_data.top.is_dragging )
				{
					Lpr_Rgba_u8 new_place_bg_color = palette_pastel[palette_pastel_bitter_sweet];
					rect.pos = hover_area_center;
					rect.pos.y += line_height * 0.5f;
					rect.size.w = size.w;
					rect.size.h = 2;
					lpr_add_colored_rect( draw_data, batch_id,
					                      rect.pos, rect.size,
					                      new_place_bg_color, 0.0f );
				} else
				if ( real_hover )
				{
					rect.pos.x = hover_area_center.x;
					rect.pos.y = hover_area_center.y;
					rect.size.w = aa_bb.size.w;
					rect.size.h = line_height;
					rect.size = rect.size;
					rect.size.x += double_border;
					rect.pos.y -= hard_coded_hacky_line_center_delta_px - 2;
					Lpr_Rgba_u8 hover_bg_color = palette_pastel[palette_pastel_bitter_sweet];
					hover_bg_color.a = 200;
					u32 id =
					lpr_add_colored_rect( draw_data, batch_id, rect.pos,
					                      rect.size, hover_bg_color, 0.0f );
					lpr_set_instance_z( draw_data, batch_id, id, z - 1 );
				}
			}

			if ( game_mem->prof_drawing_data.top.is_dragging )
			{
				s32 dragging_id = game_mem->prof_drawing_data.top.dragging_id;

				lpr_s32     drag_glyph_id;
				Lpr_AA_Rect drag_aa_bb;
				Lpr_V2      drag_ending_pt;

				rect.pos = mouse_pos - game_mem->prof_drawing_data.top.dragging_mouse_pos_rel_to_center;
				rect.size.w = aa_bb.size.w;
				rect.size.h = line_height;

				first_free = 0;
				first_free +=
				snprintf( str_buf + first_free,
				          sizeof( str_buf ) - first_free,
				          "%s %s",
				          game_mem->prof_drawing_data.top.sort_orders[dragging_id]==SORT_ASCENDANTLY?"△":"▽",
				          prof_top_sorts_name_table[game_mem->prof_drawing_data.top.sort_whats[dragging_id]] );

				lpr_add_multiline_text_in_rect(
				   draw_data, batch_id, multi_font,
				   dbg_font_id, dbg_font_size_id,
				   rect,
				   str_buf,
				   first_free, text_color,
				   true, z + 2, scale, false, false,
				   LPR_STENCIL_NONE,
				   &drag_glyph_id, &drag_aa_bb, &drag_ending_pt );
				Lpr_V2 drag_size = rect.size;
				drag_size.x += double_border;
				drag_size.y += double_border;
				u32 id =
				lpr_add_colored_rect( draw_data, batch_id, rect.pos,
				                      drag_size, bg_color, 0.0f );
				lpr_set_instance_z( draw_data, batch_id, id, z + 1 );
			}
		}
	}

	rect.pos.y = aa_bb.pos.y - aa_bb.size.h * 0.5f - 10 + pdd->top.scrolling.y;
	rect.pos.x = DEBUG_GUI_START_X_PX + pdd->top.scrolling.x;
	rect.size = lpr_V2_from_V2u32( window_size );
	rect.size.h -= rect.pos.y;
	rect.size.w -= rect.pos.x;
	rect.pos.x += rect.size.w * 0.5f;
	rect.pos.y -= rect.size.h * 0.5f;

	const u32 columns_border = border;

#define DRAW_PROF_TOP_CULUMN( fmt, property, prof_top_column ) \
if ( sections_count )                                                     \
{                                                                         \
	u32 first_free = 0;                                                   \
	for ( u32 i = 0;                                                      \
	      i < sections_count && first_free < sizeof( str_buf );           \
	      ++i )                                                           \
	{                                                                     \
		first_free +=                                                     \
		snprintf( str_buf + first_free,                                   \
		          sizeof( str_buf ) - first_free,                         \
		          fmt "\n", nodes[i].property );                          \
	}                                                                     \
	bool b_res =                                                          \
	lpr_add_multiline_text_in_rect( draw_data, batch_id, multi_font,      \
	                                dbg_font_id, dbg_font_size_id,        \
	                                rect, str_buf, first_free, text_color,\
	                                true, z, scale, false, false,         \
	                                LPR_STENCIL_NONE,                     \
	                                &glyph_id, &aa_bb, &ending_pt );      \
	if ( !b_res )                                                         \
	{                                                                     \
		lpr_log_err( "failed to add text!" );                             \
	}                                                                     \
	Lpr_Rgba_u8 bg_color =                                                \
	( col_num++ % 2 ? palette_pastel[palette_pastel_regent_st_blue] :     \
	                  palette_pastel[palette_pastel_maya_blue] );         \
	bg_color.a = 64;                                                      \
	bg_color.r = bg_color.r>>1;                                           \
	bg_color.g = bg_color.g>>1;                                           \
	bg_color.b = bg_color.b>>1;                                           \
	Lpr_V2 size = aa_bb.size;                                             \
	size.w += columns_border;                                             \
	u32 id =                                                              \
	lpr_add_colored_rect( draw_data, batch_id, aa_bb.pos, size,           \
	                      bg_color, 0.0f );                               \
	lpr_set_instance_z( draw_data, batch_id, id, z - 1 );                 \
                                                                          \
	V2 min, max;                                                          \
	min = make_V2( aa_bb.pos ) - make_V2( size ) * 0.5f;                  \
	max = min + make_V2(size);                                            \
	if ( point_in_aabb( mouse_pos, min, max ) )                           \
	{                                                                     \
		first_free = 0;                                                   \
		first_free +=                                                     \
		   snprintf( str_buf + first_free,                                \
		             sizeof( str_buf ) - first_free,                      \
		             "%s", prof_top_sorts_name_table[prof_top_column] );  \
		Lpr_AA_Rect tip_rect = sort_orders_rect;                          \
		tip_rect.pos.x += sort_orders_rect.size.w + double_border;        \
		tip_rect.pos.y -= sort_orders_rect.size.h;                        \
		tip_rect.pos.y += line_height + border;                           \
		Lpr_AA_Rect tip_aa_bb;                                            \
		lpr_add_multiline_text_in_rect(                                   \
		   draw_data, batch_id, multi_font,                               \
		   dbg_font_id, dbg_font_size_id,                                 \
		   tip_rect, str_buf, first_free, text_color,                     \
		   true, z, scale, false, false,                                  \
		   LPR_STENCIL_NONE,                                              \
		   &glyph_id, &tip_aa_bb, &ending_pt );                           \
		Lpr_V2 bg_size = tip_aa_bb.size;                                  \
		bg_size.w += double_border;                                       \
		bg_size.h += double_border;                                       \
		Lpr_Rgba_u8 bg_color = palette_pastel[palette_pastel_maya_blue];  \
		bg_color.r >>= 1;                                                 \
		bg_color.g >>= 1;                                                 \
		bg_color.b >>= 1;                                                 \
		bg_color.a = 200;                                                 \
		u32 id =                                                          \
		lpr_add_colored_rect( draw_data, batch_id, tip_aa_bb.pos, bg_size,\
		                      bg_color, 0.0f );                           \
		lpr_set_instance_z( draw_data, batch_id, id, z - 2  );            \
	}                                                                     \
	f32 new_left = aa_bb.pos.x + aa_bb.size.w * 0.5f + columns_border;    \
	rect.size.w = window_size.w - new_left;                               \
	rect.pos.x  = new_left + rect.size.w * 0.5f;                          \
}                                                                         \

	float clock_period_ns = game_mem->prof.clock_period_ns;
	float mult = clock_period_ns;
	#define dur_fmt "%20.0f ns"
	u32 col_num = 0;
	DRAW_PROF_TOP_CULUMN( "%20s",  identifier.name                                                  , PROF_TOP_SORT_NAME                              );
	DRAW_PROF_TOP_CULUMN( "%70s",  identifier.function                                              , PROF_TOP_SORT_FUNCTION                          );
	DRAW_PROF_TOP_CULUMN( "%30s",  identifier.file                                                  , PROF_TOP_SORT_FILE                              );
	DRAW_PROF_TOP_CULUMN( "%5u",   identifier.line                                                  , PROF_TOP_SORT_LINE                              );
	DRAW_PROF_TOP_CULUMN( "%u",    identifier.thread_id                                             , PROF_TOP_SORT_THREAD_ID                         );
	DRAW_PROF_TOP_CULUMN( dur_fmt, statistics.non_recursive.tot_duration * mult         , PROF_TOP_SORT_NON_RECURSIVE_TOT_DURATION        );
	DRAW_PROF_TOP_CULUMN( "%3u",   statistics.non_recursive.calls_count_tot                         , PROF_TOP_SORT_NON_RECURSIVE_CALLS_COUNT_TOT     );
	DRAW_PROF_TOP_CULUMN( dur_fmt, statistics.non_recursive.avg_duration * mult         , PROF_TOP_SORT_NON_RECURSIVE_AVG_DURATION        );
	DRAW_PROF_TOP_CULUMN( dur_fmt, statistics.non_recursive.min_duration * mult         , PROF_TOP_SORT_NON_RECURSIVE_MIN_DURATION        );
	DRAW_PROF_TOP_CULUMN( dur_fmt, statistics.non_recursive.max_duration * mult         , PROF_TOP_SORT_NON_RECURSIVE_MAX_DURATION        );
	DRAW_PROF_TOP_CULUMN( dur_fmt, statistics.non_recursive.sd_duration  * mult         , PROF_TOP_SORT_NON_RECURSIVE_SD_DURATION         );
	DRAW_PROF_TOP_CULUMN( dur_fmt, statistics.non_recursive.var_duration * mult         , PROF_TOP_SORT_NON_RECURSIVE_VAR_DURATION        );
	DRAW_PROF_TOP_CULUMN( dur_fmt, statistics.non_recursive.avg_duration_per_frame * mult, PROF_TOP_SORT_NON_RECURSIVE_AVG_DURATION        );
	DRAW_PROF_TOP_CULUMN( "%3.1f", statistics.non_recursive.calls_per_frame_avg                     , PROF_TOP_SORT_NON_RECURSIVE_CALLS_PER_FRAME_AVG );
	DRAW_PROF_TOP_CULUMN( "%3u",   statistics.non_recursive.calls_per_frame_min                     , PROF_TOP_SORT_NON_RECURSIVE_CALLS_PER_FRAME_MIN );
	DRAW_PROF_TOP_CULUMN( "%3u",   statistics.non_recursive.calls_per_frame_max                     , PROF_TOP_SORT_NON_RECURSIVE_CALLS_PER_FRAME_MIN );
	DRAW_PROF_TOP_CULUMN( dur_fmt, statistics.recursive.tot_duration * mult             , PROF_TOP_SORT_RECURSIVE_TOT_DURATION            );
	DRAW_PROF_TOP_CULUMN( "%3u",   statistics.recursive.calls_count_tot                             , PROF_TOP_SORT_RECURSIVE_CALLS_COUNT_TOT         );
	DRAW_PROF_TOP_CULUMN( dur_fmt, statistics.recursive.avg_duration * mult             , PROF_TOP_SORT_RECURSIVE_AVG_DURATION            );
	DRAW_PROF_TOP_CULUMN( dur_fmt, statistics.recursive.min_duration * mult             , PROF_TOP_SORT_RECURSIVE_MIN_DURATION            );
	DRAW_PROF_TOP_CULUMN( dur_fmt, statistics.recursive.max_duration * mult             , PROF_TOP_SORT_RECURSIVE_MAX_DURATION            );
	DRAW_PROF_TOP_CULUMN( dur_fmt, statistics.recursive.sd_duration  * mult             , PROF_TOP_SORT_RECURSIVE_SD_DURATION             );
	DRAW_PROF_TOP_CULUMN( dur_fmt, statistics.recursive.var_duration * mult             , PROF_TOP_SORT_RECURSIVE_VAR_DURATION            );
	DRAW_PROF_TOP_CULUMN( dur_fmt, statistics.recursive.avg_duration_per_frame * mult   , PROF_TOP_SORT_RECURSIVE_AVG_DURATION_PER_FRAME  );
	DRAW_PROF_TOP_CULUMN( "%3.1f", statistics.recursive.calls_per_frame_avg                         , PROF_TOP_SORT_RECURSIVE_CALLS_PER_FRAME_AVG     );
	DRAW_PROF_TOP_CULUMN( "%3u",   statistics.recursive.calls_per_frame_min                         , PROF_TOP_SORT_RECURSIVE_CALLS_PER_FRAME_MIN     );
	DRAW_PROF_TOP_CULUMN( "%3u",   statistics.recursive.calls_per_frame_max                         , PROF_TOP_SORT_RECURSIVE_CALLS_PER_FRAME_MAX     );
#undef dur_fmt
}

internal
void
draw_prof_section_time_flow( Game_Memory * game_mem, s32 window_id,
                             Lpr_Draw_Data * draw_data, s32 batch_id,
                             Prof_Section * section,
                             u32 depth, u32 max_depth,
                             f32 ticks_to_px,
                             u64 delta_ticks,
                             u64 zero_ticks,
                             u32 scrolling,
                             V2 origin_px,
                             f32 scale = 1.0f )
{
	Lpr_Rgba_u8
	color = graph_color(
	           game_mem->prof_drawing_data.color_shift,
	           section );
	u64 start = section->start_time;
	u64 end   = section->end_time;

	f32 thread_height_px = 128;
	f32 thread_padding = 2.0f;
	V2 size;
	V2 pos;

	size.w = ( end - start ) * ticks_to_px;
	size.w -= 2;
	size.h = ( thread_height_px - thread_padding * 2.0f ) / ( max_depth + 1 );

	pos.x =
	   scrolling +
	   origin_px.x - PROF_GRAPH_TIME_FLOW_RIGHT_BORDER_PX + draw_data->window_size.w - delta_ticks * ticks_to_px +
	   size.w * 0.5f + ( start - zero_ticks ) * ticks_to_px;
	pos.y = origin_px.y + thread_height_px - thread_padding - size.h * ( depth + 0.5f );
	lpr_add_colored_rect( draw_data, 1,
	                      pos, size, color, 0.0f );


	if ( depth != max_depth )
	{
		for ( Prof_Section_Child_List_Node * cn =
		         section->childs_list;
		      cn;
		      cn = cn->next )
		{
			if ( cn->section )
			{
				draw_prof_section_time_flow( game_mem, window_id, draw_data,
				                             batch_id,
				                             cn->section,
				                             depth+1, max_depth,
				                             ticks_to_px,
				                             delta_ticks,
				                             zero_ticks,
				                             scrolling,
				                             origin_px,
				                             scale );
			}
		}
	}
}

internal
f32
draw_prof_section( Game_Memory * game_mem, s32 window_id,
                   Lpr_Draw_Data * draw_data,
                   Prof_Section * section, V2 start,
                   u32 depth, u32 max_depth, f32 target_height,
                   bool * mouse_on,
                   f32 scale = 1.0f )
{
	if ( !depth )
	{
		*mouse_on = false;
	}

	lpr_assert( section->start_time < section->end_time ||
	            !section->end_time );
	u64 duration = 0;
	if ( section->end_time )
	{
		duration = section->end_time - section->start_time;
	}
	f32 clock_period_ns = game_mem->prof.clock_period_ns;
	f32 thread_height_px = target_height * scale;
	f32 time_per_px = ( 1.0f / ( clock_period_ns * 1e-9f * 120.0f ) ) / thread_height_px;
	V2 end   = {start.x,start.y + duration / time_per_px};
	// NOTE(theGiallo): pastel palette
	u64 color_shift = game_mem->prof_drawing_data.color_shift;
	Lpr_Rgba_u8 color = graph_color( color_shift, section );

	// if ( !section->childs_list || depth == max_depth )
	lpr_add_debug_line( draw_data, start, end, color, color );
	bool mouse_on_me = false;
	V2 mouse_pos = game_mem->inputs.mouse_pos;
	if ( game_mem->inputs.mouse_window_id == window_id  &&
	     mouse_pos.x == start.x &&
	     mouse_pos.y >= start.y &&
	     mouse_pos.y <= end.y )
	{
		mouse_on_me = true;
	}

	if ( depth != max_depth )
	{
		for ( Prof_Section_Child_List_Node * cn =
		         section->childs_list;
		      cn;
		      cn = cn->next )
		{
			if ( cn->section )
			{
				V2 child_start = start;
				u64 c_start_delta = cn->section->start_time - section->start_time;
				child_start.y += c_start_delta / time_per_px;
				draw_prof_section( game_mem, window_id, draw_data,
				                   cn->section, child_start,
				                   depth+1, max_depth, target_height,
				                   mouse_on, scale );
			}
		}
	}
	if ( mouse_on_me && !*mouse_on )
	{
		*mouse_on = true;
		char str[1024];
		u32 str_size = sizeof( str );
		snprintf( str, str_size, "name: %s\n"
		                         "function: %s\n"
		                         "file: %s\n"
		                         "line %u\n"
		                         "thread id: %u\n"
		                         "duration: %lu cycles (~ %.2e ns)\n"
		                         "start time: %lu\n"
		                         "end   time: %lu\n"
		                         "depth: %u\n"
		                         "section: %p",
		                         section->name,
		                         section->function,
		                         section->file,
		                         section->line,
		                         section->thread_id,
		                         duration,
		                         duration * game_mem->prof.clock_period_ns,
		                         section->start_time,
		                         section->end_time,
		                         depth,
		                         section );
		Lpr_Rot_Rect rect = {};
		rect.size = {draw_data->window_size.w - mouse_pos.x, 500};
		rect.pos = mouse_pos;
		rect.pos.x += rect.size.w / 2 + 4;
		rect.pos.y += rect.size.h / 2 + 4;
		lpr_s32 glyph_id;
		Lpr_Rot_Rect bounding_box;
		Lpr_V2 ending_pt;
		bool b_res =
		lpr_add_multiline_text_in_rot_rect_aligned_wrap(
		   draw_data, 1,
		   &game_mem->fonts.multi_font,
		   game_mem->fonts.debug_font->font_id,
		   game_mem->fonts.debug_font->size_id,
		   rect, str, str_size, LPR_TXT_HALIGN_LEFT, LPR_TXT_VALIGN_BOTTOM,
		   LPR_TXT_WRAP_NONE, lpr_C_colors_u8.black, true, 20, 1.0f, false,
		   true, true, LPR_STENCIL_NONE, &glyph_id, &bounding_box, &ending_pt );
		if ( !b_res )
		{
			lpr_log_err( "lpr_add_multiline_text_in_rot_rect_aligned_wrap failed" );
		}

		bounding_box.size.x += 4;
		bounding_box.size.y += 4;
		Lpr_Rgba_u8 c = lpr_C_colors_u8.white;
		c.a = 127;
		s32 id =
		lpr_add_colored_rect( draw_data, 1, bounding_box.pos, bounding_box.size, c, 0.0f );
		lpr_set_instance_z( draw_data, 1, id, 19 );
	}
	return end.y;
}

internal
Lpr_AA_Rect
draw_dbg_button( Game_Memory * game_mem,
                 const char * str, u32 str_bytes_size,
                 V2 min, V2 max, u16 z,
                 bool selected = false,
                 bool * clicked_left  = NULL,
                 bool * clicked_right = NULL,
                 u32 margin = 0 )
{
	Lpr_Draw_Data * draw_data = &game_mem->DEBUG_window.lpr_draw_data;

	Lpr_Multi_Font * multi_font = &game_mem->fonts.multi_font;
	u32 dbg_font_id      = game_mem->fonts.debug_font->font_id;
	u32 dbg_font_size_id = game_mem->fonts.debug_font->size_id;

	lpr_s32 glyph_id;
	Lpr_AA_Rect aa_bb;
	Lpr_V2 ending_pt;

	Lpr_AA_Rect rect;
	rect.size = max - min;
	rect.pos = ( max + min ) * 0.5f;

	bool b_res =
	lpr_add_multiline_text_in_rect_aligned(
	   draw_data, 1,
	   multi_font, dbg_font_id, dbg_font_size_id,
	   rect, str, str_bytes_size, LPR_TXT_HALIGN_CENTERED,
	   LPR_TXT_VALIGN_MIDDLE, lpr_C_colors_u8.white, true, z + 1, 1.0f,
	   false, false, LPR_STENCIL_NONE, &glyph_id, &aa_bb, &ending_pt );
	if ( !b_res )
	{
		lpr_log_err( "lpr_add_multiline_text_in_rect_aligned failed" );
	}

	Lpr_Rgba_u8 c = selected ? palette_pastel[palette_pastel_medium_slate_blue] :
	                           lpr_C_colors_u8.gray_25;
	if ( ( clicked_left || clicked_right ) &&
	     game_mem->inputs.mouse_window_id == game_mem->DEBUG_window.window_id )
	{
		V2 mouse_pos = game_mem->inputs.mouse_pos;
		if ( mouse_pos.x >= min.x && mouse_pos.x <= max.x &&
		     mouse_pos.y >= min.y && mouse_pos.y <= max.y )
		{
			if ( clicked_left && game_mem->inputs.mouse_clicked_left )
			{
				*clicked_left = true;
			}
			if ( clicked_right && game_mem->inputs.mouse_clicked_right )
			{
				*clicked_right = true;
			}
			for ( u32 i = 0; i != 3; ++i )
			{
				c.rgb[i] = c.rgb[i] << 1;
			}
		}
	}
	u32 id = lpr_add_colored_rect( draw_data, 1, rect.pos, rect.size, c );
	lpr_set_instance_z( draw_data, 1, id, z );

	rect.size.x += margin;
	rect.size.y += margin;

	return rect;
}

internal
Lpr_AA_Rect
draw_dbg_button( Game_Memory * game_mem,
                 const char * str, u32 str_bytes_size,
                 V2 txt_start, u16 z,
                 bool selected = false,
                 bool * clicked_left  = NULL,
                 bool * clicked_right = NULL,
                 u32 padding = 0, u32 margin = 0 )
{
	Lpr_Draw_Data * draw_data = &game_mem->DEBUG_window.lpr_draw_data;

	Lpr_Multi_Font * multi_font = &game_mem->fonts.multi_font;
	u32 dbg_font_id      = game_mem->fonts.debug_font->font_id;
	u32 dbg_font_size_id = game_mem->fonts.debug_font->size_id;

	lpr_s32 glyph_id;
	Lpr_AA_Rect aa_bb;
	Lpr_V2 ending_pt;

	bool b_res =
	lpr_add_multiline_text(
	   draw_data, 1,
	   multi_font, dbg_font_id, dbg_font_size_id,
	   txt_start, str, str_bytes_size,
	   lpr_C_colors_u8.white, true, z + 1, 1.0f,
	   false, false, LPR_STENCIL_NONE, &glyph_id, &aa_bb, &ending_pt );
	if ( !b_res )
	{
		lpr_log_err( "lpr_add_multiline_text failed" );
	}

	aa_bb.size.x += padding;
	aa_bb.size.y += padding;
	Lpr_Rgba_u8 c = selected ? palette_pastel[palette_pastel_medium_slate_blue] :
	                           lpr_C_colors_u8.gray_25;

	if ( ( clicked_left || clicked_right ) &&
	     game_mem->inputs.mouse_window_id == game_mem->DEBUG_window.window_id )
	{
		V2 min, max;
		min = V2{aa_bb.pos.x, aa_bb.pos.y} - V2{aa_bb.size.w, aa_bb.size.h} * 0.5f;
		max = V2{aa_bb.pos.x, aa_bb.pos.y} + V2{aa_bb.size.w, aa_bb.size.h} * 0.5f;
		V2 mouse_pos = game_mem->inputs.mouse_pos;
		if ( point_in_aabb( mouse_pos, min, max ) )
		{
			if ( clicked_left && game_mem->inputs.mouse_clicked_left )
			{
				*clicked_left = true;
			}
			if ( clicked_right && game_mem->inputs.mouse_clicked_right )
			{
				*clicked_right = true;
			}
			for ( u32 i = 0; i != 3; ++i )
			{
				c.rgb[i] = c.rgb[i] << 1;
			}
		}
	}
	u32 id = lpr_add_colored_rect( draw_data, 1, aa_bb.pos, aa_bb.size, c );
	lpr_set_instance_z( draw_data, 1, id, z );
	aa_bb.size.x += margin;
	aa_bb.size.y += margin;

	return aa_bb;
}

internal
V2
next_dbg_button_min_in_row( Lpr_AA_Rect bb, f32 margin )
{
	V2 ret;

	V2 bb_pos = V2{bb.pos.x, bb.pos.y};
	V2 bb_size = V2{bb.size.w, bb.size.h};
	ret = bb_pos + V2{bb_size.w, margin-bb_size.h} * 0.5f;

	return ret;
}
#endif // NOTE(theGiallo): DEBUG_GUI_ENABLED
