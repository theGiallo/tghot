#ifndef TG_PROF_DS_H
#define TG_PROF_DS_H 1

#include "basic_types.h"
#include "intrinsics.h"
#include "game_api.h"

struct
Prof_Time_Value
{
	u64 time;
	const char * name;
	const char * function;
	const char * file;
	u32          line;
	u8           thread_id;
	u8 tag_open_close;
	u8 padding[2];
};

enum
Tag_Open_Close : u8
{
	TAG_OPEN,
	TAG_CLOSE,
};

#define MAX_PROF_TIME_VALUES (1024L * 1024L)
PRAGMA_ALIGN_PRE(64)
struct
Thread_Times_Buff
{
	volatile u64 first;
	volatile u64 last;
	Prof_Time_Value time_values[MAX_PROF_TIME_VALUES];
} PRAGMA_ALIGN_POST(64);

struct
Prof_Section_Child_List_Node;
struct
Prof_Section
{
	u64 start_time,
	    end_time;
	const char * name;
	const char * function;
	const char * file;
	u32 line;
	u32 thread_id;

	Prof_Section_Child_List_Node * childs_list,
	                             * childs_list_last_node;
	Prof_Section * parent;
};
struct
Prof_Section_Child_List_Node
{
	Prof_Section_Child_List_Node * next, * prev;
	Prof_Section * section;
};

struct
Prof_Section_Thread_Frame
{
	Prof_Section_Child_List_Node * next, * last;
	Prof_Section * section;
};

#if GAME_API_CPP
#define MH_EMIT_BODY 1
#endif

#define MH_STATIC 1

#define MH_TYPE Prof_Section
#define MH_FLOORS_COUNT 4096
#define MH_ILLEGAL_PATH LPR_ILLEGAL_PATH
#include "mem_hotel.inc.h"

#define MH_TYPE Prof_Section_Child_List_Node
#define MH_FLOORS_COUNT 4096
#include "mem_hotel.inc.h"

struct
Prof_Section_Identifier
{
	const char * name;
	const char * function;
	const char * file;
	u32 line;
	u32 thread_id;
};
struct
Prof_Section_Statistics_Data
{
	u64 avg_duration;
	u64 min_duration;
	u64 max_duration;
	u64  sd_duration; // NOTE(theGiallo): standard deviation
	u64 var_duration; // NOTE(theGiallo): variance
	u64 tot_duration;
	u64 avg_duration_per_frame;
	u32 calls_count_tot;
	f32 calls_per_frame_avg;
	u32 calls_per_frame_min;
	u32 calls_per_frame_max;
};
struct
Prof_Section_Statistics
{
	Prof_Section_Statistics_Data non_recursive;
	Prof_Section_Statistics_Data recursive;
	struct
	{
		bool now_inside;
	} compute_data;
};
struct
Prof_Top_HT_Node
{
	Prof_Section_Identifier identifier;
	Prof_Section_Statistics statistics;

	Prof_Top_HT_Node * next;
};
#define MH_STATIC 1
#define MH_FLOORS_COUNT 2
#define MH_TYPE Prof_Top_HT_Node
#include "mem_hotel.inc.h"
struct
Prof_Top_Hash_Table
{
	Prof_Top_HT_Node      * hash_table[1024];
	Prof_Top_HT_Node_MH_2   nodes_mh;
};

#if GAME_API_CPP
#undef MH_EMIT_BODY
#endif

#define MAX_PROF_FRAMES (120 * 10)
struct
Prof_Frames
{
	u32 last_frame;
	Prof_Section_Thread_Frame
	   frames[GAME_API_MAX_THREADS_NUM][MAX_PROF_FRAMES];

	Prof_Section_MH_4096                 sections_mh;
	Prof_Section_Child_List_Node_MH_4096 sections_childs_mh;
	bool bootstrapped;
};

struct
Prof_Data
{
	Thread_Times_Buff threads_time_buffs[GAME_API_MAX_THREADS_NUM];
	Prof_Frames frames_data;
	PRAGMA_ALIGN_PRE(64) f64 clock_period_ns PRAGMA_ALIGN_POST(64);
	u64 frame_section_tsc_start;
	u64 frame_section_ns_start;
	bool is_paused;

	Prof_Top_Hash_Table top_hash_tables[MAX_PROF_FRAMES];
#if TGPROF_COPY_STRINGS
	Mem_Pool strings_mem_pools[GAME_API_MAX_THREADS_NUM];
	u8       strings_mem_buffs[GAME_API_MAX_THREADS_NUM][4 * 1024 * 1024];
#endif
};

#endif /* ifndef TG_PROF_DS_H */
