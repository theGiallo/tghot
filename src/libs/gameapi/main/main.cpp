#if !WE_LOAD_OUR_GL
#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/gl.h>
#else
#include "our_gl.h"
#endif
#include "SDL.h"

#if 0
// NOTE(theGiallo): getpid gettid
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
#endif

// NOTE(theGiallo): usleep
#include <unistd.h>

#include <time.h> // NOTE(theGiallo): difftime

// NOTE(theGiallo): for file last modified date
#include <sys/stat.h>

#if !_WIN32
	#include <execinfo.h> // NOTE(theGiallo): to get the backtrace
	#include <stdlib.h> // NOTE(theGiallo): free
#endif

#define LPR_APP_NAME "sdl platform"

#include "game_api.h"
#include "sdl_platform.h"

#include "intrinsics.h"

#define LPR_IMPLEMENTATION 1
#define LPR_INTERNAL 1
#include "lepre.h"

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <locale.h>
#include <string.h>
#define SYS_ERRNO_TO_SYS_ERROR( e ) ( - ( e + (s64)0xFFFFFFFF ) )
#define SYS_ERR_IS_ERRNO( e ) ( -(e) >= (s64)0xFFFFFFFF )
#define SYS_ERRNO_FROM_ERR( e ) ( (-(e)) >> 32 )

static GAME_API_CYCLE_BEGINNING( game_api_cycle_beginning_stub  ){ (void) sys_api; (void)game_mem; }
static GAME_API_INPUT(  game_api_input_stub  ){ (void) sys_api; (void)game_mem; (void)e; }
static GAME_API_RENDER( game_api_render_stub ){ (void) sys_api; (void)game_mem; }
static GAME_API_UPDATE( game_api_update_stub ){ (void) sys_api; (void)game_mem; (void)game_time; (void)delta_time;}

#ifndef BIN_SUFFIX
	#define BIN_SUFFIX ""
#endif /* ifndef BIN_SUFFIX */
// NOTE(theGiallo): BIN_SUFFIX is defined from command line during compilation
#if !_WIN32
	#if TGPROF_COPY_STRINGS
		#define GAME_API_SO_NAME BIN_PATH "game_api" BIN_SUFFIX  ".so"
	#else
		#define GAME_API_SO_NAME_FILE BIN_PATH "last_lib_name" BIN_SUFFIX ".txt"
		#define GAME_API_SO_NAME game_api_last_so_name
		char game_api_last_so_name[1024] = {};
	#endif
#endif

#ifndef internal
#define internal static
#endif /* ifndef internal */
#define SDL_RET_IS_ERROR(r) ((r)!=0)
#define ARRAY_SIZE(a) ( sizeof(a) / sizeof((a)[0]) )
#define MIN(a,b) ((a)<=(b)?(a):(b))

struct
Sys_Int_GL_Context
{
	SDL_GLContext sdl_context;
	Sys_GL_Version version;
};
struct
Sys_Int_Window
{
	SDL_Window * sdl_window;
	u32 sdl_window_id;
	const char * window_name;
	bool vsync;
	Sys_Int_GL_Context gl_context;
	u8 id;
};

#define INVALID_WINDOW_ID 255
#define SYS_MAX_WINDOWS 10

struct
Sys_Int_Controller
{
	SDL_GameController * sdl_controller;
	SDL_Joystick       * sdl_joystick;
	const char * name;
	s32 sdl_id;
	u8 id;
};
#define INVALID_CONTROLLER_ID 255
#define SYS_MAX_CONTROLLERS 64
struct
SDL_Stuff
{
	Sys_Int_Window windows[SYS_MAX_WINDOWS];
	u32 windows_count;

	Sys_Int_Controller controllers[SYS_MAX_CONTROLLERS];
	u32 controllers_count;
	u8 controllers_mask[LPR_BITNSLOTS( SYS_MAX_CONTROLLERS )];
	u8 sys_id_from_sdl_id_table[256];

	bool fb_srgb_if_available;
	bool fb_is_srgb;
	bool vsync;
	bool vsync_is_supported;
#if WE_LOAD_OUR_GL
	bool were_gl_funcs_loaded;
#else
	bool was_glew_initialized;
#endif
	// NOTE(theGiallo): for now, using glew no different contexts
	// TODO(theGiallo, 2016-02-26): use glew mx or handmade code
	Sys_GL_Version gl_version;
	Time time_coefficient;

	// NOTE(theGiallo): audio
	SDL_mutex* audio_mutex;
};

#define MAX_THREADS_NUM 32
struct
Threads_Agenda
{
	u64 threads_ids[MAX_THREADS_NUM];
};

PRAGMA_ALIGN_PRE(64)
struct
Game_API
{
	Game_API_Cycle_Beginning * cycle_beginning;
	Game_API_Input           * input;
	Game_API_Render          * render;
	Game_API_Update          * update;
	Game_API_Audio_Callback  * audio_callback;
} PRAGMA_ALIGN_POST(64);

#if 0
// NOTE(theGiallo): 1GB
#define EVENTS_RECORD_QUEUE_MAX_SIZE ( 1024 * 1024 * 1024 )
#else
#define EVENTS_RECORD_QUEUE_MAX_SIZE 1024
#endif
#define MAX_EVENTS_IN_REPLAY ( EVENTS_RECORD_QUEUE_MAX_SIZE / sizeof( Event ) )
struct
Events_Record
{
	bool is_recording;
	u32 events_count;
	Time start_time;
	Time end_time;
	Time total_time;
	Event events_queue[MAX_EVENTS_IN_REPLAY];
};

struct
Events_Replay
{
	bool loop;
	bool loop_mem;
	bool is_replaying;
	u32 next_event_id;
	Time start_time;
};

SYS_API_GET_ERROR_NAME(         get_sys_error_name         );
SYS_API_GET_ERROR_DESCRIPTION(  get_sys_error_description  );
SYS_API_SET_VSYNC(              set_vsync                  );
SYS_API_SET_SCREEN_MODE(        set_screen_mode            );
SYS_API_GET_SCREEN_MODE(        get_screen_mode            );
SYS_API_GET_WINDOW_SIZE(        get_window_size            );
SYS_API_SET_WINDOW_SIZE(        set_window_size            );
SYS_API_CREATE_WINDOW(          sys_create_window          );
SYS_API_MAKE_WINDOW_CURRENT(    sys_make_window_current    );
SYS_API_SWAP_WINDOW(            sys_swap_window            );
SYS_API_CLOSE_WINDOW(           sys_close_window           );
SYS_API_EXIT(                   sys_exit                   );
SYS_API_GET_FILE_MODIFIED_TIME( sys_get_file_modified_time );
SYS_API_GET_FILE_SIZE(          sys_get_file_size          );
SYS_API_READ_ENTIRE_FILE(       sys_read_entire_file       );
SYS_API_LIST_DIRECTORY_FAST(    sys_list_directory_fast    );
SYS_API_LIST_DIRECTORY(         sys_list_directory         );
SYS_API_FILE_IS_DIRECTORY(      sys_file_is_directory      );
SYS_API_THREAD_CREATE(          sys_thread_create          );
SYS_API_THREAD_WAIT(            sys_thread_wait            );
SYS_API_THREAD_DETACH(          sys_thread_detach          );
SYS_API_THREAD_ID(              sys_thread_id              );
SYS_API_WARP_MOUSE_IN_WINDOW(   sys_warp_mouse_in_window   );
SYS_API_GET_KEYBOARD_STATE(     sys_get_keyboard_state     );

struct
The_Memory
{
	Game_API          game_api;
	SDL_Stuff         sdl_stuff;
	Threads_Agenda    threads_agenda;
	System_API        sys_api;
	System_API_Tables sys_tables;
	void *            game_api_so;
	time_t            game_api_so_load_time;
	Time              game_time;
	Time              game_base_time;
	u32               game_base_ticks;
	u32               game_ticks;
	Events_Replay     events_replay;
	Events_Record     events_record;
	bool              recorded_mem;
	bool              running;
	Game_Memory       game_mem_record;
	Game_Memory       game_mem;
};
internal
The_Memory
#if THE_MEMORY_MALLOCED
&
#endif
init_the_memory()
{
// NOTE(theGiallo): ok, so now even gcc 6 has this problem...
#if defined(__clang__) || defined(__GNUC__)
	The_Memory
	#if THE_MEMORY_MALLOCED
	&
	#endif
	the_mem
	#if THE_MEMORY_MALLOCED
	= *(The_Memory*) malloc( sizeof( The_Memory ) );
	lpr_log_dbg( "sizeof( The_Memory               )= %16lu\n", sizeof( The_Memory ) );
	lpr_log_dbg( "sizeof( Game_Memory              )= %16lu\n", sizeof( Game_Memory ) );
	#endif
	;
	memset( &the_mem, 0, sizeof(The_Memory) );
#else
	The_Memory the_mem = {};
#endif
	System_API sys_api = {get_sys_error_name,
	                      get_sys_error_description,
	                      set_vsync,
	                      set_screen_mode,
	                      get_screen_mode,
	                      get_window_size,
	                      set_window_size,
	                      sys_create_window,
	                      sys_make_window_current,
	                      sys_swap_window,
	                      sys_close_window,
	                      sys_exit,
	                      sys_get_file_modified_time,
	                      sys_get_file_size,
	                      sys_read_entire_file,
	                      sys_list_directory_fast,
	                      sys_list_directory,
	                      sys_file_is_directory,
	                      sys_thread_create,
	                      sys_thread_wait,
	                      sys_thread_detach,
	                      sys_thread_id,
	                      sys_warp_mouse_in_window,
	                      sys_get_keyboard_state,
	};
	System_API_Tables sys_tables =
	{
		{ SYS_SCREEN_MODE_TABLE( SYS_SCREEN_MODE_EXPAND_AS_TABLE_ENTRY ) },
		{ SYS_ERR_TABLE( SYS_ERR_EXPAND_AS_TABLE_ENTRY ) }
	};
	the_mem.sys_api = sys_api;
	the_mem.sys_tables = sys_tables;

	return the_mem;
}
internal
The_Memory
#if THE_MEMORY_MALLOCED
&
#endif
the_mem = init_the_memory();

internal
bool
file_modified_time( const char * file_path, time_t * out_time );

internal
bool
load_game_api();

// NOTE(theGiallo): uses window_name, gl_version, fb_srgb_if_available, vsync
// returns window id or negative on error
internal
s32
create_window( SDL_Stuff * sdl_stuff,
               const char * win_name,
               Sys_GL_Version gl_version,
               bool share_context,
               bool vsync,
               Sys_Screen_Mode screen_mode,
               bool max_screen_size,
               u32 width, u32 height
             );
internal
bool
init_SDL_stuff( SDL_Stuff * sdl_stuff );

bool
load_gl_functions();

void
SDL_LockAudio_For_Real();

void
SDL_UnlockAudio_For_Real();

#if DEBUG_GL
internal
void
opengl_callback_function( GLenum source,
                          GLenum type,
                          GLuint id,
                          GLenum severity,
                          GLsizei length,
                          const GLchar * message,
                          const void * user_param );
#endif

internal
inline
u8
window_id_from_sdl_window_id( u32 sdl_window_id );

internal
inline
Event
event_from_sdl_event( SDL_Event * sdl_e );

internal
inline
void
process_gamey_input( Event * e );

internal
inline
void
start_input_recording( u32 ticks );

internal
inline
void
stop_input_recording( u32 ticks );

internal
inline
void
stop_input_recording_secs( Time time );

internal
inline
void
start_input_replaying();

internal
inline
void
stop_input_replaying();

internal
void
store_game_mem();

internal
void
restore_game_mem();

internal
Time
get_time_in_sec();

internal
inline
Time
time_from_ticks( u32 ticks );

SDL_AudioDeviceID sdl_audio_dev;
static
void
dumb_sdl_audio_callback( void *userdata, Uint8 *stream, int _len )
{
	(void)userdata;
	(void)stream;

	if ( the_mem.game_api.audio_callback )
	{
		f32 * buf = (f32*) stream;
		u32 len = _len / sizeof( f32 );

		// NOTE(theGiallo): this is because SDL_LockAudio does not really get
		// called before this callback, so we have to get redundant, until
		// those damn "FIXME" in SDL_audio.c:632 gets solved. :@
		SDL_LockAudio_For_Real();
		the_mem.game_api.audio_callback( &the_mem.sys_api, &the_mem.game_mem,
		                                 buf, len );
		SDL_UnlockAudio_For_Real();
		return;
	} else
	{
		memset( stream, 0, _len );
		//return;
	}

#if 0
	Time curr_time = get_time_in_sec();
	usleep( 0.5 * 42667 );
	static Time previous_call_time = curr_time;
	lpr_log_info( "%f", curr_time );
	//lpr_log_info( "getpid       %d",  getpid() );
	//lpr_log_info( "gettid       %lu", syscall( SYS_gettid ) );
	//lpr_log_info( "pthread_self %lu", pthread_self() );
	//lpr_log_info( "audio callback; threadid: %lu  len = %d  delta_time = %f", SDL_ThreadID(), _len, curr_time - previous_call_time );
	previous_call_time = curr_time;
	static s32 adv = 0;

	static const Time sound_duration = 0.125;
	static Time sound_played = 0.0;
	static const Time sound_delay = 1.0 - sound_duration;
	static Time next_time = curr_time;
	static u32 samples_played = 0;

	Time sym_time = curr_time;
	Time pi2 = 3.141592653589793238462643383279 * 2.0;
	f32 * f32_str = (f32*)stream;
	u32 f32_str_len = _len / sizeof( f32 );
	for ( u32 i = 0; i < f32_str_len; ++i )
	{
		if ( 0 == ( i % 2 ) )
		{
			++adv;
		} else
		{
		}
		#define SIN_OF_FREQ( freq ) ( lpr_sin( ( adv * pi2 * (freq) ) / AUDIO_SAMPLING_FREQUENCY ) )
		f32 v = ( SIN_OF_FREQ( 440 ) +
		          SIN_OF_FREQ( 277 ) +
		          SIN_OF_FREQ( 330 )   ) / 3.0;
		f32 volume = 0.0f;//0.000125f;
		f32_str[i] = 0.0f;//volume * v;
		#undef SIN_OF_FREQ

		#if 0
		if ( sym_time >= next_time )
		{
			f32 freq = 440;
			//f32 v = lpr_sin( sound_played * pi2 * freq );
			f32_str[i] = volume * v;
			if ( 1 == i % 2 )
			{
				++samples_played;
				sound_played += 1.0 / AUDIO_SAMPLING_FREQUENCY;
				sym_time = curr_time + (Time)(i>>1) / AUDIO_SAMPLING_FREQUENCY;
				if ( sound_played >= sound_duration )
				{
					sound_played = 0.0;
					next_time = sym_time + sound_delay;
					samples_played = 0;
				}
			}
		}
		#endif
	}
#endif
}

extern "C"
int
main(int argc, char *argv[])
{
	(void)argc;
	(void)argv;
	sys_thread_id();

	lpr_log_dbg( LPR_TOSTRING( MAX_EVENTS_IN_REPLAY ) "= %lu", MAX_EVENTS_IN_REPLAY );
	lpr_log_dbg( "memory size: %lu", sizeof(The_Memory) );

	bool b_res =
	init_SDL_stuff(&the_mem.sdl_stuff);
	if ( !b_res )
	{
		return 1;
	}

	if ( !load_game_api() )
	{
		SDL_Quit();
		LPR_ILLEGAL_PATH();
		return 1;
	}

	the_mem.sdl_stuff.fb_srgb_if_available = false;
	the_mem.sdl_stuff.vsync = false;
	the_mem.sdl_stuff.gl_version = {3,3,false};

	the_mem.game_base_time = get_time_in_sec();
	the_mem.game_base_time -= time_from_ticks( SDL_GetTicks() );
	lpr_log_dbg( "game time is      %lf", the_mem.game_time );
	lpr_log_dbg( "game base time is %lf", the_mem.game_base_time );
	the_mem.running = true;
	while ( the_mem.running )
	{
		bool game_api_reloaded = false;
		{
			time_t modified_time = {};
			#if TGPROF_COPY_STRINGS
			if ( file_modified_time( GAME_API_SO_NAME, &modified_time ) )
			#else
			if ( file_modified_time( GAME_API_SO_NAME_FILE, &modified_time ) )
			#endif
			{
				if ( difftime( modified_time, the_mem.game_api_so_load_time ) >
				     0 )
				{
					game_api_reloaded = load_game_api();
					lpr_log_dbg( "game api reloaded (%lf)", the_mem.game_time );
					lpr_log_dbg( "modified_time         = %lu",
					             (u64)modified_time );
					lpr_log_dbg( "game_api_so_load_time = %lu",
					             (u64)the_mem.game_api_so_load_time );
				} else
				{
					//lpr_log_dbg( "modified time was not newer: %lu ",
					//             (u64)modified_time );
				}
			} else
			{
				lpr_log_dbg( "couldn't read file modified time" );
			}
		}

		the_mem.game_ticks = SDL_GetTicks();

		the_mem.game_api.cycle_beginning( &the_mem.sys_api, &the_mem.game_mem );

		SDL_Event e = {};
		while ( SDL_PollEvent( &e ) )
		{
			// NOTE(theGiallo): these switch processes the event even when
			// in replay mode
			switch ( e.type )
			{
				case SDL_WINDOWEVENT:
					switch ( e.window.event )
					{
						case SDL_WINDOWEVENT_CLOSE:
							// NOTE(theGiallo): demanded to the game layer
							break;
						default:
							break;
					}
					break;
				case SDL_KEYDOWN:
					switch( e.key.keysym.sym )
					{
						case SDLK_F5:
						{
							if ( !( e.key.keysym.mod & KMOD_ALT ) )
							{
								if ( the_mem.events_record.is_recording )
								{
									stop_input_recording( e.common.timestamp );
								}
								if ( !the_mem.events_record.events_count )
								{
									break;
								}
								start_input_replaying();
							}
							if ( ( e.key.keysym.mod & ( KMOD_SHIFT | KMOD_ALT ) )
							     &&
							     the_mem.recorded_mem )
							{
								restore_game_mem();
							}
							break;
						}
						case SDLK_F6:
						{
							if ( the_mem.events_replay.is_replaying )
							{
								stop_input_replaying();
							}
							if ( the_mem.events_record.is_recording )
							{
								stop_input_recording( e.common.timestamp );
							}
							break;
						}
						case SDLK_F7:
						{
							if ( e.key.keysym.mod & ( KMOD_SHIFT | KMOD_ALT ) )
							{
								store_game_mem();
							}
							if ( !( e.key.keysym.mod & KMOD_ALT ) )
							{
								start_input_recording( e.common.timestamp );
							}
							break;
						}
						case SDLK_F8:
						{
							if ( e.key.keysym.mod & ( KMOD_SHIFT | KMOD_ALT ) )
							{
								the_mem.events_replay.loop_mem =
									!the_mem.events_replay.loop_mem;
							}
							if ( !( e.key.keysym.mod & KMOD_ALT ) )
							{
								the_mem.events_replay.loop =
									!the_mem.events_replay.loop;
							}
						}
							break;
						case SDLK_F11:
						{
							Sys_Screen_Mode mode;
							switch ( get_screen_mode( 0 ) )
							{
								case SYS_SCREEN_MODE_WINDOWED:
									mode = SYS_SCREEN_MODE_FULLSCREEN;
									break;
								case SYS_SCREEN_MODE_FULLSCREEN:
								case SYS_SCREEN_MODE_FULLSCREEN_DESKTOP:
									mode = SYS_SCREEN_MODE_WINDOWED;
									break;
								default:
									LPR_ILLEGAL_PATH();
									mode = {};
									break;
							}
							set_screen_mode( 0, mode );
							break;
						}
						default:
							break;
					}
					break;
				case SDL_KEYUP:
					break;
				case SDL_CONTROLLERDEVICEADDED:
				{
					s32 id = e.cdevice.which;
					lpr_log_dbg( "controller attached id:%u", id );
					if ( !SDL_IsGameController( e.cdevice.which ) )
					{
						lpr_log_err( "Unknown joystick! Mapping unknown." );
						break;
					}
					lpr_log_dbg( "   it's a game controller" );
					if ( the_mem.sdl_stuff.controllers_count ==
					     SYS_MAX_CONTROLLERS )
					{
						lpr_log_err( "only " LPR_TOSTRING(SYS_MAX_CONTROLLERS)\
						             " controllers are supported!" );
						break;
					}
					SDL_GameController * sdl_controller = NULL;
					sdl_controller =
					SDL_GameControllerOpen( id );
					if ( !sdl_controller )
					{
						lpr_log_err( "couldn't open a game controller (id=%u)",
						             id );
						break;
					}
					lpr_log_dbg( "   controller opened" );
					const char * name =
						SDL_GameControllerName( sdl_controller );
					if ( !name )
					{
						name = "unknown name";
					}
					lpr_log_dbg( "   name: '%s'", name );
					SDL_Joystick * sdl_joystick =
						SDL_GameControllerGetJoystick( sdl_controller );
					if ( !sdl_joystick)
					{
						lpr_log_err( "couldn't get joystick of controller (id=%u)", id );
						break;
					}
					lpr_log_dbg( "   joystick retrieved" );
					Sys_Int_Controller sys_int_controller = {};
					sys_int_controller.sdl_controller = sdl_controller;
					sys_int_controller.sdl_joystick = sdl_joystick;
					sys_int_controller.name = name;
					sys_int_controller.id = INVALID_CONTROLLER_ID;
					for ( u8 i = 0; i < SYS_MAX_CONTROLLERS; ++i )
					{
						if ( !LPR_BITTEST( the_mem.sdl_stuff.controllers_mask, i ) )
						{
							LPR_BITSET( the_mem.sdl_stuff.controllers_mask, i );
							sys_int_controller.id = i;
							lpr_log_dbg( "controller got internal id %u", i );
							memcpy( the_mem.sdl_stuff.controllers+i,
							        &sys_int_controller,
							        sizeof(sys_int_controller) );
							++the_mem.sdl_stuff.controllers_count;
							break;
						}
					}
					u8 h = lpr_u8_hash_32( id );
					if ( the_mem.sdl_stuff.sys_id_from_sdl_id_table[h] !=
					     INVALID_CONTROLLER_ID )
					{
						lpr_log_err( "hash collision!!! D: D: D: id=%u, table[hash(id)]=%u",
						             id, the_mem.sdl_stuff.sys_id_from_sdl_id_table[h] );
					} else
					{
						the_mem.sdl_stuff.sys_id_from_sdl_id_table[h] = id;
						lpr_assert( sys_int_controller.id != INVALID_CONTROLLER_ID );
					}
				}
					break;
				case SDL_CONTROLLERDEVICEREMOVED:
				{
					s32 id = e.cdevice.which;
					lpr_log_dbg( "controller removed id:%u", id );
					if ( !SDL_IsGameController( e.cdevice.which ) )
					{
						break;
					}
					lpr_log_dbg( "   it's a game controller" );

					u8 h = lpr_u8_hash_32( id );
					if ( the_mem.sdl_stuff.sys_id_from_sdl_id_table[h] ==
					     INVALID_CONTROLLER_ID )
					{
						lpr_log_err( "   the controller was not in hash map" );
					}
					u8 sys_id = the_mem.sdl_stuff.sys_id_from_sdl_id_table[h];
					LPR_BITCLEAR( the_mem.sdl_stuff.controllers_mask, sys_id );
					the_mem.sdl_stuff.sys_id_from_sdl_id_table[h] =
						INVALID_CONTROLLER_ID;

					SDL_GameController * sdl_controller = NULL;
					sdl_controller =
						SDL_GameControllerFromInstanceID( id );
					if ( sdl_controller )
					{
						lpr_assert( the_mem.sdl_stuff.controllers[sys_id].sdl_controller == sdl_controller );
						SDL_GameControllerClose( sdl_controller );
						lpr_log_dbg( "   controller closed (%s)",
						             the_mem.sdl_stuff.controllers[sys_id].name );
					} else
					{
						const char * name = SDL_GameControllerNameForIndex( id );
						if ( !name )
						{
							name = "unknown name";
						}
						lpr_log_dbg( "   couldn't get controller from joystick (probably hadn't been opened)(%s)", name );
						SDL_GameControllerClose( sdl_controller );
						lpr_log_dbg( "   controller closed (%s)",
						             the_mem.sdl_stuff.controllers[sys_id].name );
					}
				}
					break;
				case SDL_CONTROLLERDEVICEREMAPPED:
				{
					// TODO(theGiallo, 2016-03-04): what should we do?
					// Shouldn't 'remapped' happen only if we remap it?
					s32 id = e.cdevice.which;
					lpr_log_dbg( "controller remapped id:%u", id );
					if ( SDL_IsGameController( e.cdevice.which ) )
					{
						lpr_log_dbg( "   it's a game controller" );
						SDL_GameController *controller = NULL;
						controller =
							SDL_GameControllerOpen( id );
						if ( controller )
						{
							lpr_log_dbg( "   controller opened" );
						} else
						{
							lpr_log_err( "couldn't open a game controller (id=%u)", id );
						}
					}
				}
					break;
				default:
					break;
			}
			if ( the_mem.events_replay.is_replaying )
			{
				continue;
			}
			Event event = event_from_sdl_event( &e );
			if ( event.type == EVENT_TYPE_COUNT )
			{
				continue;
			}
			process_gamey_input( &event );
		}
		if ( game_api_reloaded )
		{
			Event event = {};
			event.type = EVENT_GAME_SYSTEM;
			event.game_system.type = EVENT_GAME_API_DL_RELOADED;
			event.common.time_fired = the_mem.game_time;
			process_gamey_input( &event );
		}
		if ( the_mem.events_replay.is_replaying )
		{
			Time present_time =
			   the_mem.game_time - the_mem.events_replay.start_time;
			for(;;)
			{
				if ( the_mem.events_replay.next_event_id ==
				     the_mem.events_record.events_count )
				{
					if ( the_mem.events_replay.loop_mem )
					{
						// TODO(theGiallo, 2016-03-04): restore time?
						restore_game_mem();
					}
					if ( the_mem.events_replay.loop )
					{
						the_mem.events_replay.start_time +=
						   the_mem.events_record.total_time;

						the_mem.events_replay.next_event_id = 0;

						present_time =
						   the_mem.game_time -
						   the_mem.events_replay.start_time;
					} else
					{
						stop_input_replaying();
						break;
					}
				}

				Event * e = the_mem.events_record.events_queue +
				            the_mem.events_replay.next_event_id;
				lpr_assert( the_mem.events_replay.next_event_id <
				            MAX_EVENTS_IN_REPLAY );
				lpr_assert( the_mem.events_replay.next_event_id <
				            the_mem.events_record.events_count );
				lpr_assert( e->common.time_fired >=
				            the_mem.events_record.start_time );
				if ( e->common.time_fired -
				     the_mem.events_record.start_time
				     <= present_time )
				{
					++the_mem.events_replay.next_event_id;
					Event warped_event;
					memcpy( &warped_event, e, sizeof( Event ) );
					warped_event.common.time_fired +=
					   the_mem.events_replay.start_time -
					   the_mem.events_record.start_time;
					if ( warped_event.type == EVENT_WINDOW )
					{
						switch ( warped_event.window.type )
						{
							case EVENT_WINDOW_RESIZED:
							{
								u32 w_id = warped_event.window.window_id;
								SDL_SetWindowSize( the_mem.sdl_stuff.windows[w_id].sdl_window,
								                   warped_event.window.w, warped_event.window.h );
							}
								break;
							case EVENT_WINDOW_MOVED:
							{
								u32 w_id = warped_event.window.window_id;
								SDL_SetWindowPosition(
								   the_mem.sdl_stuff.windows[w_id].sdl_window,
								   warped_event.window.x, warped_event.window.y );
							}
								break;
							case EVENT_WINDOW_MINIMIZED:
							{
								u32 w_id = warped_event.window.window_id;
								SDL_MinimizeWindow( the_mem.sdl_stuff.windows[w_id].sdl_window );
							}
								break;
							case EVENT_WINDOW_MAXIMIZED:
							{
								u32 w_id = warped_event.window.window_id;
								SDL_MaximizeWindow( the_mem.sdl_stuff.windows[w_id].sdl_window );
							}
								break;
							case EVENT_WINDOW_RESTORED:
							{
								u32 w_id = warped_event.window.window_id;
								SDL_RestoreWindow( the_mem.sdl_stuff.windows[w_id].sdl_window );
							}
								break;
							default:
								break;
						}
					}
					process_gamey_input( &warped_event );
				} else
				{
					break;
				}
			}
		}

		Time delta_time = -the_mem.game_time;
		the_mem.game_time = get_time_in_sec() - the_mem.game_base_time;
		delta_time += the_mem.game_time;

		the_mem.game_api.update( &the_mem.sys_api,
		                         &the_mem.game_mem,
		                         the_mem.game_time,
		                         delta_time );

		the_mem.game_api.render( &the_mem.sys_api,
		                         &the_mem.game_mem );
	}


	if ( sdl_audio_dev )
	{
		SDL_CloseAudioDevice( sdl_audio_dev );
	}
	SDL_Quit();

	return 0;
}

////////////////////////////////////////////////////////////////////////////////
// NOTE(theGiallo): functions implementations

bool
file_modified_time( const char * file_path, time_t * out_time )
{
	if ( !file_path )
	{
		return false;
	}
	struct stat file_stat;
	if ( stat( file_path, &file_stat ) < 0 )
	{
		perror( file_path );
		return false;
	} else
	{
		*out_time = file_stat.st_mtime;
	}

	return true;
}

bool
load_game_api()
{
	lpr_log_info( "going to unload obj..." );
	// NOTE(theGiallo): this is to not unload the callback during its usage
	the_mem.game_api.audio_callback = NULL;
	SDL_LockAudio_For_Real();
	STORELOAD_BARRIER();

	#if TGPROF_COPY_STRINGS
		SDL_UnloadObject( the_mem.game_api_so );
	#else

	u64 read =
	sys_read_entire_file( GAME_API_SO_NAME_FILE, game_api_last_so_name,
	                      sizeof( game_api_last_so_name ) );
	if ( read > 0 && game_api_last_so_name[read - 1] == '\n' )
	{
		game_api_last_so_name[read - 1] = 0;
	}
	game_api_last_so_name[read] = 0;
	#endif

	lpr_log_info( "going to load obj..." );
	void * loaded_so = SDL_LoadObject( GAME_API_SO_NAME );
	if ( !loaded_so )
	{
		lpr_log_err( "couldn't load the game API shared object '%s': %s",
		             GAME_API_SO_NAME, SDL_GetError() );
		the_mem.game_api.cycle_beginning  = game_api_cycle_beginning_stub;
		the_mem.game_api.input  = game_api_input_stub;
		the_mem.game_api.render = game_api_render_stub;
		the_mem.game_api.update = game_api_update_stub;
		the_mem.game_api.audio_callback = NULL;
		SDL_UnlockAudio();
		return false;
	} else
	{
		const char * fpath = (const char *)0xcafebabe;
		#if TGPROF_COPY_STRINGS
		fpath = GAME_API_SO_NAME;
		#else
		fpath = GAME_API_SO_NAME_FILE;
		#endif
		lpr_log_dbg( "fpath = %s", fpath );
		lpr_log_dbg( "BIN_PATH = " BIN_PATH );
		while ( !file_modified_time( fpath, &the_mem.game_api_so_load_time ) )
		{
			lpr_log_dbg( "trying to read file modified time of '%s'...", fpath );
			sleep( 10 );
		}
	}

	Game_API new_game_api = {};

	new_game_api.cycle_beginning =
	( Game_API_Cycle_Beginning * ) SDL_LoadFunction( loaded_so, "game_api_cycle_beginning" );
	if ( !new_game_api.cycle_beginning )
	{
		lpr_log_err( "couldn't load the game API input function: %s", SDL_GetError() );
#if 0
		SDL_UnloadObject( loaded_so );
		return false;
#endif
		new_game_api.cycle_beginning = game_api_cycle_beginning_stub;
	}

	new_game_api.input =
	( Game_API_Input * ) SDL_LoadFunction( loaded_so, "game_api_input" );
	if ( !new_game_api.input )
	{
		lpr_log_err( "couldn't load the game API input function: %s", SDL_GetError() );
#if 0
		SDL_UnloadObject( loaded_so );
		return false;
#endif
		new_game_api.input = game_api_input_stub;
	}

	new_game_api.render =
	( Game_API_Render * ) SDL_LoadFunction( loaded_so, "game_api_render" );
	if ( !new_game_api.render )
	{
		lpr_log_err( "couldn't load the game API render function: %s", SDL_GetError() );
#if 0
		SDL_UnloadObject( loaded_so );
		return false;
#endif
		new_game_api.render = game_api_render_stub;
	}

	new_game_api.update =
	( Game_API_Update * ) SDL_LoadFunction( loaded_so, "game_api_update" );
	if ( !new_game_api.update )
	{
		lpr_log_err( "couldn't load the game API update function: %s", SDL_GetError() );
#if 0
		SDL_UnloadObject( loaded_so );
		return false;
#endif
		new_game_api.update = game_api_update_stub;
	}

	lpr_log_info( "going to load callback..." );
	new_game_api.audio_callback =
	( Game_API_Audio_Callback * ) SDL_LoadFunction( loaded_so, "game_api_audio_callback" );
	if ( !new_game_api.audio_callback )
	{
		lpr_log_err( "couldn't load the game API audio_callback function: %s", SDL_GetError() );
#if 0
		SDL_UnloadObject( loaded_so );
		return false;
#endif
		new_game_api.audio_callback = NULL;
	}

	STORELOAD_BARRIER();
	// NOTE(theGiallo): this is made 64bit aligned, so it's atomic
	the_mem.game_api    = new_game_api;

	the_mem.game_api_so = loaded_so;

	STORELOAD_BARRIER();
	SDL_UnlockAudio_For_Real();
	return true;
}

bool
init_SDL_stuff( SDL_Stuff * sdl_stuff )
{
	bool ret = true;

	lpr_log_dbg( "going to init SDL" );
	int i_res =
	SDL_Init( SDL_INIT_VIDEO
	        | SDL_INIT_EVENTS
	        | SDL_INIT_AUDIO
	        );
	if ( SDL_RET_IS_ERROR( i_res ) )
	{
		// TODO(theGiallo, 2016-02-24): do something! we are failing the init!!!
		LPR_ILLEGAL_PATH();
	}

	// NOTE(theGiallo): we init game controllers after events so device events are fired at startup
	i_res =
	SDL_InitSubSystem( SDL_INIT_GAMECONTROLLER );
	if ( SDL_RET_IS_ERROR( i_res ) )
	{
		// TODO(theGiallo, 2016-02-24): do something! we are failing the init!!!
		LPR_ILLEGAL_PATH();
	}

	u64 freq = SDL_GetPerformanceFrequency();
	sdl_stuff->time_coefficient = 1.0 / (Time)freq;

	for ( u16 i = 0; i < ARRAY_SIZE( sdl_stuff->sys_id_from_sdl_id_table ); ++i )
	{
		sdl_stuff->sys_id_from_sdl_id_table[i] = INVALID_CONTROLLER_ID;
	}

	for ( s32 i = 0; i < SDL_GetNumAudioDrivers(); ++i )
	{
		lpr_log_info( "Audio driver %d: %s", i, SDL_GetAudioDriver( i ) );
	}

	const char* driver_name = SDL_GetCurrentAudioDriver();
	if ( driver_name )
	{
		lpr_log_info( "Audio subsystem initialized; driver = %s.", driver_name );
	} else
	{
		lpr_log_info( "Audio subsystem not initialized." );
	}

	s32 count = SDL_GetNumAudioDevices( 0 );
	for ( s32 i = 0; i < count; ++i )
	{
		lpr_log_info( "Audio device %d: %s", i, SDL_GetAudioDeviceName( i, 0 ) );
	}

	{
		SDL_AudioSpec want, have;

		SDL_memset( &want, 0, sizeof( want ) ); /* or SDL_zero(want) */
		want.freq = AUDIO_SAMPLING_FREQUENCY;
		want.format = AUDIO_F32;
		want.channels = 2;
		want.samples = AUDIO_SAMPLES_IN_BUF;
		want.callback = dumb_sdl_audio_callback; // you wrote this function elsewhere.

		sdl_audio_dev =
		SDL_OpenAudioDevice( /*SDL_GetAudioDeviceName( 1, 0 )*/NULL, 0, &want, &have, 0 );
		if ( sdl_audio_dev == 0 )
		{
			lpr_log_info( "Failed to open audio: %s", SDL_GetError() );
		} else
		{
			if ( have.format != want.format )
			{ // we let this one thing change.
				lpr_log_info( "We didn't get Float32 audio format." );
			}

			if ( have.freq != want.freq )
			{
				lpr_log_info( "We didn't get %d sampling frequency but %d", want.freq, have.freq );
			}

			if ( have.samples != want.samples )
			{
				lpr_log_info( "We didn't get %d samples but %d", want.samples, have.samples );
			}

			lpr_log_info( "\nformat = %u\nfreq = %d\nsamples = %d", have.format, have.freq, have.samples );

			SDL_PauseAudioDevice( sdl_audio_dev, 0 ); // start audio playing.
		}
	}
	the_mem.sdl_stuff.audio_mutex = SDL_CreateMutex();

	#if WE_LOAD_OUR_GL
	i_res = SDL_GL_LoadLibrary( NULL );
	if ( SDL_RET_IS_ERROR( i_res ) )
	{
		lpr_log_err( "Error loading OpenGL library: %s ", SDL_GetError() );
		ret = false;
	} else
	{
		lpr_log_info( "OpenGL library loaded" );
	}
	#endif

	lpr_log_dbg( "SDL initialized" );
	return ret;
}

bool
load_gl_functions()
{
	bool ret = true;
#define M_GLUE2(a,b) a##b
#define LOAD_GL_FUNCTION( fname ) \
	fname = ( M_GLUE2(fname,_t )* ) SDL_GL_GetProcAddress( LPR_TOSTRING( fname ) ); \
	if ( !fname /*|| !SDL_GL_ExtensionSupported( LPR_TOSTRING( fname ) )*/ )        \
	{                                                                               \
		lpr_log_dbg( "couldn't load " LPR_TOSTRING( fname ) );                      \
		if ( !M_GLUE2(fname,_IS_OPTIONAL) )                                         \
			ret = false;                                                            \
		fname = NULL;                                                               \
	}

	#include "gl_funcs_load.inc"

	return ret;

#undef M_GLUE2
#undef LOAD_GL_FUNCTION
}

void
SDL_LockAudio_For_Real()
{
	SDL_LockMutex( the_mem.sdl_stuff.audio_mutex );
}

void
SDL_UnlockAudio_For_Real()
{
	SDL_UnlockMutex( the_mem.sdl_stuff.audio_mutex );
}

// TODO(theGiallo, 2016-02-26): remove sdl_stuff as a parameter? It's in the global mem.
// It could be useful to see what the function interacts with.
s32
create_window( SDL_Stuff * sdl_stuff,
               const char * win_name,
               Sys_GL_Version gl_version,
               bool share_context,
               bool vsync,
               Sys_Screen_Mode screen_mode,
               bool max_screen_size,
               u32 width, u32 height
             )
{
	s32 w_id = sdl_stuff->windows_count++;
	if ( w_id >= SYS_MAX_WINDOWS )
	{
		--sdl_stuff->windows_count;
		lpr_log_err( "max number of windows (" LPR_TOSTRING( SYS_MAX_WINDOWS )\
		             ")reached! Can't create a new one!" );
		return -SYS_ERR_TOO_MANY_WINDOWS;
	}
	s32 pos_x = SDL_WINDOWPOS_CENTERED, pos_y = SDL_WINDOWPOS_CENTERED;
	if ( max_screen_size )
	{
		s32 w_w, w_h;
		w_w = w_h = -1;
		s32 display_id{};
		s32 num_displays = SDL_GetNumVideoDisplays();
		if ( num_displays < 0 )
		{
			lpr_log_err( "%s", SDL_GetError() );
		} else
		{
			for ( s32 d_id = 0; d_id < num_displays; ++d_id )
			{
				SDL_DisplayMode native_mode = {};
				s32 i_res = SDL_GetDesktopDisplayMode( d_id, &native_mode );
				if ( SDL_RET_IS_ERROR( i_res ) )
				{
					lpr_log_err( "%s", SDL_GetError() );
					return -SYS_ERR_NO_DISPLAYS;
				} else
				{
					lpr_log_info( "native mode for display %d has res %dx%d",
					              d_id, native_mode.w, native_mode.h );
				}
				if ( native_mode.w > w_w )
				{
					w_w = native_mode.w;
					w_h = native_mode.h;
					display_id = d_id;
				}
			}
		}
		SDL_Rect bounds;
		SDL_GetDisplayBounds( display_id, &bounds );
		pos_x = bounds.x;
		pos_y = bounds.y;
		width = w_w;
		height = w_h;
	}

	SDL_GL_SetAttribute( SDL_GL_SHARE_WITH_CURRENT_CONTEXT, share_context );

	bool trying_srgb = false;
	if ( sdl_stuff->fb_srgb_if_available )
	{
		SDL_GL_SetAttribute( SDL_GL_FRAMEBUFFER_SRGB_CAPABLE, 1 );
		trying_srgb = true;
	}

	SDL_Window * sdl_window;
	do
	{
		lpr_log_dbg( "Trying to create an OpenGL window..." );
		sdl_window = SDL_CreateWindow(
		   win_name,  // title
		   pos_x, // x pos, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_UNDEFINED
		   pos_y, // y pos, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_UNDEFINED
		   width,                // width, in pixels
		   height,               // height, in pixels
		   SDL_WINDOW_SHOWN      // u32 flags: window options
		   | SDL_WINDOW_OPENGL
		   | SDL_WINDOW_RESIZABLE
		   | the_mem.sys_tables.screen_modes[screen_mode].sdl_flag
		);
		if( sdl_window == NULL )
		{
			const char *error = SDL_GetError();
			// if ( *error != '\0' ){}
			lpr_log_err( "Could not create window. SDL error: %s", error );
			if ( trying_srgb )
			{
				trying_srgb = false;
				SDL_GL_SetAttribute( SDL_GL_FRAMEBUFFER_SRGB_CAPABLE, 0 );
				continue;
			}
			--sdl_stuff->windows_count;
			return -SYS_ERR_CREATING_WINDOW;
		}
		break;
	} while ( true );

	lpr_log_dbg( "window created" );

	Sys_Int_GL_Context gl_context;

	// NOTE(theGiallo): create OGL context
	if ( !share_context )
	{
		s32 context_flags = {};
		context_flags |= SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG;
		#if DEBUG_GL
		// NOTE(theGiallo): for the debug callback
		context_flags |= SDL_GL_CONTEXT_DEBUG_FLAG;
		#endif
		SDL_GL_SetAttribute( SDL_GL_CONTEXT_FLAGS,
		                     context_flags );

		if ( gl_version.es )
		{
			SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK,
			                     SDL_GL_CONTEXT_PROFILE_ES |
			                     SDL_GL_CONTEXT_PROFILE_CORE );
		} else
		{
			SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK,
			                     SDL_GL_CONTEXT_PROFILE_CORE );
		}

		if ( SDL_RET_IS_ERROR(
		     SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION,
		                          gl_version.major ) ) )
		{
			lpr_log_err( "Error on SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, %d): %s",
			              gl_version.major, SDL_GetError() );
		}
		if ( SDL_RET_IS_ERROR(
		     SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION,
		                          gl_version.minor ) ) )
		{
			lpr_log_err( "Error on SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, %d): %s",
			             gl_version.minor, SDL_GetError() );
		}

		int srgb_capability = 0,
		    sdl_err =
		SDL_GL_GetAttribute( SDL_GL_FRAMEBUFFER_SRGB_CAPABLE, &srgb_capability );
		if ( !sdl_err )
		{
			lpr_log_dbg( "OpenGL default framebuffer is%s sRGB", srgb_capability?"":" not");
		}
		sdl_stuff->fb_is_srgb = srgb_capability;

		SDL_GLContext
		sdl_glcontext = SDL_GL_CreateContext( sdl_window );
		if ( sdl_glcontext == NULL )
		{
			const char *error = SDL_GetError();
			if ( *error != '\0' )
			{
				lpr_log_err( "Could not create GL context: %s", error);
				--sdl_stuff->windows_count;
				return -SYS_ERR_CREATING_CONTEXT;
			}
		}
		lpr_log_dbg( "OpenGL context created." );
		gl_context.version = gl_version;
		gl_context.sdl_context = sdl_glcontext;

		s32 res =
		SDL_GL_MakeCurrent( sdl_window,
		                    sdl_glcontext );
		if (res<0)
		{
			lpr_log_err( "[res=%d]SDL_GL_MakeCurrent failed %s", res, SDL_GetError() );
		}

		#if WE_LOAD_OUR_GL
		if ( !sdl_stuff->were_gl_funcs_loaded )
		{
			bool b_res = load_gl_functions();
			if ( !b_res )
			{
				lpr_log_err( "couldn't load all the OpenGL functions!" );
				return -SYS_ERR_LOADING_GL_FUNCTIONS;
			} else
			{
				lpr_log_info( "OpenGL functions loaded" );
				sdl_stuff->were_gl_funcs_loaded = true;
			}
		}
		#else
		// NOTE(theGiallo): init GLEW
		if ( !sdl_stuff->was_glew_initialized )
		{
			/**
			 * NOTE:
			 * From https://www.opengl.org/wiki/OpenGL_Loading_Library
			 *
			 * GLEW has a problem with core contexts. It calls
			 * glGetString(GL_EXTENSIONS), which causes GL_INVALID_ENUM on GL 3.2+ core
			 * context as soon as glewInit() is called. It also doesn't fetch the
			 * function pointers. The solution is for GLEW to use glGetStringi​ instead.
			 * The current version of GLEW is 1.10.0 but they still haven't corrected it.
			 * The only fix is to use glewExperimental for now:
			 *
			 *	  glewExperimental=TRUE;
			 *	  GLenum err=glewInit();
			 *	  if(err!=GLEW_OK)
			 *	  {
			 *		//Problem: glewInit failed, something is seriously wrong.
			 *		cout<<"glewInit failed, aborting."<<endl;
			 *	  }
			 *
			 *	glewExperimental is a variable that is already defined by GLEW. You must
			 * set it to GL_TRUE before calling glewInit().
			 *	You might still get GL_INVALID_ENUM (depending on the version of GLEW you
			 * use), but at least GLEW ignores glGetString(GL_EXTENSIONS) and gets all
			 * function pointers. If you are creating a GL context the old way or if you
			 * are creating a backward compatible context for GL 3.2+, then you don't
			 * need glewExperimental.
			 *	As with most other loaders, you should not include gl.h, glext.h​, or any
			 * other gl related header file before glew.h, otherwise you'll get an error
			 * message that you have included gl.h before glew.h. You shouldn't be
			 * including gl.h at all; glew.h replaces it.
			 *	GLEW also provides wglew.h which provides Windows specific GL functions
			 * (wgl functions). If you include wglext.h before wglew.h, GLEW will
			 * complain. GLEW also provides glxew.h for X windows systems. If you include
			 * glxext.h before glxew.h, GLEW will complain.
			 **/
			glewExperimental = GL_TRUE;
			GLenum err = glewInit(); // NOTE(theGiallo): read the huge comment above!
			if ( GLEW_OK != err )
			{
				lpr_log_err( "Error initializing GLEW: %s", glewGetErrorString( err ) );
			} else
			{
				sdl_stuff->was_glew_initialized = true;
			}
			if ( GLEW_VERSION_1_3 )
			{
				lpr_log_dbg( "GLEW 1.3" );
			}

			lpr_log_dbg( "GLEW version: %d.%d.%d",
						GLEW_VERSION_MAJOR,GLEW_VERSION_MINOR,GLEW_VERSION_MICRO );
		}
		#endif

		if ( sdl_stuff->fb_is_srgb )
		{
			glEnable( GL_FRAMEBUFFER_SRGB );
		} else
		{
			glDisable( GL_FRAMEBUFFER_SRGB );
		}

		#if DEBUG_GL // NOTE(theGiallo): register OpenGL debug callback function
			if ( glDebugMessageCallback != NULL )
			{
				lpr_log_info( "glDebugMessageCallback available" );
				// Enable synchronous callback. This ensures that your callback function
				// is called right after an error has occurred. This capability is not
				// defined in the AMD version.
				glEnable( GL_DEBUG_OUTPUT_SYNCHRONOUS );
				glDebugMessageCallback( opengl_callback_function, NULL );
				GLuint unused_ids = 0;
				// NOTE: GL_DONT_CARE because we want all of them
				glDebugMessageControl( GL_DONT_CARE, // source
									GL_DONT_CARE, // type
									GL_DONT_CARE, // severity
									0,
									&unused_ids,
									GL_TRUE );
			} else
			if ( glDebugMessageCallbackARB != NULL )
			{
				lpr_log_info( "glDebugMessageCallbackARB available" );
				// Enable synchronous callback. This ensures that your callback function
				// is called right after an error has occurred. This capability is not
				// defined in the AMD version.
				glEnable( GL_DEBUG_OUTPUT_SYNCHRONOUS );
				glDebugMessageCallbackARB( opengl_callback_function, NULL );
			} else
			if ( glDebugMessageCallbackAMD != NULL )
			{
				lpr_log_info( "glDebugMessageCallbackAMD available but not implemented" );
				// Enable synchronous callback. This ensures that your callback function
				// is called right after an error has occurred. This capability is not
				// defined in the AMD version.
				// glEnable( GL_DEBUG_OUTPUT_SYNCHRONOUS );
				// TODO(theGiallo, 2015/09/29): maybe specialise for AMD?
				// glDebugMessageCallbackAMD( opengl_callback_function, NULL );
			} else
			{
				lpr_log_info( "glDebugMessageCallback not available" );
			}
		#endif
	} else
	{
		int major, minor, es;
		SDL_GL_GetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, &major );
		SDL_GL_GetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, &minor );
		SDL_GL_GetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, &es );
		SDL_GLContext context = SDL_GL_GetCurrentContext();
		if ( !context )
		{
			const char * error = SDL_GetError();
			lpr_log_err( "current context is NULL and we are trying to create a window sharing it! %s", error );
			SDL_DestroyWindow( sdl_window );
			if ( ( error = SDL_GetError() ) )
			{
				lpr_log_err( "error destroying the failed window: %s", error );
			}
		}
		gl_context.version.major = major;
		gl_context.version.minor = minor;
		gl_context.version.es = (bool)( es & SDL_GL_CONTEXT_PROFILE_ES );
		gl_context.sdl_context = context;

		s32 res =
		SDL_GL_MakeCurrent( sdl_window,
		                    gl_context.sdl_context );
		if ( res < 0 )
		{
			lpr_log_err( "[res=%d]SDL_GL_MakeCurrent failed %s", res, SDL_GetError() );
		}
	}

	#if DEBUG_GL // NOTE(theGiallo): check default framebuffer encoding
	{
		GLint color_encoding = LPR_ZERO_STRUCT;
		glBindFramebuffer( GL_FRAMEBUFFER, 0 );
		glGetFramebufferAttachmentParameteriv( GL_FRAMEBUFFER, GL_BACK,
		   GL_FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING, &color_encoding );
		switch ( color_encoding )
		{
			case GL_SRGB:
				lpr_log_info( "BACK of default framebuffer is encoded in SRGB" );
				break;
			case GL_LINEAR:
				lpr_log_info( "BACK of default framebuffer is encoded linearly" );
				break;
			default:
				lpr_log_err( "Unknonwn encoding value! %d", color_encoding );
				break;
		}
		glGetFramebufferAttachmentParameteriv( GL_FRAMEBUFFER, GL_FRONT,
		           GL_FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING, &color_encoding );
		switch ( color_encoding )
		{
			case GL_SRGB:
				lpr_log_info( "FRONT of default framebuffer is encoded in SRGB" );
				break;
			case GL_LINEAR:
				lpr_log_info( "FRONT of default framebuffer is encoded linearly" );
				break;
			default:
				lpr_log_err( "Unknonwn encoding value! %d", color_encoding );
				break;
		}
	}
	#endif

	if( SDL_RET_IS_ERROR( SDL_GL_SetSwapInterval( true ) ) )
	{
		sdl_stuff->vsync_is_supported = false;
		vsync = false;
		lpr_log_err( "Swap interval not supported" );
	} else
	{
		sdl_stuff->vsync_is_supported = true;
		if ( vsync )
		{
			lpr_log_info( "V-Sync enabled" );
		} else
		{
			if ( SDL_RET_IS_ERROR( SDL_GL_SetSwapInterval( false ) ) )
			{
				const char * error = SDL_GetError();
				lpr_log_err( "Error turning vsync off: %s\n", error );
				vsync = true;
			} else
			{
				lpr_log_info( "V-Sync disabled" );
			}
		}
		s32 swap_interval = SDL_GL_GetSwapInterval();
		switch ( swap_interval )
		{
			case  0:
				lpr_log_dbg( "swap_interval: disabled" );
				break;
			case  1:
				lpr_log_dbg( "swap_interval: enabled" );
				break;
			case -1:
				lpr_log_dbg( "swap_interval: late swap tearing" );
				break;
			default:
				LPR_ILLEGAL_PATH();
				break;
		}
		if( vsync != ( 1 == swap_interval ) )
		{
			lpr_log_err( "swap interval is not as requested" );
		}
	}

	// NOTE(theGiallo): save window into array
	sdl_stuff->windows[w_id].sdl_window = sdl_window;
	sdl_stuff->windows[w_id].sdl_window_id = SDL_GetWindowID( sdl_window );
	sdl_stuff->windows[w_id].window_name = win_name;
	sdl_stuff->windows[w_id].gl_context = gl_context;
	sdl_stuff->windows[w_id].vsync = vsync;
	sdl_stuff->windows[w_id].id = w_id;

	return w_id;
}

#if DEBUG_GL
void
opengl_callback_function( GLenum source,
                          GLenum type,
                          GLuint id,
                          GLenum severity,
                          GLsizei length,
                          const GLchar * message,
                          const void * user_param )
{
	(void)user_param;
	(void)length;

	if ( severity != GL_DEBUG_SEVERITY_LOW &&
	     severity != GL_DEBUG_SEVERITY_MEDIUM &&
	     severity != GL_DEBUG_SEVERITY_HIGH )
	{
		return;
	}
	lpr_log_err( "---------------------opengl-callback-start------------" );

	lpr_log_err( "message:\n\n%s\n", message );

	const char * source_string = LPR_ZERO_STRUCT;
	switch ( source )
	{
		case GL_DEBUG_CATEGORY_API_ERROR_AMD:
		case GL_DEBUG_SOURCE_API:
			source_string = "API";
			break;
		case GL_DEBUG_CATEGORY_APPLICATION_AMD:
		case GL_DEBUG_SOURCE_APPLICATION:
			source_string = "Application";
			break;
		case GL_DEBUG_CATEGORY_WINDOW_SYSTEM_AMD:
		case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
			source_string = "Window System";
			break;
		case GL_DEBUG_CATEGORY_SHADER_COMPILER_AMD:
		case GL_DEBUG_SOURCE_SHADER_COMPILER:
			source_string = "Shader Compiler";
			break;
		case GL_DEBUG_SOURCE_THIRD_PARTY:
			source_string = "Third Party";
			break;
		case GL_DEBUG_CATEGORY_OTHER_AMD:
		case GL_DEBUG_SOURCE_OTHER:
			source_string = "Other";
			break;
		default:
			source_string = "Unknown";
			break;
	}
	lpr_log_err( "source_string: %s", source_string );

	const char * type_string = LPR_ZERO_STRUCT;
	switch (type)
	{
		case GL_DEBUG_TYPE_ERROR:
			type_string = "ERROR";
			break;
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
			type_string = "DEPRECATED_BEHAVIOR";
			break;
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
			type_string = "UNDEFINED_BEHAVIOR";
			break;
		case GL_DEBUG_TYPE_PORTABILITY:
			type_string = "PORTABILITY";
			break;
		case GL_DEBUG_TYPE_PERFORMANCE:
			type_string = "PERFORMANCE";
			break;
		case GL_DEBUG_TYPE_OTHER:
			type_string = "OTHER";
			break;
	}
	lpr_log_err( "type: %s", type_string );

	lpr_log_err( "id: %d", id );
	const char * severity_string = LPR_ZERO_STRUCT;
	switch (severity)
	{
		case GL_DEBUG_SEVERITY_LOW:
			severity_string = "LOW";
			break;
		case GL_DEBUG_SEVERITY_MEDIUM:
			severity_string = "MEDIUM";
			break;
		case GL_DEBUG_SEVERITY_HIGH:
			severity_string = "HIGH";
			break;
	}
	lpr_log_err( "severity: %s", severity_string );

	lpr_log_err( "backtrace:" );

	int nptrs;
	void * buffer[100];
	char ** strings;

	nptrs = backtrace( buffer, 100 );

	strings = backtrace_symbols( buffer, nptrs );
	if ( strings == NULL )
	{
		perror( "backtrace_symbols" );
		// exit(EXIT_FAILURE);
	} else
	{
		for ( int i = 0; i != nptrs; ++i )
		{
			lpr_log_err( "%s", strings[i] );
		}

		free( strings );
	}

	if ( type == GL_DEBUG_TYPE_ERROR )
	{
		LPR_ILLEGAL_PATH();
	}

	lpr_log_err( "---------------------opengl-callback-end--------------" );
}
#endif

internal
inline
u8
window_id_from_sdl_window_id( u32 sdl_window_id )
{
	u8 ret = INVALID_WINDOW_ID;
	for ( u8 i = 0; i < the_mem.sdl_stuff.windows_count; ++i )
	{
		if ( sdl_window_id == the_mem.sdl_stuff.windows[i].sdl_window_id )
		{
			ret = i;
		}
	}
	return ret;
}

internal
Event
event_from_sdl_event( SDL_Event * sdl_e )
{
	Event e = {};

	e.common.time_fired = time_from_ticks( sdl_e->common.timestamp );

	switch ( sdl_e->type )
	{
		case SDL_WINDOWEVENT:
			e.type = EVENT_WINDOW;
			break;
		case SDL_KEYUP:
		case SDL_KEYDOWN:
			e.type = EVENT_KEYBOARD;
			break;
		case SDL_MOUSEMOTION:
		case SDL_MOUSEBUTTONUP:
		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEWHEEL:
			e.type = EVENT_MOUSE;
			break;
		case SDL_CONTROLLERAXISMOTION:
		case SDL_CONTROLLERBUTTONDOWN:
		case SDL_CONTROLLERBUTTONUP:
		case SDL_CONTROLLERDEVICEADDED:
		case SDL_CONTROLLERDEVICEREMOVED:
		case SDL_CONTROLLERDEVICEREMAPPED:
			e.type = EVENT_PAD;
			break;
		default:
			// lpr_log_dbg( "sdl_event %u is not supported", sdl_e->type );
			e.type = EVENT_TYPE_COUNT;
			return e;
			break;
	}
	switch ( e.type )
	{
		case EVENT_WINDOW:
			e.window.window_id =
				window_id_from_sdl_window_id( sdl_e->window.windowID );
			switch ( sdl_e->window.event )
			{
				case SDL_WINDOWEVENT_SHOWN:
					e.window.type = EVENT_WINDOW_SHOWN;
					break;
				case SDL_WINDOWEVENT_HIDDEN:
					e.window.type = EVENT_WINDOW_HIDDEN;
					break;
				case SDL_WINDOWEVENT_EXPOSED:
					e.window.type = EVENT_WINDOW_EXPOSED;
					break;
				case SDL_WINDOWEVENT_MOVED:
					e.window.type = EVENT_WINDOW_MOVED;
					break;
				case SDL_WINDOWEVENT_RESIZED:
					e.window.type = EVENT_WINDOW_RESIZED;
					break;
				case SDL_WINDOWEVENT_SIZE_CHANGED:
					e.window.type = EVENT_WINDOW_SIZE_CHANGED;
					break;
				case SDL_WINDOWEVENT_MINIMIZED:
					e.window.type = EVENT_WINDOW_MINIMIZED;
					break;
				case SDL_WINDOWEVENT_MAXIMIZED:
					e.window.type = EVENT_WINDOW_MAXIMIZED;
					break;
				case SDL_WINDOWEVENT_RESTORED:
					e.window.type = EVENT_WINDOW_RESTORED;
					break;
				case SDL_WINDOWEVENT_ENTER:
					e.window.type = EVENT_WINDOW_ENTER;
					break;
				case SDL_WINDOWEVENT_LEAVE:
					e.window.type = EVENT_WINDOW_LEAVE;
					break;
				case SDL_WINDOWEVENT_FOCUS_GAINED:
					e.window.type = EVENT_WINDOW_FOCUS_GAINED;
					break;
				case SDL_WINDOWEVENT_FOCUS_LOST:
					e.window.type = EVENT_WINDOW_FOCUS_LOST;
					break;
				case SDL_WINDOWEVENT_CLOSE:
					e.window.type = EVENT_WINDOW_CLOSE;
					break;
				default:
					lpr_log_err( "EVENT_WINDOW %u", u32(sdl_e->window.event) );
					LPR_ILLEGAL_PATH();
					break;
			}
			break;
		case EVENT_KEYBOARD:
			e.keyboard.window_id =
				window_id_from_sdl_window_id( sdl_e->key.windowID );
			switch ( sdl_e->key.type )
			{
				case SDL_KEYUP:
					e.keyboard.type = EVENT_KEYBOARD_KEY_RELEASED;
					break;
				case SDL_KEYDOWN:
					e.keyboard.type = EVENT_KEYBOARD_KEY_PRESSED;
					break;
				default:
					LPR_ILLEGAL_PATH();
					break;
			}
			break;
		case EVENT_MOUSE:
			e.mouse.windows_id =
				window_id_from_sdl_window_id( sdl_e->key.windowID );
			switch ( sdl_e->type )
			{
				case SDL_MOUSEMOTION:
					e.mouse.type = EVENT_MOUSE_MOTION;
					break;
				case SDL_MOUSEBUTTONUP:
					e.mouse.type = EVENT_MOUSE_BUTTON_RELEASED;
					break;
				case SDL_MOUSEBUTTONDOWN:
					e.mouse.type = EVENT_MOUSE_BUTTON_PRESSED;
					break;
				case SDL_MOUSEWHEEL:
					e.mouse.type = EVENT_MOUSE_WHEEL;
					break;
				default:
					LPR_ILLEGAL_PATH();
					break;
			}
			break;
		case EVENT_PAD:
			switch ( sdl_e->type )
			{
				case SDL_CONTROLLERAXISMOTION:
					e.controller.type = EVENT_CONTROLLER_AXIS;
					break;
				case SDL_CONTROLLERBUTTONDOWN:
					e.controller.type = EVENT_CONTROLLER_BUTTON_PRESSED;
					break;
				case SDL_CONTROLLERBUTTONUP:
					e.controller.type = EVENT_CONTROLLER_BUTTON_RELEASED;
					break;
				case SDL_CONTROLLERDEVICEADDED:
					e.controller.type = EVENT_CONTROLLER_DEVICE_ADDED;
					break;
				case SDL_CONTROLLERDEVICEREMOVED:
					e.controller.type = EVENT_CONTROLLER_DEVICE_REMOVED;
					break;
				case SDL_CONTROLLERDEVICEREMAPPED:
					e.controller.type = EVENT_CONTROLLER_DEVICE_REMAPPED;
					break;
				default:
					LPR_ILLEGAL_PATH();
					break;
			}
			break;
		default:
			break;
	}
	switch ( e.type )
	{
		case EVENT_WINDOW:
			switch ( e.window.type )
			{
				// case EVENT_WINDOW_SHOWN:
				// case EVENT_WINDOW_HIDDEN:
				// case EVENT_WINDOW_EXPOSED:
				// 	break;
				case EVENT_WINDOW_MOVED:
					e.window.x = sdl_e->window.data1;
					e.window.y = sdl_e->window.data2;
					break;
				case EVENT_WINDOW_RESIZED:
					e.window.w = sdl_e->window.data1;
					e.window.h = sdl_e->window.data2;
					break;
				case EVENT_WINDOW_SIZE_CHANGED:
					e.window.w = sdl_e->window.data1;
					e.window.h = sdl_e->window.data2;
					break;
				// case EVENT_WINDOW_MINIMIZED:
				// case EVENT_WINDOW_MAXIMIZED:
				// case EVENT_WINDOW_RESTORED:
				// case EVENT_WINDOW_ENTER:
				// case EVENT_WINDOW_LEAVE:
				// case EVENT_WINDOW_FOCUS_GAINED:
				// case EVENT_WINDOW_FOCUS_LOST:
				// case EVENT_WINDOW_CLOSE:
				default:
					break;
			}
			break;
		case EVENT_KEYBOARD:
			// NOTE(theGiallo): this is true until SDL changes, but it's unprobable
			e.keyboard.virt_key = (Virt_Key)sdl_e->key.keysym.sym;
			e.keyboard.phys_key = (Phys_Key)sdl_e->key.keysym.scancode;
			e.keyboard.repeat = sdl_e->key.repeat;
			e.keyboard.mods = sdl_e->key.keysym.mod;
			break;
		case EVENT_MOUSE:
			switch ( e.mouse.type )
			{
				case EVENT_MOUSE_MOTION:
				{
					u32 ww, wh;
					get_window_size( e.mouse.windows_id, &ww, &wh );
					e.mouse.motion.pos_x = sdl_e->motion.x;
					e.mouse.motion.pos_y = wh - sdl_e->motion.y;
					e.mouse.motion.motion_x = sdl_e->motion.xrel;
					e.mouse.motion.motion_y = -sdl_e->motion.yrel;
					break;
				}
				case EVENT_MOUSE_BUTTON_RELEASED:
				case EVENT_MOUSE_BUTTON_PRESSED:
				{
					u32 ww, wh;
					get_window_size( e.mouse.windows_id, &ww, &wh );
					// TODO(theGiallo, 2016-03-03): check why is this missing
					// e.mouse.button.clicks_count = sdl_e->button.clicks;
					e.mouse.button.pos_x = sdl_e->button.x;
					e.mouse.motion.pos_y = wh - sdl_e->button.y;
					e.mouse.button.button =
					   (Mouse_Button)(sdl_e->button.button - 1);
					break;
				}
				case EVENT_MOUSE_WHEEL:
					/*if ( sdl_e->wheel.direction )
					{
					// TODO(theGiallo, 2016-03-03): check why is this missing
					}*/
					e.mouse.wheel.scrolled_x = sdl_e->wheel.x;
					e.mouse.wheel.scrolled_y = sdl_e->wheel.y;
					break;
				default:
					LPR_ILLEGAL_PATH();
					break;
			}
			break;
		case EVENT_PAD:
			e.controller.controller_id = the_mem.sdl_stuff.sys_id_from_sdl_id_table[lpr_u8_hash_32( sdl_e->caxis.which )];
			lpr_assert( e.controller.controller_id != INVALID_CONTROLLER_ID );
			switch ( e.controller.type )
			{
				case EVENT_CONTROLLER_AXIS:
					e.controller.axis = (Controller_Axis)sdl_e->caxis.axis;
					e.controller.motion = sdl_e->caxis.value;
						break;
				case EVENT_CONTROLLER_BUTTON_PRESSED:
				case EVENT_CONTROLLER_BUTTON_RELEASED:
					e.controller.button = (Controller_Button)sdl_e->cbutton.button;
					break;
				case EVENT_CONTROLLER_DEVICE_ADDED:
					break;
				case EVENT_CONTROLLER_DEVICE_REMOVED:
				case EVENT_CONTROLLER_DEVICE_REMAPPED:
					break;
				default:
					LPR_ILLEGAL_PATH();
					break;
			}
			break;
		default:
			break;
	}
	return e;
}

internal
inline
void
process_gamey_input( Event * event )
{
	if ( the_mem.events_record.is_recording )
	{
		if ( the_mem.events_record.events_count < MAX_EVENTS_IN_REPLAY )
		{
			the_mem.events_record.events_queue[
			   the_mem.events_record.events_count++ ]
			= *event;
		} else
		{
			lpr_log_err(
			   "stopped input recording because the buffer is full!" );
			stop_input_recording_secs( event->common.time_fired );
		}
	}
	the_mem.game_api.input( &the_mem.sys_api, &the_mem.game_mem, event );
}

internal
inline
void
start_input_recording( u32 ticks )
{
	Time time = time_from_ticks( ticks );
	the_mem.events_record.events_count = 0;
	lpr_assert( ticks <= the_mem.game_ticks );
	the_mem.events_record.start_time = time;//the_mem.game_ticks;
	the_mem.events_record.is_recording = true;
	lpr_log_dbg( "started input recording" );
}

internal
inline
void
stop_input_recording( u32 ticks )
{
	Time time = time_from_ticks( ticks );
	stop_input_recording_secs( time );
}
internal
inline
void
stop_input_recording_secs( Time time )
{
	the_mem.events_record.end_time = time;
	the_mem.events_record.total_time =
		the_mem.events_record.end_time - the_mem.events_record.start_time;
	the_mem.events_record.is_recording = false;
	lpr_log_dbg( "stopped input recording" );
}

internal
inline
void
start_input_replaying()
{
	the_mem.events_replay.start_time = the_mem.game_time;
	the_mem.events_replay.is_replaying = true;
	the_mem.events_replay.next_event_id = 0;
	lpr_log_dbg( "started  input playing" );
}

internal
inline
void
stop_input_replaying()
{
	the_mem.events_replay.next_event_id = 0;
	the_mem.events_replay.is_replaying = false;
	lpr_log_dbg( "stopped input playing" );
}

internal
void
store_game_mem()
{
	lpr_log_dbg( "storing game memory" );
	DEBUG_ONLY( Time t = get_time_in_sec(); )
	memcpy( &the_mem.game_mem_record,
	        &the_mem.game_mem,
	        sizeof( Game_Memory ) );
	the_mem.recorded_mem = true;
	DEBUG_ONLY(
		t = get_time_in_sec() - t;
		lpr_log_dbg( "took %lfs", t );
	)
}

internal
void
restore_game_mem()
{
	lpr_log_dbg( "restoring game memory" );
	lpr_assert( the_mem.recorded_mem );
	DEBUG_ONLY( Time t = get_time_in_sec(); )
	memcpy( &the_mem.game_mem,
	        &the_mem.game_mem_record,
	        sizeof( Game_Memory ) );
	DEBUG_ONLY(
		t = get_time_in_sec() - t;
		lpr_log_dbg( "took %lfs", t );
	)
}

Time
get_time_in_sec()
{
	u64 val = SDL_GetPerformanceCounter();

	Time ret = (Time)val * the_mem.sdl_stuff.time_coefficient;
	return ret;
}

internal
inline
Time
time_from_ticks( u32 ticks )
{
	Time ret = 0.001 * (Time)ticks;

	return ret;
}

SYS_API_GET_ERROR_NAME( get_sys_error_name )
{
	const char * ret = {};
	lpr_assert( error < 0 );
	if ( SYS_ERR_IS_ERRNO( error ) )
	{
		ret = "errno error";
		return ret;
	}

	lpr_assert( SYS_ERR_TO_TABLE_ID( error ) < SYS_ERR_COUNT_PLUS_ONE );
	ret = the_mem.sys_tables.errors[SYS_ERR_TO_TABLE_ID( error )].name;
	return ret;
}

SYS_API_GET_ERROR_DESCRIPTION( get_sys_error_description )
{
	const char * ret = {};
	lpr_assert( error < 0 );
	if ( SYS_ERR_IS_ERRNO( error ) )
	{
		int the_errno = SYS_ERRNO_FROM_ERR( error );
		ret = strerror_l( the_errno, uselocale( (locale_t)0 ) );
		return ret;
	}
	lpr_assert( SYS_ERR_TO_TABLE_ID( error ) < SYS_ERR_COUNT_PLUS_ONE );
	ret = the_mem.sys_tables.errors[SYS_ERR_TO_TABLE_ID( error )].description;
	return ret;
}

SYS_API_SET_VSYNC( set_vsync )
{
	bool ret = true;
	for ( u32 i = 0; i < the_mem.sdl_stuff.windows_count; ++i )
	{
		SDL_GL_MakeCurrent( the_mem.sdl_stuff.windows[i].sdl_window, the_mem.sdl_stuff.windows[i].gl_context.sdl_context );
		if ( SDL_RET_IS_ERROR( SDL_GL_SetSwapInterval( vsync ) ))
		{
			lpr_log_err( "Error setting vsync %s for window '%s'", vsync?"on":"off", the_mem.sdl_stuff.windows[i].window_name );
			ret = false;
		} else
		{
			the_mem.sdl_stuff.windows[i].vsync = vsync;
		}
	}
	return ret;
}

SYS_API_SET_SCREEN_MODE( set_screen_mode )
{
	lpr_assert( window_id < SYS_MAX_WINDOWS );
	bool ret = true;
	Sys_Int_Window * w = the_mem.sdl_stuff.windows + window_id;
	u32 sdl_flag = the_mem.sys_tables.screen_modes[mode].sdl_flag;
	s32 display_id = SDL_GetWindowDisplayIndex( w->sdl_window );
	SDL_DisplayMode display_mode;
	s32 i_res =
	SDL_GetDesktopDisplayMode( display_id, &display_mode );
	if ( SDL_RET_IS_ERROR( i_res ) )
	{
		lpr_log_err( "%s", SDL_GetError() );
	} else
	{
		SDL_SetWindowSize( w->sdl_window, display_mode.w, display_mode.h );
	}
	if ( SDL_RET_IS_ERROR( SDL_SetWindowFullscreen( w->sdl_window, sdl_flag ) ) )
	{
		const char * error = SDL_GetError();
		lpr_log_err( "error setting window fullscreen to %s: %s", the_mem.sys_tables.screen_modes[mode].str, error );
		ret = false;
	}

	return ret;
}

SYS_API_GET_SCREEN_MODE( get_screen_mode )
{
	Sys_Screen_Mode mode = {};

	lpr_assert( window_id < SYS_MAX_WINDOWS );
	SDL_Window * w = the_mem.sdl_stuff.windows[window_id].sdl_window;

	u32 flags = SDL_GetWindowFlags( w );

	if ( flags & SDL_WINDOW_FULLSCREEN )
	{
		mode = SYS_SCREEN_MODE_FULLSCREEN;
	} else
	if ( flags & SDL_WINDOW_FULLSCREEN_DESKTOP )
	{
		mode = SYS_SCREEN_MODE_FULLSCREEN_DESKTOP;
	} else
	{
		mode = SYS_SCREEN_MODE_WINDOWED;
	}

	return mode;
}

SYS_API_GET_WINDOW_SIZE( get_window_size )
{
	lpr_assert( window_id >= 0 );
	if ( window_id == INVALID_WINDOW_ID )
	{
		*width = *height = 0;
		return;
	}
	lpr_assert( (u32)window_id < the_mem.sdl_stuff.windows_count );
	lpr_assert( the_mem.sdl_stuff.windows[window_id].sdl_window );


	s32 w,h;
	SDL_GetWindowSize( the_mem.sdl_stuff.windows[window_id].sdl_window, &w, &h );
	*width  = (u32)w;
	*height = (u32)h;
}

SYS_API_SET_WINDOW_SIZE( set_window_size )
{
	lpr_assert( window_id >= 0 );
	lpr_assert( (u32)window_id < the_mem.sdl_stuff.windows_count );
	lpr_assert( the_mem.sdl_stuff.windows[window_id].sdl_window );

	SDL_SetWindowSize( the_mem.sdl_stuff.windows[window_id].sdl_window, width, height );
}

SYS_API_CREATE_WINDOW( sys_create_window )
{
	s32 ret;

	ret = create_window( &the_mem.sdl_stuff, win_name, gl_version,
	                     share_context, vsync, screen_mode, max_screen_size,
	                     width, height );

	return ret;
}

SYS_API_MAKE_WINDOW_CURRENT( sys_make_window_current )
{
	lpr_assert( window_id >= 0 );
	lpr_assert( window_id < (s32)the_mem.sdl_stuff.windows_count );
	lpr_assert( the_mem.sdl_stuff.windows_count <= SYS_MAX_WINDOWS );

	SDL_GLContext * sdl_gl_context = &the_mem.sdl_stuff.windows[window_id].gl_context.sdl_context;
	SDL_Window    * sdl_window     =  the_mem.sdl_stuff.windows[window_id].sdl_window;

	lpr_assert( *sdl_gl_context );
	lpr_assert( sdl_window );

	s32 i_res =
	SDL_GL_MakeCurrent( sdl_window, *sdl_gl_context );
	if ( SDL_RET_IS_ERROR( i_res ) )
	{
		lpr_log_err( "[res=%d w_id=%d]SDL_GL_MakeCurrent failed %s", i_res, window_id, SDL_GetError() );
		return -SYS_ERR_MAKING_WINDOW_CURRENT;
	}

	return 0;
}

SYS_API_SWAP_WINDOW( sys_swap_window )
{
	lpr_assert( window_id >= 0 );
	lpr_assert( window_id < (s32)the_mem.sdl_stuff.windows_count );
	lpr_assert( the_mem.sdl_stuff.windows_count <= SYS_MAX_WINDOWS );

	SDL_Window * sdl_window = the_mem.sdl_stuff.windows[window_id].sdl_window;

	lpr_assert( sdl_window );

	SDL_GL_SwapWindow( sdl_window );
}

SYS_API_CLOSE_WINDOW( sys_close_window )
{
	lpr_assert( window_id >= 0 );
	lpr_assert( window_id < (s32)the_mem.sdl_stuff.windows_count );
	lpr_assert( the_mem.sdl_stuff.windows_count <= SYS_MAX_WINDOWS );

	SDL_Window * sdl_window = the_mem.sdl_stuff.windows[window_id].sdl_window;

	lpr_assert( sdl_window );

	SDL_DestroyWindow( sdl_window );
}

SYS_API_EXIT( sys_exit )
{
	the_mem.running = false;
}

SYS_API_GET_FILE_MODIFIED_TIME( sys_get_file_modified_time )
{
	s64 ret;
	time_t sys_time = {};

	struct stat file_stat;
	if ( stat( file_path, &file_stat ) < 0 )
	{
		ret = SYS_ERRNO_TO_SYS_ERROR( errno );
		return ret;
	} else
	{
		sys_time = file_stat.st_mtime;
	}

	*modified_time = (Time)sys_time - the_mem.game_base_time;

	ret = 0;
	return ret;
}

SYS_API_GET_FILE_SIZE( sys_get_file_size )
{
#if 0
	s64 ret = 0;

	FILE * fp = fopen( file_path, "rb" );
	if ( !fp )
	{
		return -SYS_ERR_FILE_OPEN_FAILED;
	}

	fseek( fp, 0, SEEK_END ); // NOTE(theGiallo): SEEK_END is non std, so ~non-portable
	ret = ftell ( fp );

	fclose( fp );

	return ret;
#else

	s64 ret = 0;

	struct stat buf;

	if ( stat( file_path, &buf ) < 0 )
	{
		ret = SYS_ERRNO_TO_SYS_ERROR( errno );
		return ret;
	}

	ret = buf.st_size;

	return ret;
#endif
}

SYS_API_READ_ENTIRE_FILE( sys_read_entire_file )
{
	s64 ret = 0;

	FILE * fp = fopen( file_path, "rb" );
	if ( !fp )
	{
		return -SYS_ERR_FILE_OPEN_FAILED;
	}

	fseek( fp, 0, SEEK_END ); // NOTE(theGiallo): SEEK_END is non std, so ~non-portable
	s64 size  = ftell ( fp );
	rewind( fp );
	ret = fread( mem, 1, MIN( size, mem_size ), fp );
	if ( size > mem_size )
	{
		// TODO(theGiallo, 2016-03-08): sth
		lpr_log_info( "warning: reading file '%s' of size %ld into a smaller buffer of %ld", file_path, size, mem_size );
	}
	if ( ret!=MIN( size, mem_size ) )
	{
		// TODO(theGiallo, 2016-03-08): sth
	}

	fclose ( fp );

	return ret;
}

SYS_API_LIST_DIRECTORY_FAST( sys_list_directory_fast )
{
	s64 ret = 0;

#define CHECK_MEM_SIZE(s) \
	if ( available_mem_size < s )\
	{\
		ret = -SYS_ERR_MEMORY_BUFFER_TOO_SMALL;\
		return ret;\
	}

	DIR * dir;
	int fd;
	struct dirent * ent;

	fd = open( dir_path, O_RDONLY | O_RDONLY );
	if ( fd < 0 )
	{
		ret = SYS_ERRNO_TO_SYS_ERROR( errno );
		return ret;
	}

	dir = fdopendir( fd );
	if ( !dir )
	{
		ret = SYS_ERRNO_TO_SYS_ERROR( errno );
		close( fd );
		return ret;
	}

	u32 path_bytes_count = 0;
	u32 full_path_bytes_count = 0;
	while ( dir_path[path_bytes_count++] );
	u8 full_path[path_bytes_count+1];
	memcpy( full_path, dir_path, path_bytes_count );
	full_path_bytes_count = path_bytes_count;
	if ( full_path[path_bytes_count-2] != '/' )
	{
		full_path[path_bytes_count-1] = '/';
		full_path[path_bytes_count] = 0;
		++full_path_bytes_count;
	}

	u8 * available_mem = mem;
	s64 available_mem_size = mem_size;
	u32 * files_count = (u32*)available_mem;
	available_mem_size -= sizeof(*files_count);
	available_mem      += sizeof(*files_count);
	CHECK_MEM_SIZE(0);
	u8 ** curr_file = 0;
	u8 *** next_file = 0;


	for ( ent = readdir( dir );
	      ent;
	      ent = readdir( dir ) )
	{
		u32 name_bytes_count = 0;
		while ( ent->d_name[name_bytes_count++] );

		if ( next_file )
		{
			*next_file = (u8**)available_mem;
		}
		curr_file = (u8**)available_mem;
		available_mem_size -= sizeof(*curr_file);
		available_mem      += sizeof(*curr_file);
		next_file = (u8***)available_mem;
		available_mem_size -= sizeof(*next_file);
		available_mem      += sizeof(*next_file);
		CHECK_MEM_SIZE(0);
		//lpr_assert( (u64)next_file < (u64)(mem + mem_size) );
		*next_file = 0;

		//lpr_assert( (u64)curr_file < (u64)(mem + mem_size) );
		*curr_file = available_mem;
		u8 * curr_file_target = *curr_file;
		CHECK_MEM_SIZE(1);
		//lpr_assert( (u64)*curr_file < (u64)(mem + mem_size) );
		curr_file_target[0] = 0;
		if ( options_flags & SYS_LDIR_FULL_PATHS )
		{
			CHECK_MEM_SIZE(full_path_bytes_count);
			available_mem_size -= full_path_bytes_count-1;
			available_mem      += full_path_bytes_count-1;
			curr_file_target   += full_path_bytes_count-1;
			memcpy( curr_file_target, full_path, full_path_bytes_count );
		}
		CHECK_MEM_SIZE(name_bytes_count);
		available_mem_size -= name_bytes_count;
		available_mem      += name_bytes_count;
		memcpy( curr_file_target, ent->d_name, name_bytes_count );
		++*files_count;
	}

	ret = *files_count;

	closedir( dir );
	close( fd );

	return ret;
#undef CHECK_MEM_SIZE
}

SYS_API_LIST_DIRECTORY( sys_list_directory )
{
	s64 ret = 0;

#define CHECK_MEM_SIZE(s) \
	if ( available_mem_size < s )\
	{\
		ret = -SYS_ERR_MEMORY_BUFFER_TOO_SMALL;\
		return ret;\
	}

	DIR * dir;
	int fd;
	struct dirent * ent;

	fd = open( dir_path, O_RDONLY | O_RDONLY );
	if ( fd < 0 )
	{
		ret = SYS_ERRNO_TO_SYS_ERROR( errno );
		return ret;
	}

	dir = fdopendir( fd );
	if ( !dir )
	{
		ret = SYS_ERRNO_TO_SYS_ERROR( errno );
		close( fd );
		return ret;
	}

	s64 files_count = 0;
	for ( ent = readdir( dir );
	      ent;
	      ent = readdir( dir ) )
	{
		++files_count;
	}
	rewinddir( dir );

	u32 path_bytes_count = 0;
	u32 full_path_bytes_count = 0;
	while ( dir_path[path_bytes_count++] );
	u8 full_path[path_bytes_count+1];
	memcpy( full_path, dir_path, path_bytes_count );
	full_path_bytes_count = path_bytes_count;
	if ( full_path[path_bytes_count-2] != '/' )
	{
		full_path[path_bytes_count-1] = '/';
		full_path[path_bytes_count] = 0;
		++full_path_bytes_count;
	}

	u8 * available_mem = mem;
	s64 available_mem_size = mem_size;
	u8 ** files = (u8**)available_mem;
	available_mem_size -= sizeof(u8*)*(files_count+1);
	available_mem      += sizeof(u8*)*(files_count+1);

	CHECK_MEM_SIZE(0);

	s64 file_id = 0;
	for ( ent = readdir( dir );
	      ent;
	      ent = readdir( dir ) )
	{
		// NOTE(theGiallo): is dir static once opened?
		lpr_assert( file_id != files_count );

		u32 name_bytes_count = 0;
		while ( ent->d_name[name_bytes_count++] );

		files[file_id] = available_mem;
		u8 * curr_file_target = files[file_id];
		curr_file_target[0] = 0;
		if ( options_flags & SYS_LDIR_FULL_PATHS )
		{
			CHECK_MEM_SIZE(full_path_bytes_count);
			available_mem_size -= full_path_bytes_count-1;
			available_mem      += full_path_bytes_count-1;
			memcpy( curr_file_target, full_path, full_path_bytes_count );
			curr_file_target   += full_path_bytes_count-1;
		}
		CHECK_MEM_SIZE(name_bytes_count);
		available_mem_size -= name_bytes_count;
		available_mem      += name_bytes_count;
		memcpy( curr_file_target, ent->d_name, name_bytes_count );
		++file_id;
	}
	files[file_id] = available_mem;

	ret = files_count;

	closedir( dir );
	close( fd );

	return ret;
#undef CHECK_MEM_SIZE
}

SYS_API_FILE_IS_DIRECTORY( sys_file_is_directory )
{
	s64 ret = 0;

	struct stat buf;

	if ( stat( file_path, &buf ) < 0 )
	{
		ret = SYS_ERRNO_TO_SYS_ERROR( errno );
		return ret;
	}

	if ( S_ISDIR( buf.st_mode ) )
	{
		ret = 1;
	}

	return ret;
}

SYS_API_THREAD_CREATE( sys_thread_create )
{
	SDL_Thread * ret;
	ret = SDL_CreateThread( fun, name, data );
	return (Sys_Thread*)ret;
}

SYS_API_THREAD_WAIT( sys_thread_wait )
{
	if ( status )
	{
		s32 r_status;
		SDL_WaitThread( (SDL_Thread*)thread, &r_status );

		// TODO(theGiallo, 2016-03-31): convert SDL status to Sys status
	} else
	{
		SDL_WaitThread( (SDL_Thread*)thread, NULL );
	}
}

SYS_API_THREAD_DETACH( sys_thread_detach )
{
	SDL_DetachThread( (SDL_Thread*)thread );
}

// NOTE(theGiallo): registers the thread in the threads agenda and/or returns
// its internal id
SYS_API_THREAD_ID( sys_thread_id )
{
	u8 ret = SYS_API_UNKNOWN_THREAD_ID;
	u64 tid = SDL_ThreadID();
	Threads_Agenda * ta = &the_mem.threads_agenda;
	for ( u8 i = 0; i < MAX_THREADS_NUM; ++i )
	{
		u64 * tids = ta->threads_ids + i;
		if ( 0 == interlocked_compare_exchange_64( tids, tid, 0 )  ||
		     *tids == tid )
		{
			ret = i;
			break;
		}
	}
	return ret;
}

SYS_API_WARP_MOUSE_IN_WINDOW( sys_warp_mouse_in_window )
{
	lpr_assert( window_id >= 0 );
	lpr_assert( window_id < (s32)the_mem.sdl_stuff.windows_count );

	// NOTE(theGiallo): this is to fix mouse warping under debian,
	// I'm not sure this problem shows itself on other platforms
	s32 w,h;
	SDL_GetWindowSize( the_mem.sdl_stuff.windows[window_id].sdl_window, &w, &h );
#if 0
	if ( ( h % 2 ) == 1 )
	{
		px_pos_y += 1;
	}
#endif
	SDL_WarpMouseInWindow( the_mem.sdl_stuff.windows[window_id].sdl_window, px_pos_x, h-px_pos_y );
}
SYS_API_GET_KEYBOARD_STATE( sys_get_keyboard_state )
{
	const u8 * ret = (const u8*) SDL_GetKeyboardState( bytes_size );
	return ret;
}
