#ifndef TG_PROF_H
#define TG_PROF_H 1

#if TGPROF_COPY_STRINGS
#include <string.h>
#endif

#define MH_EMIT_BODY 1
#include "tgprof_ds.h"
#undef MH_EMIT_BODY

#include "basic_types.h"
#include "intrinsics.h"
#include "global_sys_api.h"
#include "global_game_mem.h"

#define TP_GLUE2(a,b) a##b
#define TP_GLUE2M(a,b) TP_GLUE2(a,b)
#define PROF_FRAME_NAME "FRAME"
#define PROFILE_SECTION_NAMED( name ) \
   volatile Prof_Section_Timer TP_GLUE2M(_prof_section_timer,__LINE__) = \
   Prof_Section_Timer( name, __PRETTY_FUNCTION__, __FILE__, __LINE__ )
#define PROFILE_SECTION( ) \
   volatile Prof_Section_Timer TP_GLUE2M(_prof_section_timer,__LINE__) = \
    Prof_Section_Timer( NULL, __PRETTY_FUNCTION__, __FILE__, __LINE__ )
#define PROF_STOP_START_FRAME_AT_END() \
   volatile Prof_Stop_Start_Frame_At_End _prof_stop_start_frame_at_end##line = \
   Prof_Stop_Start_Frame_At_End();
#define DEFER_GATHER_ONE_FRAME() \
   volatile Defer_Gather_One_Frame TP_GLUE2M(_defer_gather_one_frame,__LINE__);

inline
void
prof_push_time_value( Thread_Times_Buff * ttb, const char * name,
                      const char * function,
                      const char * file, u32 line, u8 thread_id,
                      u8 tag_open_close )
{
	u64 f = ttb->first;
	PRAGMA_ALIGN_PRE(64) u64 next PRAGMA_ALIGN_POST(64) = ttb->last + 1;
	if ( next == f || ( f == 0 && next == MAX_PROF_TIME_VALUES ) )
	{
		// TODO(theGiallo, 2016-06-05): error? warning?
		return;
	}

	if ( next == MAX_PROF_TIME_VALUES )
	{
		next = 0;
	}

	// NOTE(theGiallo): get the time the latest possible
	Prof_Time_Value tv = { RDTSC_SERIALIZING(),
	                       name,
	                       function,
	                       file,
	                       line,
	                       thread_id,
	                       tag_open_close };

	ttb->time_values[next] = tv;

	STORELOAD_BARRIER();

	ttb->last = next;

	return;
}

inline
Prof_Time_Value
prof_pop_time_value( Thread_Times_Buff * ttb )
{
	Prof_Time_Value ret = {};

	u64 f = ttb->first;
	PRAGMA_ALIGN_PRE(64) u64 l PRAGMA_ALIGN_POST(64) = ttb->last;

	if ( f == l )
	{
		return ret;
	}

	++f;
	if ( f == MAX_PROF_TIME_VALUES )
	{
		f = 0;
	}

	ret = ttb->time_values[f];

	STORELOAD_BARRIER();

	ttb->first = f;

	return ret;
}

inline
void
prof_open_section( const char * name, const char * function,
                   const char * file, u32 line, u8 thread_id )
{
	while( !global_game_mem ) {};
	Thread_Times_Buff * ttb =
	   global_game_mem->prof.threads_time_buffs + thread_id;
	prof_push_time_value( ttb, name, function, file, line, thread_id, TAG_OPEN );
	//lpr_log_dbg( "open  section [%u] %s %s", thread_id, name, function );
}

inline
void
prof_close_section( const char * name, const char * function,
                    const char * file, u32 line, u8 thread_id )
{
	while( !global_game_mem ) {};
	Thread_Times_Buff * ttb =
	   global_game_mem->prof.threads_time_buffs + thread_id;
	prof_push_time_value( ttb, name, function, file, line, thread_id, TAG_CLOSE );
	//lpr_log_dbg( "close section [%u] %s %s", thread_id, name, function );
}

struct
Prof_Section_Timer
{
	const char * name;
	const char * function;
	const char * file;
	u32          line;
	u8 thread_id;
	inline
	Prof_Section_Timer( const char * name, const char * function,
	                    const char * file, u32 line )
	{
		this->thread_id = thread_sys_id();
#if TGPROF_COPY_STRINGS
		while( !global_game_mem || !global_game_mem->prof.strings_mem_pools[thread_id].size ) {};
		Mem_Pool * mem_pool = &global_game_mem->prof.strings_mem_pools[thread_id];
#define TGPROF_COPY_A_STRING( var_name ) \
		{                                                        \
			u32 bytes_count = str_bytes_to_null( var_name ) + 1; \
			char * rec_name =                                    \
			(char *) mem_pool->allocate( bytes_count );          \
			lpr_assert( rec_name );                              \
			if ( var_name )                                      \
			{                                                    \
				memcpy( rec_name, var_name, bytes_count );       \
			} else                                               \
			{                                                    \
				rec_name[0] = NULL;                              \
			}                                                    \
			var_name = rec_name;                                 \
		}                                                        \

		TGPROF_COPY_A_STRING( name );
		TGPROF_COPY_A_STRING( function );
		TGPROF_COPY_A_STRING( file );
#endif
		this->name      = name;
		this->function  = function;
		this->file      = file;
		this->line      = line;
		prof_open_section( this->name, this->function, this->file, this->line,
		                   this->thread_id );
	}

	inline
	~Prof_Section_Timer()
	{
		prof_close_section( this->name, this->function, this->file, this->line,
		                    this->thread_id );
	}
};

struct
Prof_Stop_Start_Frame_At_End
{
	inline
	Prof_Stop_Start_Frame_At_End( ) {}

	inline
	~Prof_Stop_Start_Frame_At_End( )
	{
		u8 thread_id = thread_sys_id();
		prof_close_section( PROF_FRAME_NAME, NULL, NULL, 0, thread_id );
		u64 tsc = RDTSC_SERIALIZING();
		u64 ns  = ns_time();
		u64 tsc_delta = tsc - global_game_mem->prof.frame_section_tsc_start;
		u64 ns_delta  = ns  - global_game_mem->prof.frame_section_ns_start;
		global_game_mem->prof.clock_period_ns = (f64)ns_delta / (f64)tsc_delta;
		global_game_mem->prof.frame_section_tsc_start = tsc;
		global_game_mem->prof.frame_section_ns_start  = ns;
		prof_open_section(  PROF_FRAME_NAME, NULL, NULL, 0, thread_id );
	}
};

internal
void
print_indentation( u32 depth )
{
	for ( u32 i = 0; i < depth; ++i )
	{
		printf( " " );
	}
}
internal
void
print_section( Prof_Section * s, u32 depth )
{
	print_indentation( depth );
	printf( "%p\n", s );
	print_indentation( depth );
	printf( "%s\n", s->function );
	Prof_Section_Child_List_Node * c = s->childs_list;
	while ( c )
	{
		print_section( c->section, depth + 1 );
		c = c->next;
	}
}

internal
void
print_frame( Prof_Section_Thread_Frame * tf )
{
	if ( !tf->section )
	{
		return;
	}
	printf( "frame %p\n", tf );
	if ( !tf )
	{
		return;
	}

	if ( tf->section )
	{
		print_section( tf->section, 1 );
	}

	Prof_Section_Child_List_Node * cn = tf->next;
	while ( cn )
	{
		if ( cn->section )
		{
			print_section( cn->section, 1 );
		}
		cn = cn->next;
	}

	printf( "frame %p end\n\n", tf );
}

internal
u32
hash_section_id( const Prof_Section_Identifier * id )
{
	u32 ret = {};
	ret = u32_FNV1a( id, sizeof( *id ) );
	ret = ret % ARRAY_COUNT(((Prof_Top_Hash_Table*)0)->hash_table );
	return ret;
}

inline
internal
bool
equal( Prof_Section_Identifier * id0, Prof_Section_Identifier * id1 )
{
	bool ret =
	   id0->name == id1->name &&
	   id0->function == id1->function &&
	   id0->file == id1->file &&
	   id0->line == id1->line &&
	   id0->thread_id == id1->thread_id;
	return ret;
}

internal
void
recursive_compute_prof_top_hash_table_min_max_tot(
   Prof_Section_Child_List_Node * sec_node,
   Prof_Top_Hash_Table * tht,
   u32 thread_id )
{
	for ( ;
	      sec_node && sec_node->section;
	      sec_node = sec_node->next )
	{
		Prof_Section * sec = sec_node->section;

		Prof_Section_Identifier
		sec_id = {
		   sec->name,
		   sec->function,
		   sec->file,
		   sec->line,
		   thread_id };
		u32 hk = hash_section_id( &sec_id );
		Prof_Top_HT_Node ** node = tht->hash_table + hk;
		while ( *node )
		{
			if ( equal( &(*node)->identifier, &sec_id ) )
			{
				break;
			}
			node = &(*node)->next;
		}
		if ( !*node )
		{
			*node = tht->nodes_mh.allocate_clean();
			memcpy( &(*node)->identifier, &sec_id, sizeof( sec_id ) );
			(*node)->statistics.non_recursive.min_duration = U64_MAX;
			(*node)->statistics.recursive.min_duration = U64_MAX;
		}
		u64 duration = sec->end_time - sec->start_time;
		Prof_Section_Statistics_Data *stat_data = NULL;
		bool close_call = false;
		if ( !(*node)->statistics.compute_data.now_inside )
		{
			(*node)->statistics.compute_data.now_inside = true;
			close_call = true;
			stat_data = &(*node)->statistics.non_recursive;
		} else
		{
			stat_data = &(*node)->statistics.recursive;
		}
		stat_data->tot_duration += duration;
		if ( stat_data->min_duration > duration )
		{
			stat_data->min_duration = duration;
		}
		if ( stat_data->max_duration < duration )
		{
			stat_data->max_duration = duration;
		}
		++stat_data->calls_count_tot;

		if ( sec->childs_list )
		{
			recursive_compute_prof_top_hash_table_min_max_tot( sec->childs_list, tht, thread_id );
		}
		if ( close_call )
		{
			(*node)->statistics.compute_data.now_inside = false;
		}
	}
}

internal
void
recursive_compute_prof_top_variance(
   Prof_Section_Child_List_Node * sec_node,
   Prof_Top_Hash_Table * tht,
   u32 thread_id )
{
	for ( ;
	      sec_node && sec_node->section;
	      sec_node = sec_node->next )
	{
		Prof_Section * sec = sec_node->section;

		Prof_Section_Identifier
		sec_id = {
		   sec->name,
		   sec->function,
		   sec->file,
		   sec->line,
		   thread_id };
		u32 hk = hash_section_id( &sec_id );
		Prof_Top_HT_Node ** node = tht->hash_table + hk;
		while ( *node )
		{
			if ( equal( &(*node)->identifier, &sec_id ) )
			{
				break;
			}
			node = &(*node)->next;
		}
		lpr_assert ( *node );

		u64 duration = sec->end_time - sec->start_time;
		Prof_Section_Statistics_Data *stat_data = NULL;
		bool close_call = false;
		if ( !(*node)->statistics.compute_data.now_inside )
		{
			(*node)->statistics.compute_data.now_inside = true;
			close_call = true;
			stat_data = &(*node)->statistics.non_recursive;
		} else
		{
			stat_data = &(*node)->statistics.recursive;
		}
		stat_data->var_duration += square( duration - stat_data->avg_duration );

		if ( sec->childs_list )
		{
			recursive_compute_prof_top_variance( sec->childs_list, tht, thread_id );
		}
		if ( close_call )
		{
			(*node)->statistics.compute_data.now_inside = false;
		}
	}
}

internal
void
compute_prof_top( Prof_Section_Child_List_Node * sec_node,
                  Prof_Top_Hash_Table * tht,
                  u32 thread_id )
{
	//PROFILE_SECTION();

	recursive_compute_prof_top_hash_table_min_max_tot(
	   sec_node, tht, thread_id );

	Prof_Top_HT_Node_MH_2_Iterator it = {};
	Prof_Top_HT_Node * node;
	while ( ( node = iterate( &tht->nodes_mh, &it ) ) )
	{
		Prof_Section_Statistics * stat = &node->statistics;
		Prof_Section_Statistics_Data * data[2] = {
		   &stat->non_recursive,
		   &stat->recursive,
		};
		for ( int i = 0; i!=2; ++i )
		{
			data[i]->avg_duration =
			   data[i]->calls_count_tot == 0 ? 0.0f :
			   data[i]->tot_duration / (f32) data[i]->calls_count_tot;
			if ( data[i]->min_duration == U64_MAX &&
			     data[i]->min_duration > data[i]->max_duration )
			{
				data[i]->min_duration = 0;
			}
		}
	}

	recursive_compute_prof_top_variance(
	   sec_node, tht, thread_id );

	it = {};
	while ( ( node = iterate( &tht->nodes_mh, &it ) ) )
	{
		Prof_Section_Statistics * stat = &node->statistics;
		Prof_Section_Statistics_Data * data[2] = {
		   &stat->non_recursive,
		   &stat->recursive,
		};
		for ( int i = 0; i!=2; ++i )
		{
			data[i]->var_duration =
			   data[i]->calls_count_tot == 0 ? 0.0f :
			   data[i]->var_duration / (f32) data[i]->calls_count_tot;
			data[i]->sd_duration = sqrtf( data[i]->var_duration );
		}
	}
}

internal
void
prof_gather_one_frame()
{
	PROFILE_SECTION();

	while ( !global_game_mem ) {}

	Prof_Data * prof = &global_game_mem->prof;
	if ( prof->is_paused )
	{
		for ( u32 tid = 0; tid < GAME_API_MAX_THREADS_NUM; ++tid )
		{
			Thread_Times_Buff * ttb = prof->threads_time_buffs + tid;
			while ( prof_pop_time_value( ttb ).time );
		}
		return;
	}

	u32 frame_id = prof->frames_data.last_frame;
	u32 prev_frame_id = frame_id ? frame_id - 1 : GAME_API_MAX_THREADS_NUM - 1;
	Prof_Frames * frames_data = &prof->frames_data;
	u64 frame_end_time = {};

	for ( u32 tid = 0; tid < GAME_API_MAX_THREADS_NUM; ++tid )
	{
		Prof_Section_Thread_Frame * t_frame = frames_data->frames[tid] + frame_id;
		Prof_Section * survived_sec = NULL;

		Prof_Section_Thread_Frame * prev_frame =
		   frames_data->frames[tid] + prev_frame_id;
		if ( prev_frame->last && !prev_frame->last->section->end_time )
		{
			survived_sec = prev_frame->last->section;
		} else
		if ( !prev_frame->last &&
		     prev_frame->section && !prev_frame->section->end_time )
		{
			survived_sec = prev_frame->section;
		}
		#if 0
		if (
		     ( prev_frame->last && prev_frame->last->section &&
		       prev_frame->last->section->function &&
		       strcmp( prev_frame->last->section->function,
		               "void game_api_audio_callback(System_API *, Game_Memory *, f32 *, u32)" ) == 0 )
		     ||
		     ( prev_frame->section && prev_frame->section->function &&
		       strcmp( prev_frame->section->function,
		               "void game_api_audio_callback(System_API *, Game_Memory *, f32 *, u32)" ) == 0 )
		   )
		{
			lpr_log_info( "survived = %p", survived_sec );
			if ( survived_sec )
			{
				lpr_log_info( "survived function= %s", survived_sec->function );
			}
		}
		#endif

		{
			// print_frame( t_frame );
			// NOTE(theGiallo): clear old frame

			u32 next_frame_id =
			   frame_id == MAX_PROF_FRAMES - 1 ? 0 : frame_id + 1;
			Prof_Section_Thread_Frame * next_frame =
			   frames_data->frames[tid] + next_frame_id;
			if ( t_frame->last &&
			     next_frame->section == t_frame->last->section )
			{
				Prof_Section_Child_List_Node * to_del = t_frame->last;
				if ( t_frame->last->prev )
				{
					t_frame->last->prev->next = NULL;
				} else
				{
					lpr_assert( t_frame->next == t_frame->last );
					t_frame->next = t_frame->last = NULL;
				}
				deallocate( &frames_data->sections_childs_mh, to_del );
			}

			if ( !t_frame->last &&
			     next_frame->section == t_frame->section )
			{
				t_frame->section = NULL;
				t_frame->next = t_frame->last = NULL;
			}

			Prof_Section * sec_to_del;
			sec_to_del = t_frame->section;

			Prof_Section_Child_List_Node * nodes_to_del = t_frame->next;
			if ( sec_to_del && sec_to_del->childs_list )
			{
				sec_to_del->childs_list_last_node->next = nodes_to_del;
				nodes_to_del = sec_to_del->childs_list;
			}
			if ( sec_to_del )
			{
				deallocate( &frames_data->sections_mh, sec_to_del );
			}
			while ( nodes_to_del )
			{
				Prof_Section_Child_List_Node * del_me = nodes_to_del;
				sec_to_del = nodes_to_del->section;
				lpr_assert( nodes_to_del != nodes_to_del->next );
				if ( nodes_to_del->next )
				{
					lpr_assert( nodes_to_del != nodes_to_del->next->next );
				}
				nodes_to_del = nodes_to_del->next;

				if ( sec_to_del->childs_list_last_node )
				{
					sec_to_del->childs_list_last_node->next = nodes_to_del;

					// TODO(theGiallo, 2016-06-07): this should be useless
					if ( sec_to_del->childs_list_last_node->next )
					{
						sec_to_del->childs_list_last_node->next->prev =
						sec_to_del->childs_list_last_node;
					}

					nodes_to_del = sec_to_del->childs_list;
				}

				del_me->next = (Prof_Section_Child_List_Node*)0xcafebabe;
#if TGPROF_COPY_STRINGS
				while( !global_game_mem ) {};
				Mem_Pool * mem_pool =
				   &global_game_mem->prof.strings_mem_pools[sec_to_del->thread_id];
				// TODO(theGiallo, 2016-08-05): now this runs in parallel and
				// makes bad stuff. Use a deallocation lockless list or mutex it
				mem_pool->deallocate( (void*)sec_to_del->name     );
				mem_pool->deallocate( (void*)sec_to_del->function );
				mem_pool->deallocate( (void*)sec_to_del->file     );
#endif
				deallocate( &frames_data->sections_mh, sec_to_del );
				deallocate( &frames_data->sections_childs_mh, del_me );
			}
		}
		memset( t_frame, 0, sizeof( *t_frame ) );
		lpr_assert( !t_frame->next   );
		lpr_assert( !t_frame->last   );
		lpr_assert( !t_frame->section);
		t_frame->section = survived_sec;
		Prof_Section * sec = survived_sec;

		Thread_Times_Buff * ttb = prof->threads_time_buffs + tid;
		Prof_Time_Value tv;
		while ( ( tv = prof_pop_time_value( ttb ) ).time )
		{
			if ( tv.tag_open_close == TAG_OPEN )
			{
				Prof_Section * new_sec =
				   allocate( &frames_data->sections_mh );
				if ( !new_sec )
				{
					lpr_log_err( "profile has no more free sections!" );
					return;
				}
				new_sec->start_time  = tv.time;
				new_sec->end_time    = {};
				new_sec->name        = tv.name;
				new_sec->function    = tv.function;
				new_sec->file        = tv.file;
				new_sec->line        = tv.line;
				new_sec->thread_id   = tv.thread_id;
				new_sec->childs_list = NULL;
				new_sec->childs_list_last_node = NULL;
				new_sec->parent      = sec;

				if ( !t_frame->section )
				{
					t_frame->section = new_sec;
				} else
				{
					Prof_Section_Child_List_Node * child =
					   allocate( &frames_data->sections_childs_mh );
					if ( !child )
					{
						lpr_log_err( "profile has no more free child nodes!" );
						deallocate( &frames_data->sections_mh, new_sec );
						return;
					}
					child->next    = NULL;
					child->prev    = NULL;
					child->section = new_sec;
					if ( sec )
					{
						if ( sec->childs_list )
						{
							lpr_assert(
							   sec->childs_list_last_node->section->end_time );
							child->prev = sec->childs_list_last_node;
							sec->childs_list_last_node->next = child;
							sec->childs_list_last_node       = child;
						} else
						{
							sec->childs_list           = child;
							sec->childs_list_last_node = child;
						}
					} else
					{
						if ( !t_frame->next )
						{
							t_frame->next = t_frame->last = child;
						} else
						{
							child->prev = t_frame->last;
							t_frame->last->next = child;
							t_frame->last = child;
						}
					}
				}
				sec = new_sec;
			} else // NOTE(theGiallo): TAG_OPEN end
			{
				// NOTE(theGiallo): TAG_CLOSE
				if ( sec )
				{
					lpr_assert( !sec->end_time );
					if( sec->name != tv.name )
					{
						lpr_log_err( "sec->name != tv.name\n"
						             "   name: %s\n"
						             "   file: %s\n"
						             "   line: %u\n"
						             "   function: %s\n"
						             "   tv.name: %s\n",
						             sec->name, sec->file, sec->line,
						             sec->function, tv.name );
					}
					if( sec->function != tv.function )
					{
						lpr_log_err( "sec->function != tv.function\n"
						             "   name: %s\n"
						             "   file: %s\n"
						             "   line: %u\n"
						             "   function: %s\n"
						             "   tv.function: %s\n",
						             sec->name, sec->file, sec->line,
						             sec->function, tv.function );
					}
					if ( sec->file != tv.file )
					{
						lpr_log_err( "sec->file != tv.file\n"
						             "   name: %s\n"
						             "   file: %s\n"
						             "   line: %u\n"
						             "   function: %s\n"
						             "   tv.file: %s\n",
						             sec->name, sec->file, sec->line,
						             sec->function, tv.file );
					}
					if ( sec->line != tv.line )
					{
						lpr_log_err( "sec->line != tv.line\n"
						             "   name: %s\n"
						             "   file: %s\n"
						             "   line: %u\n"
						             "   function: %s\n"
						             "   tv.line: %u\n",
						             sec->name, sec->file, sec->line,
						             sec->function, tv.line );
					}
					sec->end_time = tv.time;
					#if 0
					lpr_log_info( "   function: %s\n   start: %lu\n   end  : %lu",
					              sec->function, sec->start_time, sec->end_time );
					#endif
					sec = sec->parent;
				}
				if ( !sec && !frames_data->bootstrapped )
				{
					if ( tv.name && strcmp( tv.name, PROF_FRAME_NAME ) == 0 )
					{
						frame_end_time = tv.time;
						frames_data->bootstrapped = true;
						break;
					}
				} else
				if ( !sec && tid == SYS_API_MAIN_THREAD_ID )
				{
					if( !tv.name || strcmp(tv.name, PROF_FRAME_NAME) != 0 )
					{
						lpr_log_err( "tv.name = %s != " PROF_FRAME_NAME , tv.name );
						lpr_log_err( "tv.function %s = ", tv.function );
					} else
					{

						frame_end_time = tv.time;
						break;
					}
				}
			}

			if ( frame_end_time && tv.time >= frame_end_time )
			{
				break;
			}
		}
		//print_frame( t_frame );
		//if ( tid == 0 )
		{
			Prof_Section_Thread_Frame * t_frame =
			   prof->frames_data.frames[tid] + frame_id;
			Prof_Top_Hash_Table * hash_table =
			   prof->top_hash_tables + frame_id;

			if ( tid == 0 )
			{
				do
				{
					for ( u32 i = 0; i < ARRAY_COUNT( hash_table->hash_table ); ++i )
					{
						Prof_Top_HT_Node ** htnode = hash_table->hash_table + i;
						while( *htnode )
						{
							Prof_Top_HT_Node * oldhtnode = *htnode;
							*htnode = NULL;
							htnode = &oldhtnode->next;
							hash_table->nodes_mh.deallocate( oldhtnode );
						}
					}
				} while( false );
			}

			Prof_Section_Child_List_Node stub_child;
			stub_child.section = t_frame->section;
			stub_child.next = t_frame->next;

			compute_prof_top( &stub_child, hash_table, tid );
		}
	}

	frames_data->last_frame = frame_id == MAX_PROF_FRAMES - 1 ? 0 : frame_id + 1;
}

struct
Defer_Gather_One_Frame
{
	inline
	~Defer_Gather_One_Frame()
	{
		prof_gather_one_frame();
	}
};

#endif /* ifndef TG_PROF_H */
