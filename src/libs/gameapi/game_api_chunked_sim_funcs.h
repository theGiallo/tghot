#ifndef GAME_API_CHUNKED_SIM_FUNCS_H
#define GAME_API_CHUNKED_SIM_FUNCS_H 1

#include "game_api.h"
#include "game_api_chunked_sim.h"

inline
bool
spawn_sphere( Game_Memory::Simulation_Test_Data * sim, COD_Chunk * c,
              V3 pos, V3 vel = {}, V3 acc = {}, f32 mass = 1.0,
              Quat rot = make_Quat_no_rotation(), V3 ang_vel = {}, V3 ang_acc = {} )
{
	bool ret = true;

	u64 id = lpr_xorshift128plus( sim->entities_ids_seeds );

	Generic_Table_Multicolumn * table = c->tables + sim->dynamic_physics_data_table_id;
	Dynamic_Physics_Data element_struct = {};
	element_struct.pos_0 =
	element_struct.pos_1 = pos;
	element_struct.vel_0 =
	element_struct.vel_1 = vel;
	element_struct.acc_0 =
	element_struct.acc_1 = acc;
	element_struct.rot_0 =
	element_struct.rot_1 = rot;
	element_struct.ang_vel_0 =
	element_struct.ang_vel_1 = ang_vel;
	element_struct.ang_acc_0 =
	element_struct.ang_acc_1 = ang_acc;
	element_struct.inverse_mass = 1.0f / mass;
	if ( !generic_table_multicolumn_insert_struct( table, id, &element_struct ) )
	{
		lpr_log_err( "failed to insert into table" );
		ret = false;
		return ret;
	} else
	{
		if ( !u64_to_B12_hash_table_insert(
		         &sim->global_tables_status->ids_to_chunks_ht,
		         id, {.v3s32 = {c->idx_x,c->idx_y,c->idx_z}} ) )
		{
			lpr_log_err( "couldn't insert into ids-to-chunks hash table" );
			ret = false;
			return ret;
		} else
		{
			sim->last_created_id = id;
			lpr_log_dbg( "inserted sphere with pos (%f, %f %f) in chunk (%d, %d, %d), id = 0x%016x",
			             pos.x, pos.y, pos.z, c->idx_x, c->idx_y, c->idx_z, id );
		}
	}

	Generic_Table_Multicolumn * collision_sphere_table = c->tables + sim->collision_sphere_table_id;
	Collision_Sphere sphere = { .relative_position = {}, .radius = 1.0f };
	if ( !generic_table_multicolumn_insert_struct( collision_sphere_table, id, &sphere ) )
	{
		lpr_log_err( "failed to insert into table" );
		ret = false;
		return ret;
	}

	return ret;
}

#endif /* ifndef GAME_API_CHUNKED_SIM_FUNCS_H */
