#ifndef GAME_API_CACHE_ORIENTED_DESIGN_H
#define GAME_API_CACHE_ORIENTED_DESIGN_H 1

// NOTE(theGiallo): data structures and functions for cache oriented design
// ( what all other programmers call data oriented design or DOD )

#include "basic_types.h"
#include "game_api_lepre.h"
#include "game_api_utility.h"

////////////////////////////////////////////////////////////////////////////////

// NOTE(theGiallo): each array is 64bit aligned, it's up to the user to make
// each data type multiple of 8B.
struct
Generic_Table_Multicolumn
{
	u64_to_u32_Hash_Table ids_location_ht;
	u8   *  in_use_bv;
	u64  *  ids;
	void ** data;
	u32  *  data_elements_sizes;
	// NOTE(theGiallo): this has to stay at the end of the memory block
	u8   *  occupancy_per_255_blocks;
	u32 data_count;
	u32 occupancy;
	u32 capacity;
	bool is_sorted;
};

inline
u64
generic_table_multicolumn_compute_memory_size(
   u32 * data_elements_sizes, u32 data_count,
   u32 capacity )
{
	u64 ret =
	   size_round_to_64bits_alignment( u64_to_u32_hash_table_compute_memory_size( capacity ) ) +
	   size_round_to_64bits_alignment( COD_BITNSLOTS(capacity) ) +
	   size_round_to_64bits_alignment( sizeof(data_elements_sizes[0]) * data_count ) +
	   size_round_to_64bits_alignment( sizeof(Generic_Table_Multicolumn::ids[0]) * capacity ) +
	   size_round_to_64bits_alignment( sizeof(Generic_Table_Multicolumn::data[0]) * data_count ) +
	   ( sizeof(u8) * ( capacity + 254 / 255 ) );

	for ( u32 i = 0; i != data_count; ++i )
	{
		ret +=
		   size_round_to_64bits_alignment( data_elements_sizes[i] * capacity );
	}

	return ret;
}

inline
u8 *
generic_table_multicolumn_init( Generic_Table_Multicolumn * t, u8 * mem,
                                u32 * data_elements_sizes, u32 data_count,
                                u32 capacity )
{
	lpr_assert( COD_POINTER_IS_64BITS_ALIGNED(mem) );

	mem = u64_to_u32_hash_table_init( &t->ids_location_ht, mem, capacity );
	mem = memory_to_next_64bits_alignment( mem );

	t->in_use_bv = mem;
	mem += size_round_to_64bits_alignment( COD_BITNSLOTS(capacity) );

	t->ids = (u64*)mem;
	mem += size_round_to_64bits_alignment( sizeof(t->ids[0]) * capacity );

	t->data = (void**)mem;
	mem +=
	   size_round_to_64bits_alignment(
	      sizeof(Generic_Table_Multicolumn::data[0]) * data_count );
	for ( u32 i = 0; i != data_count; ++i )
	{
		t->data[i] = mem;
		mem += size_round_to_64bits_alignment( data_elements_sizes[i] * capacity );
	}

	t->data_elements_sizes = (u32*)mem;
	mem +=
	   size_round_to_64bits_alignment(
	      sizeof(t->data_elements_sizes[0]) * data_count );
	for ( u32 i = 0; i != data_count; ++i )
	{
		t->data_elements_sizes[i] = data_elements_sizes[i];
	}

	t->occupancy_per_255_blocks = mem;
	mem += ( sizeof(u8) * ( capacity + 254 / 255 ) );

	t->data_count = data_count;
	t->capacity   = capacity;
	t->is_sorted  = false;
	t->occupancy  = 0;

	return mem;
}

void
generic_table_multicolumn_compact( Generic_Table_Multicolumn * t );

// NOTE(theGiallo): does not checks for duplicates
bool
generic_table_multicolumn_insert_struct( Generic_Table_Multicolumn * t, u64 id, void * element_struct );

// NOTE(theGiallo): does not checks for duplicates
// NOTE(theGiallo): data is in the same format as the out parameter 'data' of the search
bool
generic_table_multicolumn_insert_pointers( Generic_Table_Multicolumn * t, u64 id, void ** data );

void *
generic_table_multicolumn_search_single_data( Generic_Table_Multicolumn * t, u64 id, u32 data_index );

void
generic_table_multicolumn_copy_data_to_struct( Generic_Table_Multicolumn * t, u32 entry_index, void * struct_ptr );

void
generic_table_multicolumn_fill_pointers( Generic_Table_Multicolumn * t, u32 entry_index, void ** data )
{
	u32 data_count = t->data_count;
	u8 ** t_data = (u8**)t->data;
	u32 * data_elements_sizes = t->data_elements_sizes;
	for ( u32 d = 0; d != data_count; ++d )
	{
		u32 e_s = data_elements_sizes[d];
		data[d] = t_data[d] + entry_index * e_s;
	}
}

// NOTE(theGiallo): returns the internal storage position or negative
s32
generic_table_multicolumn_search( Generic_Table_Multicolumn * t, u64 id );
// NOTE(theGiallo): returns the internal storage position or negative
// data is written, must not be NULL if is the case
s32
generic_table_multicolumn_search( Generic_Table_Multicolumn * t, u64 id, void ** data );

// NOTE(theGiallo): returns number of entries found and allocates in mem_stack
// an array of u32 storing in there the indexes of such entries
u32
generic_table_multicolumn_search_many( Generic_Table_Multicolumn * t, u64 id, Mem_Stack * mem_stack );

bool
generic_table_multicolumn_remove( Generic_Table_Multicolumn * t, u64 id );

u32
generic_table_multicolumn_remove_many( Generic_Table_Multicolumn * t, u64 id, Mem_Stack * mem_stack_tmp );

u32
generic_table_multicolumn_remove_many_and_feedback( Generic_Table_Multicolumn * t, u64 id, Mem_Stack * mem_stack );

inline
void
generic_table_multicolumn_clear( Generic_Table_Multicolumn * t )
{
	u64_to_u32_hash_table_clear( &t->ids_location_ht );
	memset( t->in_use_bv, 0x00, COD_BITNSLOTS(t->capacity) );
	memset( t->occupancy_per_255_blocks, 0x00, ( t->capacity + 254 / 255 ) );
	t->occupancy = 0;
}


////////////////////////////////////////////////////////////////////////////////

struct
Generic_Table
{
	u8   * in_use_bv;
	u64  * ids;
	void * data;
	u32 occupancy;
	u32 capacity;
	u32 data_element_size;
	bool is_sorted;
};

inline
u64
generic_table_compute_memory_size( u32 data_element_size, u32 capacity )
{
	u64 ret = COD_BITNSLOTS(capacity) +
	( sizeof(Generic_Table::ids[0]) + data_element_size ) * capacity;
	return ret;
}

inline
u8 *
generic_table_init( Generic_Table * t, u8 * mem,
                    u32 data_element_size,
                    u32 capacity )
{
	lpr_assert( COD_POINTER_IS_64BITS_ALIGNED(mem) );

	t->in_use_bv = mem;
	mem += COD_BITNSLOTS(capacity);
	t->ids = (u64*)mem;
	mem += sizeof(t->ids[0]) * capacity;
	t->data = mem;
	t->capacity = capacity;
	t->data_element_size = data_element_size;
	t->is_sorted = false;
	t->occupancy = 0;

	return mem;
}

void
generic_table_compact( Generic_Table * t );

bool
generic_table_insert( Generic_Table * t, u64 id, void * element );

void *
generic_table_search( Generic_Table * t, u64 id );

bool
generic_table_remove( Generic_Table * t, u64 id );

////////////////////////////////////////////////////////////////////////////////

void
cod_test( Mem_Stack * mem );

#endif /* ifndef GAME_API_CACHE_ORIENTED_DESIGN_H */
