#include "game_api_opengl_tools.h"
#include "game_api_utility.h"
#include "global_sys_api.h"

// NOTE(theGiallo): h in [0,1)
Lpr_Rgba_f32
rgba_from_hsva( Lpr_Rgba_f32 hsva )
{
	Lpr_Rgba_f32 ret;
	ret.a = hsva.a;

	ret.lpr_rgb_f32 = rgb_from_hsv( hsva.lpr_rgb_f32 );

	return ret;
}
Lpr_Rgb_f32
rgb_from_hsv( Lpr_Rgb_f32 hsv )
{
	Lpr_Rgb_f32 ret;
	f32 c = hsv.g * hsv.b;
	f32 hh = hsv.r * 6.0;

	f32 x = c * ( 1.0 - abs( hh - 2.0f * floorf( hh / 2.0f ) - 1.0 ) );

	if ( hh < 1.0 || hh >= 6.0 )
	{
		ret.r = c;
		ret.g = x;
		ret.b = 0;
	} else
	if ( hh < 2.0 )
	{
		ret.r = x;
		ret.g = c;
		ret.b = 0;
	} else
	if ( hh < 3.0 )
	{
		ret.r = 0;
		ret.g = c;
		ret.b = x;
	} else
	if ( hh < 4.0 )
	{
		ret.r = 0;
		ret.g = x;
		ret.b = c;
	} else
	if ( hh < 5.0 )
	{
		ret.r = x;
		ret.g = 0;
		ret.b = c;
	} else
	if ( hh < 6.0 )
	{
		ret.r = c;
		ret.g = 0;
		ret.b = x;
	}

	f32 m = hsv.b - c;
	ret.r += m;
	ret.g += m;
	ret.b += m;

	return ret;
}

V3
rgb_from_v2( V2 v )
{
	f32 hue = angle_rad( v ) / TAU_F;
	Lpr_Rgb_f32 col = rgb_from_hsv( Lpr_Rgb_f32{hue, 0.7f, 1.0f} );
	V3 ret = { col.r, col.g, col.b };
	return ret;
}

inline
void
initialize_camera( Camera3D * camera, f32 inverse_aspect_ratio, f32 near, f32 fov )
{
	camera->pos  = V3{};
	camera->look = V3{1.0f,0.0f,0.0f};
	camera->up   = V3{0.0f,0.0f,1.0f};
	update_view( camera );
	set_projection( camera, near, V2{0.5f,0.5f}, fov,
	                inverse_aspect_ratio );
}

inline
void
resize_camera( Camera3D * camera, u32 width, u32 height )
{
	f32 inverse_aspect_ratio =
	   (f32)height / (f32)width;
	set_projection( camera, camera->near, camera->vanishing_point, camera->fov,
	                inverse_aspect_ratio );
}

inline
void
check_up( Camera3D * camera )
{
	camera->up =
	   ( camera->look == V3{0.0f,0.0f,1.0f} ) ?
	   V3{0.0f,1.0f,0.0f} : V3{0.0f,0.0f,1.0f};
}
inline
void
update_view( Camera3D * camera )
{
	camera->V = view_mx4( camera->look, camera->up, camera->pos );
}
inline
void
set_projection( Camera3D * camera, f32 near, V2 vanishing_point, V2 size )
{
	camera->P = perspective_infinite_mx4( near, vanishing_point, size );
	camera->fov = fov_from_near_plane_size( near, size.w );
	camera->size = size;
	camera->vanishing_point = vanishing_point;
	camera->near = near;
}
inline
void
set_projection( Camera3D * camera, f32 near, V2 vanishing_point, f32 fov, f32 inverse_aspect_ratio )
{
#if 1
	camera->P =
	   perspective_infinite_mx4( near, vanishing_point, fov,
	                             inverse_aspect_ratio, &camera->size );
#else
	f32 far = 10000;
	camera->P =
	   perspective_mx4( near, far, vanishing_point, fov,
	                    inverse_aspect_ratio, &camera->size );
#endif
	camera->near = near;
	camera->vanishing_point = vanishing_point;
	camera->fov = fov;
	camera->inverse_aspect_ratio = inverse_aspect_ratio;
}

inline
V2u32
compute_window_center( Game_Memory * game_mem )
{
	V2u32 ret =
	   make_V2u32( game_mem->main_window.lpr_draw_data.window_size );
	ret /= 2;
	return ret;
}

inline
void
warp_mouse_to_center( System_API * sys_api, Game_Memory * game_mem )
{
	V2u32 origin =
	   make_V2u32( game_mem->main_window.lpr_draw_data.window_size );
	origin /= 2;
	sys_api->warp_mouse_in_window(
	   game_mem->main_window.window_id, origin.x,origin.y );
}

// NOTE(theGiallo):
// dir should be positive if the key is ahead or right
// dir should be negative if the key is back or left
inline
void
manage_motion_key( Game_Memory * game_mem, Phys_Key key, s32 dir, f32 * var )
{
	//s32 is_pressed = (s32)game_mem->inputs.keyboard_state[key];
	s32 pre_rel =
	(s32)game_mem->inputs.keyboard_pressions[key] -
	(s32)game_mem->inputs.keyboard_releases [key];
	if ( pre_rel > 0 )
	{
		*var += 1.0f * sign( dir );
	} else
	if ( pre_rel < 0 )
	{
		*var -= 1.0f * sign( dir );
	}
}

// NOTE(theGiallo): ahead, right and up are supposed to be normalized
// motion values are supposed to be +1, 0 or-1
inline
void
apply_controlled_motion( Controlled_Motion motion, V3 ahead, V3 right, V3 up,
                         Distributed_Speed * distributed_speed, f32 speed_mod,
                         Time delta_time, V3 * pos )
{
	*pos +=
	(
	   ahead *
	      ( motion.straight *
	         ( motion.straight > 0.0f ?
	            distributed_speed->positive.straight :
	            distributed_speed->negative.straight ) ) +
	   right *
	      ( motion.side     *
	         ( motion.side > 0.0f ?
	            distributed_speed->positive.side :
	            distributed_speed->negative.side )     ) +
	   up    *
	      ( motion.vertical *
	         ( motion.vertical > 0.0f ?
	            distributed_speed->positive.vertical :
	            distributed_speed->negative.vertical ) )
	) * ( delta_time * speed_mod );
}

inline
void
apply_controlled_motion( Controlled_Motion motion, Camera3D * camera,
                         Distributed_Speed * distributed_speed, f32 speed_mod,
                         Time delta_time )
{
	apply_controlled_motion( motion,
	                         normalize( camera->look ),
	                         normalize( camera->look ^ camera->up ),
	                         camera->up,
	                         distributed_speed, speed_mod,
	                         delta_time, &camera->pos );
}

inline
void
per_frame_camera_controls_and_update(
   Game_Memory * game_mem, Camera3D * camera, Controlled_Motion * camera_motion,
   Distributed_Speed * distributed_camera_speed, f32 shift_speed_mod = 10.0f )
{
	manage_motion_key( game_mem, PK_W,      1, &camera_motion->straight );
	manage_motion_key( game_mem, PK_S,     -1, &camera_motion->straight );
	manage_motion_key( game_mem, PK_D,      1, &camera_motion->side     );
	manage_motion_key( game_mem, PK_A,     -1, &camera_motion->side     );
	manage_motion_key( game_mem, PK_SPACE,  1, &camera_motion->vertical );
	manage_motion_key( game_mem, PK_C,     -1, &camera_motion->vertical );

	f32 speed_mod = 1.0f;
	if ( game_mem->inputs.keyboard_state[PK_LSHIFT] )
	{
		speed_mod = shift_speed_mod;
	}

	apply_controlled_motion( *camera_motion, camera,
	                         distributed_camera_speed,
	                         speed_mod,
	                         game_mem->delta_time );

	f32 a = 5.0f * game_mem->delta_time * speed_mod;
	if (  game_mem->inputs.keyboard_state[PK_Q] &&
	     !game_mem->inputs.keyboard_state[PK_E] )
	{
		Mx4 rot_up =
		   rotation_mx4( camera->up, a );
		camera->look = normalize( rot_up * camera->look );
	}
	if ( !game_mem->inputs.keyboard_state[PK_Q] &&
	      game_mem->inputs.keyboard_state[PK_E] )
	{
		Mx4 rot_up =
		   rotation_mx4( camera->up, -a );
		camera->look = normalize( rot_up * camera->look );
	}

	check_up( camera );
	update_view( camera );
}

inline
bool
fp_camera_mouse_motion( System_API * sys_api, Game_Memory * game_mem,
                        s32 pos_x, s32 pos_y,
                        s32 motion_x, s32 motion_y, Camera3D * camera )
{
	bool ret = false;
	V2u32 window_center = compute_window_center( game_mem );
	if ( pos_x != (s32)window_center.x ||
	     pos_y != (s32)window_center.y )
	{
		ret = true;
		V2 win_size =
		   (V2)make_V2u32( game_mem->main_window.lpr_draw_data.window_size );
		f32 angle_per_px = camera->fov / win_size.w;
		f32 u_a = -angle_per_px * motion_x;
		f32 r_a =  angle_per_px * motion_y;
		V3 right = normalize( camera->look ^ camera->up );
		Mx4 rot_right =
		   rotation_mx4( right, r_a );
		Mx4 rot_up =
		   rotation_mx4( camera->up, u_a );
		Mx4 rot = rot_up * rot_right;
		camera->look = normalize( rot * camera->look );
		update_view( camera );
		sys_api->warp_mouse_in_window(
		   game_mem->main_window.window_id,
		   window_center.x,window_center.y );
	}

	return ret;
}

inline
void
set_ogl_camera( Shader * shader, Camera3D * camera )
{
	glUseProgram( shader->program );
	if ( shader->VP_loc != -1 )
	{
		glUniformMatrix4fv( shader->VP_loc, 1, GL_FALSE,
		                    camera->VP.arr );
	}
	if ( shader->V_loc != -1 )
	{
		glUniformMatrix4fv( shader->V_loc, 1, GL_FALSE,
		                    camera->V.arr );
	}
	if ( shader->P_loc != -1 )
	{
		glUniformMatrix4fv( shader->P_loc, 1, GL_FALSE,
		                    camera->P.arr );
	}
}

void
render_colored_vertices( Shader * shader,
                         Colored_Vertex * vertices, u32 vertices_in_buffer,
                         u32 vertices_count,
                         Lpr_Rgba_f32 * default_color,
                         GLuint vbo, GLenum draw_mode, GLenum buffer_usage )
{
	glUseProgram( shader->program );
	if ( shader->color_loc != -1 && default_color )
	{
		glUniform4f( shader->color_loc, default_color->r, default_color->g, default_color->b, default_color->a );// 0.0f, 0.05f, 0.4f, 0.5f );
	}

	if ( shader->M_loc != -1 )
	{
		glUniformMatrix4fv( shader->M_loc, 1, GL_FALSE, mx4_identity().arr );
	}

	u32 buffer_byte_size = vertices_in_buffer * sizeof( Colored_Vertex );
	glBindBuffer( GL_ARRAY_BUFFER, vbo );
	glBufferData( GL_ARRAY_BUFFER, buffer_byte_size,
	              NULL,  buffer_usage );
	glBufferData( GL_ARRAY_BUFFER, buffer_byte_size,
	              vertices,  buffer_usage );

	u32 stride = sizeof( Colored_Vertex );
	glEnableVertexAttribArray( shader->vertex_loc );
	glVertexAttribPointer( shader->vertex_loc, // location
	                       3,        // size
	                       GL_FLOAT, // type
	                       GL_FALSE, // normalized
	                       stride,   // stride
	                       (void*)0  // array buffer offset
	                     );

	glEnableVertexAttribArray( shader->vertex_color_loc );
	glVertexAttribPointer( shader->vertex_color_loc, // location
	                       3,        // size
	                       GL_FLOAT, // type
	                       GL_FALSE, // normalized
	                       stride,   // stride
	                       (void*)sizeof( V3 ) // array buffer offset
	                     );
	glDrawArrays( draw_mode, 0, vertices_count );

	glDisableVertexAttribArray( shader->vertex_loc );
	glDisableVertexAttribArray( shader->vertex_color_loc );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glUseProgram( 0 );
}

bool
create_framebuffer_with_depth( V2u32 px_size,
                               Frame_Buffer_With_Depth * fb )
{
	if ( ! glIsFramebuffer( fb->linear_fbo ) )
	{
		glGenFramebuffers( 1, &fb->linear_fbo );
	}
	glBindFramebuffer( GL_FRAMEBUFFER, fb->linear_fbo );
	if ( ! glIsRenderbuffer( fb->linear_color_rt ) )
	{
		glGenRenderbuffers( 1, &fb->linear_color_rt );
	}
	glBindRenderbuffer(    GL_RENDERBUFFER, fb->linear_color_rt );
	glRenderbufferStorage( GL_RENDERBUFFER, GL_RGBA16F, px_size.w, px_size.h );
	glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
	                           GL_RENDERBUFFER, fb->linear_color_rt );

	glGenRenderbuffers( 1, &fb->depth_rt );
	glBindRenderbuffer(    GL_RENDERBUFFER, fb->depth_rt );
	glRenderbufferStorage( GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, px_size.w, px_size.h );
	glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
	                           GL_RENDERBUFFER, fb->depth_rt );

	// NOTE(theGiallo): Generate and bind the OGL texture for diffuse
	if ( ! glIsTexture( fb->linear_fb_texture ) )
	{
		glGenTextures( 1, &fb->linear_fb_texture );
	}
	glBindTexture( GL_TEXTURE_2D, fb->linear_fb_texture );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA16F, px_size.w, px_size.h, 0,
	              GL_RGBA, GL_HALF_FLOAT, NULL );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	// NOTE(theGiallo): Attach the texture to the FBO
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
	                        GL_TEXTURE_2D, fb->linear_fb_texture, 0 );

	// NOTE(theGiallo): Check if all worked fine and unbind the FBO
	if( !lpr_check_and_log_framebuffer_status() )
	{
		lpr_log_err( "Something failed during FBO initialization. FBO is not complete." );
		// TODO(theGiallo, 2015/09/28): proper clean
		glDeleteFramebuffers( 1, &fb->linear_fbo );
		glDeleteRenderbuffers( 1, &fb->linear_color_rt );
		glDeleteRenderbuffers( 1, &fb->depth_rt );
		LPR_ILLEGAL_PATH();
		return false;
	}
	lpr_log_framebuffer_info();

	// NOTE(theGiallo): OpenGL cleanup
	glBindTexture( GL_TEXTURE_2D, 0 );
	glBindFramebuffer( GL_FRAMEBUFFER, 0 );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	fb->size = px_size;

	return true;
}

bool
create_program_from_text( GLuint *     gl_program,
                          const char * geometry_shader_text,
                          GLint        geometry_shader_text_length,
                          const char * vertex_shader_text,
                          GLint        vertex_shader_text_length,
                          const char * fragment_shader_text,
                          GLint        fragment_shader_text_length )
{

	*gl_program = glCreateProgram( );
	GLuint gl_p = *gl_program;

	GLuint gl_shader_geometry,
	       gl_shader_vertex,
	       gl_shader_fragment;

	if ( geometry_shader_text )
	{
		gl_shader_geometry = glCreateShader( GL_GEOMETRY_SHADER   );
	}
	gl_shader_vertex   = glCreateShader( GL_VERTEX_SHADER );
	gl_shader_fragment = glCreateShader( GL_FRAGMENT_SHADER );

	GLint compiled;
	int info_log_length;

	if ( ( geometry_shader_text && !gl_shader_geometry ) ||
	     !gl_shader_vertex || !gl_shader_fragment )
	{
		lpr_log_err( "Error creating shader objects." );

		glDeleteProgram( gl_p );

		if ( geometry_shader_text )
		{
			glDetachShader( gl_p, gl_shader_geometry );
			glDeleteShader( gl_shader_geometry );
		}

		glDetachShader( gl_p, gl_shader_vertex );
		glDeleteShader( gl_shader_vertex );

		glDetachShader( gl_p, gl_shader_fragment );
		glDeleteShader( gl_shader_fragment );
		return false;
	}

	if ( geometry_shader_text )
	{
		// NOTE(theGiallo): compile and attach geometry shader
		glShaderSource( gl_shader_geometry,                    // shader
		                1,                                     // count
		                (const GLchar**)&geometry_shader_text, // string
		                &geometry_shader_text_length );        // length
		glCompileShader( gl_shader_geometry );

		// NOTE(theGiallo): check geometry shader compilation
		compiled = GL_FALSE;
		info_log_length = 0;
		glGetShaderiv( gl_shader_geometry, GL_COMPILE_STATUS,  &compiled );
		glGetShaderiv( gl_shader_geometry, GL_INFO_LOG_LENGTH, &info_log_length );
		if ( info_log_length > 0 )
		{
			char shader_info_log[info_log_length+1];
			glGetShaderInfoLog( gl_shader_geometry, info_log_length, NULL,
			                    shader_info_log );
			lpr_log_info( "\ngeometry shader compilation infos (%dB):\n" \
			              "----------------------------------------\n" \
			              "%s\n" \
			              "----------------------------------------\n",
			              info_log_length,
			              shader_info_log );
		}
		if ( compiled == GL_FALSE )
		{
			lpr_log_err( "Error compiling geometry shader." );

			glDeleteProgram( gl_p );

			glDetachShader( gl_p, gl_shader_geometry );
			glDeleteShader( gl_shader_geometry );

			glDetachShader( gl_p, gl_shader_vertex );
			glDeleteShader( gl_shader_vertex );

			glDetachShader( gl_p, gl_shader_fragment );
			glDeleteShader( gl_shader_fragment );
			return false;
		}
		// NOTE(theGiallo): compilation successfull => attach geometry shader
		glAttachShader( gl_p, gl_shader_geometry );
	}

	gl_shader_vertex   = glCreateShader( GL_VERTEX_SHADER   );
	gl_shader_fragment = glCreateShader( GL_FRAGMENT_SHADER );
	if ( !gl_shader_vertex || !gl_shader_fragment )
	{
		lpr_log_err( "Error creating shader objects." );

		glDeleteProgram( gl_p );

		if ( geometry_shader_text )
		{
			glDetachShader( gl_p, gl_shader_geometry );
			glDeleteShader( gl_shader_geometry );
		}

		glDetachShader( gl_p, gl_shader_vertex );
		glDeleteShader( gl_shader_vertex );

		glDetachShader( gl_p, gl_shader_fragment );
		glDeleteShader( gl_shader_fragment );
		return false;
	}

	// NOTE(theGiallo): compile and attach vertex shader
	glShaderSource( gl_shader_vertex,                    // shader
	                1,                                   // count
	                (const GLchar**)&vertex_shader_text, // string
	                &vertex_shader_text_length );        // length
	glCompileShader( gl_shader_vertex );

	// NOTE(theGiallo): check vertex shader compilation
	compiled = GL_FALSE;
	info_log_length = 0;
	glGetShaderiv( gl_shader_vertex, GL_COMPILE_STATUS,  &compiled );
	glGetShaderiv( gl_shader_vertex, GL_INFO_LOG_LENGTH, &info_log_length );
	if ( info_log_length > 0 )
	{
		char shader_info_log[info_log_length+1];
		glGetShaderInfoLog( gl_shader_vertex, info_log_length, NULL,
		                    shader_info_log );
		lpr_log_info( "\nVertex shader compilation infos (%dB):\n" \
		              "----------------------------------------\n" \
		              "%s\n" \
		              "----------------------------------------\n",
		              info_log_length,
		              shader_info_log );
	}
	if ( compiled == GL_FALSE )
	{
		lpr_log_err( "Error compiling vertex shader." );

		glDeleteProgram( gl_p );

		if ( geometry_shader_text )
		{
			glDetachShader( gl_p, gl_shader_geometry );
			glDeleteShader( gl_shader_geometry );
		}

		glDetachShader( gl_p, gl_shader_vertex );
		glDeleteShader( gl_shader_vertex );

		glDetachShader( gl_p, gl_shader_fragment );
		glDeleteShader( gl_shader_fragment );

		return lpr_false;
	}
	// NOTE(theGiallo): compilation successfull => attach vertex shader
	glAttachShader( gl_p, gl_shader_vertex );


	// NOTE(theGiallo): compile and attach fragment shader
	glShaderSource( gl_shader_fragment,                    // shader
	                1,                                     // count
	                (const GLchar**)&fragment_shader_text, // string
	                &fragment_shader_text_length );        // length
	glCompileShader( gl_shader_fragment );

	// NOTE(theGiallo): check fragment shader compilation
	compiled = GL_FALSE;
	info_log_length = 0;
	glGetShaderiv( gl_shader_fragment, GL_COMPILE_STATUS,  &compiled );
	glGetShaderiv( gl_shader_fragment, GL_INFO_LOG_LENGTH, &info_log_length );
	if ( info_log_length > 0 )
	{
		char shader_info_log[info_log_length+1];
		glGetShaderInfoLog( gl_shader_fragment, info_log_length, NULL,
		                    shader_info_log );
		lpr_log_info( "\nFragment shader compilation infos (%dB):\n" \
		              "----------------------------------------\n" \
		              "%s\n" \
		              "----------------------------------------\n",
		              info_log_length,
		              shader_info_log );
	}
	if ( compiled == GL_FALSE )
	{
		lpr_log_err( "Error compiling vertex shader." );

		glDeleteProgram( gl_p );

		if ( geometry_shader_text )
		{
			glDetachShader( gl_p, gl_shader_geometry );
			glDeleteShader( gl_shader_geometry );
		}

		glDetachShader( gl_p, gl_shader_vertex );
		glDeleteShader( gl_shader_vertex );

		glDetachShader( gl_p, gl_shader_fragment );
		glDeleteShader( gl_shader_fragment );
		return false;
	}
	// NOTE(theGiallo): compilation successfull => attach fragment shader
	glAttachShader( gl_p, gl_shader_fragment );


	// NOTE(theGiallo): link the program
	glLinkProgram( gl_p );

	// NOTE(theGiallo): check the program linking
	{
		GLint link_status = GL_FALSE;
		int info_log_length;
		glGetProgramiv(gl_p, GL_INFO_LOG_LENGTH, &info_log_length);
		if ( info_log_length > 0 )
		{
			char program_info_log[info_log_length+1];
			glGetProgramInfoLog( gl_p, info_log_length, NULL,
			                     program_info_log );
			lpr_log_info( "\nProgram linking infos:\n\n%s\n", program_info_log );
		}
		glGetProgramiv( gl_p, GL_LINK_STATUS, &link_status );
		if ( link_status == GL_FALSE )
		{
			lpr_log_err( "Error linking the program." );

			glDeleteProgram( gl_p );

			if ( geometry_shader_text )
			{
				glDetachShader( gl_p, gl_shader_geometry );
				glDeleteShader( gl_shader_geometry );
			}

			glDetachShader( gl_p, gl_shader_vertex );
			glDeleteShader( gl_shader_vertex );

			glDetachShader( gl_p, gl_shader_fragment );
			glDeleteShader( gl_shader_fragment );
			return false;
		}
	}

	glValidateProgram( gl_p );

	// NOTE(theGiallo): check the program validation
	{
		GLint validation_status = GL_FALSE;
		int info_log_length;
		glGetProgramiv(gl_p, GL_INFO_LOG_LENGTH, &info_log_length);
		if ( info_log_length > 0 )
		{
			char program_info_log[info_log_length+1];
			glGetProgramInfoLog( gl_p, info_log_length, NULL,
			                     program_info_log );
			lpr_log_info( "\nProgram validation infos:\n\n%s\n", program_info_log );
		}
		glGetProgramiv( gl_p, GL_VALIDATE_STATUS, &validation_status );
		if ( validation_status == GL_FALSE )
		{
			lpr_log_err( "Error validating the program." );

			glDeleteProgram( gl_p );

			if ( geometry_shader_text )
			{
				glDetachShader( gl_p, gl_shader_geometry );
				glDeleteShader( gl_shader_geometry );
			}

			glDetachShader( gl_p, gl_shader_vertex );
			glDeleteShader( gl_shader_vertex );

			glDetachShader( gl_p, gl_shader_fragment );
			glDeleteShader( gl_shader_fragment );
			return false;
		}
	}

	// NOTE(theGiallo): we will never modify or reuse the shader objects, so we can detach
	// and delete them.

	if ( geometry_shader_text )
	{
		glDetachShader( gl_p, gl_shader_geometry );
		glDeleteShader( gl_shader_geometry );
	}

	glDetachShader( gl_p, gl_shader_vertex );
	glDeleteShader( gl_shader_vertex );

	glDetachShader( gl_p, gl_shader_fragment );
	glDeleteShader( gl_shader_fragment );

	return true;
}

bool
create_program( System_API * sys_api, Game_Memory * game_mem,
                const char * geom, const char * vert, const char * frag,
                GLuint * out_program )
{
	bool ret = false;

	s64 size_geom = {};
	u8 * mem_geom = {};
	if ( geom )
	{

		size_geom = sys_api->get_file_size( geom );
		if ( size_geom < 0 )
		{
			return ret;
		}

		u64 old_top = game_mem->mem_stack_tmp.first_free;
		mem_geom = game_mem->mem_stack_tmp.push( size_geom );
		if ( !mem_geom )
		{
			return ret;
		}

		s64 res = sys_api->read_entire_file( geom, mem_geom, size_geom );
		if ( SYS_API_RET_IS_ERROR( res ) )
		{
			lpr_log_err( "failed to read geometry shader file '%s', error:\n%s\n%s",
			             geom, sys_api->get_error_name( res ),
			             sys_api->get_error_description( res ) );
			game_mem->mem_stack_tmp.first_free = old_top;
			return ret;
		}
	}

	s64 size_vert = sys_api->get_file_size( vert );
	if ( size_vert < 0 )
	{
		return ret;
	}

	u64 old_top = game_mem->mem_stack_tmp.first_free;
	u8 * mem_vert = game_mem->mem_stack_tmp.push( size_vert );
	if ( !mem_vert )
	{
		return ret;
	}

	s64 res = sys_api->read_entire_file( vert, mem_vert, size_vert );
	if ( SYS_API_RET_IS_ERROR( res ) )
	{
		lpr_log_err( "failed to read vertex shader file '%s', error:\n%s\n%s",
		             vert, sys_api->get_error_name( res ),
		             sys_api->get_error_description( res ) );
		game_mem->mem_stack_tmp.first_free = old_top;
		return ret;
	}

	s64 size_frag = sys_api->get_file_size( frag );
	if ( size_frag < 0 )
	{
		game_mem->mem_stack_tmp.first_free = old_top;
		return ret;
	}

	u8 * mem_frag = game_mem->mem_stack_tmp.push( size_frag );
	if ( !mem_frag )
	{
		return ret;
	}
	res = sys_api->read_entire_file( frag, mem_frag, size_frag );
	if ( SYS_API_RET_IS_ERROR( res ) )
	{
		lpr_log_err( "failed to read fragment shader file '%s', error:\n%s\n%s",
		             vert, sys_api->get_error_name( res ),
		             sys_api->get_error_description( res ) );
		game_mem->mem_stack_tmp.first_free = old_top;
		return ret;
	}

	GLuint program = {};
	if ( create_program_from_text( &program,
	                               (const char*)mem_geom, size_geom,
	                               (const char*)mem_vert, size_vert,
	                               (const char*)mem_frag, size_frag ) )
	{
		ret = true;
		if ( glIsProgram( *out_program ) )
		{
			glDeleteProgram( *out_program );
		}
		*out_program = program;
	}

	game_mem->mem_stack_tmp.first_free = old_top;
	return ret;
}

void
dynamically_update_shader( System_API * sys_api, Game_Memory * game_mem, Shader * shader )
{
	Time mod_time_geom, mod_time_vert, mod_time_frag;
	mod_time_geom = mod_time_vert = mod_time_frag = 0.0;
	sys_api->get_file_modified_time( shader->geom.file_path,
	                                      &mod_time_geom );
	if ( sys_api->get_file_modified_time( shader->vert.file_path,
	                                      &mod_time_vert ) >= 0 &&
	     sys_api->get_file_modified_time( shader->frag.file_path,
	                                      &mod_time_frag ) >= 0 )
	{
		if ( shader->geom.modified_time < mod_time_geom ||
		     shader->vert.modified_time < mod_time_vert ||
		     shader->frag.modified_time < mod_time_frag )
		{
			shader->geom.modified_time = mod_time_geom;
			shader->vert.modified_time = mod_time_vert;
			shader->frag.modified_time = mod_time_frag;

			lpr_log_dbg( "shader files modified, going to make a new program (%s)", shader->frag.file_path );

			if ( !create_program( sys_api, game_mem,
			                      shader->geom.file_path,
			                      shader->vert.file_path,
			                      shader->frag.file_path,
			                      &shader->program ) )
			{
				lpr_log_err( "failed to load or compile the new shader" );
			} else
			{
				s32 i_ret;
				#define LOAD_GL_LOC( gl_func, shader_variable, client_variable ) \
				   i_ret =                                                                      \
				      gl_func( shader->program, shader_variable );                              \
				   if( i_ret == -1 )                                                            \
				   {                                                                            \
				      lpr_log_dbg( "no '" shader_variable "' found. gl error = %u, i_ret = %d", \
				                   glGetError(), i_ret );                                       \
				   }                                                                            \
				   shader->client_variable = i_ret;                                             \

				LOAD_GL_LOC( glGetAttribLocation, "vertex", vertex_loc );
				LOAD_GL_LOC( glGetAttribLocation, "vertex_color", vertex_color_loc );
				LOAD_GL_LOC( glGetAttribLocation, "normal", normal_loc );
				LOAD_GL_LOC( glGetAttribLocation, "uv", uv_loc );
				LOAD_GL_LOC( glGetAttribLocation, "instance_M", instance_M_loc );
				LOAD_GL_LOC( glGetAttribLocation, "instance_color", instance_color_loc );
				LOAD_GL_LOC( glGetUniformLocation, "VP", VP_loc );
				LOAD_GL_LOC( glGetUniformLocation, "V", V_loc );
				LOAD_GL_LOC( glGetUniformLocation, "P", P_loc );
				LOAD_GL_LOC( glGetUniformLocation, "M", M_loc );
				LOAD_GL_LOC( glGetUniformLocation, "color", color_loc );

				#define LOAD_GL_LOCS( gl_func, prefix_type ) \
				for ( s32 i = 0; i != MAX_GENERIC_LOCS_COUNT; ++i )                \
				{                                                                  \
					char shader_variable[256] = {};                                \
					snprintf( shader_variable, sizeof( shader_variable ),          \
					          LPR_TOSTRING(prefix_type) "_%d", i );                \
					i_ret = gl_func( shader->program, shader_variable );           \
					if ( i_ret == -1 )                                             \
					{                                                              \
						lpr_log_dbg( "no '%s' found. gl error = %u, i_ret = %d",   \
						             shader_variable, glGetError(), i_ret );       \
					}                                                              \
					shader->prefix_type[i] = i_ret;                                \
				}                                                                  \

				LOAD_GL_LOCS( glGetAttribLocation, attr_float )
				LOAD_GL_LOCS( glGetAttribLocation, attr_vec2  )
				LOAD_GL_LOCS( glGetAttribLocation, attr_vec3  )
				LOAD_GL_LOCS( glGetAttribLocation, attr_vec4  )
				LOAD_GL_LOCS( glGetUniformLocation, unif_float )
				LOAD_GL_LOCS( glGetUniformLocation, unif_vec2  )
				LOAD_GL_LOCS( glGetUniformLocation, unif_vec3  )
				LOAD_GL_LOCS( glGetUniformLocation, unif_vec4  )
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////


void
init_grid_planes( Grid_Planes * planes )
{
	create_and_fill_ogl_buffer( &planes->points_vb,          GL_ARRAY_BUFFER, GL_STREAM_DRAW, NULL, 0 );
	create_and_fill_ogl_buffer( &planes->normals_vb,         GL_ARRAY_BUFFER, GL_STREAM_DRAW, NULL, 0 );
	create_and_fill_ogl_buffer( &planes->ups_vb,             GL_ARRAY_BUFFER, GL_STREAM_DRAW, NULL, 0 );
	create_and_fill_ogl_buffer( &planes->grid_sides_vb,      GL_ARRAY_BUFFER, GL_STREAM_DRAW, NULL, 0 );
	create_and_fill_ogl_buffer( &planes->grid_cell_sides_vb, GL_ARRAY_BUFFER, GL_STREAM_DRAW, NULL, 0 );
	create_and_fill_ogl_buffer( &planes->grid_colors_vb,     GL_ARRAY_BUFFER, GL_STREAM_DRAW, NULL, 0 );
	create_and_fill_ogl_buffer( &planes->grid_bg_colors_vb,  GL_ARRAY_BUFFER, GL_STREAM_DRAW, NULL, 0 );
}

void
render_planes( Shader * shader, Grid_Planes * planes )
{
	glUseProgram( shader->program );

	glBindBuffer( GL_ARRAY_BUFFER, planes->points_vb );
	glBufferData( GL_ARRAY_BUFFER, 0, NULL, GL_STREAM_DRAW );
	glBufferData( GL_ARRAY_BUFFER, sizeof(V3) * planes->count, planes->points, GL_STREAM_DRAW );

	glEnableVertexAttribArray( shader->vertex_loc );
	glVertexAttribPointer( shader->vertex_loc, 3, GL_FLOAT, GL_FALSE, 0, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, planes->normals_vb );
	glBufferData( GL_ARRAY_BUFFER, 0, NULL, GL_STREAM_DRAW );
	glBufferData( GL_ARRAY_BUFFER, sizeof(V3) * planes->count, planes->normals, GL_STREAM_DRAW );

	glEnableVertexAttribArray( shader->normal_loc );
	glVertexAttribPointer( shader->normal_loc, 3, GL_FLOAT, GL_FALSE, 0, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, planes->ups_vb );
	glBufferData( GL_ARRAY_BUFFER, 0, NULL, GL_STREAM_DRAW );
	glBufferData( GL_ARRAY_BUFFER, sizeof(V3) * planes->count, planes->ups, GL_STREAM_DRAW );

	glEnableVertexAttribArray( shader->attr_vec3[0] );
	glVertexAttribPointer( shader->attr_vec3[0], 3, GL_FLOAT, GL_FALSE, 0, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, planes->grid_cell_sides_vb );
	glBufferData( GL_ARRAY_BUFFER, 0, NULL, GL_STREAM_DRAW );
	glBufferData( GL_ARRAY_BUFFER, sizeof(float) * planes->count, planes->grid_cell_sides, GL_STREAM_DRAW );

	glEnableVertexAttribArray( shader->attr_float[0] );
	glVertexAttribPointer( shader->attr_float[0], 1, GL_FLOAT, GL_FALSE, 0, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, planes->grid_sides_vb );
	glBufferData( GL_ARRAY_BUFFER, 0, NULL, GL_STREAM_DRAW );
	glBufferData( GL_ARRAY_BUFFER, sizeof(float) * planes->count, planes->grid_sides, GL_STREAM_DRAW );

	glEnableVertexAttribArray( shader->attr_float[1] );
	glVertexAttribPointer( shader->attr_float[1], 1, GL_FLOAT, GL_FALSE, 0, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, planes->grid_colors_vb );
	glBufferData( GL_ARRAY_BUFFER, 0, NULL, GL_STREAM_DRAW );
	glBufferData( GL_ARRAY_BUFFER, sizeof(V4) * planes->count, planes->grid_colors, GL_STREAM_DRAW );

	glEnableVertexAttribArray( shader->attr_vec4[0] );
	glVertexAttribPointer( shader->attr_vec4[0], 4, GL_FLOAT, GL_FALSE, 0, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, planes->grid_bg_colors_vb );
	glBufferData( GL_ARRAY_BUFFER, 0, NULL, GL_STREAM_DRAW );
	glBufferData( GL_ARRAY_BUFFER, sizeof(V4) * planes->count, planes->grid_bg_colors, GL_STREAM_DRAW );

	glEnableVertexAttribArray( shader->attr_vec4[1] );
	glVertexAttribPointer( shader->attr_vec4[1], 4, GL_FLOAT, GL_FALSE, 0, 0 );

	glDrawArrays( GL_POINTS, 0, planes->count );

	glEnableVertexAttribArray( shader->vertex_loc );
	glEnableVertexAttribArray( shader->normal_loc );
	glEnableVertexAttribArray( shader->attr_vec3[0] );
	glEnableVertexAttribArray( shader->attr_float[0] );
	glEnableVertexAttribArray( shader->attr_float[1] );
	glEnableVertexAttribArray( shader->attr_vec4[0] );
	glEnableVertexAttribArray( shader->attr_vec4[1] );
}

////////////////////////////////////////////////////////////////////////////////

void
indexed_mesh_create_ogl_buffers( Indexed_Mesh * indexed_mesh, GLenum usage )
{
	create_and_fill_ogl_buffer(
	   &indexed_mesh->ogl.vertex_buffer,
	   GL_ARRAY_BUFFER, usage, indexed_mesh->vertices,
	   sizeof(indexed_mesh->vertices[0]) * indexed_mesh->vertices_count );

#if 0
	glGenBuffers( 1, &indexed_mesh->ogl.index_buffer );
#else
	create_and_fill_ogl_buffer(
	   &indexed_mesh->ogl.index_buffer,
	   GL_ARRAY_BUFFER, usage, indexed_mesh->indices,
	   sizeof(indexed_mesh->indices[0]) * indexed_mesh->indices_count );
#endif

	if ( indexed_mesh->normals )
	{
		create_and_fill_ogl_buffer(
		   &indexed_mesh->ogl.normals_buffer,
		   GL_ARRAY_BUFFER, usage, indexed_mesh->normals,
		   sizeof(indexed_mesh->normals[0]) * indexed_mesh->vertices_count );
	}
	if ( indexed_mesh->uvs )
	{
		create_and_fill_ogl_buffer(
		   &indexed_mesh->ogl.uvs_buffer,
		   GL_ARRAY_BUFFER, usage, indexed_mesh->uvs,
		   sizeof(indexed_mesh->uvs[0]) * indexed_mesh->vertices_count );
	}
	if ( indexed_mesh->u8_colors )
	{
		create_and_fill_ogl_buffer(
		   &indexed_mesh->ogl.u8_colors_buffer,
		   GL_ARRAY_BUFFER, usage, indexed_mesh->u8_colors,
		   sizeof(u8) * 3 * indexed_mesh->vertices_count );
	}
}

void
indexed_mesh_upload_to_gpu_1st_time( Indexed_Mesh * indexed_mesh, GLenum usage )
{
	glBindBuffer( GL_ARRAY_BUFFER, indexed_mesh->ogl.vertex_buffer );
	glBufferData( GL_ARRAY_BUFFER, sizeof(V3) * indexed_mesh->vertices_count, indexed_mesh->vertices, usage );

	glBindBuffer( GL_ARRAY_BUFFER, indexed_mesh->ogl.index_buffer );
	glBufferData( GL_ARRAY_BUFFER, sizeof(u16) * indexed_mesh->indices_count, indexed_mesh->indices, usage );

	if ( indexed_mesh->normals )
	{
		glBindBuffer( GL_ARRAY_BUFFER, indexed_mesh->ogl.normals_buffer );
		glBufferData( GL_ARRAY_BUFFER, sizeof(V3) * indexed_mesh->vertices_count, indexed_mesh->normals, usage );
	}
	if ( indexed_mesh->uvs )
	{
		glBindBuffer( GL_ARRAY_BUFFER, indexed_mesh->ogl.uvs_buffer );
		glBufferData( GL_ARRAY_BUFFER, sizeof(V2) * indexed_mesh->vertices_count, indexed_mesh->uvs, usage );
	}
	if ( indexed_mesh->u8_colors )
	{
		glBindBuffer( GL_ARRAY_BUFFER, indexed_mesh->ogl.u8_colors_buffer );
		glBufferData( GL_ARRAY_BUFFER, sizeof(u8) * 3 * indexed_mesh->vertices_count, indexed_mesh->u8_colors, usage );
	}
}

void
indexed_mesh_upload_to_gpu_update( Indexed_Mesh * indexed_mesh, GLenum usage )
{
	glBindBuffer( GL_ARRAY_BUFFER, indexed_mesh->ogl.vertex_buffer );
	glBufferData( GL_ARRAY_BUFFER, 0, NULL, usage );
	glBufferData( GL_ARRAY_BUFFER, sizeof(V3) * indexed_mesh->vertices_count, indexed_mesh->vertices, usage );

	glBindBuffer( GL_ARRAY_BUFFER, indexed_mesh->ogl.index_buffer );
	glBufferData( GL_ARRAY_BUFFER, 0, NULL, usage );
	glBufferData( GL_ARRAY_BUFFER, sizeof(u16) * indexed_mesh->indices_count, indexed_mesh->indices, usage );

	if ( indexed_mesh->normals )
	{
		glBindBuffer( GL_ARRAY_BUFFER, indexed_mesh->ogl.normals_buffer );
		glBufferData( GL_ARRAY_BUFFER, 0, NULL, usage );
		glBufferData( GL_ARRAY_BUFFER, sizeof(V3) * indexed_mesh->vertices_count, indexed_mesh->normals, usage );
	}
	if ( indexed_mesh->uvs )
	{
		glBindBuffer( GL_ARRAY_BUFFER, indexed_mesh->ogl.uvs_buffer );
		glBufferData( GL_ARRAY_BUFFER, 0, NULL, usage );
		glBufferData( GL_ARRAY_BUFFER, sizeof(V2) * indexed_mesh->vertices_count, indexed_mesh->uvs, usage );
	}
	if ( indexed_mesh->u8_colors )
	{
		glBindBuffer( GL_ARRAY_BUFFER, indexed_mesh->ogl.u8_colors_buffer );
		glBufferData( GL_ARRAY_BUFFER, 0, NULL, usage );
		glBufferData( GL_ARRAY_BUFFER, sizeof(u8) * 3 * indexed_mesh->vertices_count, indexed_mesh->u8_colors, usage );
	}
}

void
indexed_mesh_render_single( Indexed_Mesh * indexed_mesh, Shader * shader,
                            Mx4 model_matrix, Lpr_Rgba_f32 mesh_color )
{
	glUseProgram( shader->program );

	glUniformMatrix4fv( shader->M_loc, 1, GL_FALSE, model_matrix.arr );
	glUniform4fv( shader->color_loc, 1, mesh_color.rgba );

	glBindBuffer( GL_ARRAY_BUFFER, indexed_mesh->ogl.vertex_buffer );
	glEnableVertexAttribArray( shader->vertex_loc );
	glVertexAttribPointer( shader->vertex_loc, 3, GL_FLOAT, GL_FALSE, 0, 0 );

	if ( indexed_mesh->normals &&
	     shader->normal_loc >= 0 )
	{
		glBindBuffer( GL_ARRAY_BUFFER, indexed_mesh->ogl.normals_buffer );
		glEnableVertexAttribArray( shader->normal_loc );
		glVertexAttribPointer( shader->normal_loc, 3, GL_FLOAT, GL_FALSE, 0, 0 );
	}
	if ( indexed_mesh->uvs &&
	     shader->uv_loc >= 0 )
	{
		glBindBuffer( GL_ARRAY_BUFFER, indexed_mesh->ogl.uvs_buffer );
		glEnableVertexAttribArray( shader->uv_loc );
		glVertexAttribPointer( shader->uv_loc, 2, GL_FLOAT, GL_FALSE, 0, 0 );
	}
	if ( indexed_mesh->u8_colors &&
	     shader->vertex_color_loc >= 0 )
	{
		glBindBuffer( GL_ARRAY_BUFFER, indexed_mesh->ogl.u8_colors_buffer );
		glEnableVertexAttribArray( shader->vertex_color_loc );
		glVertexAttribPointer( shader->vertex_color_loc, 3, GL_UNSIGNED_BYTE, GL_TRUE, 0, 0 );
	}

	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, indexed_mesh->ogl.index_buffer );
	glDrawElements( GL_TRIANGLES, indexed_mesh->indices_count,
	                GL_UNSIGNED_SHORT, 0 );


	glDisableVertexAttribArray( shader->vertex_loc );
	glVertexAttribPointer( shader->vertex_loc, 3, GL_FLOAT, GL_FALSE, 0, 0 );

	if ( indexed_mesh->normals &&
	     shader->normal_loc >= 0 )
	{
		glDisableVertexAttribArray( shader->normal_loc );
	}
	if ( indexed_mesh->uvs &&
	     shader->uv_loc >= 0 )
	{
		glDisableVertexAttribArray( shader->uv_loc );
	}
	if ( indexed_mesh->u8_colors &&
	     shader->vertex_color_loc >= 0 )
	{
		glDisableVertexAttribArray( shader->vertex_color_loc );
	}
}

void
indexed_mesh_render_multiple( Indexed_Mesh * indexed_mesh,
                              u32 model_matrices_buffer, u32 mesh_colors_buffer,
                              u32 count,
                              Shader * shader )
{
	glUseProgram( shader->program );

	glBindBuffer( GL_ARRAY_BUFFER, indexed_mesh->ogl.vertex_buffer );
	glEnableVertexAttribArray( shader->vertex_loc );
	glVertexAttribDivisor( shader->vertex_loc, 0 );
	glVertexAttribPointer( shader->vertex_loc, 3, GL_FLOAT, GL_FALSE, 0, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, model_matrices_buffer );
	for ( u32 i = 0; i != 4; ++i )
	{
		glEnableVertexAttribArray( shader->instance_M_loc + i);
		glVertexAttribDivisor( shader->instance_M_loc + i, 1 );
		glVertexAttribPointer( shader->instance_M_loc + i, 4, GL_FLOAT, GL_FALSE,
		                       sizeof(f32) * 16, (void*)( sizeof(f32) * 4 * (u64)i ) );
	}

	glBindBuffer( GL_ARRAY_BUFFER, mesh_colors_buffer );
	glEnableVertexAttribArray( shader->instance_color_loc );
	glVertexAttribDivisor( shader->instance_color_loc, 1 );
	glVertexAttribPointer( shader->instance_color_loc, 4, GL_FLOAT, GL_FALSE, 0, 0 );

	if ( indexed_mesh->normals &&
	     shader->normal_loc >= 0 )
	{
		glBindBuffer( GL_ARRAY_BUFFER, indexed_mesh->ogl.normals_buffer );
		glEnableVertexAttribArray( shader->normal_loc );
		glVertexAttribDivisor( shader->normal_loc, 0 );
		glVertexAttribPointer( shader->normal_loc, 3, GL_FLOAT, GL_FALSE, 0, 0 );
	}
	if ( indexed_mesh->uvs &&
	     shader->uv_loc >= 0 )
	{
		glBindBuffer( GL_ARRAY_BUFFER, indexed_mesh->ogl.uvs_buffer );
		glEnableVertexAttribArray( shader->uv_loc );
		glVertexAttribDivisor( shader->uv_loc, 0 );
		glVertexAttribPointer( shader->uv_loc, 2, GL_FLOAT, GL_FALSE, 0, 0 );
	}
	if ( indexed_mesh->u8_colors &&
	     shader->vertex_color_loc >= 0 )
	{
		glBindBuffer( GL_ARRAY_BUFFER, indexed_mesh->ogl.u8_colors_buffer );
		glEnableVertexAttribArray( shader->vertex_color_loc );
		glVertexAttribDivisor( shader->vertex_color_loc, 0 );
		glVertexAttribPointer( shader->vertex_color_loc, 3, GL_UNSIGNED_BYTE, GL_TRUE, 0, 0 );
	}

	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, indexed_mesh->ogl.index_buffer );
	glDrawElementsInstanced( GL_TRIANGLES, indexed_mesh->indices_count,
	                         GL_UNSIGNED_SHORT, 0, count );


	glDisableVertexAttribArray( shader->vertex_loc );
	glVertexAttribDivisor( shader->vertex_loc, 0 );
	for ( u32 i = 0; i != 4; ++i )
	{
		glDisableVertexAttribArray( shader->instance_M_loc + i);
		glVertexAttribDivisor( shader->instance_M_loc + i, 0 );
	}

	glDisableVertexAttribArray( shader->instance_color_loc );
	glVertexAttribDivisor( shader->instance_color_loc, 0 );

	if ( indexed_mesh->normals &&
	     shader->normal_loc >= 0 )
	{
		glDisableVertexAttribArray( shader->normal_loc );
		glVertexAttribDivisor( shader->normal_loc, 0 );
	}
	if ( indexed_mesh->uvs &&
	     shader->uv_loc >= 0 )
	{
		glDisableVertexAttribArray( shader->uv_loc );
		glVertexAttribDivisor( shader->uv_loc, 0 );
	}
	if ( indexed_mesh->u8_colors &&
	     shader->vertex_color_loc >= 0 )
	{
		glDisableVertexAttribArray( shader->vertex_color_loc );
		glVertexAttribDivisor( shader->vertex_color_loc, 0 );
	}
}

////////////////////////////////////////////////////////////////////////////////

bool
load_obj_into_indexed_mesh( const u8 * file_path, Indexed_Mesh * indexed_mesh, Mem_Pool * mem_pool, Mem_Stack * tmp_mem_stack )
{
	bool ret = false;

	s64 file_size = global_sys_api->get_file_size( (const char*)file_path );
	if ( SYS_API_RET_IS_ERROR( file_size ) )
	{
		lpr_log_err( "couldn't load obj file '%s'.\n"
		             "   Error: %s\n"
		             "   Description: %s\n",
		             file_path,
		             global_sys_api->get_error_name( file_size ),
		             global_sys_api->get_error_description( file_size ) );
		return ret;
	}

	u64 pop = tmp_mem_stack->first_free;

	u8 * file_mem = tmp_mem_stack->push( file_size + 1 );
	if ( !file_mem )
	{
		lpr_log_err( "not enough tmp memory to load obj file '%s' of size %lu",
		             file_path, file_size );
		return ret;
	}

	s64 read = global_sys_api->read_entire_file( (const char *)file_path, file_mem, file_size );
	if ( SYS_API_RET_IS_ERROR( read ) )
	{
		lpr_log_err( "couldn't load obj file '%s'.\n"
		             "   Error: %s\n"
		             "   Description: %s\n",
		             file_path,
		             global_sys_api->get_error_name( read ),
		             global_sys_api->get_error_description( read ) );

		tmp_mem_stack->first_free = pop;
		return ret;
	}

	file_mem[file_size] = 0;

	s64 remaining_size = file_size;
	u8 * into_file = file_mem;
	u32 line_number = 0;

	u32 file_vertices_count = 0;
	u32 file_uvs_count      = 0;
	u32 file_normals_count  = 0;
	u32 file_faces_count    = 0;
	u32 file_vertices_indices_count = 0;
	u32 file_uvs_indices_count      = 0;
	u32 file_normals_indices_count  = 0;

	bool got_an_error = false;
	for (;;)
	{
		u32 read_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, ' ' );
		if ( !read_length )
		{
			break;
		}

		into_file[read_length] = 0;

		if ( string_min( "v", (char*)into_file ) == 0 )
		{
			into_file[read_length] = ' ';
			into_file += read_length;

			u32 line_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, '\n' );
			if ( !line_length )
			{
				lpr_log_err( "malformed obj file '%s'. Expected vertex coordinates at line %u",
				             file_path, line_number );
				got_an_error = true;
				break;
			}
			if ( into_file[line_length] == '\n' )
			{
				++line_length;
			}
			++file_vertices_count;
			into_file += line_length;
			remaining_size -= line_length;
			++line_number;
		} else
		if ( string_min( "vt", (char*)into_file ) == 0 )
		{
			into_file[read_length] = ' ';
			into_file += read_length;

			u32 line_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, '\n' );
			if ( !line_length )
			{
				lpr_log_err( "malformed obj file '%s'. Expected uv coordinates at line %u",
				             file_path, line_number );
				got_an_error = true;
				break;
			}
			if ( into_file[line_length] == '\n' )
			{
				++line_length;
			}
			++file_uvs_count;
			into_file += line_length;
			remaining_size -= line_length;
			++line_number;
		} else
		if ( string_min( "vn", (char*)into_file ) == 0 )
		{
			into_file[read_length] = ' ';
			into_file += read_length;

			u32 line_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, '\n' );
			if ( !line_length )
			{
				lpr_log_err( "malformed obj file '%s'. Expected normal vector at line %u",
				             file_path, line_number );
				got_an_error = true;
				break;
			}
			if ( into_file[line_length] == '\n' )
			{
				++line_length;
			}
			++file_normals_count;
			into_file += line_length;
			remaining_size -= line_length;
			++line_number;
		} else
		if ( string_min( "f", (char*)into_file ) == 0 )
		{
			into_file[read_length] = ' ';
			into_file += read_length;

			u32 line_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, '\n' );
			if ( !line_length )
			{
				lpr_log_err( "malformed obj file '%s'. Expected face indices at line %u",
				             file_path, line_number );
				got_an_error = true;
				break;
			}
			if ( into_file[line_length] == '\n' )
			{
				++line_length;
			}

			u32 foo[9];
			s32 read_elements =
			sscanf( (char*)into_file, "%u/%u/%u %u/%u/%u %u/%u/%u \n",
			        foo + 0, foo + 1, foo + 2, foo + 3, foo + 4, foo + 5,
			        foo + 6, foo + 7, foo + 8 );
			if ( read_elements != 9 )
			{
				read_elements =
				sscanf( (char*)into_file, "%u/%u %u/%u %u/%u \n",
				        foo + 0, foo + 1, foo + 2, foo + 3, foo + 4, foo + 5 );
				if ( read_elements != 6 )
				{
					read_elements =
					sscanf( (char*)into_file, "%u//%u %u//%u %u//%u \n",
					        foo + 0, foo + 1, foo + 2, foo + 3, foo + 4, foo + 5 );
					if ( read_elements != 6 )
					{
						lpr_log_err( "malformed obj. Error reading face indices at lien %u", line_number );
						got_an_error = true;
						break;
					}
					file_vertices_indices_count += 3;
					file_normals_indices_count += 3;
				} else
				{
					file_vertices_indices_count += 3;
					file_uvs_indices_count += 3;
				}
			} else
			{
				file_vertices_indices_count += 3;
				file_uvs_indices_count += 3;
				file_normals_indices_count += 3;
			}
			++file_faces_count;
			into_file += line_length;
			remaining_size -= line_length;
			++line_number;
		} else
		{
			into_file[read_length] = ' ';

			u32 line_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, '\n' );
			if ( !line_length )
			{
				break;
			}
			u8 old = into_file[line_length];
			into_file[line_length] = 0;
			lpr_log_dbg( "assuming this line is a comment: '%s'", into_file );
			into_file[line_length] = old;

			if ( into_file[line_length] == '\n' )
			{
				++line_length;
			}

			into_file += line_length;
			remaining_size -= line_length;
			++line_number;
		}
	}

	if ( got_an_error )
	{
		tmp_mem_stack->first_free = pop;

		return ret;
	}

	lpr_log_dbg( "found %4u vertices, %4u normals, %4u uvs, %4u faces",
	             file_vertices_count, file_normals_count, file_uvs_count, file_faces_count );

	V3 * vertices = (V3*)tmp_mem_stack->push( sizeof( V3 ) * file_vertices_count );
	V2 * uvs      = (V2*)tmp_mem_stack->push( sizeof( V2 ) * file_uvs_count );
	V3 * normals  = (V3*)tmp_mem_stack->push( sizeof( V3 ) * file_normals_count );
	u32 * vertex_indices = (u32*)tmp_mem_stack->push( sizeof( u32 ) * file_vertices_indices_count );
	u32 * uv_indices     = (u32*)tmp_mem_stack->push( sizeof( u32 ) * file_uvs_indices_count );
	u32 * normal_indices = (u32*)tmp_mem_stack->push( sizeof( u32 ) * file_normals_indices_count );

	if ( !vertices || !uvs || !normals || !vertex_indices || !uv_indices || !normal_indices )
	{
		lpr_log_err( "not enough tmp memory to load obj '%s'", file_path );

		tmp_mem_stack->first_free = pop;
		return ret;
	}

	u32 read_vertices_count = 0;
	u32 read_uvs_count      = 0;
	u32 read_normals_count  = 0;
	u32 read_vertices_indices_count  = 0;
	u32 read_uvs_indices_count       = 0;
	u32 read_normals_indices_count   = 0;

	remaining_size = file_size;
	into_file = file_mem;
	line_number = 0;
	for (;;)
	{
		u32 read_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, ' ' );
		if ( !read_length )
		{
			break;
		}

		into_file[read_length] = 0;

		if ( string_min( "v", (char*)into_file ) == 0 )
		{
			into_file[read_length] = ' ';
			into_file += read_length;

			u32 line_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, '\n' );
			if ( !line_length )
			{
				lpr_log_err( "malformed obj file '%s'. Expected vertex coordinates at line %u",
				             file_path, line_number );
				got_an_error = true;
				break;
			}
			if ( into_file[line_length] == '\n' )
			{
				++line_length;
			}
			s32 read_elements =
			sscanf( (char*)into_file, "%f %f %f\n",
			        &vertices[read_vertices_count].x,
			        &vertices[read_vertices_count].y,
			        &vertices[read_vertices_count].z );
			if ( read_elements != 3 )
			{
				lpr_log_err( "malformed obj file '%s'. Expected 3 vertices dimensions, %d found.",
				             file_path, read_elements );
				got_an_error = true;
				break;
			}
			++read_vertices_count;
			into_file += line_length;
			remaining_size -= line_length;
			++line_number;
		} else
		if ( string_min( "vt", (char*)into_file ) == 0 )
		{
			into_file[read_length] = ' ';
			into_file += read_length;

			u32 line_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, '\n' );
			if ( !line_length )
			{
				lpr_log_err( "malformed obj file '%s'. Expected uv coordinates at line %u",
				             file_path, line_number );
				got_an_error = true;
				break;
			}
			if ( into_file[line_length] == '\n' )
			{
				++line_length;
			}
			s32 read_elements =
			sscanf( (char*)into_file, "%f %f\n",
			        &uvs[read_uvs_count].u,
			        &uvs[read_uvs_count].v );
			if ( read_elements != 2 )
			{
				lpr_log_err( "malformed obj file '%s'. Expected 2 uv dimensions, %d found.",
				             file_path, read_elements );
				got_an_error = true;
				break;
			}
			++read_uvs_count;
			into_file += line_length;
			remaining_size -= line_length;
			++line_number;
		} else
		if ( string_min( "vn", (char*)into_file ) == 0 )
		{
			into_file[read_length] = ' ';
			into_file += read_length;

			u32 line_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, '\n' );
			if ( !line_length )
			{
				lpr_log_err( "malformed obj file '%s'. Expected normal vector at line %u",
				             file_path, line_number );
				got_an_error = true;
				break;
			}
			if ( into_file[line_length] == '\n' )
			{
				++line_length;
			}
			s32 read_elements =
			sscanf( (char*)into_file, "%f %f %f\n",
			        &normals[read_normals_count].x,
			        &normals[read_normals_count].y,
			        &normals[read_normals_count].z );
			if ( read_elements != 3 )
			{
				lpr_log_err( "malformed obj file '%s'. Expected 3 normal dimensions, %d found.",
				             file_path, read_elements );
				got_an_error = true;
				break;
			}
			++read_normals_count;
			into_file += line_length;
			remaining_size -= line_length;
			++line_number;
		} else
		if ( string_min( "f", (char*)into_file ) == 0 )
		{
			into_file[read_length] = ' ';
			into_file += read_length;

			u32 line_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, '\n' );
			if ( !line_length )
			{
				lpr_log_err( "malformed obj file '%s'. Expected face indices at line %u",
				             file_path, line_number );
				got_an_error = true;
				break;
			}
			if ( into_file[line_length] == '\n' )
			{
				++line_length;
			}

			s32 read_elements =
			sscanf( (char*)into_file, "%u/%u/%u %u/%u/%u %u/%u/%u \n",
			        vertex_indices + read_vertices_indices_count + 0,
			        uv_indices     + read_uvs_indices_count      + 0,
			        normal_indices + read_normals_indices_count  + 0,
			        vertex_indices + read_vertices_indices_count + 1,
			        uv_indices     + read_uvs_indices_count      + 1,
			        normal_indices + read_normals_indices_count  + 1,
			        vertex_indices + read_vertices_indices_count + 2,
			        uv_indices     + read_uvs_indices_count      + 2,
			        normal_indices + read_normals_indices_count  + 2 );
			if ( read_elements != 9 )
			{
				read_elements =
				sscanf( (char*)into_file, "%u/%u %u/%u %u/%u \n",
				        vertex_indices + read_vertices_indices_count + 0,
				        uv_indices     + read_uvs_indices_count      + 0,
				        vertex_indices + read_vertices_indices_count + 1,
				        uv_indices     + read_uvs_indices_count      + 1,
				        vertex_indices + read_vertices_indices_count + 2,
				        uv_indices     + read_uvs_indices_count      + 2 );
				if ( read_elements != 6 )
				{
					read_elements =
					sscanf( (char*)into_file, "%u//%u %u//%u %u//%u \n",
					        vertex_indices + read_vertices_indices_count + 0,
					        normal_indices + read_normals_indices_count  + 0,
					        vertex_indices + read_vertices_indices_count + 1,
					        normal_indices + read_normals_indices_count  + 1,
					        vertex_indices + read_vertices_indices_count + 2,
					        normal_indices + read_normals_indices_count  + 2 );
					if ( read_elements != 6 )
					{
						lpr_log_err( "malformed obj. Error reading face indices at line %u", line_number );
						got_an_error = true;
						break;
					}
					read_vertices_indices_count += 3;
					read_normals_indices_count  += 3;
				} else
				{
					read_vertices_indices_count += 3;
					read_uvs_indices_count      += 3;
				}
			} else
			{
				read_vertices_indices_count += 3;
				read_uvs_indices_count      += 3;
				read_normals_indices_count  += 3;
			}
			into_file += line_length;
			remaining_size -= line_length;
			++line_number;
		} else
		{
			into_file[read_length] = ' ';

			u32 line_length = str_bytes_to_char_or_end( (char*)into_file, remaining_size, '\n' );
			if ( !line_length )
			{
				break;
			}
			u8 old = into_file[line_length];
			into_file[line_length] = 0;
			lpr_log_dbg( "assuming this line is a comment: '%s'", into_file );
			into_file[line_length] = old;

			if ( into_file[line_length] == '\n' )
			{
				++line_length;
			}

			into_file += line_length;
			remaining_size -= line_length;
			++line_number;
		}
	}

	if ( read_vertices_indices_count != read_uvs_indices_count ||
	     read_vertices_indices_count != read_normals_indices_count )
	{
		lpr_log_err( "malformed obj file '%s'. Different number of indices for vertices, uvs, normals. ( %u, %u, %u )",
		             file_path, read_vertices_count, read_uvs_indices_count, read_normals_indices_count );
		got_an_error = true;
	}

	if ( got_an_error )
	{
		tmp_mem_stack->first_free = pop;

		return ret;
	}


	// TODO(theGiallo, 2017-02-11): optimize vertices de-duplication
	// TODO(theGiallo, 2017-02-11): make the loaded mesh into an opengl compatible mesh
	// TODO(theGiallo, 2017-02-11): split the mesh in case it's more than 65336 indices
	// INCOMPLETE IMPORTANT WORKING ON THIS
	lpr_log_err( "WTF r u doing?! Go back coding and finish this function!" );

	Packed_Vertex_PUVN * triangle_vertices =
	   (Packed_Vertex_PUVN*)tmp_mem_stack->push( sizeof(Packed_Vertex_PUVN) * read_vertices_indices_count );
	u32 * nonduplicate_ogl_indices = (u32*) tmp_mem_stack->push( sizeof(u32) * read_vertices_indices_count );
	u32 nonduplicate_ogl_indices_count  = 0;
	u32 nonduplicate_ogl_vertices_count = 0;
	if ( !triangle_vertices || !nonduplicate_ogl_indices )
	{
		lpr_log_err( "not enough tmp memory to load obj file '%s'", file_path );

		tmp_mem_stack->first_free = pop;
		return ret;
	}

	for ( u32 i = 0; i != read_vertices_indices_count; ++i )
	{
		triangle_vertices[nonduplicate_ogl_vertices_count].vertex =           vertices[vertex_indices[i] - 1];
		triangle_vertices[nonduplicate_ogl_vertices_count].normal = normals ? normals [normal_indices[i] - 1] : V3{};
		triangle_vertices[nonduplicate_ogl_vertices_count].uv     = uvs     ? uvs     [    uv_indices[i] - 1] : V2{};
		bool found = false;
		u32 found_index = U32_MAX;
		for ( u32 j = 0; j != nonduplicate_ogl_vertices_count; ++j )
		{
			if ( triangle_vertices[j].vertex ==
			     triangle_vertices[nonduplicate_ogl_vertices_count].vertex &&
			     triangle_vertices[j].normal ==
			     triangle_vertices[nonduplicate_ogl_vertices_count].normal &&
			     triangle_vertices[j].uv ==
			     triangle_vertices[nonduplicate_ogl_vertices_count].uv )
			{
				found = true;
				found_index = j;
			}
		}
		if ( found )
		{
			nonduplicate_ogl_indices[nonduplicate_ogl_indices_count] = found_index;
			++nonduplicate_ogl_indices_count;
		} else
		{
			nonduplicate_ogl_indices[nonduplicate_ogl_indices_count] = nonduplicate_ogl_vertices_count;
			++nonduplicate_ogl_indices_count;
			++nonduplicate_ogl_vertices_count;
		}
	}

	lpr_log_dbg( "nonduplicate_ogl_vertices_count %4u", nonduplicate_ogl_vertices_count );
	lpr_log_dbg( "nonduplicate_ogl_indices_count %4u",  nonduplicate_ogl_indices_count  );

	u32 optionals_flags = 0;
	if ( normals )
	{
		optionals_flags |= (u32)Indexed_Mesh_Optionals_Flags::has_normals;
	}
	if ( uvs )
	{
		optionals_flags |= (u32)Indexed_Mesh_Optionals_Flags::has_uvs;
	}
	u64 indexed_mesh_bytes_size =
	   indexed_mesh_compute_bytes_size( nonduplicate_ogl_vertices_count,
	                                    nonduplicate_ogl_indices_count,
	                                    optionals_flags );
	u8 * indexed_mesh_mem = (u8*) mem_pool->allocate_clean( indexed_mesh_bytes_size );
	if ( !indexed_mesh_mem )
	{
		lpr_log_err( "not enough memory to store read indexed mesh loaded from obj '%s'."
		             " Required mesh size is %luB. It has %u vertices and %u indices.",
		             file_path, indexed_mesh_bytes_size, nonduplicate_ogl_vertices_count,
		             nonduplicate_ogl_indices_count );

		tmp_mem_stack->first_free = pop;
		return ret;
	}

	indexed_mesh_init( indexed_mesh, indexed_mesh_mem, nonduplicate_ogl_vertices_count,
	                   nonduplicate_ogl_indices_count, optionals_flags );
	for ( u32 i = 0; i != nonduplicate_ogl_vertices_count; ++i )
	{
		indexed_mesh->vertices[i] = triangle_vertices[i].vertex;
		if ( uvs )
		{
			indexed_mesh->uvs[i]     = triangle_vertices[i].uv;
		}
		if ( normals )
		{
			indexed_mesh->normals[i] = triangle_vertices[i].normal;
		}
	}
	for ( u32 i = 0; i != nonduplicate_ogl_indices_count; ++i )
	{
		indexed_mesh->indices[i] = nonduplicate_ogl_indices[i];
	}

	ret = true;
	tmp_mem_stack->first_free = pop;

	return ret;
}

void
generate_icosphere_as_indexed_mesh( Indexed_Mesh * indexed_mesh,
                                    Mem_Pool  * mem_pool,
                                    Mem_Stack * tmp_mem_stack,
                                    u32 subdivisions_count )
{
	f32 phi = 1.61803398874989484820458683436563811772030917980576286213544862270526046281890244970720720418939113748475f;
	f32 m = 1.0f / ( 0.5f * sqrtf( 10.0f + 2.0f * ( sqrtf( 5.0f ) ) ) );
	f32 m_phi = m * phi;

	u32 vertices_count = 12;
	u32 edges_count = 30;
	u32 triangles_count = 20;

	for ( u32 s = 0; s != subdivisions_count; ++s )
	{
		vertices_count += edges_count;
		edges_count = edges_count * 2 + 3 * triangles_count;
		triangles_count *= 4;
	}

	u64 indexed_mesh_bytes_size =
	indexed_mesh_compute_bytes_size( vertices_count, triangles_count * 3, 0 );
	u8 * mem = (u8*)mem_pool->allocate_clean( indexed_mesh_bytes_size + 8 );
	mem = memory_to_next_64bits_alignment( mem );
	indexed_mesh_init( indexed_mesh, mem, vertices_count, triangles_count * 3, 0 );
	indexed_mesh->indices_count  = 0;
	indexed_mesh->vertices_count = 0;

	u32 pop = tmp_mem_stack->first_free;

	u8 * tmp_mem = (u8*)tmp_mem_stack->push( indexed_mesh_bytes_size );
	Indexed_Mesh tmp_indexed_mesh = {};
	indexed_mesh_init( &tmp_indexed_mesh, tmp_mem, vertices_count, triangles_count * 3, 0 );
	tmp_indexed_mesh.indices_count  = 0;
	tmp_indexed_mesh.vertices_count = 0;

	indexed_mesh->vertices[indexed_mesh->vertices_count++] = {-m,      m_phi,  0,    };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = { m,      m_phi,  0,    };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = {-m,     -m_phi,  0,    };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = { m,     -m_phi,  0,    };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = { 0,     -m,      m_phi };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = { 0,      m,      m_phi };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = { 0,     -m,     -m_phi };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = { 0,      m,     -m_phi };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = { m_phi,  0,     -m,    };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = { m_phi,  0,      m,    };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = {-m_phi,  0,     -m,    };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = {-m_phi,  0,      m,    };

	// 5 faces around point 0
	indexed_mesh->indices[indexed_mesh->indices_count++] =  0;
	indexed_mesh->indices[indexed_mesh->indices_count++] = 11;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  5;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  0;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  5;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  1;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  0;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  1;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  7;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  0;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  7;
	indexed_mesh->indices[indexed_mesh->indices_count++] = 10;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  0;
	indexed_mesh->indices[indexed_mesh->indices_count++] = 10;
	indexed_mesh->indices[indexed_mesh->indices_count++] = 11;

	// 5 adjacent faces
	indexed_mesh->indices[indexed_mesh->indices_count++] =  1;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  5;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  9;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  5;
	indexed_mesh->indices[indexed_mesh->indices_count++] = 11;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  4;

	indexed_mesh->indices[indexed_mesh->indices_count++] = 11;
	indexed_mesh->indices[indexed_mesh->indices_count++] = 10;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  2;

	indexed_mesh->indices[indexed_mesh->indices_count++] = 10;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  7;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  6;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  7;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  1;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  8;

	// 5 faces around point 3
	indexed_mesh->indices[indexed_mesh->indices_count++] =  3;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  9;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  4;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  3;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  4;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  2;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  3;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  2;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  6;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  3;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  6;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  8;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  3;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  8;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  9;

	// 5 adjacent faces
	indexed_mesh->indices[indexed_mesh->indices_count++] =  4;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  9;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  5;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  2;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  4;
	indexed_mesh->indices[indexed_mesh->indices_count++] = 11;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  6;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  2;
	indexed_mesh->indices[indexed_mesh->indices_count++] = 10;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  8;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  6;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  7;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  9;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  8;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  1;

	B12_to_u32_Hash_Table vertices_ht = {};
	u32 vertices_ht_bytes_size =
	B12_to_u32_hash_table_compute_memory_size( vertices_count * 2 );
	u8 * vertices_ht_mem = (u8*)tmp_mem_stack->push( vertices_ht_bytes_size + 8 );
	vertices_ht_mem =
	memory_to_next_64bits_alignment( vertices_ht_mem );
	memset( vertices_ht_mem, 0x0, vertices_ht_bytes_size );
	B12_to_u32_hash_table_init( &vertices_ht, vertices_ht_mem, vertices_count * 2 );

	Indexed_Mesh * prev_indexed_mesh = &tmp_indexed_mesh;
	Indexed_Mesh * curr_indexed_mesh = indexed_mesh;
	for ( u32 s = 0; s != subdivisions_count; ++s )
	{
		Indexed_Mesh * tmp = prev_indexed_mesh;
		 prev_indexed_mesh = curr_indexed_mesh;
		 curr_indexed_mesh = tmp;

		curr_indexed_mesh->indices_count  = 0;
		curr_indexed_mesh->vertices_count = 0;

		B12_to_u32_hash_table_clear( &vertices_ht );

		for ( u32 t_idx = 0, t_idx_end = prev_indexed_mesh->indices_count;
		      t_idx != t_idx_end;
		      t_idx += 3 )
		{
			//lpr_log_dbg( "tid %3u", t_idx );
			u32 mids_ids[3];
			u32 old_v_ids[3];
			for ( u32 internal_vid = 0;
			      internal_vid != 3;
			      ++internal_vid )
			{
				u32 next_vid;
				switch ( internal_vid )
				{
					case 0: next_vid = 1; break;
					case 1: next_vid = 2; break;
					case 2: next_vid = 0; break;
				}
				V3 a_vertex = prev_indexed_mesh->vertices[ prev_indexed_mesh->indices[t_idx + internal_vid] ];
				V3 b_vertex = prev_indexed_mesh->vertices[ prev_indexed_mesh->indices[t_idx + next_vid    ] ];
				V3 new_vertex = normalize( ( a_vertex + b_vertex ) * 0.5f );

				//lpr_assert( length(   a_vertex ) == 1.0f );
				//lpr_assert( length(   b_vertex ) == 1.0f );
				//lpr_assert( length( new_vertex ) == 1.0f );

				u32_bool found =
				B12_to_u32_hash_table_retrieve( &vertices_ht, {.v3 = new_vertex } );
				if ( !found.bool_v )
				{
					found.u32_v = curr_indexed_mesh->vertices_count++;
					bool res =
					B12_to_u32_hash_table_insert( &vertices_ht,
					                              {.v3 = new_vertex},
					                              found.u32_v );
					lpr_assert( res );
					curr_indexed_mesh->vertices[found.u32_v] = new_vertex;
				}

				mids_ids[internal_vid] = found.u32_v;

				found =
				B12_to_u32_hash_table_retrieve( &vertices_ht, {.v3 = a_vertex } );
				if ( !found.bool_v )
				{
					found.u32_v = curr_indexed_mesh->vertices_count++;
					bool res =
					B12_to_u32_hash_table_insert( &vertices_ht,
					                              {.v3 = a_vertex},
					                              found.u32_v );
					lpr_assert( res );
					curr_indexed_mesh->vertices[found.u32_v] = a_vertex;
				}

				old_v_ids[internal_vid] = found.u32_v;
			}

			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] = old_v_ids[0];
			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] =  mids_ids[0];
			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] =  mids_ids[2];

			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] = old_v_ids[1];
			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] =  mids_ids[1];
			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] =  mids_ids[0];

			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] = old_v_ids[2];
			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] =  mids_ids[2];
			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] =  mids_ids[1];

			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] =  mids_ids[0];
			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] =  mids_ids[1];
			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] =  mids_ids[2];
		}
	}

	if ( curr_indexed_mesh != indexed_mesh )
	{
		memcpy( indexed_mesh->vertices,
		        curr_indexed_mesh->vertices,
		        curr_indexed_mesh->vertices_count * sizeof( V3 ) );
		memcpy( indexed_mesh->indices,
		        curr_indexed_mesh->indices,
		        curr_indexed_mesh->indices_count * sizeof( u32 ) );
		indexed_mesh->vertices_count = curr_indexed_mesh->vertices_count;
		indexed_mesh->indices_count  = curr_indexed_mesh->indices_count;
	}

	tmp_mem_stack->first_free = pop;
}
void
generate_icospheres_as_indexed_meshes( Indexed_Mesh * indexed_meshes,
                                       Mem_Pool  * mem_pool,
                                       Mem_Stack * tmp_mem_stack,
                                       u32 subdivisions_count )
{
	f32 phi = 1.61803398874989484820458683436563811772030917980576286213544862270526046281890244970720720418939113748475f;
	f32 m = 1.0f / ( 0.5f * sqrtf( 10.0f + 2.0f * ( sqrtf( 5.0f ) ) ) );
	f32 m_phi = m * phi;

	u32 vertices_count = 12;
	u32 edges_count = 30;
	u32 triangles_count = 20;

	for ( u32 s = 0; s != subdivisions_count + 1; ++s )
	{
		u64 indexed_mesh_bytes_size =
		indexed_mesh_compute_bytes_size( vertices_count, triangles_count * 3, 0 );
		u8 * mem = (u8*)mem_pool->allocate_clean( indexed_mesh_bytes_size + 8 );
		lpr_assert( mem );
		mem = memory_to_next_64bits_alignment( mem );
		indexed_mesh_init( indexed_meshes + s, mem, vertices_count, triangles_count * 3, 0 );
		indexed_meshes[s].indices_count  = 0;
		indexed_meshes[s].vertices_count = 0;

		if ( s != subdivisions_count + 1 )
		{
			vertices_count += edges_count;
			edges_count = edges_count * 2 + 3 * triangles_count;
			triangles_count *= 4;
		}
	}

	u32 pop = tmp_mem_stack->first_free;

	Indexed_Mesh * indexed_mesh = indexed_meshes + 0;

	indexed_mesh->vertices[indexed_mesh->vertices_count++] = {-m,      m_phi,  0,    };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = { m,      m_phi,  0,    };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = {-m,     -m_phi,  0,    };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = { m,     -m_phi,  0,    };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = { 0,     -m,      m_phi };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = { 0,      m,      m_phi };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = { 0,     -m,     -m_phi };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = { 0,      m,     -m_phi };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = { m_phi,  0,     -m,    };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = { m_phi,  0,      m,    };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = {-m_phi,  0,     -m,    };
	indexed_mesh->vertices[indexed_mesh->vertices_count++] = {-m_phi,  0,      m,    };

	// 5 faces around point 0
	indexed_mesh->indices[indexed_mesh->indices_count++] =  0;
	indexed_mesh->indices[indexed_mesh->indices_count++] = 11;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  5;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  0;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  5;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  1;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  0;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  1;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  7;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  0;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  7;
	indexed_mesh->indices[indexed_mesh->indices_count++] = 10;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  0;
	indexed_mesh->indices[indexed_mesh->indices_count++] = 10;
	indexed_mesh->indices[indexed_mesh->indices_count++] = 11;

	// 5 adjacent faces
	indexed_mesh->indices[indexed_mesh->indices_count++] =  1;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  5;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  9;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  5;
	indexed_mesh->indices[indexed_mesh->indices_count++] = 11;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  4;

	indexed_mesh->indices[indexed_mesh->indices_count++] = 11;
	indexed_mesh->indices[indexed_mesh->indices_count++] = 10;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  2;

	indexed_mesh->indices[indexed_mesh->indices_count++] = 10;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  7;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  6;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  7;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  1;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  8;

	// 5 faces around point 3
	indexed_mesh->indices[indexed_mesh->indices_count++] =  3;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  9;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  4;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  3;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  4;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  2;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  3;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  2;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  6;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  3;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  6;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  8;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  3;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  8;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  9;

	// 5 adjacent faces
	indexed_mesh->indices[indexed_mesh->indices_count++] =  4;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  9;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  5;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  2;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  4;
	indexed_mesh->indices[indexed_mesh->indices_count++] = 11;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  6;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  2;
	indexed_mesh->indices[indexed_mesh->indices_count++] = 10;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  8;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  6;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  7;

	indexed_mesh->indices[indexed_mesh->indices_count++] =  9;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  8;
	indexed_mesh->indices[indexed_mesh->indices_count++] =  1;

	B12_to_u32_Hash_Table vertices_ht = {};
	u32 vertices_ht_bytes_size =
	B12_to_u32_hash_table_compute_memory_size( vertices_count * 2 );
	u8 * vertices_ht_mem = (u8*)tmp_mem_stack->push( vertices_ht_bytes_size + 8 );
	vertices_ht_mem =
	memory_to_next_64bits_alignment( vertices_ht_mem );
	memset( vertices_ht_mem, 0x0, vertices_ht_bytes_size );
	B12_to_u32_hash_table_init( &vertices_ht, vertices_ht_mem, vertices_count * 2 );

	for ( u32 s = 1; s != subdivisions_count + 1; ++s )
	{
		Indexed_Mesh * prev_indexed_mesh = indexed_meshes + ( s - 1 );
		Indexed_Mesh * curr_indexed_mesh = indexed_mesh + s;

		curr_indexed_mesh->indices_count  = 0;
		curr_indexed_mesh->vertices_count = 0;

		B12_to_u32_hash_table_clear( &vertices_ht );

		for ( u32 t_idx = 0, t_idx_end = prev_indexed_mesh->indices_count;
		      t_idx != t_idx_end;
		      t_idx += 3 )
		{
			//lpr_log_dbg( "tid %3u", t_idx );
			u32 mids_ids[3];
			u32 old_v_ids[3];
			for ( u32 internal_vid = 0;
			      internal_vid != 3;
			      ++internal_vid )
			{
				u32 next_vid;
				switch ( internal_vid )
				{
					case 0: next_vid = 1; break;
					case 1: next_vid = 2; break;
					case 2: next_vid = 0; break;
				}
				V3 a_vertex = prev_indexed_mesh->vertices[ prev_indexed_mesh->indices[t_idx + internal_vid] ];
				V3 b_vertex = prev_indexed_mesh->vertices[ prev_indexed_mesh->indices[t_idx + next_vid    ] ];
				V3 new_vertex = normalize( ( a_vertex + b_vertex ) * 0.5f );

				//lpr_assert( length(   a_vertex ) == 1.0f );
				//lpr_assert( length(   b_vertex ) == 1.0f );
				//lpr_assert( length( new_vertex ) == 1.0f );

				u32_bool found =
				B12_to_u32_hash_table_retrieve( &vertices_ht, {.v3 = new_vertex } );
				if ( !found.bool_v )
				{
					found.u32_v = curr_indexed_mesh->vertices_count++;
					bool res =
					B12_to_u32_hash_table_insert( &vertices_ht,
					                              {.v3 = new_vertex},
					                              found.u32_v );
					lpr_assert( res );
					curr_indexed_mesh->vertices[found.u32_v] = new_vertex;
				}

				mids_ids[internal_vid] = found.u32_v;

				found =
				B12_to_u32_hash_table_retrieve( &vertices_ht, {.v3 = a_vertex } );
				if ( !found.bool_v )
				{
					found.u32_v = curr_indexed_mesh->vertices_count++;
					bool res =
					B12_to_u32_hash_table_insert( &vertices_ht,
					                              {.v3 = a_vertex},
					                              found.u32_v );
					lpr_assert( res );
					curr_indexed_mesh->vertices[found.u32_v] = a_vertex;
				}

				old_v_ids[internal_vid] = found.u32_v;
			}

			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] = old_v_ids[0];
			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] =  mids_ids[0];
			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] =  mids_ids[2];

			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] = old_v_ids[1];
			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] =  mids_ids[1];
			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] =  mids_ids[0];

			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] = old_v_ids[2];
			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] =  mids_ids[2];
			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] =  mids_ids[1];

			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] =  mids_ids[0];
			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] =  mids_ids[1];
			curr_indexed_mesh->indices[curr_indexed_mesh->indices_count++] =  mids_ids[2];
		}
	}

	tmp_mem_stack->first_free = pop;
}
