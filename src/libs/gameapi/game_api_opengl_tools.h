#ifndef OPENGL_TOOLS_H
#define OPENGL_TOOLS_H 1

struct Game_Memory;
#include "game_api_lepre.h"
#include "game_api.h"
#include "basic_types.h"
#include "tgmath.h"
#include "our_gl.h"

struct
File
{
	const char * file_path;
	Time modified_time;
};

struct
Shader
{
	File geom, vert, frag;
	GLuint program;
	GLint VP_loc;
	GLint V_loc;
	GLint P_loc;
	GLint M_loc;
	GLint instance_M_loc;

	GLint color_loc;
	GLint instance_color_loc;
	GLint vertex_loc;
	GLint vertex_color_loc;

	GLint normal_loc;
	GLint uv_loc;

#define MAX_GENERIC_LOCS_COUNT 8
	GLint attr_float[MAX_GENERIC_LOCS_COUNT];
	GLint attr_vec2 [MAX_GENERIC_LOCS_COUNT];
	GLint attr_vec3 [MAX_GENERIC_LOCS_COUNT];
	GLint attr_vec4 [MAX_GENERIC_LOCS_COUNT];
	GLint unif_float[MAX_GENERIC_LOCS_COUNT];
	GLint unif_vec2 [MAX_GENERIC_LOCS_COUNT];
	GLint unif_vec3 [MAX_GENERIC_LOCS_COUNT];
	GLint unif_vec4 [MAX_GENERIC_LOCS_COUNT];
};


struct
Camera3D
{
	Mx4 V,P,VP;
	V3  pos, up, look;
	V2  vanishing_point;
	V2  size;
	f32 near;
	f32 fov;
	f32 inverse_aspect_ratio; // NOTE(theGiallo): h / w
};

union
Controlled_Motion
{
	struct
	{
		f32 side;
		f32 straight;
		f32 vertical;
	};
	V3 v3;
};
struct
Distributed_Speed
{
	Controlled_Motion positive, negative;
};

struct
Colored_Vertex
{
	V3 vertex;
	V3 color;
};

struct
Frame_Buffer_With_Depth
{
	GLuint linear_fbo,
	       linear_color_rt,
	       depth_rt,
	       linear_fb_texture;
	V2u32 size;
};

// NOTE(theGiallo): h in [0,1)
Lpr_Rgba_f32
rgba_from_hsva( Lpr_Rgba_f32 hsva );
Lpr_Rgb_f32
rgb_from_hsv( Lpr_Rgb_f32 hsv );

V3
rgb_from_v2( V2 v );

bool
create_framebuffer_with_depth( V2u32 px_size,
                               Frame_Buffer_With_Depth * fb );

inline
void
create_and_fill_ogl_buffer( GLuint * buffer, GLenum target, GLenum usage, void * data, GLsizeiptr byte_size )
{
	glGenBuffers( 1, buffer );
	glBindBuffer( target, *buffer );
	glBufferData( target, byte_size,
	              data,  usage );
	glBindBuffer( target, 0 );
}

bool
create_program_from_text( GLuint *     gl_program,
                          const char * geometry_shader_text,
                          GLint        geometry_shader_text_length,
                          const char * vertex_shader_text,
                          GLint        vertex_shader_text_length,
                          const char * fragment_shader_text,
                          GLint        fragment_shader_text_length );

bool
create_program( System_API * sys_api, Game_Memory * game_mem,
                const char * geom, const char * vert, const char * frag,
                GLuint * out_program );

void
render_colored_vertices( Shader * shader,
                         Colored_Vertex * vertices, u32 vertices_in_buffer,
                         u32 vertices_count,
                         Lpr_Rgba_f32 * default_color,
                         GLuint vbo, GLenum draw_mode, GLenum buffer_usage );

////////////////////////////////////////////////////////////////////////////////
// NOTE(theGiallo): camera

extern
inline
void
initialize_camera( Camera3D * camera, f32 inverse_aspect_ratio, f32 near = 0.1f, f32 fov = 90.0f );

extern
inline
void
resize_camera( Camera3D * camera, u32 width, u32 height );

extern
inline
void
update_view( Camera3D * camera );

extern
inline
void
set_projection( Camera3D * camera, f32 near, V2 vanishing_point, V2 size );

extern
inline
void
set_projection( Camera3D * camera, f32 near, V2 vanishing_point, f32 fov, f32 inverse_aspect_ratio );

extern
inline
V2u32
compute_window_center( Game_Memory * game_mem );

extern
inline
void
warp_mouse_to_center( System_API * sys_api, Game_Memory * game_mem );

// NOTE(theGiallo):
// dir should be positive if the key is ahead or right
// dir should be negative if the key is back or left
extern
inline
void
manage_motion_key( Game_Memory * game_mem, Phys_Key key, s32 dir, f32 * var );

// NOTE(theGiallo): ahead, right and up are supposed to be normalized
// motion values are supposed to be +1, 0 or-1
extern
inline
void
apply_controlled_motion( Controlled_Motion motion, V3 ahead, V3 right, V3 up,
                         Distributed_Speed * distributed_speed, f32 speed_mod,
                         Time delta_time, V3 * pos );

extern
inline
void
apply_controlled_motion( Controlled_Motion motion, Camera3D * camera,
                         Distributed_Speed * distributed_speed, f32 speed_mod,
                         Time delta_time );

extern
inline
void
per_frame_camera_controls_and_update(
   Game_Memory * game_mem, Camera3D * camera, Controlled_Motion * camera_motion,
   Distributed_Speed * distributed_camera_speed, f32 shift_speed_mod );

extern
inline
bool
fp_camera_mouse_motion( System_API * sys_api, Game_Memory * game_mem,
                        s32 pos_x, s32 pos_y,
                        s32 motion_x, s32 motion_y, Camera3D * camera );
extern
inline
void
set_ogl_camera( Shader * shader, Camera3D * camera );

extern
void
dynamically_update_shader( System_API * sys_api, Game_Memory * game_mem, Shader * shader );

////////////////////////////////////////////////////////////////////////////////

struct
Grid_Planes
{
	V3    * points;
	V3    * normals;
	V3    * ups;
	float * grid_cell_sides;
	float * grid_sides;
	V4    * grid_colors;
	V4    * grid_bg_colors;
	u32 count;
	u32 points_vb;
	u32 normals_vb;
	u32 ups_vb;
	u32 grid_cell_sides_vb;
	u32 grid_sides_vb;
	u32 grid_colors_vb;
	u32 grid_bg_colors_vb;
};

void
init_grid_planes( Grid_Planes * planes );

void
render_planes( Shader * shader, Grid_Planes * planes );

////////////////////////////////////////////////////////////////////////////////

struct
Indexed_Mesh
{
	V3         * vertices;
	u16        * indices;

	// NOTE(theGiallo): optionals
	V3         * normals;
	V2         * uvs;
	Lpr_Rgb_u8 * u8_colors;

	u32 vertices_count;
	u32 indices_count;

	struct
	OGL
	{
		u32 vertex_buffer;
		u32 index_buffer;
		u32 normals_buffer;
		u32 uvs_buffer;
		u32 u8_colors_buffer;
	} ogl;
};

enum class
Indexed_Mesh_Optionals_Flags
{
	has_normals   = 0x1 << 0,
	has_uvs       = 0x1 << 1,
	has_u8_colors = 0x1 << 2,
};

inline
u64
indexed_mesh_compute_bytes_size( u32 vertices_count, u32 indices_count, u32 optionals_flags )
{
	u64 ret = 0;

	ret += vertices_count * sizeof( ((Indexed_Mesh*)0)->vertices[0] );
	ret += indices_count  * sizeof( ((Indexed_Mesh*)0)->indices[0]);

	if ( optionals_flags & (u32)Indexed_Mesh_Optionals_Flags::has_normals )
	{
		ret += vertices_count * sizeof( ((Indexed_Mesh*)0)->normals[0] );
	}

	if ( optionals_flags & (u32)Indexed_Mesh_Optionals_Flags::has_uvs )
	{
		ret += vertices_count * sizeof( ((Indexed_Mesh*)0)->uvs[0] );
	}

	if ( optionals_flags & (u32)Indexed_Mesh_Optionals_Flags::has_u8_colors )
	{
		ret += vertices_count * sizeof( ((Indexed_Mesh*)0)->u8_colors[0] );
	}

	return ret;
}

inline
u8 *
indexed_mesh_init( Indexed_Mesh * indexed_mesh, u8 * mem, u32 vertices_count, u32 indices_count, u32 optionals_flags )
{
	u8 * ret = mem;

	indexed_mesh->vertices = (V3*)ret;
	ret += vertices_count * sizeof( ((Indexed_Mesh*)0)->vertices[0] );

	indexed_mesh->indices = (u16*)ret;
	ret += indices_count  * sizeof( ((Indexed_Mesh*)0)->indices[0]);

	if ( optionals_flags & (u32)Indexed_Mesh_Optionals_Flags::has_normals )
	{
		indexed_mesh->normals = (V3*)ret;
		ret += vertices_count * sizeof( ((Indexed_Mesh*)0)->normals[0] );
	} else
	{
		indexed_mesh->normals = NULL;
	}

	if ( optionals_flags & (u32)Indexed_Mesh_Optionals_Flags::has_uvs )
	{
		indexed_mesh->uvs = (V2*)ret;
		ret += vertices_count * sizeof( ((Indexed_Mesh*)0)->uvs[0] );
	} else
	{
		indexed_mesh->uvs = NULL;
	}

	if ( optionals_flags & (u32)Indexed_Mesh_Optionals_Flags::has_u8_colors )
	{
		indexed_mesh->u8_colors = (Lpr_Rgb_u8*)ret;
		ret += vertices_count * sizeof( ((Indexed_Mesh*)0)->u8_colors[0] );
	} else
	{
		indexed_mesh->u8_colors = NULL;
	}

	indexed_mesh->vertices_count = vertices_count;
	indexed_mesh->indices_count  = indices_count;

	return ret;
}

void
indexed_mesh_create_ogl_buffers( Indexed_Mesh * indexed_mesh, GLenum usage );
void
indexed_mesh_upload_to_gpu_1st_time( Indexed_Mesh * indexed_mesh, GLenum usage );

void
indexed_mesh_upload_to_gpu_update( Indexed_Mesh * indexed_mesh, GLenum usage );

void
indexed_mesh_render_single( Indexed_Mesh * indexed_mesh, Shader * shader,
                            Mx4 model_matrix, Lpr_Rgba_f32 mesh_color );

void
indexed_mesh_render_multiple( Indexed_Mesh * indexed_mesh,
                              u32 model_matrices_buffer, u32 mesh_colors_buffer,
                              u32 count,
                              Shader * shader );


inline
void
upload_matrices_buffer_static( Mx4 * matrices, u32 count, u32 buffer )
{
	glBindBuffer( GL_ARRAY_BUFFER, buffer );
	glBufferData( GL_ARRAY_BUFFER, sizeof(Mx4) * count, matrices, GL_STATIC_DRAW );
}

inline
void
upload_mesh_color_buffer_static( u8 * matrices, u32 count, u32 buffer )
{
	glBindBuffer( GL_ARRAY_BUFFER, buffer );
	glBufferData( GL_ARRAY_BUFFER, sizeof(u8) * 3 * count, matrices, GL_STATIC_DRAW );
}

inline
void
update_matrices_buffer_stream( Mx4 * matrices, u32 count, u32 buffer )
{
	glBindBuffer( GL_ARRAY_BUFFER, buffer );
	glBufferData( GL_ARRAY_BUFFER, sizeof(Mx4) * count, matrices, GL_STREAM_DRAW );
}

inline
void
update_mesh_color_buffer_stream( u8 * matrices, u32 count, u32 buffer )
{
	glBindBuffer( GL_ARRAY_BUFFER, buffer );
	glBufferData( GL_ARRAY_BUFFER, sizeof(u8) * 3 * count, matrices, GL_STREAM_DRAW );
}

struct
Packed_Vertex_PUVN
{
	V3 vertex;
	V3 normal;
	V2 uv;
};

bool
load_obj_into_indexed_mesh( const u8 * file_path, Indexed_Mesh * indexed_mesh, Mem_Pool * mem_pool, Mem_Stack * tmp_mem_stack );

void
generate_icosphere_as_indexed_mesh( Indexed_Mesh * indexed_mesh,
                                    Mem_Pool * mem_pool,
                                    Mem_Stack * tmp_mem_stack,
                                    u32 subdivisions_count );


#endif
