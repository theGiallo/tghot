#include "game_api_cod.h"

#include "game_api_utility.h"

////////////////////////////////////////////////////////////////////////////////

void
generic_table_multicolumn_compact( Generic_Table_Multicolumn * t )
{
#define COD_BOTH_BIT_AND_BLOCK_IN_LOOP 0
	u32 capacity   = t->capacity;
	u32 occupancy  = t->occupancy;
	u32 data_count = t->data_count;
	u32 visited    = 0;
	u32 hole_size  = 0;
	u32 solid_size = 0;
	u64 * ids = (u64*)t->ids;
	u8 ** data = (u8**)t->data;
	u32 * data_elements_sizes = t->data_elements_sizes;
	u8  * in_use_bv = t->in_use_bv;
	u8  * occupancy_per_255_blocks = t->occupancy_per_255_blocks;
	u64_to_u32_Hash_Table * ids_location_ht = &t->ids_location_ht;
	// NOTE(theGiallo): the '<=' makes i to overflow of 1 but it's necessary
	// to compact a bloc that reaches the end.
	for ( u32 i = 0;
	      i <= capacity && visited <= occupancy;
	      ++i )
	{
		if ( COD_BITTEST( in_use_bv, i ) )
		{
			++visited;
			++solid_size;
		} else
		{
			if ( solid_size )
			{
				if ( hole_size )
				{
					u32 src_idx = i - solid_size;
					for ( u32 d = 0; d != data_count; ++d )
					{
						u32 e_s = data_elements_sizes[d];
						memmove( data[d] + e_s * ( src_idx - hole_size ),
						         data[d] + e_s * src_idx,
						         e_s * ( solid_size ) );
					}
				#if 0
					memmove( ids + ( src_idx - hole_size ),
					         ids + src_idx,
					         sizeof(ids[0]) * ( solid_size ) );
				#endif
					for ( u32 j = src_idx - hole_size,
					          k = src_idx;
					          k != src_idx + solid_size;
					          ++j, ++k )
					{
					#if COD_BOTH_BIT_AND_BLOCK_IN_LOOP
						if ( j/255 != k/255 )
						{
							// NOTE(theGiallo): check to avoid overflow
							occupancy_per_255_blocks[j/255] += COD_BITTEST( in_use_bv, j ) ? 0 : 1;
							occupancy_per_255_blocks[k/255] -= COD_BITTEST( in_use_bv, k ) ? 1 : 0;
						}
						COD_BITSET( in_use_bv, j );
						COD_BITCLEAR( in_use_bv, k );
					#endif
						ids[j] = ids[k];
						bool b_res =
						u64_to_u32_hash_table_change( ids_location_ht, ids[k], j );
						lpr_assert( b_res );
					}
				}

				solid_size = 0;
			}
			++hole_size;
		}
	}
#if !COD_BOTH_BIT_AND_BLOCK_IN_LOOP
	// NOTE(theGiallo): this assumes u8 is 8bits == 1B, the macros don't
	u32 ff_slots_count = occupancy >> 3;
	memset( in_use_bv, 0xFF, ff_slots_count );
	if ( ff_slots_count != COD_BITNSLOTS( capacity ) )
	{
		in_use_bv[ff_slots_count] = 0x00;
		for ( u32 i = occupancy & ( ~7 );
		      i != occupancy;
		      ++i )
		{
			COD_BITSET( in_use_bv, i );
		}
		// TODO(theGiallo, 2017-01-28): passed count could be 0, maybe check and
		// don't call.
		memset( in_use_bv + ( ff_slots_count + 1 ),
		        0x00, COD_BITNSLOTS( capacity ) - ( ff_slots_count + 1 ) );
	}

	u32 blocks_255_count = ( capacity + 254 ) / 255;
	ff_slots_count = occupancy / 255;
	memset( occupancy_per_255_blocks, 0xFF, ff_slots_count );
	if ( ff_slots_count != blocks_255_count )
	{
		occupancy_per_255_blocks[ff_slots_count] = occupancy % 255;

		// TODO(theGiallo, 2017-01-28): maybe put here a check on the count?
		memset( occupancy_per_255_blocks + ( ff_slots_count + 1 ),
		        0x00, blocks_255_count - ( ff_slots_count + 1 ) );
	}
#endif

#if 0
#if 0
	u64_to_u32_hash_table_clear( ids_location_ht );
	for ( u32 i = 0; i != occupancy; ++i )
	{
		u64_to_u32_hash_table_insert( ids_location_ht, ids[i], i );
	}
#else
	for ( u32 i = 0; i != occupancy; ++i )
	{
		u64_to_u32_hash_table_change( ids_location_ht, ids[i], i );
	}
#endif
#endif
}

bool
generic_table_multicolumn_insert_struct( Generic_Table_Multicolumn * t, u64 id, void * element_struct )
{
	bool ret = false;

	// NOTE(theGiallo): dumb search
	u32 capacity   = t->capacity;
	u32 occupancy  = t->occupancy;
	if ( occupancy == capacity )
	{
		return ret;
	}

	u32 occupancy_blocks_count = ( capacity + 254 ) / 255;
	u32 occupancy_block_id;
	for ( occupancy_block_id = 0;
	      occupancy_block_id != occupancy_blocks_count &&
	      t->occupancy_per_255_blocks[occupancy_block_id] == 0xFF;
	      ++occupancy_block_id )
	{}
	lpr_assert( occupancy_block_id != occupancy_blocks_count );

	u8 * in_use_bv = t->in_use_bv;
	for ( u32 i = occupancy_block_id * 0xFF;
	      i < occupancy_block_id * 0xFF + 0xFF;
	      ++i )
	{
		if ( !COD_BITTEST( in_use_bv, i ) )
		{
			ret = true;
			COD_BITSET( in_use_bv, i );
			t->ids[i] = id;
			u32 data_count = t->data_count;
			u32 * data_elements_sizes = t->data_elements_sizes;
			u8 ** data = (u8**)t->data;
			u8 * s = (u8*)element_struct;
			for ( u32 d = 0; d != data_count; ++d )
			{
				u32 e_s = data_elements_sizes[d];
				memcpy( data[d] + i * e_s, s, e_s );
				lpr_assert( ret );
				s += e_s;
			}
			lpr_assert( ret );
			ret = u64_to_u32_hash_table_insert( &t->ids_location_ht, id, i );
			lpr_assert( ret );
			++t->occupancy;
			++t->occupancy_per_255_blocks[occupancy_block_id];
			lpr_assert( ret );
			break;
		}
	}

#if DEBUG
	if ( !ret )
	{
		lpr_log_dbg( "occupancy_blocks_count = %u", occupancy_blocks_count );
		lpr_log_dbg( "occupancy_block_id = %u -> %u", occupancy_block_id, t->occupancy_per_255_blocks[occupancy_block_id] );
		for ( u32 i = occupancy_block_id * 0xFF;
		      i < occupancy_block_id * 0xFF + 0xFF;//capacity;
		      ++i )
		{
			lpr_log_dbg( "i = %d -> i/8 = %d -> %x", i, i/8, t->in_use_bv[i/8] );
		}
	}
#endif
	lpr_assert( ret );

	return ret;
}

// NOTE(theGiallo): does not checks for duplicates
// NOTE(theGiallo): data is in the same format as the out parameter 'data' of the search
bool
generic_table_multicolumn_insert_pointers( Generic_Table_Multicolumn * t, u64 id, void ** in_data )
{
	bool ret = false;

	// NOTE(theGiallo): dumb search
	u32 capacity   = t->capacity;
	u32 occupancy  = t->occupancy;
	if ( occupancy == capacity )
	{
		return ret;
	}

	u32 occupancy_blocks_count = ( capacity + 254 ) / 255;
	u32 occupancy_block_id;
	for ( occupancy_block_id = 0;
	      occupancy_block_id != occupancy_blocks_count &&
	      t->occupancy_per_255_blocks[occupancy_block_id] == 0xFF;
	      ++occupancy_block_id )
	{}
	lpr_assert( occupancy_block_id != occupancy_blocks_count );

	u8 * in_use_bv = t->in_use_bv;
	for ( u32 i = occupancy_block_id * 0xFF;
	      i < occupancy_block_id * 0xFF + 0xFF;
	      ++i )
	{
		if ( !COD_BITTEST( in_use_bv, i ) )
		{
			ret = true;
			COD_BITSET( in_use_bv, i );
			t->ids[i] = id;
			u32 data_count = t->data_count;
			u32 * data_elements_sizes = t->data_elements_sizes;
			u8 ** data = (u8**)t->data;
			for ( u32 d = 0; d != data_count; ++d )
			{
				u8 * s = (u8*)in_data[d];
				u32 e_s = data_elements_sizes[d];
				memcpy( data[d] + i * e_s, s, e_s );
			}
			ret = u64_to_u32_hash_table_insert( &t->ids_location_ht, id, i );
			lpr_assert( ret );
			++t->occupancy;
			++t->occupancy_per_255_blocks[occupancy_block_id];
			lpr_assert( ret );
			break;
		}
	}

#if DEBUG
	if ( !ret )
	{
		lpr_log_dbg( "occupancy_blocks_count = %u", occupancy_blocks_count );
		lpr_log_dbg( "occupancy_block_id = %u -> %u", occupancy_block_id, t->occupancy_per_255_blocks[occupancy_block_id] );
		for ( u32 i = occupancy_block_id * 0xFF;
		      i < occupancy_block_id * 0xFF + 0xFF;//capacity;
		      ++i )
		{
			lpr_log_dbg( "i = %d -> i/8 = %d -> %x", i, i/8, t->in_use_bv[i/8] );
		}
	}
#endif
	lpr_assert( ret );

	return ret;
}

void *
generic_table_multicolumn_search_single_data( Generic_Table_Multicolumn * t, u64 id, u32 data_index )
{
	lpr_assert( data_index <= t->data_count );

	void * ret = NULL;

	if ( !t->data_count )
	{
		return ret;
	}

	u32_bool loc = u64_to_u32_hash_table_retrieve( &t->ids_location_ht, id );
	if ( loc.bool_v )
	{
		lpr_assert( COD_BITTEST( t->in_use_bv, loc.u32_v ) );
		{
			lpr_assert( t->ids[loc.u32_v] == id );
			{
				u32 e_s = t->data_elements_sizes[data_index];
				ret = ((u8*)t->data[data_index]) + loc.u32_v * e_s;
			}
		}
	}

	return ret;
}
s32
generic_table_multicolumn_search( Generic_Table_Multicolumn * t, u64 id )
{
	s32 ret = -1;

	if ( !t->capacity )
	{
		return ret;
	}

	u32_bool loc = u64_to_u32_hash_table_retrieve( &t->ids_location_ht, id );
	if ( loc.bool_v )
	{
		lpr_assert( COD_BITTEST( t->in_use_bv, loc.u32_v ) );
		{
			lpr_assert( t->ids[loc.u32_v] == id );
			ret = loc.u32_v;
		}
	}

	return ret;
}
s32
generic_table_multicolumn_search( Generic_Table_Multicolumn * t, u64 id, void ** out_data )
{
	s32 ret = -1;

	if ( !t->capacity )
	{
		return ret;
	}

	u32_bool loc = u64_to_u32_hash_table_retrieve( &t->ids_location_ht, id );
	if ( loc.bool_v )
	{
		lpr_assert( COD_BITTEST( t->in_use_bv, loc.u32_v ) );
		{
			lpr_assert( t->ids[loc.u32_v] == id );
			generic_table_multicolumn_fill_pointers( t, loc.u32_v, out_data );
			ret = loc.u32_v;
		}
	}

	return ret;
}

// NOTE(theGiallo): returns number of entries found and allocates in mem_stack
// an array of u32 storing in there the indexes of such entries
u32
generic_table_multicolumn_search_many( Generic_Table_Multicolumn * t, u64 id, Mem_Stack * mem_stack )
{
	u32 ret = 0;

	if ( !t->capacity )
	{
		return ret;
	}

	ret = u64_to_u32_hash_table_retrieve_many( &t->ids_location_ht, id, mem_stack );

	return ret;
}

void
generic_table_multicolumn_copy_data_to_struct( Generic_Table_Multicolumn * t, u32 entry_index, void * struct_ptr )
{
	lpr_assert( entry_index < t->capacity );

	for ( u32 data_count = t->data_count, i = 0;
	      i != data_count;
	      ++i )
	{
		u32 data_size = t->data_elements_sizes[i];
		memcpy( struct_ptr, (u8*)(t->data[i]) + data_size * entry_index, data_size );
		struct_ptr = (u8*)struct_ptr + data_size;
	}
}

bool
generic_table_multicolumn_remove( Generic_Table_Multicolumn * t, u64 id )
{
	bool ret = false;

	u8 * in_use_bv = t->in_use_bv;

	u32_bool loc = u64_to_u32_hash_table_remove( &t->ids_location_ht, id );
	if ( loc.bool_v )
	{
		lpr_assert( COD_BITTEST( in_use_bv, loc.u32_v ) );
		{
			lpr_assert( t->ids[loc.u32_v] == id );
			{
				ret = true;
				COD_BITCLEAR( in_use_bv, loc.u32_v );
				lpr_assert( !COD_BITTEST( in_use_bv, loc.u32_v ) );
				--t->occupancy;
				--t->occupancy_per_255_blocks[loc.u32_v/0xFF];
			}
		}
	}
	if ( !ret )
	{
		lpr_log_dbg( "asdf" );
	}

	return ret;
}

u32
generic_table_multicolumn_remove_many_and_feedback( Generic_Table_Multicolumn * t, u64 id, Mem_Stack * mem_stack )
{
	u32 ret = 0;

	u8 * in_use_bv = t->in_use_bv;

	u32 * ret_array = (u32*)( mem_stack->mem_buf + mem_stack->first_free );
	ret = u64_to_u32_hash_table_remove_many_and_feedback( &t->ids_location_ht, id, mem_stack );
	for ( u32 i = 0; i != ret; ++i )
	{
		u32 index = ret_array[i];
		lpr_assert( COD_BITTEST( in_use_bv, index ) );
		{
			lpr_assert( t->ids[index] == id );
			{
				COD_BITCLEAR( in_use_bv, index );
				lpr_assert( !COD_BITTEST( in_use_bv, index ) );
				--t->occupancy;
				--t->occupancy_per_255_blocks[index/0xFF];
			}
		}
	}
	if ( !ret )
	{
		lpr_log_dbg( "asdf" );
	}

	return ret;
}

u32
generic_table_multicolumn_remove_many( Generic_Table_Multicolumn * t, u64 id, Mem_Stack * mem_stack_tmp )
{
	u32 ret = 0;

	u32 pop = mem_stack_tmp->first_free;

	ret = generic_table_multicolumn_remove_many_and_feedback( t, id, mem_stack_tmp );

	mem_stack_tmp->first_free = pop;

	return ret;
}

////////////////////////////////////////////////////////////////////////////////

void
generic_table_compact( Generic_Table * t )
{
	u32 capacity   = t->capacity;
	u32 occupancy  = t->occupancy;
	u32 data_element_size = t->data_element_size;
	u32 visited    = 0;
	u32 hole_size  = 0;
	u32 solid_size = 0;
	u8 * data = (u8*)t->data;
	u64 * ids = (u64*)t->ids;
	// NOTE(theGiallo): the '<=' makes i to overflow of 1 but it's necessary
	// to compact a bloc that reaches the end.
	for ( u32 i = 0;
	      i <= capacity && visited <= occupancy;
	      ++i )
	{
		if ( COD_BITTEST( t->in_use_bv, i ) )
		{
			++visited;
			++solid_size;
		} else
		{
			if ( solid_size )
			{
				if ( hole_size )
				{
					u32 src_idx = i - solid_size;
					memmove( data + data_element_size * ( src_idx - hole_size ),
					         data + data_element_size * src_idx,
					         data_element_size * ( solid_size ) );
					memmove( ids + ( src_idx - hole_size ),
					         ids + src_idx,
					         sizeof(ids[0]) * ( solid_size ) );
				}

				solid_size = 0;
			}
			++hole_size;
		}
	}
	// NOTE(theGiallo): this assumes u8 is 8bits == 1B, the macros don't
	memset( t->in_use_bv, 0xFF, occupancy >> 3 );
	if ( occupancy != capacity )
	{
		t->in_use_bv[occupancy >> 3] = 0x00;
		for ( u32 i = occupancy & ( ~7 );
		      i != occupancy;
		      ++i )
		{
			COD_BITSET( t->in_use_bv, i );
		}
		memset( t->in_use_bv + ( capacity - ( ( capacity - occupancy ) >> 3 ) ),
		        0x00, ( capacity - occupancy ) >> 3 );
	}
}

bool
generic_table_insert( Generic_Table * t, u64 id, void * element )
{
	bool ret = false;

	// NOTE(theGiallo): dumb search
	u32 capacity   = t->capacity;
	u32 occupancy  = t->occupancy;
	if ( occupancy == capacity )
	{
		return ret;
	}

	u8 * in_use_bv = t->in_use_bv;
	for ( u32 i = 0;
	      i < capacity;
	      ++i )
	{
		if ( !COD_BITTEST( in_use_bv, i ) )
		{
			ret = true;
			COD_BITSET( in_use_bv, i );
			t->ids[i] = id;
			u32 data_element_size = t->data_element_size;
			u8 * data = (u8*)t->data;
			memcpy( data + i * data_element_size, element, data_element_size );
			break;
		}
	}

	return ret;
}

void *
generic_table_search( Generic_Table * t, u64 id )
{
	void * ret = NULL;
	// NOTE(theGiallo): dumb search
	u32 capacity   = t->capacity;
	u32 occupancy  = t->occupancy;
	u32 visited    = 0;
	u8 * in_use_bv = t->in_use_bv;

	for ( u32 i = 0;
	      i < capacity && visited < occupancy;
	      ++i )
	{
		if ( COD_BITTEST( in_use_bv, i ) )
		{
			++visited;
			if ( t->ids[i] == id )
			{
				u32 e_s = t->data_element_size;
				ret = ((u8*)t->data) + i * e_s;
				break;
			}
		}
	}

	return ret;
}

bool
generic_table_remove( Generic_Table * t, u64 id )
{
	bool ret = false;
	// NOTE(theGiallo): dumb search
	u32 capacity   = t->capacity;
	u32 occupancy  = t->occupancy;
	u32 visited    = 0;
	u8 * in_use_bv = t->in_use_bv;

	for ( u32 i = 0;
	      i < capacity && visited < occupancy;
	      ++i )
	{
		if ( COD_BITTEST( in_use_bv, i ) )
		{
			++visited;
			if ( t->ids[i] == id )
			{
				ret = true;
				COD_BITCLEAR( in_use_bv, i );
				break;
			}
		}
	}

	return ret;
}

////////////////////////////////////////////////////////////////////////////////

#include "game_api.h"
#include "tgmath.h"

// NOTE(theGiallo): notes on the benchmarks
// - dumb search
// Performing 1M iterations and for each one choosing with 75% to do an
// insertion and with 25% a removal we get an avg time per insertion of
// 0.25ms per iteration in release build and 0.5ms in debug build.
// Performing a compact every 100k iterations does not modify the timings.
// The avg compact duration is 1.8ms.
//
// 1M iterations with compact at each iteration:
//    release   insert: 0.000074969ms   removal: 0.21ms       compact: 1.5ms
// 1M iterations with no compact:
//    release   insert: 0.289ms         removal: 0.20ms       compact: -
// 1M iterations with compact every 100k iterations:
//    release   insert: 0.2928ms        removal: 0.20ms       compact: 1.65ms
// 1M iterations with compact every 10k iterations:
//    release   insert: 0.2839ms        removal: 0.1964ms     compact: 1.72ms
//
// - hash table with data in array + lists ( compact clears and rebuilds ht )
// 1M iterations with no compact:
//    release   insert: 0.0007598ms     removal: 0.000084ms   compact: -
// 1M iterations with compact every 10k iterations:
//    release   insert: 0.0007946ms     removal: 0.0001ms     compact: 15.0ms
// 1M iterations with compact every 100k iterations:
//    release   insert: 0.000794ms      removal: 0.000098ms   compact: 12.8ms
//  + using 'change' instead of clear and insert for ht rebuild
//                                                            compact: 12.49ms
//  + using 'change' instead of clear and insert for ht rebuild
//    but just after each memmove
//                                                            compact: 2.79ms
//  + using 'change' instead of clear and insert for ht rebuild
//    but just after each ids moved, in a loop, no memmove
//                                                            compact: 3.57ms
//  + using 'change' instead of clear and insert for ht rebuild
//    but just after each ids moved, in a loop, no memmove
//    and no memcpy for both bits and 255 block counts but in the same loop
//    one at a time
//                                                            compact: 3.9ms

void
cod_test( Mem_Stack * mem )
{
	u64 pop = mem->first_free;
	Generic_Table_Multicolumn dynamic_physics_data = {};
	u32 data_elements_sizes[7] = {
	   sizeof(V3),   // pos
	   sizeof(V3),   // vel
	   sizeof(V3),   // acc
	   sizeof(Quat), // rot
	   sizeof(V3),   // angular_vel
	   sizeof(V3),   // angular_acc
	   sizeof(Mx3),  // inverse inertia tensor
	};
	u32 data_count = ARRAY_COUNT( data_elements_sizes );
	u32 capacity = 1024 * 1024;
	void * table_mem =
	   mem->push(
	      generic_table_multicolumn_compute_memory_size(
	         data_elements_sizes, data_count, capacity ) );
	generic_table_multicolumn_init(
	   &dynamic_physics_data, (u8*)table_mem, data_elements_sizes, data_count, capacity );

	Dynamic_Physics_Data value = {
	   {1.0f,2.0f,3.0f},
	   {4.0f,5.0f,6.0f},
	   {7.0f,8.0f,9.0f},
	   {10.0f,11.0f,12.0f,13.0f},
	   {14.0f,15.0f,16.0f},
	   {17.0f,18.0f,19.0f},
	   {1001.0f,1002.0f,1003.0f},
	   {1004.0f,1005.0f,1006.0f},
	   {1007.0f,1008.0f,1009.0f},
	   {1010.0f,1011.0f,1012.0f,1013.0f},
	   {1014.0f,1015.0f,1016.0f},
	   {1017.0f,1018.0f,1019.0f},
	   {20.0f,21.0f,22.0f,
	    23.0f,24.0f,25.0f,
	    26.0f,27.0f,28.0f},
	};
	u64 id = 0xcafebabe;
	generic_table_multicolumn_insert_struct( &dynamic_physics_data, id, &value );

	Dynamic_Physics_Data retrived_value = {};
	void * pointers_data[ARRAY_COUNT(data_elements_sizes)] = {};
	s32 internal_index =
	generic_table_multicolumn_search( &dynamic_physics_data, id, pointers_data );
	lpr_log_info( "internal index = %d", internal_index );
	lpr_assert_m ( internal_index >= 0, "couldn't find a just inserted data" );
	retrived_value.pos_0     = *(V3*)  pointers_data[(u32)Dynamic_Physics_Data_enum::pos_0     ];
	retrived_value.vel_0     = *(V3*)  pointers_data[(u32)Dynamic_Physics_Data_enum::vel_0     ];
	retrived_value.acc_0     = *(V3*)  pointers_data[(u32)Dynamic_Physics_Data_enum::acc_0     ];
	retrived_value.rot_0     = *(Quat*)pointers_data[(u32)Dynamic_Physics_Data_enum::rot_0     ];
	retrived_value.ang_vel_0 = *(V3*)  pointers_data[(u32)Dynamic_Physics_Data_enum::ang_vel_0 ];
	retrived_value.ang_acc_0 = *(V3*)  pointers_data[(u32)Dynamic_Physics_Data_enum::ang_acc_0 ];
	retrived_value.pos_1     = *(V3*)  pointers_data[(u32)Dynamic_Physics_Data_enum::pos_1     ];
	retrived_value.vel_1     = *(V3*)  pointers_data[(u32)Dynamic_Physics_Data_enum::vel_1     ];
	retrived_value.acc_1     = *(V3*)  pointers_data[(u32)Dynamic_Physics_Data_enum::acc_1     ];
	retrived_value.rot_1     = *(Quat*)pointers_data[(u32)Dynamic_Physics_Data_enum::rot_1     ];
	retrived_value.ang_vel_1 = *(V3*)  pointers_data[(u32)Dynamic_Physics_Data_enum::ang_vel_1 ];
	retrived_value.ang_acc_1 = *(V3*)  pointers_data[(u32)Dynamic_Physics_Data_enum::ang_acc_1 ];
	retrived_value.inverse_inertia_tensor = *(Mx3*)pointers_data[(u32)Dynamic_Physics_Data_enum::inverse_inertia_tensor];
	lpr_assert( 0 == memcmp( &retrived_value, &value, sizeof(value) ) );

	u64 * inserted_ids = (u64*) mem->push( capacity * sizeof(u64) );
	u32 total_count_of_ids = 0;
	u64  id_seeds[2] = {89217364825221UL,91182739121987UL};
	u64 rnd_seeds[2] = {89217364825221UL,91182739121987UL};
	u64 internal_data   = 0;
	u8  internal_bit_id = 0;
	u64 total_insertion_duration = 0;
	u64 total_removal_duration = 0;
	u64 total_compact_duration = 0;
	u32 total_insertions = 0;
	u32 total_removals = 0;
	u32 total_compacts = 0;
	for ( s32 i = 0; i != 1000000; ++i )
	{
		if ( dynamic_physics_data.occupancy == 305302 )
		{
			lpr_log_dbg( "lllll" );
		}
		u64 new_id = lpr_xorshift128plus( id_seeds );
		if ( rand_bit( rnd_seeds, &internal_data, &internal_bit_id ) ||
		     rand_bit( rnd_seeds, &internal_data, &internal_bit_id ) ||
		     !total_count_of_ids )
		{
			value.pos_0.x = i;
			u64 start = RDTSC_SERIALIZING();
			bool res =
			generic_table_multicolumn_insert_struct(
			   &dynamic_physics_data, new_id, &value );
			u64 end = RDTSC_SERIALIZING();
			total_insertion_duration += end - start;
			++total_insertions;
			if ( res )
			{
				inserted_ids[total_count_of_ids] = new_id;
				++total_count_of_ids;
				lpr_log_dbg( "inserted id %016lx [occupancy = %10u]", new_id, dynamic_physics_data.occupancy );
			} else
			{
				lpr_log_dbg( "dynamic_physics_data table is full -> failed requested insert" );
			}
		} else
		{
			u32 idid = randu_between( rnd_seeds, 0, total_count_of_ids - 1 );
			u64 start = RDTSC_SERIALIZING();
			bool res =
			generic_table_multicolumn_remove( &dynamic_physics_data, inserted_ids[idid] );
			u64 end = RDTSC_SERIALIZING();
			total_removal_duration += end - start;
			++total_removals;
			if ( res )
			{
				lpr_log_dbg( "removed  id %016lx [occupancy = %10u]", inserted_ids[idid], dynamic_physics_data.occupancy );
				--total_count_of_ids;
				inserted_ids[idid] = inserted_ids[total_count_of_ids];
			} else
			{
				lpr_log_err( "failed to remove an id that we thought was there (%016lx)", inserted_ids[idid] );
				LPR_ILLEGAL_PATH();
			}
		}
#if 1
		if ( ( i % 100000 ) == 0 )
		{
			lpr_log_dbg( "compacting" );
			u64 start = RDTSC_SERIALIZING();
			generic_table_multicolumn_compact( &dynamic_physics_data );
			u64 end = RDTSC_SERIALIZING();
			total_compact_duration += end - start;
			++total_compacts;
		}
#endif
	}
	lpr_log_info( "total_insertion_duration = %lu", total_insertion_duration );
	lpr_log_info( "total_removal_duration   = %lu", total_removal_duration );
	lpr_log_info( "total_compact_duration   = %lu", total_compact_duration );
	lpr_log_info( "avg insertion duration   = %f", total_insertion_duration / (f64)total_insertions );
	lpr_log_info( "avg removal duration     = %f", total_removal_duration / (f64)total_removals );
	lpr_log_info( "avg compact duration     = %f", total_compact_duration / (f64)total_compacts );
	lpr_log_info( "total_insertions         = %u", total_insertions );
	lpr_log_info( "total_removals           = %u", total_removals );
	lpr_log_info( "total_compacts           = %u", total_compacts );
	lpr_log_info( "occupancy                = %u", dynamic_physics_data.occupancy );

	mem->first_free = pop;
}
