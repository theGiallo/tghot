#ifndef GAME_API_FFT_FIXED_POINT
#define GAME_API_FFT_FIXED_POINT 32
#endif

#ifndef GAME_API_FFT_H
#define GAME_API_FFT_H 1

#define FIXED_POINT GAME_API_FFT_FIXED_POINT
#include "kiss_fft.h"
#include "tools/kiss_fftr.h"
#include "tools/kiss_fftnd.h"
#include "tools/kiss_fftndr.h"
#undef FIXED_POINT

#endif /* ifndef GAME_API_FFT_H */

#if GAME_API_FFT_EMIT_IMPLEMENTATION && ( !defined(GAME_API_FFT_IMPLEMENTATION_EMITTED) || !GAME_API_FFT_IMPLEMENTATION_EMITTED )

#ifdef GAME_API_FFT_IMPLEMENTATION_EMITTED
#undef GAME_API_FFT_IMPLEMENTATION_EMITTED
#endif
#define GAME_API_FFT_IMPLEMENTATION_EMITTED 1
#define FIXED_POINT GAME_API_FFT_FIXED_POINT
#include "kiss_fft.c"
#include "tools/kiss_fftr.c"
#include "tools/kiss_fftnd.c"
#include "tools/kiss_fftndr.c"
#undef FIXED_POINT

#endif
