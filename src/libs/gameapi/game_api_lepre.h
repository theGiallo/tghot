#ifndef GAME_API_LEPRE_H
#define GAME_API_LEPRE_H 1

#if DEBUG
	#define LPR_DEBUG 1
#endif
#define LPR_NO_CPP_MATH 1
#define LPR_DEBUG_LINES 1
#define LPR_MAX_DEBUG_LINES 65536
#define LPR_SHADERS_FOLDER_PATH "./resources/shaders/"
#ifndef LPR_APP_NAME
	#define LPR_APP_NAME "game api"
#endif
#define LPR_IMPLEMENTATION 1
#define LPR_INTERNAL 1
#include "lepre.h"

#endif /* ifndef GAME_API_LEPRE_H */
