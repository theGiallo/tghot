#ifndef __TG_ARRAY_H__
#define __TG_ARRAY_H__ 1

#include <initializer_list>

#include "basic_types.h"
#include "macro_tools.h"
#include "system.h"

namespace tg
{
	template<typename T>
	struct
	Array
	{
		T * array;
		s64 count;

		static
		Array
		Make( s64 count )
		{
			Array ret = {};
			ret.allocate( count );
			return ret;
		}

		static
		Array
		Make( s64 count, T * arr )
		{
			Array ret = {};
			if ( ret.allocate( count ) )
			{
				memcpy( ret.array, arr, sizeof ( arr ) * count );
			}
			return ret;
		}

		static
		Array
		Make( std::initializer_list<T> il )
		{
			Array ret = {};

			if ( ret.allocate( il.size() ) )
			{
				s64 i = 0;
				for ( T e : il )
				{
					ret.array[i] = e;
					++i;
				}
			}

			return ret;
		}

		bool
		allocate( s64 count )
		{
			if ( array )
			{
				deallocate();
			}

			s64 size = count * sizeof ( T );
			array = (T*)sys_mem_reserve( size );
			bool ret = array != 0;
			if ( ret )
			{
				ret = sys_mem_commit( array, size );
				if ( ret )
				{
					this->count = count;
				}
			}

			return ret;
		}
		bool
		allocate_clean( s64 count )
		{
			bool ret = allocate( count );
			if ( ret )
			{
				s64 size = count * sizeof ( T );
				memset( array, 0x0, size );
			}
			return ret;
		}

		void
		deallocate()
		{
			s64 size = count * sizeof ( T );
			if ( array && count )
			{
				sys_mem_free( array, size );
			}
			zero_struct( *this );
		}
	};
}

#endif /* ifndef __TG_ARRAY_H__ */
