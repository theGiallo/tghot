#include "system.h"
#include "macro_tools.h"

#include <stdio.h>

s64
get_file_size( const char * file_path )
{
#if 0
	s64 ret = 0;

	FILE * fp = fopen( file_path, "rb" );
	if ( !fp )
	{
		return -SYS_ERR_FILE_OPEN_FAILED;
	}

	fseek( fp, 0, SEEK_END ); // NOTE(theGiallo): SEEK_END is non std, so ~non-portable
	ret = ftell ( fp );

	fclose( fp );

	return ret;
#else

	s64 ret = 0;

	struct stat buf;

	if ( stat( file_path, &buf ) < 0 )
	{
		ret = SYS_ERRNO_TO_SYS_ERROR( errno );
		return ret;
	}

	ret = buf.st_size;

	return ret;
#endif
}
s64
read_entire_file( const char * file_path, void * mem, s64 mem_size )
{
	s64 ret = 0;

	FILE * fp = fopen( file_path, "rb" );
	if ( !fp )
	{
		return -SYS_ERR_FILE_OPEN_FAILED;
	}

	fseek( fp, 0, SEEK_END ); // NOTE(theGiallo): SEEK_END is non std, so ~non-portable
	s64 size  = ftell ( fp );
	rewind( fp );
	ret = fread( mem, 1, MIN( size, mem_size ), fp );
	if ( size > mem_size )
	{
		// TODO(theGiallo, 2016-03-08): sth
		log_info( "warning: reading file '%s' of size %ld into a smaller buffer of %ld", file_path, size, mem_size );
	}
	if ( ret!=MIN( size, mem_size ) )
	{
		// TODO(theGiallo, 2016-03-08): sth
	}

	fclose ( fp );

	return ret;
}

tg_internal
inline
bool
_mkdir( const char * path )
{
	bool ret;
	#ifdef _WIN32
	ret = ::_mkdir( path ) == 0;
	#elif _POSIX_C_SOURCE
	ret = ::mkdir( path, 0755 ) == 0;
	#else
	ret = ::mkdir( path, 0755 ) == 0;
	#endif
	return ret;
}

u32
make_folder_path( const char * path )
{
	u32 ret = 0;
	char * p = strdup( path );

	char * sp = p;
	while ( ( sp = strpbrk( sp, "\\/" ) ) )
	{
		char bkp = sp[0];
		sp[0] = 0;
		//log_dbg( "checking '%s'", p );
		if ( ! folder_exists( p ) )
		{
			ret += _mkdir( p ) ? 1 : 0;
		}
		sp[0] = bkp;
		++sp;
	}

	free( p );
	return ret;
}

char *
folder_of_path( const char * path )
{
	char * ret = NULL;
	s32 l = strlen( path );
	s32 index = -1;
	for_M0( i, l )
	{
		if ( path[i] == '\\'
		  || path[i] == '/' )
		{
			index = i;
			break;
		}
	}
	if ( index >= 0 )
	{
		ret = (char*)malloc( index + 2 );
		memcpy( ret, path, index + 1 );
		ret[index + 1] = 0;
	}
	return ret;
}
char *
file_name_of_path( const char * path )
{
	char * ret = NULL;
	s32 l = strlen( path );
	s32 index = -1;
	for_M0( i, l )
	{
		if ( path[i] == '\\'
		  || path[i] == '/' )
		{
			index = i;
			break;
		}
	}
	s32 rl = l - index - 1 + 1;
	ret = (char*)malloc( rl );
	memcpy( ret, path + index + 1, rl );
	ret[rl - 1] = 0;
	return ret;
}

s32
file_path_extension_index( const char * path )
{
	s32 ret = -1;
	s32 l = strlen( path );
	for_M0( i, l )
	{
		if ( path[i] == '.' )
		{
			ret = i;
			break;
		}
		if ( path[i] == '/'
		  || path[i] == '\\' )
		{
			break;
		}
	}
	return ret;
}

char *
file_path_remove_extension( const char * path )
{
	char * ret;
	s32 i = file_path_extension_index( path );
	if ( i >= 0 )
	{
		ret = (char*)malloc( i + 1 );
		memcpy( ret, path, i );
		ret[i] = 0;
	} else
	{
		ret = strdup( path );
	}
	return ret;
}

#if LINUX
struct
Statm
{
	u64 size,
	    resident,
	    share,
	    text,
	    lib,
	    data,
	    dt;
};

tg_internal
bool
linux_read_statm( Statm * result )
{
	bool ret = false;
	const char * statm_path = "/proc/self/statm";

	FILE *f = fopen( statm_path,"r" );
	if ( !f )
	{
		perror( statm_path );
	}  else
	{

		if ( 7 != fscanf( f, "%ld %ld %ld %ld %ld %ld %ld",
		                  &result->size,
		                  &result->resident,
		                  &result->share,
		                  &result->text,
		                  &result->lib,
		                  &result->data,
		                  &result->dt ) )
		{
			perror( statm_path );
		} else
		{
			ret = true;
		}

		fclose( f );
	}

	return ret;
}

Proc_Mem_Info
sys_get_proc_mem_info()
{
	Proc_Mem_Info ret = {};

	Statm statm = {};
	bool res_b = linux_read_statm( &statm );
	if ( res_b )
	{
		ret.bytes_virtual  = statm.size;
		ret.bytes_physical = statm.resident;
	}

	return ret;
}
#elif WINDOWS
#include <Psapi.h>
Proc_Mem_Info
sys_get_proc_mem_info()
{
	Proc_Mem_Info ret = {};

	HANDLE                  Process;
	PROCESS_MEMORY_COUNTERS_EX psmemCounters = {};

	DWORD processID = GetCurrentProcessId();
	Process = OpenProcess(  PROCESS_QUERY_INFORMATION |
	                        PROCESS_VM_READ,
	                        FALSE,
	                        processID );
	bool res_b =
	   GetProcessMemoryInfo( Process,
	                         &psmemCounters, sizeof( psmemCounters ) );
	if ( res_b )
	{
		ret.bytes_virtual  = psmemCounters.WorkingSetSize;
		ret.bytes_physical = psmemCounters.PrivateUsage;
	}

	CloseHandle( Process );

	return ret;
}
#endif
