#ifndef _UTILITY_H
#define _UTILITY_H 1

#include "system.h"

#include <string.h>

#include "basic_types.h"
#include "macro_tools.h"
#include "system.h"
#include "tgmath.h"

#define ARRAY_COUNT( a ) (sizeof(a) / sizeof((a)[0]))
#define SWAP_T(t,a,b) do{ t _tmp_for_swap_ = (a); (a) = (b); (b) = _tmp_for_swap_; } while(0)

#if TG_NO_LIMITS_H
	// NOTE(theGiallo): this is not safe, maybe is better to disable
	// bitvectors if you can't be sure.
	#define TG_CHAR_BIT ( 8 * sizeof( u8 ) )
#else
	#include <limits.h>
	#define TG_CHAR_BIT CHAR_BIT
#endif
#define TG_BITMASK(b) (1 << ((b) % TG_CHAR_BIT))
#define TG_BITSLOT(b) ((b) / TG_CHAR_BIT)
#define TG_BITSET(a, b) ((a)[TG_BITSLOT(b)] |= TG_BITMASK(b))
#define TG_BITCLEAR(a, b) ((a)[TG_BITSLOT(b)] &= ~TG_BITMASK(b))
#define TG_BITTEST(a, b) ((a)[TG_BITSLOT(b)] & TG_BITMASK(b))
#define TG_BITNSLOTS(nb) ((nb + TG_CHAR_BIT - 1) / TG_CHAR_BIT)

#define TG_POINTER_IS_64BITS_ALIGNED(p) ( 0x0LU == ( (u64)(p) & 0x7LU ) )


#define BTOS(b) ((b)?"true":"false")


// NOTE(theGiallo): the first time pass *internal_bit = 0
tg_internal
u8
rand_bit( u64 seeds[2], u64 * internal_data,
          u8 * internal_bit_id );

tg_internal
f32
randf( u64 seeds[2] );

tg_internal
f32
randf_between( u64 seeds[2], f32 left, f32 right );

tg_internal
f32
randf_01( u64 seeds[2] );

tg_internal
f64
randd( u64 seeds[2] );

tg_internal
f64
randd_between( u64 seeds[2], f32 left, f32 right );

tg_internal
f64
randd_01( u64 seeds[2] );

tg_internal
u8
randu8( u64 seeds[2] );

tg_internal
u32
randu( u64 seeds[2] );

// NOTE(theGiallo): random u32 in [min, max] interval
tg_internal
u32
randu_between( u64 seeds[2], u32 min, u32 max );

// NOTE(theGiallo): random u32 in [0, count-1] interval
tg_internal
u32
randu_with_distribution( u64 seeds[2], f32 * probability_distribution, u32 count );

struct
Marsaglia_Gauss_Data
{
	f64 v2, s, u1, u2;
	s32 phase;
	u64 seeds[2];
};

tg_internal
f64
randd_gauss_m0std1( Marsaglia_Gauss_Data * g_data );

tg_internal
f64
randd_gauss( Marsaglia_Gauss_Data * g_data, f64 mean, f64 sd );

tg_internal
f64
randd_gauss_clamped( Marsaglia_Gauss_Data * g_data, f64 mean, f64 sd,
                     f64 min, f64 max );

tg_internal
u32
u32_FNV1a( const char * str );

tg_internal
u32
u32_FNV1a( const void * str, u32 bytes_size );

tg_internal
u64
u64_FNV1a( const void * data, u64 bytes_size );

tg_internal
u64
u64_FNV1a( const char * str );

////////////////////////////////////////////////////////////////////////////////

tg_internal
void
shuffle( s32 * arr, s32 count, u64 * seeds );

////////////////////////////////////////////////////////////////////////////////

// NOTE(theGiallo): reorders the data array as data[i] = data[ids[i]] i=0..count-1
void
reorder_16_s( u16 * ids, void * data, u32 count,
                  u32 elem_size );
void
reorder_16_8( u16 * ids, u8 * data, u32 count );
void
reorder_16( u16 * ids, u16 * data, u32 count );
void
reorder_32( u32 * ids, u32 * data, u32 count );

/* NOTE(theGiallo): sorts data according to keys with ascending order
   tmp has to be at least of count size. It will be written many times.
   At the end it will contain no useful data. It's allocation is on the user
   because it's performance critical.
   IMPORTANT keys are not sorted! You can sort then with reorder_*.
   'mc' stands for many-count. This is because the counting of the radices is
   done in only one pass, so many are counted at once.

   This is the fastest sorting algorithm lepre has!
*/
void
radix_sort_mc_16( u16 * keys, u16 * data, u16 * tmp,
                      u32 count );
void
radix_sort_mc_32( u32 * keys, u32 * data, u32 * tmp,
                      u32 count );

void
radix_sort_mc_32_16( u32 * keys, u16 * ids, u16 * tmp,
                         u32 count );

void
radix_sort_mc_32_keys_only( u32 * keys, u32 * tmp, u32 count );

#ifndef INSERTION_SORT_16_IS_BETTER_MAX_COUNT
	#define INSERTION_SORT_16_IS_BETTER_MAX_COUNT 84
#endif
#ifndef INSERTION_SORT_32_IS_BETTER_MAX_COUNT
	#define INSERTION_SORT_32_IS_BETTER_MAX_COUNT 84
#endif

// NOTE(theGiallo): insertion sort is the fastest only with very small count.
// On my machine it's ~80. Implace radix sort is already optimized to use it with.
// This is regulated by INSERTION_SORT_*_IS_BETTER_MAX_COUNT

void
insertion_sort_16( u16 * keys, u16 * data, u32 count );

void
insertion_sort_32( u32 * keys, u32 * data, u32 count );

void
radix_sort_bucket_32( u32 * keys, u32 * data, u32 count,
                          u8 shift );

void
radix_sort_32( u32 * keys, u32 * data, u32 count );

void
radix_sort_bucket_16( u16 * keys, u16 * data, u32 count,
                          u8 shift );

void
radix_sort_16( u16 * keys, u16 * data, u32 count );

#ifndef MAX_STACK_QSIT
#define MAX_STACK_QSIT 64
#endif

void
quicksort_iterative_32( u32 * keys, u32 * data, u32 count );

void
quicksort_iterative_16( u16 * keys, u16 * data, u32 count );



tg_internal
inline
f32
f32_from_u32_after_radixsort( u32 uf )
{
	f32 ret;
	u32 mask = ( ( uf >> 31 ) - 1 ) | 0x80000000;
	uf = uf ^ mask;
	f32 * fp = (f32*)&uf;
	ret = *fp;

	return ret;
}

enum
Sort_Order : u8
{
	SORT_ASCENDANTLY,
	SORT_DESCENDANTLY
};

tg_internal
void
radix_sort_f32( f32 * keys_arr, u32 keys_arr_size, u16 * ids,
                Sort_Order sort_order,
                bool restore_keys_values = false );

tg_internal
void
radix_sort_mc_32_keys_only( u32 * keys, u32 * tmp, u32 count, Sort_Order sort_order = SORT_ASCENDANTLY );

tg_internal
void
radix_sort_mc_32_16( const u32 * keys, u16 * ids, u16 * tmp, u32 count, Sort_Order sort_order = SORT_ASCENDANTLY );

tg_internal
void
radix_sort_mc_64_16( const u64 * keys, u16 * ids, u16 * tmp, u32 count, Sort_Order sort_order = SORT_ASCENDANTLY );

tg_internal
void
radix_sort_mc_64_32( const u64 * keys, u32 * ids, u32 * tmp, u32 count, Sort_Order sort_order );

// NOTE(theGiallo): -1: string0 <  string1
//                   0: string0 == string1
//                   1: string0 >  string1
// NULL is the least possible
tg_internal
s32
string_min( const char * string0, const char * string1 );

tg_internal
inline
u32
str_bytes_to_char_or_end( const char * string, u32 bytes_length, u8 c )
{
	u32 ret = 0;

	if ( string )
	{
		for ( ; ret != bytes_length && string[ret] != c && string[ret]; ++ret );
	}

	return ret;
}

tg_internal
inline
u32
str_bytes_to_null_or_end( const char * string, u32 bytes_length )
{
	u32 ret = 0;

	if ( string )
	{
		for ( ; ret != bytes_length && string[ret]; ++ret );
	}

	return ret;
}

tg_internal
inline
u32
str_bytes_to_null( const char * string )
{
	u32 ret = 0;

	if ( string )
	{
		for ( ; ret != U32_MAX - 1 && string[ret]; ++ret );
	}

	return ret;
}

tg_internal
inline
u32
str_bytes_to_null( u8 * string )
{
	return str_bytes_to_null( (const char *) string );
}



tg_internal
inline
s32
string_min( const u8 * string0, const u8 * string1 )
{
	return string_min( (const char*) string0,  (const char*) string1 );
}

tg_internal
s32
string_min( const char * string0, s64 length0, const char * string1, s64 length1 );

tg_internal
inline
bool
string_equal( const char * string0, s64 length0, const char * string1, s64 length1 )
{
	bool ret = string_min( string0, length0, string1, length1 ) == 0;
	return ret;
}

inline
bool
string_equal( const char * string0, s64 length0, const char * string1 )
{
	bool ret = string_equal( string0, length0, string1, str_bytes_to_null( string1 ) );
	return ret;
}

inline
bool
string_equal( const u8 * string0, s64 length0, const char * string1 )
{
	bool ret = string_equal( (const char*)string0, length0, string1, str_bytes_to_null( string1 ) );
	return ret;
}

tg_internal
bool
string_equal( const char * string0, const char * string1, s64 length_of_both );

tg_internal
inline
bool
string_equal( const u8 * string0, const u8 * string1, s64 length_of_both )
{
	return string_equal( (const char*) string0, (const char*) string1, length_of_both );
}

tg_internal
inline
bool
string_equal( const char * string0, const char * string1 )
{
	return 0 == string_min( string0, string1 );
}

tg_internal
inline
bool
string_equal( const u8 * string0, const u8 * string1 )
{
	return 0 == string_min( string0, string1 );
}

tg_internal
void
sort_lexicographically_insertion( const char ** strings, u32 count,
                                  u32 * data = NULL, Sort_Order sort_order = SORT_ASCENDANTLY );

tg_internal
void
sort_lexicographically_insertion_ids_only( const char *const* strings, u32 count,
                                           u32 * ids, Sort_Order sort_order );

tg_internal
void
sort_lexicographically_insertion_ids_only_16( const char *const* strings, u32 count,
                                              u16 * ids, Sort_Order sort_order );

tg_internal
bool
sort_lexicographically_radix_insertion( char const** strings, u32 count,
                                        char const** tmp,
                                        Sort_Order sort_order = SORT_ASCENDANTLY,
                                        u32 * data     = NULL,
                                        u32 * data_tmp = NULL,
                                        u32 * lengths_strings = NULL,
                                        u32 * lengths_tmp     = NULL,
                                        u32 char_idx = 0 );

tg_internal
bool
sort_lexicographically_radix_insertion_ids_only( char const*const* strings, u32 count,
                                                 Sort_Order sort_order,
                                                 u32 * ids,
                                                 u32 * ids_tmp = NULL,
                                                 u32 const * lengths_strings = NULL,
                                                 u32 char_idx = 0 );

tg_internal
bool
sort_lexicographically_radix_insertion_ids_only_16( char const*const* strings, u32 count,
                                                    Sort_Order sort_order,
                                                    u16 * ids,
                                                    u16 * ids_tmp = NULL,
                                                    u32 const * lengths_strings = NULL,
                                                    u32 char_idx = 0 );

// NOTE(theGiallo): Stable sort of UTF-8 strings.
// NOTE(theGiallo): if you provide lengths* you have to fill them.
// If not provided they will be allocated on the stack (<- IMPORTANT!)
// and their values computed (aka string traversal).
// NOTE(theGiallo): the strings, data and lengths array will be sorted after
// the call. *tmp content can be discarded.
// NOTE(theGiallo): if you won't provide tmp, it will be allocated on the stack.
tg_internal
void
sort_lexicographically( char const** strings, u32 count,
                        char const** tmp = NULL,
                        Sort_Order sort_order = SORT_ASCENDANTLY,
                        u32 * data     = NULL,
                        u32 * data_tmp = NULL,
                        u32 * lengths_strings = NULL,
                        u32 * lengths_tmp     = NULL );

tg_internal
void
sort_lexicographically_ids_only( char const*const* strings, u32 count,
                                 Sort_Order sort_order,
                                 u32 * ids,
                                 u32 * ids_tmp = NULL,
                                 u32 * lengths_strings = NULL );

tg_internal
void
sort_lexicographically_ids_only_16( char const*const* strings, u32 count,
                                    Sort_Order sort_order,
                                    u16 * ids,
                                    u16 * ids_tmp = NULL,
                                    u32 * lengths_strings = NULL );

u32
inline
str_copy( u8 * dst, u32 dst_bytes_size, u8 * src )
{
	u32 ret = 0;
	for ( ret = 0;
	     !ret || ( ret && src[ret-1] );
	      ++ret )
	{
		if ( ret == dst_bytes_size )
		{
			ret = 0;
			break;
		}
		dst[ret] = src[ret];
	}

	return ret;
}

bool
inline
str_ends_with( u8 * str, u32 str_bytes_to_null, u8 * pattern, u32 pattern_bytes_to_null )
{
	log_dbg( "str = '%s'|%u pattern = '%s'|%u",
	         str, str_bytes_to_null, pattern, pattern_bytes_to_null );
	bool ret = str_bytes_to_null >= pattern_bytes_to_null;
	for ( u32 i = 0; i != pattern_bytes_to_null && ret; ++i )
	{
		ret = ret && ( pattern[pattern_bytes_to_null - 1 - i] ==
		               str    [str_bytes_to_null     - 1 - i] );
	}
	return ret;
}

bool
inline
str_ends_with( u8 * str, u32 str_bytes_to_null, u8 ** patterns, u32 patterns_count, u32 * patterns_bytes_to_null )
{
	bool ret = true;
	for ( u32 i = 0; i != patterns_count && ret; ++i )
	{
		ret = ret && str_ends_with( str, str_bytes_to_null, patterns[i], patterns_bytes_to_null[i] );
	}
	return ret;
}

// NOTE(theGiallo): returns new strings_count
u32
strings_filter_ends( u8 ** strings, u32 strings_count, u32 * strings_bytes_to_null,
                     u8 ** patterns, u32 patterns_count, u32 * patterns_bytes_to_null )
{
	u32 first_free = 0;
	for ( u32 curr = 0;
	      curr != strings_count;
	      ++curr )
	{
		if ( str_ends_with( strings[curr], strings_bytes_to_null[curr], patterns, patterns_count, patterns_bytes_to_null ) )
		{
			if ( first_free != curr )
			{
				u8 * tmp = strings[first_free];
				strings[first_free] = strings[curr];
				strings[curr] = tmp;
				u32 tmp32 = strings_bytes_to_null[first_free];
				strings_bytes_to_null[first_free] = strings_bytes_to_null[curr];
				strings_bytes_to_null[curr] = tmp32;
			}
			++first_free;
		}
	}
	return first_free;
}

void
inline
strings_compute_bytes_to_null( u8 ** strings, u32 strings_count, u32 * bytes_to_null )
{
	for ( u32 i = 0; i != strings_count; ++i )
	{
		bytes_to_null[i] = str_bytes_to_null( strings[i] );
	}
}

tg_internal
void
reorder( u32 * ids, void * data, u32 count, u32 elem_size )
{
	u8 v[elem_size];
	u8 moved[BITNSLOTS( count )];
	memset( moved, 0x0, BITNSLOTS( count ) );
	for ( u32 i = 0; i < count; ++i )
	{
		if ( BITTEST( moved, i ) )
		{
			continue;
		}

		memcpy( v, (u8*)data + i * elem_size, elem_size );
		for ( u32 j = i; ; )
		{
			u32 next = ids[j];
			if ( next == i )
			{
				memcpy( (u8*)data + j * elem_size, v, elem_size );
				BITSET( moved, j );
				break;
			}
			memcpy( (u8*)data + j * elem_size, (u8*)data + next * elem_size, elem_size );
			BITSET( moved, j );
			j = next;
		}
	}
}

struct
Mem_Stack
{
	u8 * mem_buf;
	u64 first_free;
	u64 total_size;
	u8 *
	push( u64 size );
};

inline
u8 *
Mem_Stack::
push( u64 size )
{
	u8 * ret = NULL;

	if ( this->first_free + size <= this->total_size )
	{
		ret = this->mem_buf + this->first_free;
		this->first_free += size;
	}

	return ret;
}

struct
ptr_u64_LL
{
	ptr_u64_LL * next;
	void * ptr;
	u64 size;
};

struct
u32_u32_LL
{
	u32_u32_LL * next;
	u32 value;
	u32 key;
};

#if TG_MEM_POOL
#undef MH_EMIT_BODY
#undef MH_BODY_ONLY
#undef MH_STATIC

#define MH_TYPE ptr_u64_LL
#include "mem_hotel.inc.h"

#define MH_TYPE u32_u32_LL
#include "mem_hotel.inc.h"

struct
Mem_Pool
{
	u8 * mem_buf;
	u64 size;
	u64 occupied;

	ptr_u64_LL * free_list;
	ptr_u64_LL_MH freelist_nodes_mh;

	void
	init( void * mem, u64 size );

	void *
	allocate( u64 size );

	void *
	allocate_clean( u64 size );

	void
	deallocate( void * ptr );
};

inline
void
Mem_Pool::
init( void * mem, u64 size )
{
	this->mem_buf = (u8*)mem;
	this->size = size;
	this->occupied = 0;
	this->free_list = ::allocate( &this->freelist_nodes_mh );
	this->free_list->next = NULL;
	this->free_list->size = size;
	this->free_list->ptr  = this->mem_buf;
}

inline
void *
Mem_Pool::
allocate_clean( u64 size )
{
	void * ret = allocate( size );

	if ( ret )
	{
		memset( ret, 0, size );
	}

	return ret;
}
#endif // TG_MEM_POOL

inline
u64
size_round_to_64bits_alignment( u64 size )
{
	u64 ret = ( size + 0x7LU ) & ( ~0x7LU );

	return ret;
}

inline
void *
memory_to_next_64bits_alignment( void * mem )
{
	void * ret = (void*)( ( (u64)mem + 0x7LU ) & ( ~0x7LU ) );
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

struct
u32_bool
{
	u32  u32_v;
	bool bool_v;
};

struct
u64_to_u32_Hash_Table
{
	struct
	Key_Value_List_Node
	{
		u64 key;
		u32 value;
		s32 next_id;
	};
	u8  * values_table_presence_bv;
	Key_Value_List_Node * values_table;
	Key_Value_List_Node * list_nodes_storage;
	u8  * list_nodes_storage_presence_bv;
	u8  * list_nodes_storage_255_blocks_occupancy;
	u32 table_size;
	u32 list_nodes_storage_capacity;
	u32 list_nodes_storage_occupancy;
	u32 padding;
};

inline
u32
hash_table_n_n_expected_items_per_slot( u32 capacity )
{
	u32 ret;
	f32 log_capacity = logf( capacity );
	ret = (u32)ceilf( log_capacity / logf( log_capacity ) );
	return ret;
}
inline
u32
u64_to_u32_hash_table_list_nodes_storage_capacity( u32 hash_table_capacity )
{
	u32 expected_items_per_slot = hash_table_n_n_expected_items_per_slot( hash_table_capacity );
	u32 ret = ( expected_items_per_slot - 1 ) * ( ( hash_table_capacity + expected_items_per_slot - 1 ) / expected_items_per_slot );
	return ret;
}
inline
u64
u64_to_u32_hash_table_compute_memory_size( u32 capacity )
{
	u64 ret = 0;

	u32 expected_items_per_slot = hash_table_n_n_expected_items_per_slot( capacity );
	u32 list_nodes_storage_capacity = capacity * ( expected_items_per_slot - 1 );
	ret += size_round_to_64bits_alignment( sizeof(u8) * TG_BITNSLOTS(capacity) );
	ret += size_round_to_64bits_alignment( capacity * sizeof( u64_to_u32_Hash_Table::Key_Value_List_Node ) );
	ret += size_round_to_64bits_alignment( u64_to_u32_hash_table_list_nodes_storage_capacity( capacity ) * sizeof( u64_to_u32_Hash_Table::Key_Value_List_Node ) );
	ret += size_round_to_64bits_alignment( sizeof(u8) * TG_BITNSLOTS(list_nodes_storage_capacity) );
	ret += sizeof(u8) * ( ( capacity + 254 ) / 255 );

	return ret;
}

inline
u8 *
u64_to_u32_hash_table_init( u64_to_u32_Hash_Table * ht, u8 * mem, u32 capacity )
{
	tg_assert( TG_POINTER_IS_64BITS_ALIGNED(mem) );

	ht->values_table_presence_bv = mem;
	mem += size_round_to_64bits_alignment( sizeof(u8) * TG_BITNSLOTS(capacity) );

	ht->values_table = (u64_to_u32_Hash_Table::Key_Value_List_Node*)mem;
	mem += size_round_to_64bits_alignment( capacity * sizeof( u64_to_u32_Hash_Table::Key_Value_List_Node ) );

	ht->list_nodes_storage_capacity = u64_to_u32_hash_table_list_nodes_storage_capacity( capacity );

	ht->list_nodes_storage = (u64_to_u32_Hash_Table::Key_Value_List_Node*)mem;
	mem += size_round_to_64bits_alignment( ht->list_nodes_storage_capacity * sizeof( u64_to_u32_Hash_Table::Key_Value_List_Node ) );

	ht->list_nodes_storage_presence_bv = mem;
	mem += size_round_to_64bits_alignment( sizeof(u8) * TG_BITNSLOTS(ht->list_nodes_storage_capacity) );

	ht->list_nodes_storage_255_blocks_occupancy = mem;
	mem += sizeof(u8) * ( ( capacity + 254 ) / 255 );

	ht->table_size = capacity;
	ht->list_nodes_storage_occupancy = 0;

	return mem;
}

inline
u32
u64_to_u32_hash_table_hash_key( u64 key, u32 table_size )
{
	u32 ret = 0;
	u32 * u32_keys = (u32*)&key;
#if 0
	ret = ((u32)( 0xFFFFFFFFUL & ( key ^ ( key >> 32 ) ) ) ) % table_size;
#else
	ret = ( u32_keys[0] ^ u32_keys[1] ) % table_size;
#endif

	return ret;
}

inline
void
u64_to_u32_hash_table_clear( u64_to_u32_Hash_Table * ht )
{
	ht->list_nodes_storage_occupancy = 0;
	memset( ht->values_table_presence_bv, 0x00,
	        size_round_to_64bits_alignment(
	           sizeof(u8) * TG_BITNSLOTS(ht->table_size) ) );
	memset( ht->list_nodes_storage_presence_bv, 0x00,
	        size_round_to_64bits_alignment(
	           sizeof(u8) * TG_BITNSLOTS(ht->list_nodes_storage_capacity) ) );
	memset( ht->list_nodes_storage_255_blocks_occupancy, 0x00,
	        sizeof(u8) * ( ( ht->list_nodes_storage_capacity + 254 ) / 255 ) );
}

inline
u32_bool
u64_to_u32_hash_table_retrieve( u64_to_u32_Hash_Table * ht, u64 key )
{
	u32_bool ret = {};

	u32 hash = u64_to_u32_hash_table_hash_key( key, ht->table_size );

	if ( TG_BITTEST(ht->values_table_presence_bv, hash) )
	{
		u64_to_u32_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		for(;;)
		{
			if ( node->key == key )
			{
				ret.u32_v = node->value;
				ret.bool_v = true;
				break;
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			node = ht->list_nodes_storage + node->next_id;
		}
	}

	return ret;
}

inline
u32
u64_to_u32_hash_table_retrieve_many( u64_to_u32_Hash_Table * ht, u64 key, Mem_Stack * mem_stack )
{
	u32 ret = 0;

	u32 hash = u64_to_u32_hash_table_hash_key( key, ht->table_size );

	if ( TG_BITTEST(ht->values_table_presence_bv, hash) )
	{
		u32 * ret_array = NULL;

		u64_to_u32_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		for(;;)
		{
			if ( node->key == key )
			{
				++ret;
				u32 * m = (u32*) mem_stack->push( sizeof( u32 ) );
				if ( !ret_array )
				{
					ret_array = m;
				}
				*m = node->value;
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			node = ht->list_nodes_storage + node->next_id;
		}
	}

	return ret;
}

bool
u64_to_u32_hash_table_change( u64_to_u32_Hash_Table * ht, u64 key, u32 value );

bool
u64_to_u32_hash_table_insert( u64_to_u32_Hash_Table * ht, u64 key, u32 value );

inline
u32_bool
u64_to_u32_hash_table_remove( u64_to_u32_Hash_Table * ht, u64 key )
{
	u32_bool ret = {};

	u32 hash = u64_to_u32_hash_table_hash_key( key, ht->table_size );

	if ( TG_BITTEST(ht->values_table_presence_bv, hash) )
	{
		u64_to_u32_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		u64_to_u32_Hash_Table::Key_Value_List_Node * prev_node = NULL;
		s32 id = -1;
		for(;;)
		{
			if ( node->key == key )
			{
				ret.u32_v  = node->value;
				ret.bool_v = true;
				if ( id < 0 )
				{
					if ( node->next_id < 0 )
					{
						TG_BITCLEAR(ht->values_table_presence_bv, hash);
					} else
					{
						u32 id_to_delete = node->next_id;
						u64_to_u32_Hash_Table::Key_Value_List_Node * next_node =
						   ht->list_nodes_storage + node->next_id;
						memcpy( node, next_node, sizeof(*node) );
						TG_BITCLEAR(ht->list_nodes_storage_presence_bv, id_to_delete );
						--ht->list_nodes_storage_255_blocks_occupancy[id_to_delete/255];
					}
				} else
				{
					TG_BITCLEAR(ht->list_nodes_storage_presence_bv, id );
					--ht->list_nodes_storage_255_blocks_occupancy[id/255];
					prev_node->next_id = node->next_id;
				}
				break;
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			prev_node = node;
			id = node->next_id;
			node = ht->list_nodes_storage + id;
		}
	}

	return ret;
}

inline
u32
u64_to_u32_hash_table_remove_many( u64_to_u32_Hash_Table * ht, u64 key )
{
	u32 ret = 0;

	u32 hash = u64_to_u32_hash_table_hash_key( key, ht->table_size );

	if ( TG_BITTEST(ht->values_table_presence_bv, hash) )
	{
		u64_to_u32_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		u64_to_u32_Hash_Table::Key_Value_List_Node * prev_node = NULL;
		s32 id = -1;
		for(;;)
		{
			if ( node->key == key )
			{
				++ret;
				if ( id < 0 )
				{
					if ( node->next_id < 0 )
					{
						TG_BITCLEAR(ht->values_table_presence_bv, hash);
					} else
					{
						u32 id_to_delete = node->next_id;
						u64_to_u32_Hash_Table::Key_Value_List_Node * next_node =
						   ht->list_nodes_storage + node->next_id;
						memcpy( node, next_node, sizeof(*node) );
						TG_BITCLEAR(ht->list_nodes_storage_presence_bv, id_to_delete );
						--ht->list_nodes_storage_255_blocks_occupancy[id_to_delete/255];
					}
				} else
				{
					TG_BITCLEAR(ht->list_nodes_storage_presence_bv, id );
					--ht->list_nodes_storage_255_blocks_occupancy[id/255];
					prev_node->next_id = node->next_id;
				}
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			prev_node = node;
			id = node->next_id;
			node = ht->list_nodes_storage + id;
		}
	}

	return ret;
}

inline
u32
u64_to_u32_hash_table_remove_many_and_feedback( u64_to_u32_Hash_Table * ht, u64 key, Mem_Stack * mem_stack )
{
	u32 ret = 0;

	u32 hash = u64_to_u32_hash_table_hash_key( key, ht->table_size );

	if ( TG_BITTEST(ht->values_table_presence_bv, hash) )
	{
		u32 * ret_array = NULL;

		u64_to_u32_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		u64_to_u32_Hash_Table::Key_Value_List_Node * prev_node = NULL;
		s32 id = -1;
		for(;;)
		{
			if ( node->key == key )
			{
				++ret;

				u32 * m = (u32*) mem_stack->push( sizeof( u32 ) );
				if ( !ret_array )
				{
					ret_array = m;
				}
				*m = node->value;

				if ( id < 0 )
				{
					if ( node->next_id < 0 )
					{
						TG_BITCLEAR(ht->values_table_presence_bv, hash);
					} else
					{
						u32 id_to_delete = node->next_id;
						u64_to_u32_Hash_Table::Key_Value_List_Node * next_node =
						   ht->list_nodes_storage + node->next_id;
						memcpy( node, next_node, sizeof(*node) );
						TG_BITCLEAR(ht->list_nodes_storage_presence_bv, id_to_delete );
						--ht->list_nodes_storage_255_blocks_occupancy[id_to_delete/255];
					}
				} else
				{
					TG_BITCLEAR(ht->list_nodes_storage_presence_bv, id );
					--ht->list_nodes_storage_255_blocks_occupancy[id/255];
					prev_node->next_id = node->next_id;
				}
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			prev_node = node;
			id = node->next_id;
			node = ht->list_nodes_storage + id;
		}
	}

	return ret;
}


////////////////////////////////////////////////////////////////////////////////

struct
B12_to_u32_Hash_Table
{
	struct
	Key_Value_List_Node
	{
		u64 key_xy;
		u32 key_z;
		u32 value;
		s32 next_id;
	};
	u8  * values_table_presence_bv;
	Key_Value_List_Node * values_table;
	Key_Value_List_Node * list_nodes_storage;
	u8  * list_nodes_storage_presence_bv;
	u8  * list_nodes_storage_255_blocks_occupancy;
	u32 table_size;
	u32 list_nodes_storage_capacity;
	u32 list_nodes_storage_occupancy;
	u32 padding;
};

union
B12
{
	struct
	{
		u64 xy;
		u32 z;
	};
	struct
	{
		s32 s32_x;
		s32 s32_y;
		s32 s32_z;
	};
	tg::math::V3s32 v3s32;
	tg::math::V3 v3;
	struct
	{
		u32 u32_x;
		u32 u32_y;
		u32 u32_z;
	};
	struct
	{
		u8 u8_12[12];
	};
};

inline
u32
B12_to_u32_hash_table_list_nodes_storage_capacity( u32 hash_table_capacity )
{
	// TODO(theGiallo, 2017-02-04): this is the same for the other hash table,
	// maybe use a single generic function
	u32 expected_items_per_slot = hash_table_n_n_expected_items_per_slot( hash_table_capacity );
	u32 ret = ( expected_items_per_slot - 1 ) * ( ( hash_table_capacity + expected_items_per_slot - 1 ) / expected_items_per_slot );
	return ret;
}
inline
u64
B12_to_u32_hash_table_compute_memory_size( u32 capacity )
{
	u64 ret = 0;

	u32 expected_items_per_slot = hash_table_n_n_expected_items_per_slot( capacity );
	u32 list_nodes_storage_capacity = capacity * ( expected_items_per_slot - 1 );
	ret += size_round_to_64bits_alignment( sizeof(u8) * TG_BITNSLOTS(capacity) );
	ret += size_round_to_64bits_alignment( capacity * sizeof( B12_to_u32_Hash_Table::Key_Value_List_Node ) );
	ret += size_round_to_64bits_alignment( B12_to_u32_hash_table_list_nodes_storage_capacity( capacity ) * sizeof( B12_to_u32_Hash_Table::Key_Value_List_Node ) );
	ret += size_round_to_64bits_alignment( sizeof(u8) * TG_BITNSLOTS(list_nodes_storage_capacity) );
	ret += sizeof(u8) * ( ( capacity + 254 ) / 255 );

	return ret;
}

inline
u8 *
B12_to_u32_hash_table_init( B12_to_u32_Hash_Table * ht, u8 * mem, u32 capacity )
{
	tg_assert( TG_POINTER_IS_64BITS_ALIGNED(mem) );

	ht->values_table_presence_bv = mem;
	mem += size_round_to_64bits_alignment( sizeof(u8) * TG_BITNSLOTS(capacity) );

	ht->values_table = (B12_to_u32_Hash_Table::Key_Value_List_Node*)mem;
	mem += size_round_to_64bits_alignment( capacity * sizeof( B12_to_u32_Hash_Table::Key_Value_List_Node ) );

	ht->list_nodes_storage_capacity = u64_to_u32_hash_table_list_nodes_storage_capacity( capacity );

	ht->list_nodes_storage = (B12_to_u32_Hash_Table::Key_Value_List_Node*)mem;
	mem += size_round_to_64bits_alignment( ht->list_nodes_storage_capacity * sizeof( B12_to_u32_Hash_Table::Key_Value_List_Node ) );

	ht->list_nodes_storage_presence_bv = mem;
	mem += size_round_to_64bits_alignment( sizeof(u8) * TG_BITNSLOTS(ht->list_nodes_storage_capacity) );

	ht->list_nodes_storage_255_blocks_occupancy = mem;
	mem += sizeof(u8) * ( ( capacity + 254 ) / 255 );

	ht->table_size = capacity;
	ht->list_nodes_storage_occupancy = 0;

	return mem;
}

inline
u32
B12_to_u32_hash_table_hash_key( B12 key, u32 table_size )
{
	// TODO(theGiallo, 2017-02-04): if this generates too many collisions use u32_FNV1a
	u32 ret = 0;
	u64 seeds[2];
	seeds[0] = key.xy;
	seeds[1] = key.z;
	seeds[0] = seeds[0] ^ 29364829169487291LU;
	seeds[1] = seeds[1] ^ 9283469126382374107LU;
	ret = randu_between( seeds, 0, table_size-1 );

	return ret;
}

inline
void
B12_to_u32_hash_table_clear( B12_to_u32_Hash_Table * ht )
{
	ht->list_nodes_storage_occupancy = 0;
	memset( ht->values_table_presence_bv, 0x00,
	        size_round_to_64bits_alignment(
	           sizeof(u8) * TG_BITNSLOTS(ht->table_size) ) );
	memset( ht->list_nodes_storage_presence_bv, 0x00,
	        size_round_to_64bits_alignment(
	           sizeof(u8) * TG_BITNSLOTS(ht->list_nodes_storage_capacity) ) );
	memset( ht->list_nodes_storage_255_blocks_occupancy, 0x00,
	        sizeof(u8) * ( ( ht->list_nodes_storage_capacity + 254 ) / 255 ) );
}

inline
u32_bool
B12_to_u32_hash_table_retrieve( B12_to_u32_Hash_Table * ht, B12 key )
{
	u32_bool ret = {};

	u32 hash = B12_to_u32_hash_table_hash_key( key, ht->table_size );

	if ( TG_BITTEST(ht->values_table_presence_bv, hash) )
	{
		B12_to_u32_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		for(;;)
		{
			if ( node->key_xy == key.xy && node->key_z == key.z )
			{
				ret.u32_v = node->value;
				ret.bool_v = true;
				break;
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			node = ht->list_nodes_storage + node->next_id;
		}
	}

	return ret;
}

bool
B12_to_u32_hash_table_change( B12_to_u32_Hash_Table * ht, B12 key, u32 value );

bool
B12_to_u32_hash_table_insert( B12_to_u32_Hash_Table * ht, B12 key, u32 value );

inline
u32_bool
B12_to_u32_hash_table_remove( B12_to_u32_Hash_Table * ht, B12 key )
{
	u32_bool ret = {};

	u32 hash = B12_to_u32_hash_table_hash_key( key, ht->table_size );

	if ( TG_BITTEST(ht->values_table_presence_bv, hash) )
	{
		B12_to_u32_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		B12_to_u32_Hash_Table::Key_Value_List_Node * prev_node = NULL;
		s32 id = -1;
		for(;;)
		{
			if ( node->key_xy == key.xy && node->key_z == key.z )
			{
				ret.u32_v  = node->value;
				ret.bool_v = true;
				if ( id < 0 )
				{
					if ( node->next_id < 0 )
					{
						TG_BITCLEAR(ht->values_table_presence_bv, hash);
					} else
					{
						u32 id_to_delete = node->next_id;
						B12_to_u32_Hash_Table::Key_Value_List_Node * next_node =
						   ht->list_nodes_storage + node->next_id;
						memcpy( node, next_node, sizeof(*node) );
						TG_BITCLEAR(ht->list_nodes_storage_presence_bv, id_to_delete );
						--ht->list_nodes_storage_255_blocks_occupancy[id_to_delete/255];
					}
				} else
				{
					TG_BITCLEAR(ht->list_nodes_storage_presence_bv, id );
					--ht->list_nodes_storage_255_blocks_occupancy[id/255];
					prev_node->next_id = node->next_id;
				}
				break;
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			prev_node = node;
			id = node->next_id;
			node = ht->list_nodes_storage + id;
		}
	}

	return ret;
}

////////////////////////////////////////////////////////////////////////////////

struct
B12_bool
{
	B12  B12_v;
	bool bool_v;
};

struct
u64_to_B12_Hash_Table
{
	struct
	Key_Value_List_Node
	{
		u64 key;
		B12 value;
		s32 next_id;
	};
	u8  * values_table_presence_bv;
	Key_Value_List_Node * values_table;
	Key_Value_List_Node * list_nodes_storage;
	u8  * list_nodes_storage_presence_bv;
	u8  * list_nodes_storage_255_blocks_occupancy;
	u32 table_size;
	u32 list_nodes_storage_capacity;
	u32 list_nodes_storage_occupancy;
	u32 padding;
};

inline
u32
u64_to_B12_hash_table_list_nodes_storage_capacity( u32 hash_table_capacity )
{
	u32 expected_items_per_slot = hash_table_n_n_expected_items_per_slot( hash_table_capacity );
	u32 ret = ( expected_items_per_slot - 1 ) * ( ( hash_table_capacity + expected_items_per_slot - 1 ) / expected_items_per_slot );
	return ret;
}
inline
u64
u64_to_B12_hash_table_compute_memory_size( u32 capacity )
{
	u64 ret = 0;

	u32 expected_items_per_slot = hash_table_n_n_expected_items_per_slot( capacity );
	u32 list_nodes_storage_capacity = capacity * ( expected_items_per_slot - 1 );
	ret += size_round_to_64bits_alignment( sizeof(u8) * TG_BITNSLOTS(capacity) );
	ret += size_round_to_64bits_alignment( capacity * sizeof( u64_to_B12_Hash_Table::Key_Value_List_Node ) );
	ret += size_round_to_64bits_alignment( u64_to_B12_hash_table_list_nodes_storage_capacity( capacity ) * sizeof( u64_to_B12_Hash_Table::Key_Value_List_Node ) );
	ret += size_round_to_64bits_alignment( sizeof(u8) * TG_BITNSLOTS(list_nodes_storage_capacity) );
	ret += sizeof(u8) * ( ( capacity + 254 ) / 255 );

	return ret;
}

inline
u8 *
u64_to_B12_hash_table_init( u64_to_B12_Hash_Table * ht, u8 * mem, u32 capacity )
{
	tg_assert( TG_POINTER_IS_64BITS_ALIGNED(mem) );

	ht->values_table_presence_bv = mem;
	mem += size_round_to_64bits_alignment( sizeof(u8) * TG_BITNSLOTS(capacity) );

	ht->values_table = (u64_to_B12_Hash_Table::Key_Value_List_Node*)mem;
	mem += size_round_to_64bits_alignment( capacity * sizeof( u64_to_B12_Hash_Table::Key_Value_List_Node ) );

	ht->list_nodes_storage_capacity = u64_to_B12_hash_table_list_nodes_storage_capacity( capacity );

	ht->list_nodes_storage = (u64_to_B12_Hash_Table::Key_Value_List_Node*)mem;
	mem += size_round_to_64bits_alignment( ht->list_nodes_storage_capacity * sizeof( u64_to_B12_Hash_Table::Key_Value_List_Node ) );

	ht->list_nodes_storage_presence_bv = mem;
	mem += size_round_to_64bits_alignment( sizeof(u8) * TG_BITNSLOTS(ht->list_nodes_storage_capacity) );

	ht->list_nodes_storage_255_blocks_occupancy = mem;
	mem += sizeof(u8) * ( ( capacity + 254 ) / 255 );

	ht->table_size = capacity;
	ht->list_nodes_storage_occupancy = 0;

	return mem;
}

inline
u32
u64_to_B12_hash_table_hash_key( u64 key, u32 table_size )
{
	u32 ret = 0;
	u32 * u32_keys = (u32*)&key;
#if 0
	ret = ((u32)( 0xFFFFFFFFUL & ( key ^ ( key >> 32 ) ) ) ) % table_size;
#else
	ret = ( u32_keys[0] ^ u32_keys[1] ) % table_size;
#endif

	return ret;
}

inline
void
u64_to_B12_hash_table_clear( u64_to_B12_Hash_Table * ht )
{
	ht->list_nodes_storage_occupancy = 0;
	memset( ht->values_table_presence_bv, 0x00,
	        size_round_to_64bits_alignment(
	           sizeof(u8) * TG_BITNSLOTS(ht->table_size) ) );
	memset( ht->list_nodes_storage_presence_bv, 0x00,
	        size_round_to_64bits_alignment(
	           sizeof(u8) * TG_BITNSLOTS(ht->list_nodes_storage_capacity) ) );
	memset( ht->list_nodes_storage_255_blocks_occupancy, 0x00,
	        sizeof(u8) * ( ( ht->list_nodes_storage_capacity + 254 ) / 255 ) );
}

inline
B12_bool
u64_to_B12_hash_table_retrieve( u64_to_B12_Hash_Table * ht, u64 key )
{
	B12_bool ret = {};

	u32 hash = u64_to_B12_hash_table_hash_key( key, ht->table_size );

	if ( TG_BITTEST(ht->values_table_presence_bv, hash) )
	{
		u64_to_B12_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		for(;;)
		{
			if ( node->key == key )
			{
				ret.B12_v = node->value;
				ret.bool_v = true;
				break;
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			node = ht->list_nodes_storage + node->next_id;
		}
	}

	return ret;
}

bool
u64_to_B12_hash_table_change( u64_to_B12_Hash_Table * ht, u64 key, B12 value );

bool
u64_to_B12_hash_table_insert( u64_to_B12_Hash_Table * ht, u64 key, B12 value );

inline
B12_bool
u64_to_B12_hash_table_remove( u64_to_B12_Hash_Table * ht, u64 key )
{
	B12_bool ret = {};

	u32 hash = u64_to_B12_hash_table_hash_key( key, ht->table_size );

	if ( TG_BITTEST(ht->values_table_presence_bv, hash) )
	{
		u64_to_B12_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		u64_to_B12_Hash_Table::Key_Value_List_Node * prev_node = NULL;
		s32 id = -1;
		for(;;)
		{
			if ( node->key == key )
			{
				ret.B12_v  = node->value;
				ret.bool_v = true;
				if ( id < 0 )
				{
					if ( node->next_id < 0 )
					{
						TG_BITCLEAR(ht->values_table_presence_bv, hash);
					} else
					{
						u32 id_to_delete = node->next_id;
						u64_to_B12_Hash_Table::Key_Value_List_Node * next_node =
						   ht->list_nodes_storage + node->next_id;
						memcpy( node, next_node, sizeof(*node) );
						TG_BITCLEAR(ht->list_nodes_storage_presence_bv, id_to_delete );
						--ht->list_nodes_storage_255_blocks_occupancy[id_to_delete/255];
					}
				} else
				{
					TG_BITCLEAR(ht->list_nodes_storage_presence_bv, id );
					--ht->list_nodes_storage_255_blocks_occupancy[id/255];
					prev_node->next_id = node->next_id;
				}
				break;
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			prev_node = node;
			id = node->next_id;
			node = ht->list_nodes_storage + id;
		}
	}

	return ret;
}

////////////////////////////////////////////////////////////////////////////////
// printing

inline
void
put_bytes( u8 * bytes, u64 count )
{
	if ( fwrite ( bytes, 1, count, stdout ) != count )
	{
		perror( "fwrite" );
	}
}
#define PUT_BYTES( s ) put_bytes( (u8*)s, str_bytes_to_null( s ) );

// printing
////////////////////////////////////////////////////////////////////////////////
// ASCII

inline
bool
ascii_is_digit( u8 c )
{
	bool ret = c >= '0' && c <= '9';
	return ret;
}

inline
bool
ascii_is_hexadecimal_digit( u8 c )
{
	bool ret = ascii_is_digit( c )
	        || ( c >= 'a' && c <= 'f' )
	        || ( c >= 'A' && c <= 'F' );
	return ret;
}

inline
bool
ascii_is_octal_digit( u8 c )
{
	bool ret = c >= '0' && c <= '7';
	return ret;
}

inline
bool
ascii_is_binary_digit( u8 c )
{
	bool ret = c == '0' || c == '1';
	return ret;
}

inline
bool
ascii_is_digit( s8 c )
{
	bool ret = c >= '0' && c <= '9';
	return ret;
}

inline
bool
ascii_is_hexadecimal_digit( s8 c )
{
	bool ret = ascii_is_digit( c )
	        || ( c >= 'a' && c <= 'f' )
	        || ( c >= 'A' && c <= 'F' );
	return ret;
}

inline
bool
ascii_is_octal_digit( s8 c )
{
	bool ret = c >= '0' && c <= '7';
	return ret;
}

inline
bool
ascii_is_binary_digit( s8 c )
{
	bool ret = c == '0' || c == '1';
	return ret;
}

// ASCII
////////////////////////////////////////////////////////////////////////////////

struct
Time_HMS
{
	s32 h, m;
	f32 s;
};

inline
void
decompose_s( f32 total_seconds, s32 * hours, s32 * minutes, f32 * seconds )
{
	f32 total_minutes = floor( total_seconds / 60.f );
	f32 total_hours   = floor( total_minutes / 60.f );
	*seconds = total_seconds - 60 * total_minutes;
	*hours   = (s32)total_hours;
	*minutes = (s32)total_minutes - 60 * *hours;
}
inline
void
decompose_ns( f32 total_ns, s32 * hours, s32 * minutes, f32 * seconds )
{
	f32 total_seconds = total_ns * 1e-9f;
	decompose_s( total_seconds, hours, minutes, seconds );
}
inline
Time_HMS
decompose_s( f32 total_seconds )
{
	Time_HMS ret = {};
	decompose_s( total_seconds, &ret.h, &ret.m, &ret.s );
	return ret;
}
inline
Time_HMS
decompose_ns( f32 total_ns )
{
	Time_HMS ret = {};
	decompose_ns( total_ns, &ret.h, &ret.m, &ret.s );
	return ret;
}


inline
u8
srgb_u8_from_linear01_f32( f32 v )
{
	u8 ret = pow( v, 1.0f / 2.2f ) * 255;
	return ret;
}
inline
f32
linear01_f32_from_srgb_u8( u8 v )
{
	f32 ret = pow( v / 255.f, 2.2f );
	return ret;
}

template <typename T>
struct Permuter
{
	Permuter( const T * from, s32 from_size, s32 permutation_size, const T * append, s32 append_size )
	 : from(from),
	   //append(append),
	   from_size(from_size),
	   permutation_size(permutation_size),
	   append_size(append_size)
	{
		s64 pmbs = permutation_bytes_size();
		permutation = (T*)sys_mem_reserve( pmbs );
		bool committed = sys_mem_commit( permutation, pmbs );
		if ( ! committed )
		{
			free();
			return;
		}

		s64 indexes_bs = indexes_bytes_size();
		indexes = (s32*)sys_mem_reserve( indexes_bs );
		committed = sys_mem_commit( indexes, indexes_bs );
		if ( ! committed )
		{
			free();
			return;
		}

		reset();

		memcpy( permutation + permutation_size * sizeof ( T ), append, append_size * sizeof ( T ) );
	}

	~Permuter()
	{
		free();
	}

	Permuter( Permuter && other )
	#if 0
	 : from            ( other.from ),
	   //append          ( other.append ),
	   from_size       ( other.from_size ),
	   permutation_size( other.permutation_size ),
	   append_size     ( other.append_size )
	{
		permutation = other.permutation;
		other.permutation = 0;
	#else
	{
		memcpy( this, &other, sizeof( *this ) );
		memset( &other, 1, sizeof ( *this ) );
	#endif
	}

	Permuter & operator=( Permuter && other )
	{
		if ( this == other )
		{
			return *this;
		}

		free();

		memcpy( this, &other, sizeof( *this ) );
		memset( &other, 1, sizeof ( *this ) );

		return *this;
	}

	void
	next()
	{
		if ( _ended || permutation_is_last() )
		{
			_ended = true;
			return;
		}

		s32 indexes_index = 0;
		for (;;)
		{
			++indexes[indexes_index];
			if ( indexes[indexes_index] == from_size )
			{
				indexes[indexes_index] = 0;
				permutation[indexes_index] = from[indexes[indexes_index]];
				++indexes_index;
			} else
			{
				permutation[indexes_index] = from[indexes[indexes_index]];
				break;
			}
		}
	}

	bool
	permutation_is_last()
	{
		bool ret = true;
		if ( permutation_size < 128 )
		{
			for_0M ( i, permutation_size )
			{
				ret = ret && indexes[i] == from_size - 1;
			}
		} else
		{
			for_0M ( i, permutation_size )
			{
				ret = ret && indexes[i] == from_size - 1;
				if ( ! ret )
				{
					break;
				}
			}
		}
		return ret;
	}
	bool
	ended()
	{
		return _ended;
	}

	void
	reset()
	{
		memset( indexes, 0, indexes_bytes_size() );

		for_0M( i, permutation_size )
		{
			permutation[i] = from[0];
		}

		_ended = false;
	}

	s64
	permutation_bytes_size()
	{
		s64 ret = ( permutation_size + append_size )* sizeof ( T );
		return ret;
	}
	s64
	indexes_bytes_size()
	{
		s64 ret = sizeof ( s32 ) * permutation_size;
		return ret;
	}

	void
	free()
	{
		if ( permutation )
		{
			sys_mem_free( permutation, permutation_bytes_size() );
		}
		if ( indexes )
		{
			sys_mem_free( indexes, indexes_bytes_size() );
		}
	}

	const T * from;
	//T * append;
	T * permutation;
	s32 * indexes;
	s32 from_size;
	s32 permutation_size;
	s32 append_size;
	bool _ended;
};

struct
Allocator
{
	void * ( * allocate_function )( void * data, u64 bytes_size );
	void * data;
	void *
	allocate( u64 bytes_size )
	{
		return allocate_function( data, bytes_size );
	}
};
#endif /* ifndef _UTILITY_H */
