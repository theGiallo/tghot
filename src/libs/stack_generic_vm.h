#ifndef _STACK_GENERIC_VM_H_
#define _STACK_GENERIC_VM_H_ 1

#include "basic_types.h"

struct
Stack_Generic_VM;

bool
stack_generic_vm_init( Stack_Generic_VM * stack, u64 element_size, u64 max_capacity, u64 initial_capacity = 0, f64 capacity_increment_multiplier = 1.5, u64 capacity_increment_adder = 0 );

bool
stack_generic_vm_reserve_free( Stack_Generic_VM * stack, u64 free_count );

bool
stack_generic_vm_reserve( Stack_Generic_VM * stack, u64 new_capacity_at_least );

bool
stack_generic_vm_push_space( Stack_Generic_VM * stack, void ** space );

bool
stack_generic_vm_push_space( Stack_Generic_VM * stack, void ** first_space, u64 count );

bool
stack_generic_vm_push( Stack_Generic_VM * stack, void * element );

void *
stack_generic_vm_peek( Stack_Generic_VM * stack );

bool
stack_generic_vm_peek( Stack_Generic_VM * stack, void * out_copied_element );

void *
stack_generic_vm_pop( Stack_Generic_VM * stack );

bool
stack_generic_vm_pop( Stack_Generic_VM * stack, void * out_copied_element );

void
stack_generic_vm_destroy( Stack_Generic_VM * stack );

struct
Stack_Generic_VM
{
	void * mem;
	u64    element_size;
	u64    max_capacity;
	u64    current_capacity;
	u64    occupancy;
	// NOTE(theGiallo): new_size = adder + old_size * multiplier;
	f64    capacity_increment_multiplier;
	u64    capacity_increment_adder;


	inline
	bool
	init( u64 element_size,
	      u64 max_capacity,
	      u64 initial_capacity = 0,
	      f64 capacity_increment_multiplier = 1.5,
	      u64 capacity_increment_adder = 0 )
	{
		return stack_generic_vm_init( this,
		                              element_size,
		                              max_capacity,
		                              initial_capacity,
		                              capacity_increment_multiplier,
		                              capacity_increment_adder );
	}


	inline
	void *
	at( s64 index )
	{
		void * ret = (void*)( (u8*)mem + element_size * index );
		return ret;
	}

	inline
	bool
	reserve_free( u64 free_count )
	{
		return stack_generic_vm_reserve_free( this, free_count );
	}

	inline
	bool
	reserve( u64 new_capacity_at_least )
	{
		return stack_generic_vm_reserve( this, new_capacity_at_least );
	}

	inline
	bool
	push_space( void ** element )
	{
		return stack_generic_vm_push_space( this, element );
	}

	inline
	bool
	push_space( void ** first_element, u64 count )
	{
		return stack_generic_vm_push_space( this, first_element, count );
	}

	inline
	bool
	push( void * element )
	{
		return stack_generic_vm_push( this, element );
	}

	inline
	void *
	peek()
	{
		return stack_generic_vm_peek( this );
	}

	inline
	bool
	peek( void * out_copied_element )
	{
		return stack_generic_vm_peek( this, out_copied_element );
	}

	inline
	void *
	pop()
	{
		return stack_generic_vm_pop( this );
	}

	inline
	bool
	pop( void * out_copied_element )
	{
		return stack_generic_vm_pop( this, out_copied_element );
	}

	inline
	void
	destroy()
	{
		return stack_generic_vm_destroy( this );
	}
};

#endif /* ifndef _STACK_GENERIC_VM_H_ */
