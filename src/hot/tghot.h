#ifndef _TG_HOT_H_
#define _TG_HOT_H_ 1

#include <initializer_list>
#include <dlfcn.h>

#include "macro_tools.h"
#include "basic_types.h"
#include "tgarray.h"
#include "tgstring.h"
#include "hash_table_generic_vm.h"
#include "hash_table_generic_multi_vm.h"
#include "utility.h"


//__declspec(__dllexport)
#define _TG_DL_EXPORTED_ATTR __attribute__((visibility("default")))

#ifdef  __cplusplus
#define TG_DL_EXPORTED extern "C" _TG_DL_EXPORTED_ATTR
#else
#define TG_DL_EXPORTED extern _TG_DL_EXPORTED_ATTR
#endif

namespace tg::hot
{
	using String = tg::String;

	////////////////////////////////////////////////////////////////////////////////
	// NOTE(theGiallo): Structs

	struct
	Hot_Api_Lib_Data
	{
		void * data;
		s64    size;
	};

	struct
	DL
	{
		const char * name;
		tg::Array<const char*> functions_names;
		tg::Array<void*      > functions;

		static
		inline
		DL
		Make( const char * name, std::initializer_list<const char *> functions_names )
		{
			DL ret;

			ret.name = name;
			ret.functions_names = tg::Array<const char*>::Make( functions_names );
			ret.functions       = tg::Array<void *     >::Make( functions_names.size() );

			return ret;
		}
	};

	// NOTE(theGiallo): Structs ...
	////////////////////////////////////////////////////////////////////////////////

	TG_DL_EXPORTED
	void
	set_lib_data( Hot_Api_Lib_Data data, const char * lib_name );

	TG_DL_EXPORTED
	bool
	request_dl( DL & dl, void ( * callback ) ( DL * dl, void * custom_data ), const char * registerer_name, void * custom_data = 0 );


	////////////////////////////////////////////////////////////////////////////////
	// NOTE(theGiallo): ... Structs

	struct
	Hot_Api
	{
		const char * lib_name;

		bool
		request_dl( DL & dl, void ( * callback ) ( DL * dl, void * custom_data ), void * custom_data = 0 )
		{
			return ::tg::hot::request_dl( dl, callback, lib_name, custom_data );
		}

		void
		set_lib_data( void * data, s64 size )
		{
			::tg::hot::set_lib_data( Hot_Api_Lib_Data{ data, size }, lib_name );
		}
	};

	// NOTE(theGiallo): Structs
	////////////////////////////////////////////////////////////////////////////////

	extern
	void
	reload_if_you_want();

	extern
	void
	register_callback( DL & dl, void ( * callback ) ( DL * dl, void * custom_data ), void * custom_data, const char * requester_name );

	extern
	void
	remove_callbacks_by( const char * requester_name );

	extern
	bool
	dl_is_present( DL & dl );

	extern
	bool
	build_lib_version_path( char * str, s32 buf_len, const char * dl_name );
	extern
	bool
	build_lib_path_versioned( char * str, s32 buf_len, const char * dl_name, const char * lib_version );


	String __DEBUG_STRING = {};
}

#endif /* ifndef _TG_HOT_H_ */
