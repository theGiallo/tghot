#if 0
	echo $#
	echo $0 $1 $2

	#echo fullpath should be $(readlink -e $0)
	#export THIS_FILE_PATH=$0
	export THIS_FILE_PATH=$(readlink -e $0)
	#export THIS_FILE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	export THIS_FILE_DIR=$(dirname $THIS_FILE_PATH)
	export THIS_FILE_NAME=${THIS_FILE_PATH##*/}
	export THIS_FILE_BASE_NAME=${THIS_FILE_NAME%%.*}

	exec /bin/bash -c 'exec "/bin/bash" <(tail -n +15 "$0") "$@"' "$0" "$@"
	exit $!

	# IFS= read -r -d '' BASH_TEXT <<BASH_SECTION
	# !/bin/bash
	echo -n "curr dir: "
	pwd

	APP_NAME='"tg::hot_test"'

	REL_ROOT_PROJ=../
	REL_DLIBS_DIR=./libs/
	DLIBS_DIR_FULL_PATH="$THIS_FILE_DIR/$REL_DLIBS_DIR"

	LIB_NAME=$THIS_FILE_BASE_NAME
	EXECUTABLE_NAME=$THIS_FILE_BASE_NAME
	LIB_FILE_NAME=lib${THIS_FILE_NAME%%.*}.so
	if [ -z "${LIB_VERSION}" ]
	then
		LIB_VERSION=$(date +%Y.%m.%d.%H.%M.%S);
	fi

	echo "this file dir   = '$THIS_FILE_DIR'"
	echo "this file path  = '$THIS_FILE_PATH'"
	echo "this file name  = '$THIS_FILE_NAME'"
	echo "base file name  = '$THIS_FILE_BASE_NAME'"
	echo "executable name = '$EXECUTABLE_NAME'"
	echo "lib name        = '$LIB_NAME'"
	echo "app name        = '$APP_NAME'"
	echo
	echo args count: $#
	i=0
	for arg in $*; do
		echo -e "\targ $i: $arg"
		((i++))
	done
	echo

	if [ -z "${CXX+x}" ]
	then
		#CXX=arm-linux-gnueabihf-g++
		CXX=clang++
		#CXX=g++
	fi

	if [[ "${CXX}" == @(g++*|gcc*) ]]
	then

	CXX_FLAGS="-std=c++20 \
	           -Wall \
	           -Wextra \
	           -Wno-missing-braces \
	           -Wno-unused-function \
	           -Wno-comment \
	           -fno-exceptions \
	           -feliminate-unused-debug-types \
	           -rdynamic \
	           -ffunction-sections -Wl,--gc-sections \
	           -fvisibility=hidden \
	           -O0 -ggdb3 \
	           -march=native \
	           -DAPP_NAME='$APP_NAME' \
	           -DEXECUTABLE_NAME='\"$EXECUTABLE_NAME\"' \
	           -DTGHOT_DLIBS_DIR='\"$DLIBS_DIR_FULL_PATH\"' \
	           $CXX_FLAGS"

	elif [[ $CXX == clang* ]]
	then
	CXX_FLAGS="-std=c++20 \
	           -Wall \
	           -Wextra \
	           -Wno-missing-braces \
	           -Wno-unused-function \
	           -Wno-comment \
	           -Rpass-missed=loop-vectorize -Rpass-analysis=loop-vectorize \
	           -fno-exceptions \
	           -feliminate-unused-debug-types \
	           -Wl,--export-dynamic \
	           -fvisibility=hidden \
	           -flto=thin \
	           -O0 -ggdb3 \
	           -march=native \
	           -DAPP_NAME='$APP_NAME' \
	           -DEXECUTABLE_NAME='\"$EXECUTABLE_NAME\"' \
	           -DTGHOT_DLIBS_DIR='\"$DLIBS_DIR_FULL_PATH\"' \
	           $CXX_FLAGS"
	fi

	#          -fvisibility=hidden \ makes all functions internal to the compilation unit, and exported only if specified
	#          -fsave-optimization-record \ clang outputs text file for what it did
	#          -O3 \
	#          -O0 -ggdb3 \
	# -fwhole-program # ensures this CU represents the whole program, enabling more aggressive opts. Don t use if -flto is present
	# -flto=auto # link time optimization ( removes unused functions ) (g++ only)
	# -flto=thin # clang outputs small .so http://blog.llvm.org/2016/06/thinlto-scalable-and-incremental-lto.html
	# -ffunction-sections -Wl,--gc-sections # remove unused functions (this is also achieved with -flto), separating code from data
	# -fpermissive # downgrades some errors to warnings for non-conformant code
	#-fPIC # position independent code, useful for linked libraries

	declare -a DEFINES
	DEFINES[${#DEFINES[@]}]=DEBUG=1

	declare -a INCLUDE_DIRS
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR/../
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"libs"

	declare -a LIBS_DIRS
	LIBS_DIRS[${#LIBS_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"src/"
	#LIBS_DIRS[${#LIBS_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"libraries/"

	declare -a LIBS
	LIBS[${#LIBS[@]}]=pthread
	LIBS[${#LIBS[@]}]=dl
	#LIBS[${#LIBS[@]}]=rt

	function \
	defines()
	{
		for d in ${DEFINES[@]}
		do
			printf " -D$d "
		done
	}
	function \
	include_dirs()
	{
		for p in ${INCLUDE_DIRS[@]}
		do
			printf " -I$p "
		done
	}
	function \
	libs_dirs()
	{
		for p in ${LIBS_DIRS[@]}
		do
			printf " -L$p "
	#printf " -Wl,-rpath=$p "
		done
	}
	function \
	link_libs()
	{
		for l in ${LIBS[@]}
		do
			printf " -l$l "
		done
	}

	ret=3

	echo "calling compiler"

	# -Wl,-z,origin libfoo.so -Wl,-rpath,./
	CMD="$CXX $CXX_FLAGS `defines` `include_dirs` -o $EXECUTABLE_NAME $THIS_FILE_PATH    `libs_dirs` `link_libs` -Wl,--disable-new-dtags "
	echo CMD:
	echo $CMD
	RUNNABLE_CMD=$(echo $CMD | sed "s/'\"/\"/g" | sed "s/\"'/\"/g")
	echo RUNNABLE_CMD:
	echo $RUNNABLE_CMD

	#echo
	STRIPPED_CMD=$(echo $CMD | sed 's/"/\\"/g')
	#echo
	#echo $STRIPPED_CMD

	JSON="[\n{\n\t\"directory\":\"$THIS_FILE_DIR/$REL_ROOT_PROJ\",\n\t\"command\":\"$STRIPPED_CMD\",\n\t\"file\":\"$THIS_FILE_PATH\"\n},\n]";
	echo -e $JSON > compile_commands.json

	TIME="real %E | user %U | sys %S | max mem %M kB | avg mem %K kB" time $RUNNABLE_CMD

	ret=$?;

	if [ $ret -eq 0 ]
	then
		printf 'compilation succeeded\n'
		file $EXECUTABLE_NAME
		ls -l $EXECUTABLE_NAME

		echo "running ./$EXECUTABLE_NAME $@"
		echo
		chmod +x ./$EXECUTABLE_NAME
		#./$EXECUTABLE_NAME "$@"
		gdb --args ./$EXECUTABLE_NAME "$@"
		#gdb -ex run ./$EXECUTABLE_NAME --args "$@"

		ret=$?

		if [ ! $ret -eq 0 ]
		then
			printf "run failed\n"
		fi
		#trash "$EXECUTABLE_NAME"
		#trash "$EXECUTABLE_NAME.o"
	else
		printf 'compilation failed\n' >&2;
	fi

	echo "exiting $ret"
	exit $ret
	# BASH_SECTION

	# bash -c "exec <($BASH_TEXT)"
	# ret=$?
	# exit $ret
#endif


#include "tghot.h"

using namespace tg::hot;

struct
Some_Library
{
	DL dl;

	void (*some_function      )( s32 some_arg );
	s32  (*some_other_function)( s32 some_arg, const char * some_other_arg );
	bool (*another_one        )( void * some_arg );

} some_library = { .dl = DL::Make( "somelibrary", { "some_function", "some_other_function", "another_one" } ) };

namespace
Some_Other_Library
{
	DL dl = DL::Make( "someotherlibrary", { "some_function", "some_other_function", "another_one", "another_one_2" } );

	const char * (*some_function      )();
	void         (*some_other_function)( s32 some_arg, const char * some_other_arg );
	bool         (*another_one        )( void * some_arg, s64 arg );
	bool         (*another_one_2      )( void * some_arg, s64 arg );

};

void
dl_load_callback_some_library( DL * dl, void * custom_data )
{
	(void)custom_data;
	log_dbg( "some_library_callback( %s ) %ld functions", dl->name, dl->functions.count );

	*(void**)&some_library.some_function       = dl->functions.array[0];
	*(void**)&some_library.some_other_function = dl->functions.array[1];
	*(void**)&some_library.another_one         = dl->functions.array[2];
}
void
dl_load_callback_some_other_library( DL * dl, void * custom_data )
{
	(void)custom_data;
	*(void**)&Some_Other_Library::some_function       = dl->functions.array[0];
	*(void**)&Some_Other_Library::some_other_function = dl->functions.array[1];
	*(void**)&Some_Other_Library::another_one         = dl->functions.array[2];
	*(void**)&Some_Other_Library::another_one_2       = dl->functions.array[3];

	// NOTE(theGiallo): maybe we need to call something here, to do some bookkeeping
}

s32
main( s32 argc, char * argv[] )
{
	(void)argc;
	(void)argv;

	bool b_res = tg::hot::request_dl( some_library.dl, dl_load_callback_some_library, "main" );
	if ( ! b_res )
	{
		log_err( "request_dl failed" );
		return 1;
	}

	b_res = tg::hot::request_dl( Some_Other_Library::dl, dl_load_callback_some_other_library, "main" );
	if ( ! b_res )
	{
		log_err( "request_dl failed" );
		return 1;
	}


	//for_0M( it, 5 )
	bool run = true;
	while ( run )
	{
		//log_dbg( "#  #  #  #  #  #  #  #  #  #  iteration %d", it );

		if ( some_library.some_function )
		{
			some_library.some_function( 314 );
		} else
		{
			log_dbg( "some_library.some_function is null" );
		}

		if ( Some_Other_Library::some_function )
		{
			Some_Other_Library::some_function();
		}

		u64 ns_start = ns_CPU_time();
		tg::hot::reload_if_you_want();
		u64 ns_end   = ns_CPU_time();
		u64 reload_if_you_want_ns = ns_end - ns_start;
		//log_dbg( "sleeping" );

		fd_set rfds = {};
		FD_ZERO( &rfds );
		FD_SET( STDIN_FILENO, &rfds );

		struct timeval tv = {};
		tv.tv_usec = 16666;
		s32 retval = select(1, &rfds, NULL, NULL, &tv);

		if ( FD_ISSET( STDIN_FILENO, &rfds ) )
		{
			log_dbg( "stdin is set" );
		}

		if ( retval == -1 )
		{
			perror( "select()" );
		} else
		if (retval)
		{
			log_dbg( "Data is available now.\n" );
			char buf[1024] = {};
			char line[1024] = {};
			char * res = fgets( buf, sizeof ( buf ) - 1, stdin );
			if ( res )
			{
				tg_assert( buf[sizeof( buf ) - 1] == 0 );
				s32 read = 0;
				s32 el_read = 0;
				do
				{
					s32 line_len = 0;
					el_read = sscanf( buf + read, "%s\n%n", line, &line_len );
					if ( el_read == 1 )
					{
						read += line_len;
						log_dbg( "read (%d): %s", read, buf );
						if ( string_equal( line, "exit" )
						  || string_equal( line, "quit" ) )
						{
							run = false;
							break;
						} else
						if ( string_equal( line, "help" ) )
						{
							log_info( "type 'exit' or 'quit' to exit the program\ntype 'perf' to get tg::hot timings" );
						}
						else
						if ( string_equal( line, "perf" ) )
						{
							log_info( "reload_if_you_want took %lu ns that is %f ms",
							          reload_if_you_want_ns, reload_if_you_want_ns / (f64)1e6 );
						}
					}
				} while ( el_read == 1 );
			}
		} else
		{
			//printf("No data within five seconds.\n");
		}
	}

	#if 0
	b_res = tg::hot::request_dl( Some_Other_Library::dl, dl_load_callback_some_other_library, "main" );
	if ( b_res )
	{
		log_err( "request_dl failed" );
		return 1;
	}
	#endif

	return 0;
}

#include "tghot.cpp"
#include "pool_allocator_generic_vm.cpp"
#include "hash_table_generic_vm.cpp"
#include "hash_table_generic_multi_vm.cpp"
#include "utility.cpp"
#include "system.cpp"
#include "tgmath.cpp"
