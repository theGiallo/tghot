#if 0
	echo $#
	echo $0 $1 $2

	#echo fullpath should be $(readlink -e $0)
	#export THIS_FILE_PATH=$0
	export THIS_FILE_PATH=$(readlink -e $0)
	#export THIS_FILE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	export THIS_FILE_DIR=$(dirname $THIS_FILE_PATH)
	export THIS_FILE_NAME=${THIS_FILE_PATH##*/}
	export THIS_FILE_BASE_NAME=${THIS_FILE_NAME%%.*}

	exec /bin/bash -c 'exec "/bin/bash" <(tail -n +15 "$0") "$@"' "$0" "$@"
	exit $!

	# IFS= read -r -d '' BASH_TEXT <<BASH_SECTION
	# !/bin/bash
	echo -n "curr dir: "
	pwd

	APP_NAME='"somelibrary"'

	REL_ROOT_PROJ=../
	REL_DLIBS_DIR=./libs/
	DLIBS_DIR_FULL_PATH="$THIS_FILE_DIR/$REL_DLIBS_DIR"

	if [ ! -d $DLIBS_DIR_FULL_PATH ]
	then
		mkdir -p $DLIBS_DIR_FULL_PATH
	fi

	DLIB_NAME=$THIS_FILE_BASE_NAME
	DLIB_FILE_NAME=lib$DLIB_NAME.so
	if [ -z "${DLIB_VERSION}" ]
	then
		DLIB_VERSION=$(date +%Y.%m.%d.%H.%M.%S);
	fi
	DLIB_FILE_NAME_VERSIONED=$DLIB_FILE_NAME.$DLIB_VERSION

	DLIB_FULL_PATH="$DLIBS_DIR_FULL_PATH/$DLIB_FILE_NAME"
	DLIB_VERSIONED_FULL_PATH="$DLIBS_DIR_FULL_PATH/$DLIB_FILE_NAME_VERSIONED"

	echo "this file dir   = '$THIS_FILE_DIR'"
	echo "this file path  = '$THIS_FILE_PATH'"
	echo "this file name  = '$THIS_FILE_NAME'"
	echo "base file name  = '$THIS_FILE_BASE_NAME'"
	echo "executable name = '$EXECUTABLE_NAME'"
	echo "libs dir        = '$DLIBS_DIR_FULL_PATH'"
	echo "lib name        = '$DLIB_NAME'"
	echo "lib full path   = '$DLIB_FULL_PATH'"
	echo "lib v. fullpath = '$DLIB_VERSIONED_FULL_PATH'"
	echo "app name        = '$APP_NAME'"
	echo
	echo args count: $#
	i=0
	for arg in $*; do
		echo -e "\targ $i: $arg"
		((i++))
	done
	echo

	if [ -z "${CXX+x}" ]
	then
		#CXX=arm-linux-gnueabihf-g++
		CXX=clang++
		#CXX=g++
	fi

	if [[ "${CXX}" == @(g++*|gcc*) ]]
	then

	CXX_FLAGS="-std=c++20 \
	           -Wall \
	           -Wextra \
	           -Wno-missing-braces \
	           -Wno-unused-function \
	           -Wno-comment \
	           -Wl,-export-dynamic -rdynamic -fPIC -shared -fno-exceptions \
	           -feliminate-unused-debug-types \
	           -ffunction-sections -Wl,--gc-sections \
	           -fvisibility=hidden \
	           -O0 -ggdb3 \
	           -march=native \
	           -DAPP_NAME='$APP_NAME' \
	           -DLIB_NAME='\"$DLIB_NAME\"' \
	           -DLIB_VERSION=$DLIB_VERSION \
	           $CXX_FLAGS"

	elif [[ $CXX == clang* ]]
	then
	CXX_FLAGS="-std=c++20 \
	           -Wall \
	           -Wextra \
	           -Wno-missing-braces \
	           -Wno-unused-function \
	           -Wno-comment \
	           -Rpass-missed=loop-vectorize -Rpass-analysis=loop-vectorize \
	           -Wl,-export-dynamic -rdynamic -fdeclspec -fPIC -shared -fno-exceptions \
	           -feliminate-unused-debug-types \
	           -fvisibility=hidden \
	           -flto=thin \
	           -O0 -ggdb3 \
	           -march=native \
	           -DAPP_NAME='$APP_NAME' \
	           -DLIB_NAME='\"$DLIB_NAME\"' \
	           -DLIB_VERSION=$DLIB_VERSION \
	           $CXX_FLAGS"
	fi

	#          -fvisibility=hidden \ makes all functions internal to the compilation unit, and exported only if specified
	#          -fsave-optimization-record \ clang outputs text file for what it did
	#          -O3 \
	#          -O0 -ggdb3 \
	# -fwhole-program # ensures this CU represents the whole program, enabling more aggressive opts. Don t use if -flto is present
	# -flto=auto # link time optimization ( removes unused functions ) (g++ only)
	# -flto=thin # clang outputs small .so http://blog.llvm.org/2016/06/thinlto-scalable-and-incremental-lto.html
	# -ffunction-sections -Wl,--gc-sections # remove unused functions (this is also achieved with -flto), separating code from data
	# -fpermissive # downgrades some errors to warnings for non-conformant code
	#-fPIC # position independent code, useful for linked libraries

	declare -a DEFINES
	DEFINES[${#DEFINES[@]}]=DEBUG=1

	declare -a INCLUDE_DIRS
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"libs"

	declare -a LIBS_DIRS
	#LIBS_DIRS[${#LIBS_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"libraries/"

	declare -a LIBS
	LIBS[${#LIBS[@]}]=pthread
	LIBS[${#LIBS[@]}]=dl
	#LIBS[${#LIBS[@]}]=rt

	function \
	defines()
	{
		for d in ${DEFINES[@]}
		do
			printf " -D$d "
		done
	}
	function \
	include_dirs()
	{
		for p in ${INCLUDE_DIRS[@]}
		do
			printf " -I$p "
		done
	}
	function \
	libs_dirs()
	{
		for p in ${LIBS_DIRS[@]}
		do
			printf " -L$p "
	#printf " -Wl,-rpath=$p "
		done
	}
	function \
	link_libs()
	{
		for l in ${LIBS[@]}
		do
			printf " -l$l "
		done
	}

	ret=3

	echo "calling compiler"

	CMD="$CXX $CXX_FLAGS `defines` `include_dirs` -Wl,-soname,lib$DLIB_NAME.so.$DLIB_VERSION -o $DLIB_FULL_PATH $THIS_FILE_PATH `libs_dirs` `link_libs`"
	echo CMD:
	echo $CMD
	RUNNABLE_CMD=$(echo $CMD | sed "s/'\"/\"/g" | sed "s/\"'/\"/g")
	echo RUNNABLE_CMD:
	echo $RUNNABLE_CMD

	#echo
	STRIPPED_CMD=$(echo $CMD | sed 's/"/\\"/g')
	#echo
	#echo $STRIPPED_CMD

	JSON="[\n{\n\t\"directory\":\"$THIS_FILE_DIR/$REL_ROOT_PROJ\",\n\t\"command\":\"$STRIPPED_CMD\",\n\t\"file\":\"$THIS_FILE_PATH\"\n},\n]";
	echo -e $JSON > compile_commands.json

	TIME="real %E | user %U | sys %S | max mem %M kB | avg mem %K kB" time $RUNNABLE_CMD

	ret=$?;

	if [ $ret -eq 0 ]
	then
		printf 'compilation succeeded\n'
		ln -s $DLIB_FULL_PATH $DLIB_VERSIONED_FULL_PATH
		ls -l $DLIB_FULL_PATH $DLIB_VERSIONED_FULL_PATH
		file $DLIB_FULL_PATH
		echo -n $DLIB_VERSION > "$DLIBS_DIR_FULL_PATH/lib$DLIB_NAME.version.txt"
		objdump -p $DLIB_FULL_PATH | grep SONAME

		#chmod +x ./$EXECUTABLE_NAME
	else
		printf 'compilation failed\n' >&2;
	fi

	echo "exiting $ret"
	exit $ret
	# BASH_SECTION

	# bash -c "exec <($BASH_TEXT)"
	# ret=$?
	# exit $ret
#endif

#include "basic_types.h"
#include "macro_tools.h"
#include "tghot.h"

namespace
Some_Other_Library
{
	using namespace tg::hot;

	DL dl = DL::Make( "someotherlibrary", { "some_function", "some_other_function", "another_one", "another_one_2" } );

	const char * (*some_function      )();
	void         (*some_other_function)( s32 some_arg, const char * some_other_arg );
	bool         (*another_one        )( void * some_arg, s64 arg );
	bool         (*another_one_2      )( void * some_arg, s64 arg );

	void
	hotapi_callback( DL * dl, void * custom_data )
	{
		log_dbg( "updating Some_Other_Library dl.name %s custom_data: %s", dl->name, (char*)custom_data );
		*(void**)&Some_Other_Library::some_function       = dl->functions.array[0];
		*(void**)&Some_Other_Library::some_other_function = dl->functions.array[1];
		*(void**)&Some_Other_Library::another_one         = dl->functions.array[2];
		*(void**)&Some_Other_Library::another_one_2       = dl->functions.array[3];

		// NOTE(theGiallo): maybe we need to call something here, to do some bookkeeping
		if ( Some_Other_Library::some_other_function )
		{
			Some_Other_Library::some_other_function( 33, "hello" );
		}
	}
};

struct
Sem_Ver
{
	u32 major, minor, patch;

	inline
	bool
	operator == ( Sem_Ver r ) const
	{
		const Sem_Ver & l = *this;
		bool ret = l.major == r.major
		        && l.minor == r.minor
		        && l.patch == r.patch;
		return ret;
	}
	inline
	bool
	operator != ( Sem_Ver r ) const
	{
		const Sem_Ver & l = *this;
		bool ret = ! ( l == r );
		return ret;
	}
	inline
	bool
	operator >( Sem_Ver r ) const
	{
		const Sem_Ver & l = *this;
		bool ret = l.major > r.major
		        || ( l.major == r.major
		          && ( l.minor > r.minor
		            || ( l.minor == r.minor
		              && ( l.patch > r.patch ) ) ) );
		return ret;
	}
	inline
	bool
	operator <( Sem_Ver r ) const
	{
		const Sem_Ver & l = *this;
		bool ret = l != r && l > r;
		return ret;
	}
	inline
	bool
	operator >=( Sem_Ver r ) const
	{
		const Sem_Ver & l = *this;
		bool ret = l > r || l == r;
		return ret;
	}
	inline
	bool
	operator <=( Sem_Ver r ) const
	{
		const Sem_Ver & l = *this;
		bool ret = l == r || l < r;
		return ret;
	}
};

namespace
Some_Library
{
	struct
	Glob
	{
		Sem_Ver version;
		s32 counter;
	};
	Glob glob = {.version = {0,1,0}};
};

TG_DL_EXPORTED
void
hot_boot( tg::hot::Hot_Api * hot_api, tg::hot::Hot_Api_Lib_Data data )
{
	(void)hot_api;
	(void)data;
	const char * version_str = TOSTRING(LIB_VERSION);
	log_dbg( "hot boot of " LIB_NAME " %s", version_str );

	s64 gs = sizeof ( Some_Library::glob );

	if ( data.data )
	{
		Sem_Ver * ov = (Sem_Ver*)data.data;
		Sem_Ver v = Some_Library::glob.version;
		log_dbg( "data version %d.%d.%d -> %d.%d.%d", ov->major, ov->minor, ov->patch, v.major, v.minor, v.patch );
		if ( data.size == gs )
		{
			memcpy( &Some_Library::glob, data.data, data.size );
		}
		else
		{
			log_dbg( "data size changed %ld -> %ld", data.size, gs );
			s64 ms = MIN( data.size, gs );
			memcpy( &Some_Library::glob, data.data, ms );
		}
		Some_Library::glob.version = v;
	}

	log_dbg( "counter = %d", Some_Library::glob.counter );
	++Some_Library::glob.counter;

	hot_api->set_lib_data( (void*)&Some_Library::glob, gs );

	log_dbg( "requesting dl = %lx", (u64)&Some_Other_Library::dl );
	hot_api->request_dl( Some_Other_Library::dl, Some_Other_Library::hotapi_callback, (void*)"first request" );
	hot_api->request_dl( Some_Other_Library::dl, Some_Other_Library::hotapi_callback, (void*)"second request" );
}

TG_DL_EXPORTED
void
hot_outdated()
{
}


TG_DL_EXPORTED void some_function      ( s32 some_arg )
{
	(void)some_arg;
	//log_dbg( "some_function %d hello world", some_arg );
	if ( Some_Other_Library::another_one )
	{
		bool b_res = Some_Other_Library::another_one( 0, 0 );
		//log_dbg( "Some_Other_Library::another_one -> %s", BOOL_TO_S( b_res ) );
	}
}
TG_DL_EXPORTED s32  some_other_function( s32 some_arg, const char * some_other_arg ) { (void)some_arg; (void)some_other_arg; return 0; }
TG_DL_EXPORTED bool another_one        ( void * some_arg ) { (void)some_arg; return false; }

//__attribute__((__visibility__("default")))
//void(*foo_ptr)() = &foo;

void
__attribute ((constructor))
init_function()
{
	log_dbg( "constructor lib %s version %s", TOSTRING(LIB_NAME), TOSTRING(LIB_VERSION) );
}
void
__attribute ((destructor))
fini_function()
{
	log_dbg( "destructor lib %s version %s", TOSTRING(LIB_NAME), TOSTRING(LIB_VERSION) );
}

#include "utility.cpp"
#include "tgmath.cpp"
