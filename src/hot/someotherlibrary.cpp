#if 0
	echo $#
	echo $0 $1 $2

	#echo fullpath should be $(readlink -e $0)
	#export THIS_FILE_PATH=$0
	export THIS_FILE_PATH=$(readlink -e $0)
	#export THIS_FILE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	export THIS_FILE_DIR=$(dirname $THIS_FILE_PATH)
	export THIS_FILE_NAME=${THIS_FILE_PATH##*/}
	export THIS_FILE_BASE_NAME=${THIS_FILE_NAME%%.*}

	exec /bin/bash -c 'exec "/bin/bash" <(tail -n +15 "$0") "$@"' "$0" "$@"
	exit $!

	# IFS= read -r -d '' BASH_TEXT <<BASH_SECTION
	# !/bin/bash
	echo -n "curr dir: "
	pwd

	APP_NAME='"someotherlibrary"'

	REL_ROOT_PROJ=../
	REL_DLIBS_DIR=./libs/
	DLIBS_DIR_FULL_PATH="$THIS_FILE_DIR/$REL_DLIBS_DIR"

	if [ ! -d $DLIBS_DIR_FULL_PATH ]
	then
		mkdir -p $DLIBS_DIR_FULL_PATH
	fi

	DLIB_NAME=$THIS_FILE_BASE_NAME
	DLIB_FILE_NAME=lib$DLIB_NAME.so
	if [ -z "${DLIB_VERSION}" ]
	then
		DLIB_VERSION=$(date +%Y.%m.%d.%H.%M.%S);
	fi
	DLIB_FILE_NAME_VERSIONED=$DLIB_FILE_NAME.$DLIB_VERSION

	DLIB_FULL_PATH="$DLIBS_DIR_FULL_PATH/$DLIB_FILE_NAME"
	DLIB_VERSIONED_FULL_PATH="$DLIBS_DIR_FULL_PATH/$DLIB_FILE_NAME_VERSIONED"

	echo "this file dir   = '$THIS_FILE_DIR'"
	echo "this file path  = '$THIS_FILE_PATH'"
	echo "this file name  = '$THIS_FILE_NAME'"
	echo "base file name  = '$THIS_FILE_BASE_NAME'"
	echo "executable name = '$EXECUTABLE_NAME'"
	echo "libs dir        = '$DLIBS_DIR_FULL_PATH'"
	echo "lib name        = '$DLIB_NAME'"
	echo "lib full path   = '$DLIB_FULL_PATH'"
	echo "lib v. fullpath = '$DLIB_VERSIONED_FULL_PATH'"
	echo "app name        = '$APP_NAME'"
	echo
	echo args count: $#
	i=0
	for arg in $*; do
		echo -e "\targ $i: $arg"
		((i++))
	done
	echo

	if [ -z "${CXX+x}" ]
	then
		#CXX=arm-linux-gnueabihf-g++
		CXX=clang++
		#CXX=g++
	fi

	if [[ "${CXX}" == @(g++*|gcc*) ]]
	then

	CXX_FLAGS="-std=c++20 \
	           -Wall \
	           -Wextra \
	           -Wno-missing-braces \
	           -Wno-unused-function \
	           -Wno-comment \
	           -Wl,-export-dynamic -rdynamic -fPIC -shared -fno-exceptions \
	           -feliminate-unused-debug-types \
	           -ffunction-sections -Wl,--gc-sections \
	           -fvisibility=hidden \
	           -O0 -ggdb3 \
	           -march=native \
	           -DAPP_NAME='$APP_NAME' \
	           -DLIB_NAME='\"$DLIB_NAME\"' \
	           -DLIB_VERSION=$DLIB_VERSION \
	           $CXX_FLAGS"

	elif [[ $CXX == clang* ]]
	then
	CXX_FLAGS="-std=c++20 \
	           -Wall \
	           -Wextra \
	           -Wno-missing-braces \
	           -Wno-unused-function \
	           -Wno-comment \
	           -Rpass-missed=loop-vectorize -Rpass-analysis=loop-vectorize \
	           -Wl,-export-dynamic -rdynamic -fdeclspec -fPIC -shared -fno-exceptions \
	           -feliminate-unused-debug-types \
	           -fvisibility=hidden \
	           -flto=thin \
	           -O0 -ggdb3 \
	           -march=native \
	           -DAPP_NAME='$APP_NAME' \
	           -DLIB_NAME='\"$DLIB_NAME\"' \
	           -DLIB_VERSION=$DLIB_VERSION \
	           $CXX_FLAGS"
	fi

	#          -fvisibility=hidden \ makes all functions internal to the compilation unit, and exported only if specified
	#          -fsave-optimization-record \ clang outputs text file for what it did
	#          -O3 \
	#          -O0 -ggdb3 \
	# -fwhole-program # ensures this CU represents the whole program, enabling more aggressive opts. Don t use if -flto is present
	# -flto=auto # link time optimization ( removes unused functions ) (g++ only)
	# -flto=thin # clang outputs small .so http://blog.llvm.org/2016/06/thinlto-scalable-and-incremental-lto.html
	# -ffunction-sections -Wl,--gc-sections # remove unused functions (this is also achieved with -flto), separating code from data
	# -fpermissive # downgrades some errors to warnings for non-conformant code
	#-fPIC # position independent code, useful for linked libraries

	declare -a DEFINES
	DEFINES[${#DEFINES[@]}]=DEBUG=1

	declare -a INCLUDE_DIRS
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"libs"

	declare -a LIBS_DIRS
	#LIBS_DIRS[${#LIBS_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"libraries/"

	declare -a LIBS
	LIBS[${#LIBS[@]}]=pthread
	LIBS[${#LIBS[@]}]=dl
	#LIBS[${#LIBS[@]}]=rt

	function \
	defines()
	{
		for d in ${DEFINES[@]}
		do
			printf " -D$d "
		done
	}
	function \
	include_dirs()
	{
		for p in ${INCLUDE_DIRS[@]}
		do
			printf " -I$p "
		done
	}
	function \
	libs_dirs()
	{
		for p in ${LIBS_DIRS[@]}
		do
			printf " -L$p "
	#printf " -Wl,-rpath=$p "
		done
	}
	function \
	link_libs()
	{
		for l in ${LIBS[@]}
		do
			printf " -l$l "
		done
	}

	ret=3

	echo "calling compiler"

	CMD="$CXX $CXX_FLAGS `defines` `include_dirs` -Wl,-soname,lib$DLIB_NAME.so.$DLIB_VERSION -o $DLIB_FULL_PATH $THIS_FILE_PATH `libs_dirs` `link_libs`"
	echo CMD:
	echo $CMD
	RUNNABLE_CMD=$(echo $CMD | sed "s/'\"/\"/g" | sed "s/\"'/\"/g")
	echo RUNNABLE_CMD:
	echo $RUNNABLE_CMD

	#echo
	STRIPPED_CMD=$(echo $CMD | sed 's/"/\\"/g')
	#echo
	#echo $STRIPPED_CMD

	JSON="[\n{\n\t\"directory\":\"$THIS_FILE_DIR/$REL_ROOT_PROJ\",\n\t\"command\":\"$STRIPPED_CMD\",\n\t\"file\":\"$THIS_FILE_PATH\"\n},\n]";
	echo -e $JSON > compile_commands.json

	TIME="real %E | user %U | sys %S | max mem %M kB | avg mem %K kB" time $RUNNABLE_CMD

	ret=$?;

	if [ $ret -eq 0 ]
	then
		printf 'compilation succeeded\n'
		ln -s $DLIB_FULL_PATH $DLIB_VERSIONED_FULL_PATH
		ls -l $DLIB_FULL_PATH $DLIB_VERSIONED_FULL_PATH
		file $DLIB_FULL_PATH
		echo -n $DLIB_VERSION > "$DLIBS_DIR_FULL_PATH/lib$DLIB_NAME.version.txt"
		objdump -p $DLIB_FULL_PATH | grep SONAME

		#chmod +x ./$EXECUTABLE_NAME
	else
		printf 'compilation failed\n' >&2;
	fi

	echo "exiting $ret"
	exit $ret
	# BASH_SECTION

	# bash -c "exec <($BASH_TEXT)"
	# ret=$?
	# exit $ret
#endif

#include "basic_types.h"
#include "macro_tools.h"
#include "tghot.h"

TG_DL_EXPORTED
void
hot_boot( tg::hot::Hot_Api * hot_api, tg::hot::Hot_Api_Lib_Data data )
{
	const char * version_str = TOSTRING(LIB_VERSION);
	log_dbg( "hot boot of " LIB_NAME " %s", version_str );
}

TG_DL_EXPORTED
void
hot_outdated()
{
}


TG_DL_EXPORTED void some_function      ()
{
	//log_dbg( "some_function was called from " LIB_NAME TOSTRING( LIB_VERSION ) );
}
TG_DL_EXPORTED s32  some_other_function( s32 some_arg, const char * some_other_arg )
{
	log_dbg( "%d %s", some_arg, some_other_arg );
	return 0;
}
TG_DL_EXPORTED bool another_one        ( void * some_arg, s64 arg ) { return false; }
TG_DL_EXPORTED bool another_one_2      ( void * some_arg, s64 arg ) { return false; }

//__attribute__((__visibility__("default")))
//void(*foo_ptr)() = &foo;

void
__attribute ((constructor))
init_function()
{
	log_dbg( "constructor lib %s version %s", TOSTRING(LIB_NAME), TOSTRING(LIB_VERSION) );
}
void
__attribute ((destructor))
fini_function()
{
	log_dbg( "destructor lib %s version %s", TOSTRING(LIB_NAME), TOSTRING(LIB_VERSION) );
}

#include "utility.cpp"
#include "tgmath.cpp"
