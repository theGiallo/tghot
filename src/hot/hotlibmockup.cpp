#include <initializer_list>
#include <dlfcn.h>

#include "macro_tools.h"
#include "basic_types.h"
#include "tgarray.h"
#include "tgstring.h"
#include "hash_table_generic_vm.h"
#include "hash_table_generic_multi_vm.h"
#include "utility.h"

////////////////////////////////////////////////////////////////////////////////
// NOTE(theGiallo): Hot Api

//__declspec(__dllexport)
#define _TG_DL_EXPORTED_ATTR __attribute__((visibility("default")))

#ifdef  __cplusplus
#define TG_DL_EXPORTED extern "C" _TG_DL_EXPORTED_ATTR
#else
#define TG_DL_EXPORTED _TG_DL_EXPORTED_ATTR
#endif

namespace tg::hot
{
	#ifdef  __cplusplus
	//__declspec(__dllexport)
	#define TG_DL_EXPORTED extern "C" __attribute__((visibility("default")))
	#else
	#define TG_DL_EXPORTED __attribute__((visibility("default")))
	#endif

	using String = tg::String;

	struct
	Hot_Api_Lib_Data
	{
		void * data;
		s64    size;
	};
	struct
	DL
	{
		const char * name;
		tg::Array<const char*> functions_names;
		tg::Array<void*      > functions;

		static DL Make( const char * name, std::initializer_list<const char *> functions_names )
		{
			DL ret;

			ret.name = name;
			ret.functions_names = tg::Array<const char*>::Make( functions_names );
			ret.functions       = tg::Array<void *     >::Make( functions_names.size() );

			return ret;
		}
	};

	void set_lib_data( Hot_Api_Lib_Data data, const char * lib_name );
	bool request_dl( DL & dl, void ( * callback ) ( DL * dl ), const char * registerer_name );

	////////////////////////////////////////////////////////////////////////////////
	// NOTE(theGiallo): Structs

	struct
	Hot_Api
	{
		const char * lib_name;

		bool
		request_dl( DL dl, void ( * callback ) ( DL * dl ) )
		{
			return ::tg::hot::request_dl( dl, callback, lib_name );
		}

		void
		set_lib_data( void * data, s64 size )
		{
			::tg::hot::set_lib_data( Hot_Api_Lib_Data{ data, size }, lib_name );
		}
	};

	struct
	Callbacks
	{
		struct
		CallbackEntry
		{
			DL * dl;
			void ( * callback ) ( DL * dl );
		};


		// NOTE(theGiallo): key: const char * dl_name, value: CallbackEntry *
		Hash_Table_Generic_Multi_VM callbacks_about_dl;

		// NOTE(theGiallo): key: const char * requester_name, value: CallbackEntry *
		Hash_Table_Generic_Multi_VM callbacks_of_requester;

		Pool_Allocator_Generic_VM entries_pool;

		static const s64 MAX_CALLBACKS = 1024;
		static const s64 CALLBACKS_ABOUT_DL_TABLE_SIZE = MAX_CALLBACKS;
		static const s64 CALLBACKS_OF_REQUESTER_TABLE_SIZE = MAX_CALLBACKS;

		static
		Callbacks
		Make()
		{
			Callbacks ret = {};
			ret.init();
			return ret;
		}

		bool
		init()
		{
			bool ret;

			ret = entries_pool.init( sizeof( CallbackEntry ), MAX_CALLBACKS );

			ret = callbacks_about_dl    .init( -1, MAX_CALLBACKS, CALLBACKS_ABOUT_DL_TABLE_SIZE     ); if ( ! ret ) return ret;
			ret = callbacks_of_requester.init( -1, MAX_CALLBACKS, CALLBACKS_OF_REQUESTER_TABLE_SIZE ); if ( ! ret ) return ret;

			return ret;
		}

		void
		register_callback( DL & dl, void ( * callback ) ( DL * dl ), const char * requester_name )
		{
			CallbackEntry * callback_entry = allocate_callback_entry();
			if ( ! callback_entry )
			{
				log_err( "callback entry just allocated is NULL" );
			}
			callback_entry->dl       = &dl;
			callback_entry->callback = callback;
			callbacks_of_requester.insert( (void*)requester_name, (void*)callback_entry );
			callbacks_about_dl    .insert( (void*)dl.name,        (void*)callback_entry );
		}

		void
		get_callbacks_of_requester( const char * requester_name, HTGMVM_Element_List_VM ** out_callbacks_entries_list )
		{
			callbacks_of_requester.get_all( (void*)requester_name, out_callbacks_entries_list );
		}
		void
		get_callbacks_of_requester( const char * requester_name, tg::Array<CallbackEntry*> & out_callbacks )
		{
			HTGMVM_Element_List_VM * values_list = {};
			bool b_res = callbacks_of_requester.get_all( (void*)requester_name, &values_list );
			if ( ! b_res )
			{
				return;
			}

			b_res = out_callbacks.allocate( values_list->length );
			if ( ! b_res )
			{
				ILLEGAL_PATH_M( "failed to allocate memory for %ld callbacks", values_list->length );
			}

			s32 i = 0;
			for ( HTGMVM_Element_List_VM_Node * node = values_list->first;
			      node;
			      node = node->next )
			{
				out_callbacks.array[i] = (CallbackEntry*)node->element.value;
			}
		}

		void
		get_callbacks_about_dl( const char * dl_name, HTGMVM_Element_List_VM ** out_callbacks_entries_list )
		{
			callbacks_about_dl.get_all( (void*)dl_name, out_callbacks_entries_list );
		}

		void
		get_callbacks_about_dl( const char * dl_name, tg::Array<CallbackEntry*> & out_callbacks )
		{
			HTGMVM_Element_List_VM * values_list = {};
			bool b_res = callbacks_about_dl.get_all( (void*)dl_name, &values_list );
			if ( ! b_res )
			{
				return;
			}

			b_res = out_callbacks.allocate( values_list->length );
			if ( ! b_res )
			{
				ILLEGAL_PATH_M( "failed to allocate memory for %ld callbacks", values_list->length );
			}

			s32 i = 0;
			for ( HTGMVM_Element_List_VM_Node * node = values_list->first;
			      node;
			      node = node->next )
			{
				out_callbacks.array[i] = (CallbackEntry*)node->element.value;
			}
		}

		void
		remove_callbacks_of_requester( const char * requester_name )
		{
			HTGMVM_Element_List_VM list = {};
			bool b_res = callbacks_of_requester.remove_and_get_all( (void*)requester_name, & list );
			if ( ! b_res )
			{
				return;
			}

			for ( HTGMVM_Element_List_VM_Node * node = list.first;
			      node;
			      node = node->next )
			{
				CallbackEntry * ce = (CallbackEntry *) node->element.value;
				deallocate_callback_entry( ce );
			}

			list_vm_remove_all( &list );
		}

		CallbackEntry *
		allocate_callback_entry()
		{
			CallbackEntry * ret = (CallbackEntry *)entries_pool.allocate_clean();
			return ret;
		}

		void
		deallocate_callback_entry( CallbackEntry * e )
		{
			entries_pool.deallocate( (void*) e );
		}
	};

	struct
	Lib
	{
		const char * name;
		void * dl_handle;
		Hot_Api_Lib_Data data;

		// NOTE(theGiallo): key: const char * f_name, value: void * function
		Hash_Table_Generic_VM functions_table = {};

		void
		init()
		{
			bool b_res;
			b_res = functions_table.init( -1 );
			if ( ! b_res )
			{
				ILLEGAL_PATH_M( "failed to initialize hash table" );
			}
		}

		void *
		_load_function( const char * fname )
		{
			dlerror();

			void * ret = dlsym( dl_handle, fname );

			const char * error = dlerror();
			if ( error )
			{
				log_err( "error loading function '%s' from library '%s': %s",
				         fname, name, error );
			}

			return ret;
		}

		bool
		get_function( const char * fname, void ** out_fptr )
		{
			bool ret = functions_table.get_one( (void*)fname, out_fptr );
			return ret;
		}

		void
		get_or_load_function( const char * fname, void ** out_fptr )
		{
			bool b_res = get_function( fname, out_fptr );
			if ( ! b_res )
			{
				*out_fptr = _load_function( fname );
				_add_function( fname, *out_fptr );
			}
		}

		void
		reload_function( const char * fname, void ** out_fptr )
		{
			void * new_function = _load_function( fname );
			bool b_res = get_function( fname, out_fptr );
			if ( ! b_res )
			{
				bool b_res = _add_function( fname, *out_fptr );

				if ( ! b_res )
				{
					log_err( "failed to add function to table:'%s' of library '%s'",
					         fname, name );
					*out_fptr = 0;
					ILLEGAL_PATH();
					return;
				}
			}
			*out_fptr = new_function;
		}

		void
		reload_all_functions()
		{
			for_0M( i, functions_table.table_length )
			{
				HTGVM_Element_List_VM list = functions_table.table[i];
				for ( HTGVM_Element_List_VM_Node * node = list.first; node; node = node->next )
				{
					const char * f_name = (const char *)node->element.key;
					void * func;
					reload_function( f_name, &func );
					node->element.value = func;
				}
			}
		}

		void
		fill_dl( DL & dl )
		{
			tg_assert( string_equal( dl.name, name ) );

			for_0M( fi, dl.functions_names.count )
			{
				const char * fname = dl.functions_names.array[fi];
				void * func;
				get_or_load_function( fname, &func );
				dl.functions.array[fi] = func;
			}
		}

		void
		reload_all_and_call_callbacks( Callbacks * callbacks )
		{
			reload_all_functions();

			HTGMVM_Element_List_VM * callbacks_entries_list;
			callbacks->get_callbacks_about_dl( name, &callbacks_entries_list );

			for ( HTGMVM_Element_List_VM_Node * node = callbacks_entries_list->first;
			      node;
			      node = node->next )
			{
				Callbacks::CallbackEntry * c = (Callbacks::CallbackEntry*)node->element.value;
				fill_dl( *c->dl );

				// TODO(theGiallo, 2021-03-03): what if in here they want to modify `callbacks`?
				c->callback( c->dl );
			}
		}

		bool
		is_present( const char * fname )
		{
			bool ret = functions_table.is_present( (void*)fname );
			return ret;
		}

		bool
		_add_function( const char * fname, void * fptr )
		{
			bool ret = functions_table.insert( (void*)fname, fptr );
			return ret;
		}
	};

	struct
	Libraries
	{
		Hash_Table_Generic_VM libraries_table = {};
		Pool_Allocator_Generic_VM lib_pool = {};

		void
		init()
		{
			s64 max_libs = 128 * 1024;
			lib_pool.init( sizeof ( Lib ), max_libs );

			libraries_table.init( -1 );
		}

		static
		Libraries
		Make()
		{
			Libraries ret = {};
			ret.init();
			return ret;
		}

		Lib *
		allocate_lib()
		{
			Lib * ret = (Lib*)lib_pool.allocate_clean();
			return ret;
		}
		void
		deallocate_lib( Lib * lib )
		{
			lib_pool.deallocate( lib );
		}

		bool
		is_lib_present( const char * lib_name )
		{
			bool ret = libraries_table.is_present( (void*)lib_name );
			return ret;
		}

		bool
		add_lib( const char * lib_name )
		{
			bool ret = false;
			Lib * lib = allocate_lib();
			lib->init();
			lib->name = lib_name;
			libraries_table.insert( (void*)lib_name, lib );
			return ret;
		}

		bool
		get_lib( const char * lib_name, Lib ** out_lib )
		{
			bool ret = libraries_table.get_one( (void*)lib_name, (void**)out_lib );
			return ret;
		}

		bool
		get_or_add_lib( const char * lib_name, Lib ** out_lib, bool * is_new )
		{
			bool ret = libraries_table.insert_unique_or_get_one( (void*)lib_name, 0, (void**)out_lib );
			if ( ret )
			{
				*is_new = ! *out_lib;
				if ( is_new )
				{
					*out_lib = allocate_lib();
				}
				ret = *out_lib;
			}
			return ret;
		}

		void
		reload_all_and_call_callbacks( Callbacks * callbacks )
		{
			for_0M( ti, libraries_table.table_length )
			{
				HTGVM_Element_List_VM * list = libraries_table.table + ti;
				for ( HTGVM_Element_List_VM_Node * node = list->first;
				      node;
				      node = node->next )
				{
					((Lib*)node->element.value)->reload_all_and_call_callbacks( callbacks );
				}
			}
		}
	};


	// NOTE(theGiallo): Structs
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// NOTE(theGiallo): Statics
	Libraries libraries = Libraries::Make();
	Callbacks callbacks = Callbacks::Make();
	// NOTE(theGiallo): Statics
	////////////////////////////////////////////////////////////////////////////////


	void
	reload_if_you_want()
	{
		libraries.reload_all_and_call_callbacks( & callbacks );
	}

	void
	set_lib_data( Hot_Api_Lib_Data data, const char * lib_name )
	{
		Lib * lib;
		bool b_res = libraries.get_lib( lib_name, &lib );
		tg_assert_m( b_res, "failed to get lib %s", lib_name );
		lib->data = data;
	}

	void
	register_callback( DL & dl, void ( * callback ) ( DL * dl ), const char * requester_name )
	{
		callbacks.register_callback( dl, callback, requester_name );
	}

	void
	remove_callbacks_by( const char * requester_name )
	{
		callbacks.remove_callbacks_of_requester( requester_name );
	}

	bool
	dl_is_present( DL & dl )
	{
		bool ret = libraries.is_lib_present( dl.name );
		return ret;
	}

	bool
	build_lib_version_path( char * str, s32 buf_len, const char * dl_name )
	{
		bool ret = false;

		s32 i_res = snprintf( str, buf_len, "./lib%s.version.txt", dl_name );
		ret = i_res >= 0;
		if ( ! ret )
		{
			log_err( "error building lib version file path for %s", dl_name );
		}

		return ret;
	}
	bool
	build_lib_path_versioned( char * str, s32 buf_len, const char * dl_name, const char * lib_version )
	{
		bool ret = false;

		s32 i_res = snprintf( str, buf_len, "./lib%s.%s", dl_name, lib_version );
		ret = i_res >= 0;
		if ( ! ret )
		{
			log_err( "error building lib path versioned for %s", dl_name );
		}

		return ret;
	}

	bool
	load_dl( DL & dl )
	{
		bool ret = false;

		#define DLCLOSE_SO( so ) \
		if ( so ) \
		{ \
			i_res = dlclose( so ); \
			if ( i_res ) log_err( "dlclose error: %s", dlerror() ); \
		} \

		s32 i_res;

		char lib_version_path[256] = {};
		bool b_res = build_lib_version_path( &lib_version_path[0], (s32)sizeof ( lib_version_path ), dl.name );
		if ( ! b_res )
		{
			return ret;
		}

		char lib_version[512] = {};
		s64 lib_version_size = read_entire_file( lib_version_path, lib_version, sizeof(lib_version) );
		if ( lib_version_size > (s64)sizeof ( lib_version ) )
		{
			log_err( "failed to read file of lib version of '%s' ('%s') because it's too large. %lu > %lu", dl.name, lib_version_path, lib_version_size, sizeof ( lib_version ) );
			return ret;
		} else
		if ( lib_version_size < 0 )
		{
			log_err( "failed to read file of lib version of '%s' ('%s')", dl.name, lib_version_path );
			perror(0);
			return ret;
		} else
		if ( lib_version_size == 0 )
		{
			log_err( "file of lib version of '%s' is empty ('%s')", dl.name, lib_version_path );
			return ret;
		}

		log_dbg( "lib version for '%s' is '%s'", dl.name, lib_version );

		// TODO(theGiallo): sanitize lib_version (refuse if it has slashes or invalid file name characters)
		for_0M( i, lib_version_size )
		{
			if ( lib_version[i] == '/'
			  || lib_version[i] == '\\' )
			{
				log_err( "library version for '%s' contains an invalid character", dl.name );
				return ret;
			}
		}

		char lib_path_versioned[512] = {};
		b_res = build_lib_path_versioned( lib_path_versioned, sizeof ( lib_path_versioned ), dl.name, lib_version );
		if ( ! b_res )
		{
			return ret;
		}

		log_dbg( "versioned lib path for '%s' is '%s'", dl.name, lib_path_versioned );

		//if ( so ) log_dbg( "so previously loaded" );
		//DLCLOSE_SO();

		void * so = dlopen( lib_path_versioned, RTLD_NOW | RTLD_NOLOAD );

		if ( ! so )
		{
			log_dbg( "library not already loaded" );
			so = dlopen( lib_path_versioned, RTLD_NOW | RTLD_DEEPBIND | RTLD_GLOBAL );
		} else
		{
			log_dbg( "library already loaded" );
			DLCLOSE_SO( so );
			so = dlopen( lib_path_versioned, RTLD_NOW | RTLD_DEEPBIND | RTLD_GLOBAL );
		}

		if ( ! so )
		{
			perror(0);
			log_err( "failed to load %s", lib_path_versioned );
			return ret;
		}

		Lib * lib;
		bool is_new;
		libraries.get_or_add_lib( dl.name, &lib, &is_new );

		if ( ! is_new )
		{
			void ( * hot_outdated ) ();
			lib->get_or_load_function( "hot_outdated", (void**)&hot_outdated );
			if ( hot_outdated )
			{
				hot_outdated();
			} else
			{
				log_err( "failed to get hot_outdated from lib %s", dl.name );
			}

			// TODO(theGiallo, 2021-03-09): we could make it optional to close old versions
			//void * old_so = lib->dl_handle;
			//DLCLOSE_SO( old_so );
		}

		lib->dl_handle = so;

		remove_callbacks_by( dl.name );

		// NOTE(theGiallo, 2021-02-19): if we need to keep string literals we can't close
		// DLCLOSE_SO( old_so );

		void ( * hot_boot )( Hot_Api * hot_api, Hot_Api_Lib_Data data );
		lib->reload_function( "hot_boot", (void**)&hot_boot );

		{
			void ( * hot_outdated )();
			lib->reload_function( "hot_outdated", (void**)&hot_outdated );
		}

		Hot_Api_Lib_Data data = lib->data;
		Hot_Api hot_api = Hot_Api{ .lib_name = dl.name };
		hot_boot( &hot_api, data );

		for_0M( i, dl.functions_names.count )
		{
			const char * fname = dl.functions_names.array[i];
			lib->reload_function( fname, dl.functions.array + i );
		}

		ret = true;

		return ret;
	}

	bool
	fill_functions( DL & dl )
	{
		Lib * lib;
		bool ret = libraries.get_lib( dl.name, &lib );
		if ( ! ret )
		{
			return ret;
		}

		lib->fill_dl( dl );

		return ret;
	}

	bool
	request_dl( DL & dl, void ( * callback ) ( DL * dl ), const char * registerer_name )
	{
		bool ret = false;

		register_callback( dl, callback, registerer_name );

		if ( ! dl_is_present( dl ) )
		{
			bool b_res = load_dl( dl );
			if ( ! b_res )
			{
				log_err( "failed to load dl %s", dl.name );
				return ret;
			}
		}
		tg_assert( dl_is_present( dl ) );

		fill_functions( dl );
		callback( &dl );

		ret = true;
		return ret;
	}
}
//
////////////////////////////////////////////////////////////////////////////////

using namespace tg;
using namespace tg::hot;

typedef String Compute_The_String( s32 argc, char ** argv );

struct
Global
{
} glob;

// NOTE(theGiallo): externally linked
Compute_The_String * compute_the_string;

struct
Some_Library
{
	DL dl;

	void (*some_function      )( s32 some_arg );
	s32  (*some_other_function)( s32 some_arg, const char * some_other_arg );
	bool (*another_one        )( void * some_arg );

} some_library = { .dl = DL::Make( "somelibrary", { "some_function", "some_other_function", "another_one" } ) };

namespace
Some_Other_Library
{
	DL dl = DL::Make( "someotherlibrary", { "some_function", "some_other_function", "another_one", "another_one_2" } );

	const char * (*some_function      )();
	void         (*some_other_function)( s32 some_arg, const char * some_other_arg );
	bool         (*another_one        )( void * some_arg, s64 arg );
	bool         (*another_one_2      )( void * some_arg, s64 arg );

};

void
dl_load_callback_some_library( DL * dl )
{
	*(void**)some_library.some_function       = dl->functions.array[0];
	*(void**)some_library.some_other_function = dl->functions.array[1];
	*(void**)some_library.another_one         = dl->functions.array[2];
}
void
dl_load_callback_some_other_library( DL * dl )
{
	*(void**)Some_Other_Library::some_function       = dl->functions.array[0];
	*(void**)Some_Other_Library::some_other_function = dl->functions.array[1];
	*(void**)Some_Other_Library::another_one         = dl->functions.array[2];
	*(void**)Some_Other_Library::another_one_2       = dl->functions.array[3];

	// NOTE(theGiallo): maybe we need to call something here, to do some bookkeeping
}

// NOTE(theGiallo): called after the library is loaded
TG_DL_EXPORTED
void
hot_boot( Hot_Api * hot_api, Hot_Api_Lib_Data data )
{
	if ( ! data.data )
	{
		s64 min_size = MIN( data.size, sizeof ( glob ) );
		memcpy( &glob, data.data, min_size );
	}

	hot_api->set_lib_data( & glob, sizeof ( glob ) );

	hot_api->request_dl( some_library.dl, dl_load_callback_some_library );
	hot_api->request_dl( Some_Other_Library::dl, dl_load_callback_some_other_library );
}

TG_DL_EXPORTED
void
hot_outdated()
{
}

////////////////////////////////////////////////////////////////////////////////
// NOTE(theGiallo): example
s32
main( s32 argc, char * argv[] )
{
	struct _
	{
		static
		void
		some_library_callback( DL * dl )
		{
			log_dbg( "some_library_callback( %s )", dl->name );
		}
	};

	bool b_res = tg::hot::request_dl( some_library.dl, _::some_library_callback, "main" );
	if ( b_res )
	{
		log_err( "request_dl failed" );
		return 1;
	}

	return 0;
}
// NOTE(theGiallo): example
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// NOTE(theGiallo): example
// || means it's on another thread
#if ______
Main requests libA.{ funA0, funA1, funA2 }
	libA is loaded with dl_open
	A.hot_boot is called
		A requests libB.{ funB0, funB1 }
			libB is loaded with dl_open
			B.hot_boot is called
				B requests libA.{ funA2, funA3 } // what do we do here? libA boot is amidst, can B call libA functions?
					libA is already loaded at the latest version
				B callback is called for libA.{ funA2, funA3 }
				B sets its lib data
		A callback is called for libB.{ funB0, funB1 }
		A sets its lib data
Main callback is called for libA.{ funA0, funA1, funA2 }
Main calls libA.funA1
Main calla hot.reload_if_you_want()
Main calls libA.funA1
|| a change is detected in the versioning for libA
Main calls hot.reload_if_you_want()
|| libA is loaded with dl_open and marked for reloadable
Main calls libA.funA1
Main calls hot.reload_if_you_want()
	libA.hot_boot is called
		A copies its data
		A sets its data
		A requests libB.{ funB0, funB1 }
			libB is already loaded
		A callback is called for libB.{ funB0, funB1 }
	Main callback is called for libA // NOTE(theGiallo): do we call the callbacks FIFO or LIFO?
	libB callback is called for libA
Main calls libA.funA1
#endif
////////////////////////////////////////////////////////////////////////////////


#include "pool_allocator_generic_vm.cpp"
#include "hash_table_generic_vm.cpp"
#include "hash_table_generic_multi_vm.cpp"
#include "utility.cpp"
#include "system.cpp"
#include "tgmath.cpp"
