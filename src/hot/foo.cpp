#if 0
	APP_NAME='"GEN_GEN_AST"'

	REL_ROOT_PROJ=../

	THIS_FILE_PATH=$0
	THIS_FILE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	#THIS_FILE_DIR=${THIS_FILE_PATH%/*}
	THIS_FILE=${THIS_FILE_PATH##*/}
	LIB_NAME=${THIS_FILE%%.*}
	LIB_FILE_NAME=lib${THIS_FILE%%.*}.so
	if [ -z "${LIB_VERSION+x}" ]
	then
		LIB_VERSION=$(date '+%Y.%m.%d.%H.%M.%S');
	fi

	#echo this file dir = "'$THIS_FILE_DIR'"
	#echo this file path = "'$THIS_FILE_PATH'"
	#echo this file = "'$THIS_FILE'"
	#echo executable name = "'$EXECUTABLE_NAME'"

	if [ -z "${CXX+x}" ]
	then
		#CXX=arm-linux-gnueabihf-g++
		CXX=clang++
		#CXX=g++
	fi

	CXX_FLAGS="-std=c++17 \
	           -Wall \
	           -Wextra \
	           -Wno-missing-braces \
	           -Wno-unused-function \
	           -Wno-comment \
	           -Rpass-missed=loop-vectorize -Rpass-analysis=loop-vectorize \
	           -fsave-optimization-record \
	           -Wl,-export-dynamic -rdynamic -fdeclspec -fPIC -shared -fno-exceptions \
	           -O0 -ggdb3 \
	           -march=native \
	           -DAPP_NAME='$APP_NAME' \
	           -DLIB_NAME='\"$LIB_NAME\"' \
	           -DLIB_VERSION='$LIB_VERSION' \
	           $CXX_FLAGS"

	#          -O3 \
	#          -O0 -ggdb3 \
	# -fwhole-program # ensures this CU represents the whole program, enabling more aggressive opts. Don't use if -flto is present
	# -flto=auto # link time optimization ( removes unused functions ) (g++ only)
	# -ffunction-sections -Wl,--gc-sections # remove unused functions (this is also achieved with -flto), separating code from data
	# -fpermissive # downgrades some errors to warnings for non-conformant code
	#-fPIC # position independent code, useful for linked libraries

	declare -a DEFINES
	DEFINES[${#DEFINES[@]}]=DEBUG=1

	declare -a INCLUDE_DIRS
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"libs"

	declare -a LIBS_DIRS
	#LIBS_DIRS[${#LIBS_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"libraries/"

	declare -a LIBS
	LIBS[${#LIBS[@]}]=pthread
	LIBS[${#LIBS[@]}]=dl
	#LIBS[${#LIBS[@]}]=rt

	function \
	defines()
	{
		for d in ${DEFINES[@]}
		do
			printf " -D$d "
		done
	}
	function \
	include_dirs()
	{
		for p in ${INCLUDE_DIRS[@]}
		do
			printf " -I$p "
		done
	}
	function \
	libs_dirs()
	{
		for p in ${LIBS_DIRS[@]}
		do
			printf " -L$p "
	#printf " -Wl,-rpath=$p "
		done
	}
	function \
	link_libs()
	{
		for l in ${LIBS[@]}
		do
			printf " -l$l "
		done
	}

	ret=3

	echo "calling compiler"

	CMD="$CXX $CXX_FLAGS `defines` `include_dirs` -Wl,-soname,lib$LIB_NAME.so.$LIB_VERSION -o $LIB_FILE_NAME $THIS_FILE_PATH `libs_dirs` `link_libs`"
	echo CMD:
	echo $CMD
	RUNNABLE_CMD=$(echo $CMD | sed "s/'\"/\"/g" | sed "s/\"'/\"/g")
	echo RUNNABLE_CMD:
	echo $RUNNABLE_CMD

	#echo
	STRIPPED_CMD=$(echo $CMD | sed 's/"/\\"/g')
	#echo
	#echo $STRIPPED_CMD

	JSON="[\n{\n\t\"directory\":\"$THIS_FILE_DIR/$REL_ROOT_PROJ\",\n\t\"command\":\"$STRIPPED_CMD\",\n\t\"file\":\"$THIS_FILE_PATH\"\n},\n]";
	echo -e $JSON > compile_commands.json

	TIME="real %E | user %U | sys %S | max mem %M kB | avg mem %K kB" time $RUNNABLE_CMD

	ret=$?;

	if [ $ret -eq 0 ]
	then
		printf 'compilation succeeded\n'
		LIB_FILE_NAME_VERSIONED=$LIB_FILE_NAME.$LIB_VERSION
		ln -s $LIB_FILE_NAME $LIB_FILE_NAME_VERSIONED
		ls -l $LIB_FILE_NAME $LIB_FILE_NAME_VERSIONED
		file $LIB_FILE_NAME
		echo -n $LIB_VERSION > ./lib$LIB_NAME.version.txt
		objdump -p $LIB_FILE_NAME | grep SONAME

		#chmod +x ./$EXECUTABLE_NAME
	else
		printf 'compilation failed\n' >&2;
	fi

	echo "exiting $ret"
	exit $ret
#endif

#include "basic_types.h"
#include "macro_tools.h"

#ifdef  __cplusplus
extern "C"
#endif
//__declspec(__dllexport)
__attribute__((visibility("default")))
void
foo()
{
	const char * version_str = TOSTRING(LIB_VERSION);
	log_dbg( "foo %s", version_str );
}

__attribute__((__visibility__("default")))
void(*foo_ptr)() = &foo;

void
__attribute ((constructor))
init_function()
{
	log_dbg( "constructor lib %s version %s", TOSTRING(LIB_NAME), TOSTRING(LIB_VERSION) );
}
void
__attribute ((destructor))
fini_function()
{
	log_dbg( "destructor lib %s version %s", TOSTRING(LIB_NAME), TOSTRING(LIB_VERSION) );
}
