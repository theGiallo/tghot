#if 0
echo $#
echo $0 $1 $2

#echo fullpath should be $(readlink -e $0)
#export THIS_FILE_PATH=$0
export THIS_FILE_PATH=$(readlink -e $0)
#export THIS_FILE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export THIS_FILE_DIR=$(dirname $THIS_FILE_PATH)
export THIS_FILE_NAME=${THIS_FILE_PATH##*/}
export THIS_FILE_BASE_NAME=${THIS_FILE_NAME%%.*}

exec /bin/bash -c 'exec "python3" <(tail -n +15 "$0" | sed "/#endif/q") "$@"' "$0" "$@"
exit $!
import os
import sys

print( 'Hello from python' )
print( 'This file path:      ' + os.environ['THIS_FILE_PATH'] )
print( 'This file dir:       ' + os.environ['THIS_FILE_DIR'] )
print( 'This file name:      ' + os.environ['THIS_FILE_NAME'] )
print( 'This file base name: ' + os.environ['THIS_FILE_BASE_NAME'] )
print( 'Number of arguments:', len(sys.argv), 'arguments.' )
print( 'Argument List:', str(sys.argv) )

exit( 123 )

#endif

#include "basic_types.h"
#include "macro_tools.h"
#include "tghot.h"

TG_DL_EXPORTED
void
hot_boot( tg::hot::Hot_Api * hot_api, tg::hot::Hot_Api_Lib_Data data )
{
	(void)hot_api;
	(void)data;
	const char * version_str = TOSTRING(LIB_VERSION);
	log_dbg( "hot boot of " LIB_NAME " %s", version_str );
}

TG_DL_EXPORTED
void
hot_outdated()
{
}


TG_DL_EXPORTED void some_function      ( s32 some_arg )
{
	(void)some_arg;
	//log_dbg( "some_function %d hello world", some_arg );
}
TG_DL_EXPORTED s32  some_other_function( s32 some_arg, const char * some_other_arg ) { (void)some_arg; (void)some_other_arg; return 0; }
TG_DL_EXPORTED bool another_one        ( void * some_arg ) { (void)some_arg; return false; }

//__attribute__((__visibility__("default")))
//void(*foo_ptr)() = &foo;

void
__attribute ((constructor))
init_function()
{
	log_dbg( "constructor lib %s version %s", TOSTRING(LIB_NAME), TOSTRING(LIB_VERSION) );
}
void
__attribute ((destructor))
fini_function()
{
	log_dbg( "destructor lib %s version %s", TOSTRING(LIB_NAME), TOSTRING(LIB_VERSION) );
}

//#include "tghot.cpp"
#include "utility.cpp"
#include "tgmath.cpp"
