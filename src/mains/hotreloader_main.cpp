#if 0
	APP_NAME='"GEN_GEN_AST"'

	REL_ROOT_PROJ=../

	THIS_FILE_PATH=$0
	THIS_FILE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	#THIS_FILE_DIR=${THIS_FILE_PATH%/*}
	THIS_FILE=${THIS_FILE_PATH##*/}
	EXECUTABLE_NAME=${THIS_FILE%%.*}

	#echo this file dir = "'$THIS_FILE_DIR'"
	#echo this file path = "'$THIS_FILE_PATH'"
	#echo this file = "'$THIS_FILE'"
	#echo executable name = "'$EXECUTABLE_NAME'"

	if [ -z "${CXX+x}" ]
	then
		#CXX=arm-linux-gnueabihf-g++
		CXX=clang++
		#CXX=g++
	fi

	CXX_FLAGS="-std=c++17 \
	           -Wall \
	           -Wextra \
	           -Wno-missing-braces \
	           -Wno-unused-function \
	           -Wno-comment \
	           -ffunction-sections \
	           -Wl,--gc-sections  \
	           -Rpass-missed=loop-vectorize -Rpass-analysis=loop-vectorize \
	           -fsave-optimization-record \
	           -Wl,-export-dynamic -rdynamic \
	           -O0 -ggdb3 \
	           -march=native \
	           -DAPP_NAME='$APP_NAME' \
	           -DEXECUTABLE_NAME='\"$EXECUTABLE_NAME\"' \
	           $CXX_FLAGS"

	#          -O3 \
	#          -O0 -ggdb3 \
	# -fwhole-program # ensures this CU represents the whole program, enabling more aggressive opts. Don't use if -flto is present
	# -flto=auto # link time optimization ( removes unused functions ) (g++ only)
	# -ffunction-sections -Wl,--gc-sections # remove unused functions (this is also achieved with -flto), separating code from data
	# -fpermissive # downgrades some errors to warnings for non-conformant code
	#-fPIC # position independent code, useful for linked libraries

	declare -a DEFINES
	DEFINES[${#DEFINES[@]}]=DEBUG=1

	declare -a INCLUDE_DIRS
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR/../
	INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"libs"

	declare -a LIBS_DIRS
	LIBS_DIRS[${#LIBS_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"src/"
	#LIBS_DIRS[${#LIBS_DIRS[@]}]=$THIS_FILE_DIR/$REL_ROOT_PROJ"libraries/"

	declare -a LIBS
	LIBS[${#LIBS[@]}]=pthread
	LIBS[${#LIBS[@]}]=dl
	#LIBS[${#LIBS[@]}]=foo
	#LIBS[${#LIBS[@]}]=rt

	function \
	defines()
	{
		for d in ${DEFINES[@]}
		do
			printf " -D$d "
		done
	}
	function \
	include_dirs()
	{
		for p in ${INCLUDE_DIRS[@]}
		do
			printf " -I$p "
		done
	}
	function \
	libs_dirs()
	{
		for p in ${LIBS_DIRS[@]}
		do
			printf " -L$p "
	#printf " -Wl,-rpath=$p "
		done
	}
	function \
	link_libs()
	{
		for l in ${LIBS[@]}
		do
			printf " -l$l "
		done
	}

	ret=3

	echo "calling compiler"

	CMD="$CXX $CXX_FLAGS `defines` `include_dirs` -o $EXECUTABLE_NAME $THIS_FILE_PATH    `libs_dirs` `link_libs` -Wl,--disable-new-dtags -Wl,-z,origin libfoo.so -Wl,-rpath,./"
	echo CMD:
	echo $CMD
	RUNNABLE_CMD=$(echo $CMD | sed "s/'\"/\"/g" | sed "s/\"'/\"/g")
	echo RUNNABLE_CMD:
	echo $RUNNABLE_CMD

	#echo
	STRIPPED_CMD=$(echo $CMD | sed 's/"/\\"/g')
	#echo
	#echo $STRIPPED_CMD

	JSON="[\n{\n\t\"directory\":\"$THIS_FILE_DIR/$REL_ROOT_PROJ\",\n\t\"command\":\"$STRIPPED_CMD\",\n\t\"file\":\"$THIS_FILE_PATH\"\n},\n]";
	echo -e $JSON > compile_commands.json

	TIME="real %E | user %U | sys %S | max mem %M kB | avg mem %K kB" time $RUNNABLE_CMD

	ret=$?;

	if [ $ret -eq 0 ]
	then
		printf 'compilation succeeded\n'
		file $EXECUTABLE_NAME
		ls -l $EXECUTABLE_NAME

		echo "running ./$EXECUTABLE_NAME $@"
		echo
		chmod +x ./$EXECUTABLE_NAME
		#./$EXECUTABLE_NAME "$@"
		gdb --args ./$EXECUTABLE_NAME "$@"
		#gdb -ex run ./$EXECUTABLE_NAME --args "$@"

		ret=$?

		if [ ! $ret -eq 0 ]
		then
			printf "run failed\n"
		fi
		#trash "$EXECUTABLE_NAME"
		#trash "$EXECUTABLE_NAME.o"
	else
		printf 'compilation failed\n' >&2;
	fi

	echo "exiting $ret"
	exit $ret
#endif

#include <errno.h> // perror()
#include <stdlib.h>
#include <math.h>
#include <cstdio>

#include <dlfcn.h>
#include <link.h>

#include "basic_types.h"
#include "macro_tools.h"
#include "utility.h"
#include "utf8.h"
#include "system.h"
#include "tg_template_containers.h"
#include "stack_generic_vm.h"
#include "pool_allocator_generic.h"
#include "pool_allocator_generic_vm.h"
#include "freelist_allocator.h"
#include "hash_table_generic_multi_vm.h"
#include "hash_table_generic_vm.h"
#include "tgmath.h"
#include "mx_template.h"

#include "argparse.h"
#include "lodepng/lodepng.h"

#include "tgstring.h"

using namespace tg;

typedef void VoidFunction();
//#define GAME_API_CYCLE_BEGINNING( name ) void name( System_API * sys_api, Game_Memory * game_mem )
//typedef GAME_API_CYCLE_BEGINNING( Game_API_Cycle_Beginning );
//extern "C" GAME_API_CYCLE_BEGINNING( game_api_cycle_beginning );

extern "C" void foo();
extern "C" void (*foo_ptr)();

void
test_link();

int
main( int argc, const char *argv[] )
{
	(void)( argc );
	(void)( argv );

	static const char *const usage[] = {
	    EXECUTABLE_NAME " [options] [[--] args]",
	    NULL,
	};


	// NOTE(theGiallo): CLI parameters
	//

	argparse_option options[] = {
		OPT_HELP(),
		//OPT_STRING('n', "native", &native_file_path, "file path to load native functions from", NULL, NULL, NULL),
		//OPT_STRING('o', "output", &output_file_path, "file path to save the generated program into, default '" GEN_GEN_DEFAULT_OUTPUT_FILE "'", NULL, NULL, NULL),
		//OPT_INTEGER('c', "count", &iterations_count, "iterations count. Each iteration an add operation is performed on the ast.", NULL, OPT_NONEG ),
		//OPT_STRING('w', "weights", &load_from_file_path, "file path to load weights from", NULL, NULL, NULL),
		//OPT_INTEGER('L', "layer", &target_layer, "target layer index", NULL, NULL, OPT_NONEG ),
		//OPT_FLOAT('o', "ovalue", &other_nodes_values, "other nodes value", NULL, NULL, NULL),
		//OPT_BIT('W', "viz-weights", &viz_weights, "generate an image for each W vector", NULL, 1, NULL ),
		OPT_END(),
	};

	struct argparse argparse;
	argparse_init( &argparse, options, usage, 0 );
	argparse_describe( &argparse, "\nno description", "\n" );
	argc = argparse_parse( &argparse, argc, argv );

	u32 srand_seed = u32(ns_wall_time());

	log_info( "srand %x", srand_seed );
	srand( srand_seed );

	int ret = 1;

	bool b_res;

	test_link();


	const char * lib_version_path = "./libfoo.version.txt";

	FILE * lib_version_fp = fopen( lib_version_path, "r" );
	if ( ! lib_version_fp )
	{
		log_err( "failed to load %s", lib_version_path );
		return 1;
	}

	const char * lib_path = "./libfoo.so";
	void * so = 0;
	int c;
	int dlret = 0;
	String lib_path_versioned = {};
	VoidFunction * function = 0;
	VoidFunction ** function_ptr = 0;
	while ( ( c = getchar() ) != 'x' && c != 'q' )
	{
		#if 0
		#define DLCLOSE_SO() \
		if ( so ) \
		{ \
			do { dlret = dlclose( so ); \
			if ( dlret ) log_err( "dlclose error: %s", dlerror() ); \
			} while ( ! dlret ); \
		} \

		#else
		#define DLCLOSE_SO() \
		if ( so ) \
		{ \
			dlret = dlclose( so ); \
			if ( dlret ) log_err( "dlclose error: %s", dlerror() ); \
		} \

		#endif

		char lib_version[1024] = {};
		s64 lib_version_size = read_entire_file( lib_version_path, lib_version, sizeof(lib_version) );
		lib_path_versioned.clear();
		lib_path_versioned.add( "%s.%s", lib_path, lib_version );

		if ( so ) log_dbg( "so previously loaded" );
		DLCLOSE_SO();

		so = dlopen( lib_path_versioned.str, RTLD_NOW | RTLD_NOLOAD );

		if ( ! so )
		{
			log_dbg( "library not already loaded" );
			so = dlopen( lib_path_versioned.str, RTLD_NOW | RTLD_DEEPBIND | RTLD_GLOBAL );
		} else
		{
			log_dbg( "library already loaded" );
			DLCLOSE_SO();
			so = dlopen( lib_path_versioned.str, RTLD_NOW | RTLD_DEEPBIND | RTLD_GLOBAL );
		}

		if ( ! so )
		{
			perror(0);
			log_err( "failed to load %s", lib_path_versioned.str );
		}


		//if ( ! function )
		{
			log_dbg( "loading foo" );
			*(void**)(&function) = dlsym( so, "foo" );
		}

		//if ( ! function_ptr )
		{
			log_dbg( "loading foo_ptr" );
			*(void**)(&function_ptr) = dlsym( so, "foo_ptr" );
		}

		if ( function )
		{
			log_dbg( "manually loaded function:" );
			function();
		} else
		{
			log_err( "failed to load void function: %s", dlerror() );
			perror(0);
		}

		log_dbg( "automatically linked foo():" );
		foo();
		log_dbg( "automatically linked foo_ptr:" );
		foo_ptr();
		if ( function_ptr )
		{
			log_dbg( "manually linked foo_ptr:" );
			(*function_ptr)();
		} else
		{
			log_err( "failed to load void function ptr: %s", dlerror() );
			perror(0);
		}
	}


	ret = 0;

	return ret;
}

void
test_link()
{
#if ___DOC

	typedef u64 ElfW(Addr);
	typedef u16 ElfW(Half);
	typedef u64 ElfW(Xword);
	typedef	s64 ElfW(Sxword);
	typedef u64 ElfW(Off);
	typedef u16 ElfW(Section);
	typedef u16 ElfW(Versym);


	struct
	dl_phdr_info
	{
		ElfW(Addr)         dlpi_addr;  // Base address of object
		const char       * dlpi_name;  // (Null-terminated) name of object
		const ElfW(Phdr) * dlpi_phdr;  // Pointer to array of ELF program headers for this object
		ElfW(Half)         dlpi_phnum; // # of items in dlpi_phdr
	};

	typedef struct
	{
		Elf64_Word	p_type;   // Segment type
		Elf64_Word	p_flags;  // Segment flags
		Elf64_Off	p_offset; // Segment file offset
		Elf64_Addr	p_vaddr;  // Segment virtual address
		Elf64_Addr	p_paddr;  // Segment physical address
		Elf64_Xword	p_filesz; // Segment size in file
		Elf64_Xword	p_memsz;  // Segment size in memory
		Elf64_Xword	p_align;  // Segment alignment
	} Elf64_Phdr;

	typedef struct
	{
		Elf64_Word	sh_name;      // Section name (string tbl index)
		Elf64_Word	sh_type;      // Section type
		Elf64_Xword	sh_flags;     // Section flags
		Elf64_Addr	sh_addr;      // Section virtual addr at execution
		Elf64_Off	sh_offset;    // Section file offset
		Elf64_Xword	sh_size;      // Section size in bytes
		Elf64_Word	sh_link;      // Link to another section
		Elf64_Word	sh_info;      // Additional section information
		Elf64_Xword	sh_addralign; // Section alignment
		Elf64_Xword	sh_entsize;   // Entry size if section holds table
	} Elf64_Shdr;

	typedef struct
	{
		Elf64_Word n_namesz; // Length of the note's name.
		Elf64_Word n_descsz; // Length of the note's descriptor.
		Elf64_Word n_type;   // Type of the note.
	} Elf64_Nhdr;

	typedef struct
	{
		Elf64_Sxword	d_tag;			/* Dynamic entry type */
		union
		{
			Elf64_Xword d_val; // Integer value
			Elf64_Addr d_ptr;  // Address value
		} d_un;
	} Elf64_Dyn;

	/* Legal values for d_tag (dynamic entry type).  */

	#define DT_NULL		0		/* Marks end of dynamic section */
	#define DT_NEEDED	1		/* Name of needed library */
	#define DT_PLTRELSZ	2		/* Size in bytes of PLT relocs */
	#define DT_PLTGOT	3		/* Processor defined value */
	#define DT_HASH		4		/* Address of symbol hash table */
	#define DT_STRTAB	5		/* Address of string table */
	#define DT_SYMTAB	6		/* Address of symbol table */
	#define DT_RELA		7		/* Address of Rela relocs */
	#define DT_RELASZ	8		/* Total size of Rela relocs */
	#define DT_RELAENT	9		/* Size of one Rela reloc */
	#define DT_STRSZ	10		/* Size of string table */
	#define DT_SYMENT	11		/* Size of one symbol table entry */
	#define DT_INIT		12		/* Address of init function */
	#define DT_FINI		13		/* Address of termination function */
	#define DT_SONAME	14		/* Name of shared object */
	#define DT_RPATH	15		/* Library search path (deprecated) */
	#define DT_SYMBOLIC	16		/* Start symbol search here */
	#define DT_REL		17		/* Address of Rel relocs */
	#define DT_RELSZ	18		/* Total size of Rel relocs */
	#define DT_RELENT	19		/* Size of one Rel reloc */
	#define DT_PLTREL	20		/* Type of reloc in PLT */
	#define DT_DEBUG	21		/* For debugging; unspecified */
	#define DT_TEXTREL	22		/* Reloc might modify .text */
	#define DT_JMPREL	23		/* Address of PLT relocs */
	#define	DT_BIND_NOW	24		/* Process relocations of object */
	#define	DT_INIT_ARRAY	25		/* Array with addresses of init fct */
	#define	DT_FINI_ARRAY	26		/* Array with addresses of fini fct */
	#define	DT_INIT_ARRAYSZ	27		/* Size in bytes of DT_INIT_ARRAY */
	#define	DT_FINI_ARRAYSZ	28		/* Size in bytes of DT_FINI_ARRAY */
	#define DT_RUNPATH	29		/* Library search path */
	#define DT_FLAGS	30		/* Flags for the object being loaded */
	#define DT_ENCODING	32		/* Start of encoded range */
	#define DT_PREINIT_ARRAY 32		/* Array with addresses of preinit fct*/
	#define DT_PREINIT_ARRAYSZ 33		/* size in bytes of DT_PREINIT_ARRAY */
	#define DT_SYMTAB_SHNDX	34		/* Address of SYMTAB_SHNDX section */
	#define	DT_NUM		35		/* Number used */
	#define DT_LOOS		0x6000000d	/* Start of OS-specific */
	#define DT_HIOS		0x6ffff000	/* End of OS-specific */
	#define DT_LOPROC	0x70000000	/* Start of processor-specific */
	#define DT_HIPROC	0x7fffffff	/* End of processor-specific */
	#define	DT_PROCNUM	DT_MIPS_NUM	/* Most used by any processor */

	/* DT_* entries which fall between DT_VALRNGHI & DT_VALRNGLO use the
	   Dyn.d_un.d_val field of the Elf*_Dyn structure.  This follows Sun's
	   approach.  */
	#define DT_VALRNGLO	0x6ffffd00
	#define DT_GNU_PRELINKED 0x6ffffdf5	/* Prelinking timestamp */
	#define DT_GNU_CONFLICTSZ 0x6ffffdf6	/* Size of conflict section */
	#define DT_GNU_LIBLISTSZ 0x6ffffdf7	/* Size of library list */
	#define DT_CHECKSUM	0x6ffffdf8
	#define DT_PLTPADSZ	0x6ffffdf9
	#define DT_MOVEENT	0x6ffffdfa
	#define DT_MOVESZ	0x6ffffdfb
	#define DT_FEATURE_1	0x6ffffdfc	/* Feature selection (DTF_*).  */
	#define DT_POSFLAG_1	0x6ffffdfd	/* Flags for DT_* entries, effecting
						   the following DT_* entry.  */
	#define DT_SYMINSZ	0x6ffffdfe	/* Size of syminfo table (in bytes) */
	#define DT_SYMINENT	0x6ffffdff	/* Entry size of syminfo */
	#define DT_VALRNGHI	0x6ffffdff
	#define DT_VALTAGIDX(tag)	(DT_VALRNGHI - (tag))	/* Reverse order! */
	#define DT_VALNUM 12

	/* DT_* entries which fall between DT_ADDRRNGHI & DT_ADDRRNGLO use the
	   Dyn.d_un.d_ptr field of the Elf*_Dyn structure.

	   If any adjustment is made to the ELF object after it has been
	   built these entries will need to be adjusted.  */
	#define DT_ADDRRNGLO	0x6ffffe00
	#define DT_GNU_HASH	0x6ffffef5	/* GNU-style hash table.  */
	#define DT_TLSDESC_PLT	0x6ffffef6
	#define DT_TLSDESC_GOT	0x6ffffef7
	#define DT_GNU_CONFLICT	0x6ffffef8	/* Start of conflict section */
	#define DT_GNU_LIBLIST	0x6ffffef9	/* Library list */
	#define DT_CONFIG	0x6ffffefa	/* Configuration information.  */
	#define DT_DEPAUDIT	0x6ffffefb	/* Dependency auditing.  */
	#define DT_AUDIT	0x6ffffefc	/* Object auditing.  */
	#define	DT_PLTPAD	0x6ffffefd	/* PLT padding.  */
	#define	DT_MOVETAB	0x6ffffefe	/* Move table.  */
	#define DT_SYMINFO	0x6ffffeff	/* Syminfo table.  */
	#define DT_ADDRRNGHI	0x6ffffeff
	#define DT_ADDRTAGIDX(tag)	(DT_ADDRRNGHI - (tag))	/* Reverse order! */
	#define DT_ADDRNUM 11


#endif // ___DOC
	struct _
	{
		static
		s32
		callback( dl_phdr_info * info, size_t size, void * data )
		{
			s32 ret = 0;
			log_dbg( "info->dlpi_addr : %lx", info->dlpi_addr  );
			log_dbg( "info->dlpi_name : %s" , info->dlpi_name  );
			log_dbg( "info->dlpi_phnum: %u" , info->dlpi_phnum );
			s32 phnum = info->dlpi_phnum;
			if ( phnum == 0xffff )
			{
				log_err( "phnum == 0xffff, should we do something?" );
			}
			for ( s32 i = 0, phnum = info->dlpi_phnum ; i != phnum; ++i )
			{
				const ElfW(Phdr) * phdr = info->dlpi_phdr + i;
				log_dbg( "info->dlpi_phdr[%2d].p_type   : %s (%u)" , i, p_type_to_string( phdr->p_type ), phdr->p_type   );
				if ( phdr->p_type == PT_INTERP )
				{
					log_dbg( "interpreter: %s", (const char *)phdr->p_vaddr );
				}
				log_dbg( "info->dlpi_phdr[%2d].p_flags  : %u" , i, phdr->p_flags  );
				if ( phdr->p_flags & PF_X )
				{
					log_dbg( "Executable" );
				}
				if ( phdr->p_flags & PF_W )
				{
					log_dbg( "Writable" );
				}
				if ( phdr->p_flags & PF_R )
				{
					log_dbg( "Readable" );
				}
				log_dbg( "info->dlpi_phdr[%2d].p_offset : %lu", i, phdr->p_offset );
				log_dbg( "info->dlpi_phdr[%2d].p_vaddr  : %lx", i, phdr->p_vaddr  );
				log_dbg( "info->dlpi_phdr[%2d].p_paddr  : %lx", i, phdr->p_paddr  );
				log_dbg( "info->dlpi_phdr[%2d].p_filesz : %lu", i, phdr->p_filesz );
				log_dbg( "info->dlpi_phdr[%2d].p_memsz  : %lu", i, phdr->p_memsz  );
				log_dbg( "info->dlpi_phdr[%2d].p_align  : %lu", i, phdr->p_align  );

				if ( phdr->p_type == PT_DYNAMIC )
				{
					ElfW(Dyn) * dyn = (ElfW(Dyn)*)( info->dlpi_addr + phdr->p_vaddr );

					if ( dyn )
					{
						log_dbg( "sizeof ElfW(Dyn): %lu", sizeof( ElfW(Dyn) ) );
						log_dbg( "dyn type: %s", dyn_type_to_string( dyn->d_tag ) );
					} else
					{
						log_err( "dyn vaddr is null" );
					}
				}
			}
			log_dbg( "" );
			return ret;
		}
		static
		const char *
		p_type_to_string( u32 p_type )
		{
			const char * ret = "";
			switch ( p_type )
			{
				case PT_NULL        : ret = "PT_NULL"        ; break; // 0           /* Program header table entry unused */
				case PT_LOAD        : ret = "PT_LOAD"        ; break; // 1           /* Loadable program segment */
				case PT_DYNAMIC     : ret = "PT_DYNAMIC"     ; break; // 2           /* Dynamic linking information */
				case PT_INTERP      : ret = "PT_INTERP"      ; break; // 3           /* Program interpreter */
				case PT_NOTE        : ret = "PT_NOTE"        ; break; // 4           /* Auxiliary information */
				case PT_SHLIB       : ret = "PT_SHLIB"       ; break; // 5           /* Reserved */
				case PT_PHDR        : ret = "PT_PHDR"        ; break; // 6           /* Entry for header table itself */
				case PT_TLS         : ret = "PT_TLS"         ; break; // 7           /* Thread-local storage segment */
				case PT_NUM         : ret = "PT_NUM"         ; break; // 8           /* Number of defined types */
				case PT_LOOS        : ret = "PT_LOOS"        ; break; // 0x60000000  /* Start of OS-specific */
				case PT_GNU_EH_FRAME: ret = "PT_GNU_EH_FRAME"; break; // 0x6474e550  /* GCC .eh_frame_hdr segment */
				case PT_GNU_STACK   : ret = "PT_GNU_STACK"   ; break; // 0x6474e551  /* Indicates stack executability */
				case PT_GNU_RELRO   : ret = "PT_GNU_RELRO"   ; break; // 0x6474e552  /* Read-only after relocation */
				case PT_LOSUNW      : ret = "PT_LOSUNW"      ; break; // 0x6ffffffa
				//case PT_SUNWBSS     : ret = "PT_SUNWBSS"     ; break; // 0x6ffffffa  /* Sun Specific segment */
				case PT_SUNWSTACK   : ret = "PT_SUNWSTACK"   ; break; // 0x6ffffffb  /* Stack segment */
				case PT_HISUNW      : ret = "PT_HISUNW"      ; break; // 0x6fffffff
				//case PT_HIOS        : ret = "PT_HIOS"        ; break; // 0x6fffffff  /* End of OS-specific */
				case PT_LOPROC      : ret = "PT_LOPROC"      ; break; // 0x70000000  /* Start of processor-specific */
				case PT_HIPROC      : ret = "PT_HIPROC"      ; break; // 0x7fffffff  /* End of processor-specific */
			}
			return ret;
		}
		static
		const char *
		dyn_type_to_string( u32 tag )
		{
			const char * ret = "";

			switch ( tag )
			{
				/* Legal values for d_tag (dynamic entry type).  */

				case DT_NULL:            ret = "DT_NULL";             break; //		0		/* Marks end of dynamic section */
				case DT_NEEDED:          ret = "DT_NEEDED";           break; //	1		/* Name of needed library */
				case DT_PLTRELSZ:        ret = "DT_PLTRELSZ";         break; //	2		/* Size in bytes of PLT relocs */
				case DT_PLTGOT:          ret = "DT_PLTGOT";           break; //	3		/* Processor defined value */
				case DT_HASH:            ret = "DT_HASH";             break; //		4		/* Address of symbol hash table */
				case DT_STRTAB:          ret = "DT_STRTAB";           break; //	5		/* Address of string table */
				case DT_SYMTAB:          ret = "DT_SYMTAB";           break; //	6		/* Address of symbol table */
				case DT_RELA:            ret = "DT_RELA";             break; //		7		/* Address of Rela relocs */
				case DT_RELASZ:          ret = "DT_RELASZ";           break; //	8		/* Total size of Rela relocs */
				case DT_RELAENT:         ret = "DT_RELAENT";          break; //	9		/* Size of one Rela reloc */
				case DT_STRSZ:           ret = "DT_STRSZ";            break; //	10		/* Size of string table */
				case DT_SYMENT:          ret = "DT_SYMENT";           break; //	11		/* Size of one symbol table entry */
				case DT_INIT:            ret = "DT_INIT";             break; //		12		/* Address of init function */
				case DT_FINI:            ret = "DT_FINI";             break; //		13		/* Address of termination function */
				case DT_SONAME:          ret = "DT_SONAME";           break; //	14		/* Name of shared object */
				case DT_RPATH:           ret = "DT_RPATH";            break; //	15		/* Library search path (deprecated) */
				case DT_SYMBOLIC:        ret = "DT_SYMBOLIC";         break; //	16		/* Start symbol search here */
				case DT_REL:             ret = "DT_REL";              break; //		17		/* Address of Rel relocs */
				case DT_RELSZ:           ret = "DT_RELSZ";            break; //	18		/* Total size of Rel relocs */
				case DT_RELENT:          ret = "DT_RELENT";           break; //	19		/* Size of one Rel reloc */
				case DT_PLTREL:          ret = "DT_PLTREL";           break; //	20		/* Type of reloc in PLT */
				case DT_DEBUG:           ret = "DT_DEBUG";            break; //	21		/* For debugging; unspecified */
				case DT_TEXTREL:         ret = "DT_TEXTREL";          break; //	22		/* Reloc might modify .text */
				case DT_JMPREL:          ret = "DT_JMPREL";           break; //	23		/* Address of PLT relocs */
				case DT_BIND_NOW:        ret = "DT_BIND_NOW";         break; //	24		/* Process relocations of object */
				case DT_INIT_ARRAY:      ret = "DT_INIT_ARRAY";       break; //	25		/* Array with addresses of init fct */
				case DT_FINI_ARRAY:      ret = "DT_FINI_ARRAY";       break; //	26		/* Array with addresses of fini fct */
				case DT_INIT_ARRAYSZ:    ret = "DT_INIT_ARRAYSZ";     break; //	27		/* Size in bytes of DT_INIT_ARRAY */
				case DT_FINI_ARRAYSZ:    ret = "DT_FINI_ARRAYSZ";     break; //	28		/* Size in bytes of DT_FINI_ARRAY */
				case DT_RUNPATH:         ret = "DT_RUNPATH";          break; //	29		/* Library search path */
				case DT_FLAGS:           ret = "DT_FLAGS";            break; //	30		/* Flags for the object being loaded */
				//case DT_ENCODING: ret = "DT_ENCODING";  break; //	32		/* Start of encoded range */
				case DT_PREINIT_ARRAY:   ret = "DT_PREINIT_ARRAY";    break; // 32		/* Array with addresses of preinit fct*/
				case DT_PREINIT_ARRAYSZ: ret = "DT_PREINIT_ARRAYSZ";  break; // 33		/* size in bytes of DT_PREINIT_ARRAY */
				case DT_SYMTAB_SHNDX:    ret = "DT_SYMTAB_SHNDX";     break; //	34		/* Address of SYMTAB_SHNDX section */
				case DT_NUM:             ret = "DT_NUM";              break; //		35		/* Number used */
				case DT_LOOS:            ret = "DT_LOOS";             break; //		0x6000000d	/* Start of OS-specific */
				case DT_HIOS:            ret = "DT_HIOS";             break; //		0x6ffff000	/* End of OS-specific */
				case DT_LOPROC:          ret = "DT_LOPROC";           break; //	0x70000000	/* Start of processor-specific */
				case DT_HIPROC:          ret = "DT_HIPROC";           break; //	0x7fffffff	/* End of processor-specific */
				//case	DT_PROCNUM	DT_MIPS_NUM	/* Most used by any processor */

				/* DT_* entries which fall between DT_VALRNGHI & DT_VALRNGLO use the
				   Dyn.d_un.d_val field of the Elf*_Dyn structure.  This follows Sun's
				   approach.  */
				case DT_VALRNGLO:        ret = "DT_VALRNGLO";         break; //	0x6ffffd00
				case DT_GNU_PRELINKED:   ret = "DT_GNU_PRELINKED";    break; // 0x6ffffdf5	/* Prelinking timestamp */
				case DT_GNU_CONFLICTSZ:  ret = "DT_GNU_CONFLICTSZ";   break; // 0x6ffffdf6	/* Size of conflict section */
				case DT_GNU_LIBLISTSZ:   ret = "DT_GNU_LIBLISTSZ";    break; // 0x6ffffdf7	/* Size of library list */
				case DT_CHECKSUM:        ret = "DT_CHECKSUM";         break; //	0x6ffffdf8
				case DT_PLTPADSZ:        ret = "DT_PLTPADSZ";         break; //	0x6ffffdf9
				case DT_MOVEENT:         ret = "DT_MOVEENT";          break; //	0x6ffffdfa
				case DT_MOVESZ:          ret = "DT_MOVESZ";           break; //	0x6ffffdfb
				case DT_FEATURE_1:       ret = "DT_FEATURE_1";        break; //	0x6ffffdfc	/* Feature selection (DTF_*).  */
				case DT_POSFLAG_1:       ret = "DT_POSFLAG_1";        break; //	0x6ffffdfd	/* Flags for DT_* entries, effecting the following DT_* entry.  */
				case DT_SYMINSZ:         ret = "DT_SYMINSZ";          break; //	0x6ffffdfe	/* Size of syminfo table (in bytes) */
				case DT_SYMINENT:        ret = "DT_SYMINENT";         break; //	0x6ffffdff	/* Entry size of syminfo */
				//case DT_VALRNGHI: ret = "DT_VALRNGHI"; break; //	0x6ffffdff
				//case DT_VALTAGIDX(tag)	(DT_VALRNGHI - (tag))	/* Reverse order! */
				//case DT_VALNUM 12

				/* DT_* entries which fall between DT_ADDRRNGHI & DT_ADDRRNGLO use the
				   Dyn.d_un.d_ptr field of the Elf*_Dyn structure.

				   If any adjustment is made to the ELF object after it has been
				   built these entries will need to be adjusted.  */
				case DT_ADDRRNGLO:       ret = "DT_ADDRRNGLO";        break; //	0x6ffffe00
				case DT_GNU_HASH:        ret = "DT_GNU_HASH";         break; //	0x6ffffef5	/* GNU-style hash table.  */
				case DT_TLSDESC_PLT:     ret = "DT_TLSDESC_PLT";      break; //	0x6ffffef6
				case DT_TLSDESC_GOT:     ret = "DT_TLSDESC_GOT";      break; //	0x6ffffef7
				case DT_GNU_CONFLICT:    ret = "DT_GNU_CONFLICT";     break; //	0x6ffffef8	/* Start of conflict section */
				case DT_GNU_LIBLIST:     ret = "DT_GNU_LIBLIST";      break; //	0x6ffffef9	/* Library list */
				case DT_CONFIG:          ret = "DT_CONFIG";           break; //	0x6ffffefa	/* Configuration information.  */
				case DT_DEPAUDIT:        ret = "DT_DEPAUDIT";         break; //	0x6ffffefb	/* Dependency auditing.  */
				case DT_AUDIT:           ret = "DT_AUDIT";            break; //	0x6ffffefc	/* Object auditing.  */
				case DT_PLTPAD:          ret = "DT_PLTPAD";           break; //	0x6ffffefd	/* PLT padding.  */
				case DT_MOVETAB:         ret = "DT_MOVETAB";          break; //	0x6ffffefe	/* Move table.  */
				case DT_SYMINFO:         ret = "DT_SYMINFO";          break; //	0x6ffffeff	/* Syminfo table.  */
				//case DT_ADDRRNGHI: ret = "DT_ADDRRNGHI"; break; //	0x6ffffeff
				//case DT_ADDRTAGIDX(tag)	(DT_ADDRRNGHI - (tag))	/* Reverse order! */
				//case DT_ADDRNUM 11
			}

			return ret;
		}
	};
	void * data_ptr = 0;
	s32 r = dl_iterate_phdr( _::callback, data_ptr );
	log_dbg( "dl_iterate_phdr returned %d", r );

	// Array containing all the dynamic structures in the
	// .dynamic section.  This is automatically populated by the
	// linker.
	_DYNAMIC[0].d_tag;
}

#include "utility.cpp"
#include "utf8.cpp"
#include "system.cpp"
#include "tgmath.cpp"
#include "stack_generic_vm.cpp"
#include "pool_allocator_generic.cpp"
#include "pool_allocator_generic_vm.cpp"
#include "freelist_allocator.cpp"
#include "hash_table_generic_multi_vm.cpp"
#include "hash_table_generic_vm.cpp"
#include "argparse.c"
#undef internal
#include "lodepng/lodepng.cpp"
